<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_configurator')) {
                // _configurator_home
                if (rtrim($pathinfo, '/') === '/_configurator') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_configurator_home');
                    }

                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
                }

                // _configurator_step
                if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_configurator_step')), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',));
                }

                // _configurator_final
                if ($pathinfo === '/_configurator/final') {
                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        if (0 === strpos($pathinfo, '/intervenant')) {
            // intervenant
            if (rtrim($pathinfo, '/') === '/intervenant') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'intervenant');
                }

                return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\IntervenantController::indexAction',  '_route' => 'intervenant',);
            }

            // intervenant_show
            if (preg_match('#^/intervenant/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'intervenant_show')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\IntervenantController::showAction',));
            }

            // intervenant_new
            if ($pathinfo === '/intervenant/new') {
                return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\IntervenantController::newAction',  '_route' => 'intervenant_new',);
            }

            // intervenant_create
            if ($pathinfo === '/intervenant/create') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_intervenant_create;
                }

                return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\IntervenantController::createAction',  '_route' => 'intervenant_create',);
            }
            not_intervenant_create:

            // intervenant_edit
            if (preg_match('#^/intervenant/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'intervenant_edit')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\IntervenantController::editAction',));
            }

            // intervenant_update
            if (preg_match('#^/intervenant/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_intervenant_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'intervenant_update')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\IntervenantController::updateAction',));
            }
            not_intervenant_update:

            // intervenant_delete
            if (preg_match('#^/intervenant/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_intervenant_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'intervenant_delete')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\IntervenantController::deleteAction',));
            }
            not_intervenant_delete:

        }

        if (0 === strpos($pathinfo, '/admin/users')) {
            // users
            if (rtrim($pathinfo, '/') === '/admin/users') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'users');
                }

                return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\ResponsableController::indexAction',  '_route' => 'users',);
            }

            // users_show
            if (preg_match('#^/admin/users/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'users_show')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\ResponsableController::showAction',));
            }

            // users_new
            if ($pathinfo === '/admin/users/new') {
                return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\ResponsableController::newAction',  '_route' => 'users_new',);
            }

            // users_create
            if ($pathinfo === '/admin/users/create') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_users_create;
                }

                return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\ResponsableController::createAction',  '_route' => 'users_create',);
            }
            not_users_create:

            // users_edit
            if (preg_match('#^/admin/users/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'users_edit')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\ResponsableController::editAction',));
            }

            // users_update
            if (preg_match('#^/admin/users/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_users_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'users_update')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\ResponsableController::updateAction',));
            }
            not_users_update:

            // users_delete
            if (preg_match('#^/admin/users/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_users_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'users_delete')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\ResponsableController::deleteAction',));
            }
            not_users_delete:

        }

        // laiso_arm_homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'laiso_arm_homepage');
            }

            return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\DefaultController::indexAction',  '_route' => 'laiso_arm_homepage',);
        }

        // laiso_arm_ui_homepage
        if ($pathinfo === '/admin') {
            return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\DefaultController::uiAction',  '_route' => 'laiso_arm_ui_homepage',);
        }

        // laiso_arm_dtec_homepage
        if ($pathinfo === '/dtec') {
            return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\DefaultController::dtecAction',  '_route' => 'laiso_arm_dtec_homepage',);
        }

        if (0 === strpos($pathinfo, '/campagne')) {
            // campagne
            if (rtrim($pathinfo, '/') === '/campagne') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'campagne');
                }

                return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\CampagneController::indexAction',  '_route' => 'campagne',);
            }

            // campagne_show
            if (preg_match('#^/campagne/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'campagne_show')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\CampagneController::showAction',));
            }

            // campagne_new
            if ($pathinfo === '/campagne/new') {
                return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\CampagneController::newAction',  '_route' => 'campagne_new',);
            }

            // campagne_create
            if ($pathinfo === '/campagne/create') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_campagne_create;
                }

                return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\CampagneController::createAction',  '_route' => 'campagne_create',);
            }
            not_campagne_create:

            // campagne_edit
            if (preg_match('#^/campagne/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'campagne_edit')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\CampagneController::editAction',));
            }

            // campagne_update
            if (preg_match('#^/campagne/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_campagne_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'campagne_update')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\CampagneController::updateAction',));
            }
            not_campagne_update:

            // campagne_delete
            if (preg_match('#^/campagne/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_campagne_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'campagne_delete')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\CampagneController::deleteAction',));
            }
            not_campagne_delete:

        }

        if (0 === strpos($pathinfo, '/marche')) {
            // avenant
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/avenant/?$#s', $pathinfo, $matches)) {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'avenant');
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'avenant')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AvenantController::indexAction',));
            }

            // avenant_show
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/avenant/(?P<numero>\\d+)/show$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'avenant_show')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AvenantController::showAction',));
            }

            // avenant_new
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/avenant/new$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'avenant_new')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AvenantController::newAction',));
            }

            // avenant_create
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/avenant/create$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_avenant_create;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'avenant_create')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AvenantController::createAction',));
            }
            not_avenant_create:

            // avenant_edit
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/avenant/(?P<numero>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'avenant_edit')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AvenantController::editAction',));
            }

            // avenant_update
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/avenant/(?P<numero>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_avenant_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'avenant_update')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AvenantController::updateAction',));
            }
            not_avenant_update:

            // avenant_delete
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/avenant/(?P<numero>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_avenant_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'avenant_delete')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AvenantController::deleteAction',));
            }
            not_avenant_delete:

            // avenant_validate
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/avenant/(?P<numero>[^/]++)/validate$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'avenant_validate')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AvenantController::validateAction',));
            }

            // avenant_add_line
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/avenant/(?P<numero>[^/]++)/consistance/(?P<consistance>[^/]++)/add$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'avenant_add_line')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AvenantController::addLigneAction',));
            }

            // avenant_create_line
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/avenant/(?P<numero>[^/]++)/consistance/(?P<consistance>[^/]++)/create$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'avenant_create_line')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AvenantController::createLigneAction',));
            }

            // avenant_edit_ligne
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/avenant/(?P<numero>[^/]++)/consistance/(?P<consistance>[^/]++)/serie/(?P<serie>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'avenant_edit_ligne')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AvenantController::editLigneAction',));
            }

            // avenant_update_ligne
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/avenant/(?P<numero>[^/]++)/consistance/(?P<consistance>[^/]++)/serie/(?P<serie>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_avenant_update_ligne;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'avenant_update_ligne')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AvenantController::updateLigneAction',));
            }
            not_avenant_update_ligne:

            // marche
            if (rtrim($pathinfo, '/') === '/marche') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'marche');
                }

                return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\MarcheController::indexAction',  '_route' => 'marche',);
            }

            // marche_show
            if (preg_match('#^/marche/(?P<id>\\d+)/show$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'marche_show')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\MarcheController::showAction',));
            }

            // marche_new
            if ($pathinfo === '/marche/new') {
                return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\MarcheController::newAction',  '_route' => 'marche_new',);
            }

            // marche_create
            if ($pathinfo === '/marche/create') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_marche_create;
                }

                return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\MarcheController::createAction',  '_route' => 'marche_create',);
            }
            not_marche_create:

            // marche_edit
            if (preg_match('#^/marche/(?P<id>\\d+)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'marche_edit')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\MarcheController::editAction',));
            }

            // pu_validate
            if (preg_match('#^/marche/(?P<id>\\d+)/validatePu$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'pu_validate')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\MarcheController::validatePuAction',));
            }

            // marche_show_dqe
            if (preg_match('#^/marche/(?P<id>\\d+)/dqe$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'marche_show_dqe')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\MarcheController::showDqeAction',));
            }

            // marche_update
            if (preg_match('#^/marche/(?P<id>\\d+)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_marche_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'marche_update')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\MarcheController::updateAction',));
            }
            not_marche_update:

            // marche_delete
            if (preg_match('#^/marche/(?P<id>\\d+)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_marche_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'marche_delete')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\MarcheController::deleteAction',));
            }
            not_marche_delete:

            // prix_unitaire_add
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/consistance/(?P<consistanceId>[^/]++)/prixUnitaire/(?P<numero>[^/]++)/add$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'prix_unitaire_add')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AffectationPrixController::addPrixUnitaireAction',));
            }

            // prix_unitaire_update
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/consistance/(?P<consistanceId>[^/]++)/prixUnitaire/(?P<numero>[^/]++)/create$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_prix_unitaire_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'prix_unitaire_update')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AffectationPrixController::createAction',));
            }
            not_prix_unitaire_update:

            // decompte
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/?$#s', $pathinfo, $matches)) {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'decompte');
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'decompte')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\DecompteController::indexAction',));
            }

            // decompte_show
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/(?P<decompteId>[^/]++)/show$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'decompte_show')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\DecompteController::showAction',));
            }

            // decompte_new
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/new$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'decompte_new')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\DecompteController::newAction',));
            }

            // decompte_zero
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/zero$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'decompte_zero')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\DecompteController::zeroAction',));
            }

            // decompte_create
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/create$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_decompte_create;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'decompte_create')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\DecompteController::createAction',));
            }
            not_decompte_create:

            // decompte_create_zero
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/create_zero$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_decompte_create_zero;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'decompte_create_zero')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\DecompteController::createZeroAction',));
            }
            not_decompte_create_zero:

            // decompte_edit
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/(?P<decompteId>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'decompte_edit')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\DecompteController::editAction',));
            }

            // decompte_update
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/(?P<decompteId>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_decompte_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'decompte_update')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\DecompteController::updateAction',));
            }
            not_decompte_update:

            // decompte_delete
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/(?P<decompteId>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_decompte_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'decompte_delete')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\DecompteController::deleteAction',));
            }
            not_decompte_delete:

            // decompte_cpa
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/(?P<decompteId>[^/]++)/cpa$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'decompte_cpa')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\DecompteController::cpaAction',));
            }

            // decompte_export_cpa
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/(?P<decompteId>[^/]++)/cpa\\-pdf$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'decompte_export_cpa')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\DecompteController::exportCpaAction',));
            }

            // decompte_cloture
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/(?P<decompteId>[^/]++)/cloture$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'decompte_cloture')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\DecompteController::clotureAction',));
            }

            // decompte_preview
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/(?P<decompteId>[^/]++)/dp\\-recto$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'decompte_preview')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\DecompteController::dpRectoAction',));
            }

            // decompte_print
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/(?P<decompteId>[^/]++)/dp\\-recto\\-pdf$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'decompte_print')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\DecompteController::printDpRectoAction',));
            }

            // decompte_recapitulation
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/(?P<decompteId>[^/]++)/dp\\-verso$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'decompte_recapitulation')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\DecompteController::recapitulationAction',));
            }

            // decompte_export_recapitulation
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/(?P<decompteId>[^/]++)/dp\\-verso\\-pdf$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'decompte_export_recapitulation')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\DecompteController::exportRecapitulationAction',));
            }

            // decompte_apercu
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/apercu$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'decompte_apercu')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\DecompteController::apercuAction',));
            }

            // suivi
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/(?P<decompteId>[^/]++)/suivi/?$#s', $pathinfo, $matches)) {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'suivi');
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'suivi')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LigneSuiviController::indexAction',));
            }

            // suivi_new
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/(?P<decompteId>[^/]++)/suivi/new$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'suivi_new')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LigneSuiviController::newAction',));
            }

            // suivi_create
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/(?P<decompteId>[^/]++)/suivi/create$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_suivi_create;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'suivi_create')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LigneSuiviController::createAction',));
            }
            not_suivi_create:

            // suivi_edit
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/(?P<decompteId>[^/]++)/suivi/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'suivi_edit')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LigneSuiviController::editAction',));
            }

            // suivi_update
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/(?P<decompteId>[^/]++)/suivi/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_suivi_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'suivi_update')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LigneSuiviController::updateAction',));
            }
            not_suivi_update:

            // suivi_delete
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/(?P<decompteId>[^/]++)/suivi/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_suivi_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'suivi_delete')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LigneSuiviController::deleteAction',));
            }
            not_suivi_delete:

            // attachement
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/(?P<decompteId>[^/]++)/attachement/?$#s', $pathinfo, $matches)) {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'attachement');
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'attachement')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AttachementController::indexAction',));
            }

            // attachement_show
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/(?P<decompteId>[^/]++)/attachement/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'attachement_show')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AttachementController::showAction',));
            }

            // attachement_new
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/(?P<decompteId>[^/]++)/attachement/new$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'attachement_new')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AttachementController::newAction',));
            }

            // attachement_create
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/(?P<decompteId>[^/]++)/attachement/create$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_attachement_create;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'attachement_create')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AttachementController::createAction',));
            }
            not_attachement_create:

            // attachement_edit
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/(?P<decompteId>[^/]++)/attachement/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'attachement_edit')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AttachementController::editAction',));
            }

            // attachement_update
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/(?P<decompteId>[^/]++)/attachement/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_attachement_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'attachement_update')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AttachementController::updateAction',));
            }
            not_attachement_update:

            // attachement_delete
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/(?P<decompteId>[^/]++)/attachement/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_attachement_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'attachement_delete')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AttachementController::deleteAction',));
            }
            not_attachement_delete:

            // editQuantite
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/(?P<decompteId>[^/]++)/attachement/(?P<attachementId>[^/]++)/editQuantite/(?P<ligneId>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'editQuantite')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AttachementController::editQuantiteAction',));
            }

            // updateQuantite
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/(?P<decompteId>[^/]++)/attachement/(?P<attachementId>[^/]++)/updateQuantite/(?P<ligneId>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_updateQuantite;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'updateQuantite')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AttachementController::updateQuantiteAction',));
            }
            not_updateQuantite:

            // attachement_validate
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/(?P<decompteId>[^/]++)/attachement/(?P<attachementId>[^/]++)/validate$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'attachement_validate')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AttachementController::validateAction',));
            }

            // attachement_to_pdf
            if (preg_match('#^/marche/(?P<marcheId>[^/]++)/decompte/(?P<decompteId>[^/]++)/attachement/(?P<attachementId>[^/]++)/pdf$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'attachement_to_pdf')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AttachementController::generatePdfAction',));
            }

        }

        if (0 === strpos($pathinfo, '/consistance')) {
            // localisation_new
            if (preg_match('#^/consistance/(?P<consistanceId>[^/]++)/localisation/new$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'localisation_new')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LocalisationController::newAction',));
            }

            // localisation_create
            if (preg_match('#^/consistance/(?P<consistanceId>[^/]++)/localisation/create$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_localisation_create;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'localisation_create')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LocalisationController::createAction',));
            }
            not_localisation_create:

            // localisation_edit
            if (preg_match('#^/consistance/(?P<consistanceId>[^/]++)/localisation/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'localisation_edit')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LocalisationController::editAction',));
            }

            // localisation_update
            if (preg_match('#^/consistance/(?P<consistanceId>[^/]++)/localisation/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_localisation_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'localisation_update')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LocalisationController::updateAction',));
            }
            not_localisation_update:

            // localisation_delete
            if (preg_match('#^/consistance/(?P<consistanceId>[^/]++)/localisation/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_localisation_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'localisation_delete')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LocalisationController::deleteAction',));
            }
            not_localisation_delete:

            // localisation_ajax_delete
            if (preg_match('#^/consistance/(?P<consistanceId>[^/]++)/localisation/(?P<id>[^/]++)/supprimer$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'localisation_ajax_delete')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LocalisationController::deleteAjaxAction',));
            }

        }

        if (0 === strpos($pathinfo, '/dao')) {
            // dao
            if (rtrim($pathinfo, '/') === '/dao') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'dao');
                }

                return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LotissementController::indexAction',  '_route' => 'dao',);
            }

            // dao_show
            if (preg_match('#^/dao/(?P<id>\\d+)/show$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'dao_show')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LotissementController::showAction',));
            }

            // dao_validate
            if (preg_match('#^/dao/(?P<id>\\d+)/validate$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'dao_validate')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LotissementController::validateAction',));
            }

            // qte_validate
            if (preg_match('#^/dao/(?P<id>\\d+)/validateQte$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'qte_validate')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LotissementController::validateQteAction',));
            }

            // dao_new
            if ($pathinfo === '/dao/new') {
                return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LotissementController::newAction',  '_route' => 'dao_new',);
            }

            // dao_create
            if ($pathinfo === '/dao/create') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_dao_create;
                }

                return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LotissementController::createAction',  '_route' => 'dao_create',);
            }
            not_dao_create:

            // dao_edit
            if (preg_match('#^/dao/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'dao_edit')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LotissementController::editAction',));
            }

            // dao_update
            if (preg_match('#^/dao/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_dao_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'dao_update')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LotissementController::updateAction',));
            }
            not_dao_update:

            // dao_delete
            if (preg_match('#^/dao/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_dao_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'dao_delete')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LotissementController::deleteAction',));
            }
            not_dao_delete:

            // dao_ajax_delete
            if (0 === strpos($pathinfo, '/dao/ajax') && preg_match('#^/dao/ajax/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'dao_ajax_delete')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LotissementController::deleteAjaxAction',));
            }

            // dqe_new
            if (preg_match('#^/dao/(?P<daoId>[^/]++)/consistance/(?P<consistanceId>[^/]++)/lignesDqe/new$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'dqe_new')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LigneDQEController::newAction',));
            }

            // dqe_create
            if (preg_match('#^/dao/(?P<daoId>[^/]++)/consistance/(?P<consistanceId>[^/]++)/lignesDqe/create$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_dqe_create;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'dqe_create')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LigneDQEController::createAction',));
            }
            not_dqe_create:

            // dqe_edit
            if (preg_match('#^/dao/(?P<daoId>[^/]++)/consistance/(?P<consistanceId>[^/]++)/lignesDqe/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'dqe_edit')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LigneDQEController::editAction',));
            }

            // dqe_update
            if (preg_match('#^/dao/(?P<daoId>[^/]++)/consistance/(?P<consistanceId>[^/]++)/lignesDqe/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_dqe_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'dqe_update')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LigneDQEController::updateAction',));
            }
            not_dqe_update:

            // dqe_delete
            if (preg_match('#^/dao/(?P<daoId>[^/]++)/consistance/(?P<consistanceId>[^/]++)/lignesDqe/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_dqe_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'dqe_delete')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LigneDQEController::deleteAction',));
            }
            not_dqe_delete:

            // dqe_ajax_delete
            if (preg_match('#^/dao/(?P<daoId>[^/]++)/consistance/(?P<consistanceId>[^/]++)/lignesDqe/ajax/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'dqe_ajax_delete')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LigneDQEController::ajaxDeleteAction',));
            }

            // consistance_show
            if (preg_match('#^/dao/(?P<daoId>[^/]++)/consistance/(?P<id>\\d+)/show$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'consistance_show')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\ConsistanceController::showAction',));
            }

            // consistance_new
            if (preg_match('#^/dao/(?P<daoId>[^/]++)/consistance/new$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'consistance_new')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\ConsistanceController::newAction',));
            }

            // consistance_create
            if (preg_match('#^/dao/(?P<daoId>[^/]++)/consistance/create$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_consistance_create;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'consistance_create')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\ConsistanceController::createAction',));
            }
            not_consistance_create:

            // consistance_edit
            if (preg_match('#^/dao/(?P<daoId>[^/]++)/consistance/(?P<id>\\d+)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'consistance_edit')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\ConsistanceController::editAction',));
            }

            // consistance_update
            if (preg_match('#^/dao/(?P<daoId>[^/]++)/consistance/(?P<id>\\d+)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_consistance_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'consistance_update')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\ConsistanceController::updateAction',));
            }
            not_consistance_update:

            // consistance_delete
            if (preg_match('#^/dao/(?P<daoId>[^/]++)/consistance/(?P<id>\\d+)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_consistance_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'consistance_delete')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\ConsistanceController::deleteAction',));
            }
            not_consistance_delete:

            // consistance__ajax_delete
            if (preg_match('#^/dao/(?P<daoId>[^/]++)/consistance/(?P<id>\\d+)/supprimer$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'consistance__ajax_delete')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\ConsistanceController::ajaxDeleteAction',));
            }

        }

        if (0 === strpos($pathinfo, '/prix')) {
            if (0 === strpos($pathinfo, '/prix/serie')) {
                // prix_serie
                if (rtrim($pathinfo, '/') === '/prix/serie') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'prix_serie');
                    }

                    return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\SerieDePrixController::indexAction',  '_route' => 'prix_serie',);
                }

                // prix_serie_show
                if (preg_match('#^/prix/serie/(?P<numero>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'prix_serie_show')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\SerieDePrixController::showAction',));
                }

                // prix_serie_new
                if ($pathinfo === '/prix/serie/new') {
                    return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\SerieDePrixController::newAction',  '_route' => 'prix_serie_new',);
                }

                // prix_serie_create
                if ($pathinfo === '/prix/serie/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_prix_serie_create;
                    }

                    return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\SerieDePrixController::createAction',  '_route' => 'prix_serie_create',);
                }
                not_prix_serie_create:

                // prix_serie_edit
                if (preg_match('#^/prix/serie/(?P<numero>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'prix_serie_edit')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\SerieDePrixController::editAction',));
                }

                // prix_serie_update
                if (preg_match('#^/prix/serie/(?P<numero>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_prix_serie_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'prix_serie_update')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\SerieDePrixController::updateAction',));
                }
                not_prix_serie_update:

                // prix_serie_delete
                if (preg_match('#^/prix/serie/(?P<numero>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_prix_serie_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'prix_serie_delete')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\SerieDePrixController::deleteAction',));
                }
                not_prix_serie_delete:

                // prix_serie_ajax_delete
                if (preg_match('#^/prix/serie/(?P<numero>[^/]++)/supprimer$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'prix_serie_ajax_delete')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\SerieDePrixController::deleteAjaxAction',));
                }

            }

            if (0 === strpos($pathinfo, '/prix/categorie')) {
                // prix_categorie
                if (rtrim($pathinfo, '/') === '/prix/categorie') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'prix_categorie');
                    }

                    return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\CategoriePrixController::indexAction',  '_route' => 'prix_categorie',);
                }

                // prix_categorie_show
                if (preg_match('#^/prix/categorie/(?P<id>\\d+)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'prix_categorie_show')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\CategoriePrixController::showAction',));
                }

                // prix_categorie_new
                if ($pathinfo === '/prix/categorie/new') {
                    return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\CategoriePrixController::newAction',  '_route' => 'prix_categorie_new',);
                }

                // prix_categorie_create
                if ($pathinfo === '/prix/categorie/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_prix_categorie_create;
                    }

                    return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\CategoriePrixController::createAction',  '_route' => 'prix_categorie_create',);
                }
                not_prix_categorie_create:

                // prix_categorie_edit
                if (preg_match('#^/prix/categorie/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'prix_categorie_edit')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\CategoriePrixController::editAction',));
                }

                // prix_categorie_update
                if (preg_match('#^/prix/categorie/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_prix_categorie_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'prix_categorie_update')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\CategoriePrixController::updateAction',));
                }
                not_prix_categorie_update:

                // prix_categorie_delete
                if (preg_match('#^/prix/categorie/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_prix_categorie_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'prix_categorie_delete')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\CategoriePrixController::deleteAction',));
                }
                not_prix_categorie_delete:

                // prix_categorie_ajax_delete
                if (preg_match('#^/prix/categorie/(?P<id>[^/]++)/supprimer$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'prix_categorie_ajax_delete')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\CategoriePrixController::deleteAjaxAction',));
                }

            }

        }

        if (0 === strpos($pathinfo, '/admin')) {
            if (0 === strpos($pathinfo, '/admin/bloc')) {
                // bloc
                if (rtrim($pathinfo, '/') === '/admin/bloc') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'bloc');
                    }

                    return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\BlocController::indexAction',  '_route' => 'bloc',);
                }

                // bloc_show
                if (preg_match('#^/admin/bloc/(?P<id>\\d+)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'bloc_show')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\BlocController::showAction',));
                }

                // bloc_new
                if ($pathinfo === '/admin/bloc/new') {
                    return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\BlocController::newAction',  '_route' => 'bloc_new',);
                }

                // bloc_create
                if ($pathinfo === '/admin/bloc/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_bloc_create;
                    }

                    return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\BlocController::createAction',  '_route' => 'bloc_create',);
                }
                not_bloc_create:

                // bloc_edit
                if (preg_match('#^/admin/bloc/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'bloc_edit')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\BlocController::editAction',));
                }

                // bloc_update
                if (preg_match('#^/admin/bloc/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_bloc_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'bloc_update')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\BlocController::updateAction',));
                }
                not_bloc_update:

                // bloc_delete
                if (preg_match('#^/admin/bloc/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_bloc_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'bloc_delete')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\BlocController::deleteAction',));
                }
                not_bloc_delete:

                // bloc_delete_ajax
                if (preg_match('#^/admin/bloc/(?P<id>[^/]++)/supprimer$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'bloc_delete_ajax')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\BlocController::deleteAjaxAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin/libelleavenant')) {
                // libelleavenant
                if (rtrim($pathinfo, '/') === '/admin/libelleavenant') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'libelleavenant');
                    }

                    return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LibelleAvenantController::indexAction',  '_route' => 'libelleavenant',);
                }

                // libelleavenant_show
                if (preg_match('#^/admin/libelleavenant/(?P<id>\\d+)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'libelleavenant_show')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LibelleAvenantController::showAction',));
                }

                // libelleavenant_new
                if ($pathinfo === '/admin/libelleavenant/new') {
                    return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LibelleAvenantController::newAction',  '_route' => 'libelleavenant_new',);
                }

                // libelleavenant_create
                if ($pathinfo === '/admin/libelleavenant/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_libelleavenant_create;
                    }

                    return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LibelleAvenantController::createAction',  '_route' => 'libelleavenant_create',);
                }
                not_libelleavenant_create:

                // libelleavenant_edit
                if (preg_match('#^/admin/libelleavenant/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'libelleavenant_edit')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LibelleAvenantController::editAction',));
                }

                // libelleavenant_update
                if (preg_match('#^/admin/libelleavenant/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_libelleavenant_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'libelleavenant_update')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LibelleAvenantController::updateAction',));
                }
                not_libelleavenant_update:

                // libelleavenant_delete
                if (preg_match('#^/admin/libelleavenant/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_libelleavenant_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'libelleavenant_delete')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\LibelleAvenantController::deleteAction',));
                }
                not_libelleavenant_delete:

            }

            if (0 === strpos($pathinfo, '/admin/unitedemesure')) {
                // unitedemesure
                if (rtrim($pathinfo, '/') === '/admin/unitedemesure') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'unitedemesure');
                    }

                    return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\UniteDeMesureController::indexAction',  '_route' => 'unitedemesure',);
                }

                // unitedemesure_show
                if (preg_match('#^/admin/unitedemesure/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'unitedemesure_show')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\UniteDeMesureController::showAction',));
                }

                // unitedemesure_new
                if ($pathinfo === '/admin/unitedemesure/new') {
                    return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\UniteDeMesureController::newAction',  '_route' => 'unitedemesure_new',);
                }

                // unitedemesure_create
                if ($pathinfo === '/admin/unitedemesure/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_unitedemesure_create;
                    }

                    return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\UniteDeMesureController::createAction',  '_route' => 'unitedemesure_create',);
                }
                not_unitedemesure_create:

                // unitedemesure_edit
                if (preg_match('#^/admin/unitedemesure/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'unitedemesure_edit')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\UniteDeMesureController::editAction',));
                }

                // unitedemesure_update
                if (preg_match('#^/admin/unitedemesure/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_unitedemesure_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'unitedemesure_update')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\UniteDeMesureController::updateAction',));
                }
                not_unitedemesure_update:

                // unitedemesure_delete
                if (preg_match('#^/admin/unitedemesure/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_unitedemesure_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'unitedemesure_delete')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\UniteDeMesureController::deleteAction',));
                }
                not_unitedemesure_delete:

            }

            if (0 === strpos($pathinfo, '/admin/typeconsistance')) {
                // typeconsistance
                if (rtrim($pathinfo, '/') === '/admin/typeconsistance') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'typeconsistance');
                    }

                    return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\TypeConsistanceController::indexAction',  '_route' => 'typeconsistance',);
                }

                // typeconsistance_show
                if (preg_match('#^/admin/typeconsistance/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'typeconsistance_show')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\TypeConsistanceController::showAction',));
                }

                // typeconsistance_new
                if ($pathinfo === '/admin/typeconsistance/new') {
                    return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\TypeConsistanceController::newAction',  '_route' => 'typeconsistance_new',);
                }

                // typeconsistance_create
                if ($pathinfo === '/admin/typeconsistance/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_typeconsistance_create;
                    }

                    return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\TypeConsistanceController::createAction',  '_route' => 'typeconsistance_create',);
                }
                not_typeconsistance_create:

                // typeconsistance_edit
                if (preg_match('#^/admin/typeconsistance/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'typeconsistance_edit')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\TypeConsistanceController::editAction',));
                }

                // typeconsistance_update
                if (preg_match('#^/admin/typeconsistance/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_typeconsistance_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'typeconsistance_update')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\TypeConsistanceController::updateAction',));
                }
                not_typeconsistance_update:

                // typeconsistance_delete
                if (preg_match('#^/admin/typeconsistance/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_typeconsistance_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'typeconsistance_delete')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\TypeConsistanceController::deleteAction',));
                }
                not_typeconsistance_delete:

            }

            if (0 === strpos($pathinfo, '/admin/region')) {
                // region
                if (rtrim($pathinfo, '/') === '/admin/region') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'region');
                    }

                    return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\RegionController::indexAction',  '_route' => 'region',);
                }

                // region_show
                if (preg_match('#^/admin/region/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'region_show')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\RegionController::showAction',));
                }

                // region_new
                if ($pathinfo === '/admin/region/new') {
                    return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\RegionController::newAction',  '_route' => 'region_new',);
                }

                // region_create
                if ($pathinfo === '/admin/region/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_region_create;
                    }

                    return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\RegionController::createAction',  '_route' => 'region_create',);
                }
                not_region_create:

                // region_edit
                if (preg_match('#^/admin/region/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'region_edit')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\RegionController::editAction',));
                }

                // region_update
                if (preg_match('#^/admin/region/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_region_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'region_update')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\RegionController::updateAction',));
                }
                not_region_update:

                // region_delete
                if (preg_match('#^/admin/region/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_region_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'region_delete')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\RegionController::deleteAction',));
                }
                not_region_delete:

                // region_ajax_delete
                if (preg_match('#^/admin/region/(?P<id>[^/]++)/supprimer$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'region_ajax_delete')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\RegionController::ajaxDeleteAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin/axe')) {
                // axe
                if (rtrim($pathinfo, '/') === '/admin/axe') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'axe');
                    }

                    return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AxeController::indexAction',  '_route' => 'axe',);
                }

                // axe_show
                if (preg_match('#^/admin/axe/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'axe_show')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AxeController::showAction',));
                }

                // axe_new
                if ($pathinfo === '/admin/axe/new') {
                    return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AxeController::newAction',  '_route' => 'axe_new',);
                }

                // axe_create
                if ($pathinfo === '/admin/axe/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_axe_create;
                    }

                    return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AxeController::createAction',  '_route' => 'axe_create',);
                }
                not_axe_create:

                // axe_edit
                if (preg_match('#^/admin/axe/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'axe_edit')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AxeController::editAction',));
                }

                // axe_update
                if (preg_match('#^/admin/axe/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_axe_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'axe_update')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AxeController::updateAction',));
                }
                not_axe_update:

                // axe_delete
                if (preg_match('#^/admin/axe/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_axe_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'axe_delete')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\AxeController::deleteAction',));
                }
                not_axe_delete:

            }

        }

        if (0 === strpos($pathinfo, '/entreprise')) {
            // entreprise
            if (rtrim($pathinfo, '/') === '/entreprise') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'entreprise');
                }

                return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\EntrepriseController::indexAction',  '_route' => 'entreprise',);
            }

            // entreprise_show
            if (preg_match('#^/entreprise/(?P<id>\\d+)/show$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'entreprise_show')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\EntrepriseController::showAction',));
            }

            // entreprise_new
            if ($pathinfo === '/entreprise/new') {
                return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\EntrepriseController::newAction',  '_route' => 'entreprise_new',);
            }

            // entreprise_create
            if ($pathinfo === '/entreprise/create') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_entreprise_create;
                }

                return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\EntrepriseController::createAction',  '_route' => 'entreprise_create',);
            }
            not_entreprise_create:

            // entreprise_edit
            if (preg_match('#^/entreprise/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'entreprise_edit')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\EntrepriseController::editAction',));
            }

            // entreprise_update
            if (preg_match('#^/entreprise/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_entreprise_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'entreprise_update')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\EntrepriseController::updateAction',));
            }
            not_entreprise_update:

            // entreprise_delete
            if (preg_match('#^/entreprise/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_entreprise_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'entreprise_delete')), array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\EntrepriseController::deleteAction',));
            }
            not_entreprise_delete:

        }

        // dashboard_index
        if (rtrim($pathinfo, '/') === '/dashboard') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'dashboard_index');
            }

            return array (  '_controller' => 'Laiso\\ArmBundle\\Controller\\DashboardController::indexAction',  '_route' => 'dashboard_index',);
        }

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // fos_user_security_login
                if ($pathinfo === '/login') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_security_login;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'fos_user_security_login',);
                }
                not_fos_user_security_login:

                // fos_user_security_check
                if ($pathinfo === '/login_check') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_security_check;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_route' => 'fos_user_security_check',);
                }
                not_fos_user_security_check:

            }

            // fos_user_security_logout
            if ($pathinfo === '/logout') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_security_logout;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'fos_user_security_logout',);
            }
            not_fos_user_security_logout:

        }

        if (0 === strpos($pathinfo, '/profile')) {
            // fos_user_profile_show
            if (rtrim($pathinfo, '/') === '/profile') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_profile_show;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fos_user_profile_show');
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::showAction',  '_route' => 'fos_user_profile_show',);
            }
            not_fos_user_profile_show:

            // fos_user_profile_edit
            if ($pathinfo === '/profile/edit') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_profile_edit;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::editAction',  '_route' => 'fos_user_profile_edit',);
            }
            not_fos_user_profile_edit:

        }

        if (0 === strpos($pathinfo, '/re')) {
            if (0 === strpos($pathinfo, '/register')) {
                // fos_user_registration_register
                if (rtrim($pathinfo, '/') === '/register') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_registration_register;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'fos_user_registration_register');
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',  '_route' => 'fos_user_registration_register',);
                }
                not_fos_user_registration_register:

                if (0 === strpos($pathinfo, '/register/c')) {
                    // fos_user_registration_check_email
                    if ($pathinfo === '/register/check-email') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_fos_user_registration_check_email;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',  '_route' => 'fos_user_registration_check_email',);
                    }
                    not_fos_user_registration_check_email:

                    if (0 === strpos($pathinfo, '/register/confirm')) {
                        // fos_user_registration_confirm
                        if (preg_match('#^/register/confirm/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirm;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_confirm')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',));
                        }
                        not_fos_user_registration_confirm:

                        // fos_user_registration_confirmed
                        if ($pathinfo === '/register/confirmed') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirmed;
                            }

                            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',  '_route' => 'fos_user_registration_confirmed',);
                        }
                        not_fos_user_registration_confirmed:

                    }

                }

            }

            if (0 === strpos($pathinfo, '/resetting')) {
                // fos_user_resetting_request
                if ($pathinfo === '/resetting/request') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_request;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',  '_route' => 'fos_user_resetting_request',);
                }
                not_fos_user_resetting_request:

                // fos_user_resetting_send_email
                if ($pathinfo === '/resetting/send-email') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_resetting_send_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
                }
                not_fos_user_resetting_send_email:

                // fos_user_resetting_check_email
                if ($pathinfo === '/resetting/check-email') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_check_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
                }
                not_fos_user_resetting_check_email:

                // fos_user_resetting_reset
                if (0 === strpos($pathinfo, '/resetting/reset') && preg_match('#^/resetting/reset/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_resetting_reset;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_resetting_reset')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',));
                }
                not_fos_user_resetting_reset:

            }

        }

        // fos_user_change_password
        if ($pathinfo === '/profile/change-password') {
            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                goto not_fos_user_change_password;
            }

            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  '_route' => 'fos_user_change_password',);
        }
        not_fos_user_change_password:

        if (0 === strpos($pathinfo, '/group')) {
            // fos_user_group_list
            if ($pathinfo === '/group/list') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_group_list;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\GroupController::listAction',  '_route' => 'fos_user_group_list',);
            }
            not_fos_user_group_list:

            // fos_user_group_new
            if ($pathinfo === '/group/new') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_group_new;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\GroupController::newAction',  '_route' => 'fos_user_group_new',);
            }
            not_fos_user_group_new:

            // fos_user_group_show
            if (preg_match('#^/group/(?P<groupName>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_group_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_group_show')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\GroupController::showAction',));
            }
            not_fos_user_group_show:

            // fos_user_group_edit
            if (preg_match('#^/group/(?P<groupName>[^/]++)/edit$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_group_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_group_edit')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\GroupController::editAction',));
            }
            not_fos_user_group_edit:

            // fos_user_group_delete
            if (preg_match('#^/group/(?P<groupName>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_group_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_group_delete')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\GroupController::deleteAction',));
            }
            not_fos_user_group_delete:

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
