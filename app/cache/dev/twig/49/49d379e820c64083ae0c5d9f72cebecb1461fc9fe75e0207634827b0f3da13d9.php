<?php

/* LaisoArmBundle:Responsable:index.html.twig */
class __TwigTemplate_6e638122ef8a0e3e2044c32d1fe9b99f69eae5366ce2e8a0b9622e5e83f3db0c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "LaisoArmBundle:Responsable:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'breadcumbs' => array($this, 'block_breadcumbs'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a5f4b50fadaec5b7cdc2e632a517b37d557d5578874e89d9de3aea1b79eb6c49 = $this->env->getExtension("native_profiler");
        $__internal_a5f4b50fadaec5b7cdc2e632a517b37d557d5578874e89d9de3aea1b79eb6c49->enter($__internal_a5f4b50fadaec5b7cdc2e632a517b37d557d5578874e89d9de3aea1b79eb6c49_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "LaisoArmBundle:Responsable:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a5f4b50fadaec5b7cdc2e632a517b37d557d5578874e89d9de3aea1b79eb6c49->leave($__internal_a5f4b50fadaec5b7cdc2e632a517b37d557d5578874e89d9de3aea1b79eb6c49_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_9338e293faedc7315313230507aad290c890e1784a9b51e106f859d795c0d676 = $this->env->getExtension("native_profiler");
        $__internal_9338e293faedc7315313230507aad290c890e1784a9b51e106f859d795c0d676->enter($__internal_9338e293faedc7315313230507aad290c890e1784a9b51e106f859d795c0d676_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Gestion des utilisateurs";
        
        $__internal_9338e293faedc7315313230507aad290c890e1784a9b51e106f859d795c0d676->leave($__internal_9338e293faedc7315313230507aad290c890e1784a9b51e106f859d795c0d676_prof);

    }

    // line 5
    public function block_breadcumbs($context, array $blocks = array())
    {
        $__internal_b9f8da0383e33e2a763d074978e43cc53c800b67a29b2f54b7a67efad2335293 = $this->env->getExtension("native_profiler");
        $__internal_b9f8da0383e33e2a763d074978e43cc53c800b67a29b2f54b7a67efad2335293->enter($__internal_b9f8da0383e33e2a763d074978e43cc53c800b67a29b2f54b7a67efad2335293_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "breadcumbs"));

        // line 6
        echo "    <li><a href=\"#\">Super Administrateur</a></li>
    <li><a href=\"";
        // line 7
        echo $this->env->getExtension('routing')->getPath("users");
        echo "\">Utilisateurs</a></li>
";
        
        $__internal_b9f8da0383e33e2a763d074978e43cc53c800b67a29b2f54b7a67efad2335293->leave($__internal_b9f8da0383e33e2a763d074978e43cc53c800b67a29b2f54b7a67efad2335293_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_4099367c48084ed4fe3958958eb33c61587cdca2de862174535d76e0dae22957 = $this->env->getExtension("native_profiler");
        $__internal_4099367c48084ed4fe3958958eb33c61587cdca2de862174535d76e0dae22957->enter($__internal_4099367c48084ed4fe3958958eb33c61587cdca2de862174535d76e0dae22957_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 11
        echo "<table class=\"table border bordered\">
        <thead>
        <tr>
            <th>Renseignements</th>
            <th>Rôles</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 21
            echo "            <tr>
                <td>
                    Nom : ";
            // line 23
            echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute($context["entity"], "nom", array())), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "prenom", array()), "html", null, true);
            echo "
                    <hr class=\"thin\">
                    <span class=\"tag success\">";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "username", array()), "html", null, true);
            echo "</span> <span class=\"tag info\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "email", array()), "html", null, true);
            echo "</span>
                </td>
                <td>
                    ";
            // line 28
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["entity"], "roles", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["role"]) {
                // line 29
                echo "                        <span class=\"tag info\">";
                echo twig_escape_filter($this->env, $context["role"], "html", null, true);
                echo "</span>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['role'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 30
            echo "</td>
                <td class=\"align-center\">
                    <a href=\"";
            // line 32
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("users_show", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\">Détails</a>
                    <a href=\"";
            // line 33
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("users_edit", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\">Modifier</a>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "        </tbody>
    </table>

    <a class=\"button primary ajax-button\" href=\"";
        // line 40
        echo $this->env->getExtension('routing')->getPath("users_new");
        echo "\">
        Ajouter un nouvel utilisateur
    </a>
";
        
        $__internal_4099367c48084ed4fe3958958eb33c61587cdca2de862174535d76e0dae22957->leave($__internal_4099367c48084ed4fe3958958eb33c61587cdca2de862174535d76e0dae22957_prof);

    }

    public function getTemplateName()
    {
        return "LaisoArmBundle:Responsable:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 40,  137 => 37,  127 => 33,  123 => 32,  119 => 30,  110 => 29,  106 => 28,  98 => 25,  91 => 23,  87 => 21,  83 => 20,  72 => 11,  66 => 10,  57 => 7,  54 => 6,  48 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block title %}Gestion des utilisateurs{% endblock %}*/
/* */
/* {% block breadcumbs %}*/
/*     <li><a href="#">Super Administrateur</a></li>*/
/*     <li><a href="{{ path('users') }}">Utilisateurs</a></li>*/
/* {% endblock %}*/
/* */
/* {% block body -%}*/
/*     <table class="table border bordered">*/
/*         <thead>*/
/*         <tr>*/
/*             <th>Renseignements</th>*/
/*             <th>Rôles</th>*/
/*             <th>Actions</th>*/
/*         </tr>*/
/*         </thead>*/
/*         <tbody>*/
/*         {% for entity in entities %}*/
/*             <tr>*/
/*                 <td>*/
/*                     Nom : {{ entity.nom | upper }} {{ entity.prenom }}*/
/*                     <hr class="thin">*/
/*                     <span class="tag success">{{ entity.username }}</span> <span class="tag info">{{ entity.email }}</span>*/
/*                 </td>*/
/*                 <td>*/
/*                     {% for role in entity.roles %}*/
/*                         <span class="tag info">{{ role }}</span>*/
/*                     {% endfor %}</td>*/
/*                 <td class="align-center">*/
/*                     <a href="{{ path('users_show', { 'id': entity.id }) }}">Détails</a>*/
/*                     <a href="{{ path('users_edit', { 'id': entity.id }) }}">Modifier</a>*/
/*                 </td>*/
/*             </tr>*/
/*         {% endfor %}*/
/*         </tbody>*/
/*     </table>*/
/* */
/*     <a class="button primary ajax-button" href="{{ path('users_new') }}">*/
/*         Ajouter un nouvel utilisateur*/
/*     </a>*/
/* {% endblock %}*/
/* */
