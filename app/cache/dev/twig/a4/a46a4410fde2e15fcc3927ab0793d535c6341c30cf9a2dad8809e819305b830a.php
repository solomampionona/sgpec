<?php

/* LaisoArmBundle:Lotissement:show.html.twig */
class __TwigTemplate_35780e11d4199a3e88ce08896338cb9e3e448c85ed7934766be936e3b7a0ba7b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "LaisoArmBundle:Lotissement:show.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'breadcumbs' => array($this, 'block_breadcumbs'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_35d19cc6638e1842ac2b28340d15f58953d99d7347718ca5049eaacf77e9429b = $this->env->getExtension("native_profiler");
        $__internal_35d19cc6638e1842ac2b28340d15f58953d99d7347718ca5049eaacf77e9429b->enter($__internal_35d19cc6638e1842ac2b28340d15f58953d99d7347718ca5049eaacf77e9429b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "LaisoArmBundle:Lotissement:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_35d19cc6638e1842ac2b28340d15f58953d99d7347718ca5049eaacf77e9429b->leave($__internal_35d19cc6638e1842ac2b28340d15f58953d99d7347718ca5049eaacf77e9429b_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_9ba38fec74b1cc87facb1a0ab4a04915074e158dca7bc47d59d904340199b419 = $this->env->getExtension("native_profiler");
        $__internal_9ba38fec74b1cc87facb1a0ab4a04915074e158dca7bc47d59d904340199b419->enter($__internal_9ba38fec74b1cc87facb1a0ab4a04915074e158dca7bc47d59d904340199b419_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Appel d'offre - Lot n° ";
        echo twig_escape_filter($this->env, (isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "html", null, true);
        
        $__internal_9ba38fec74b1cc87facb1a0ab4a04915074e158dca7bc47d59d904340199b419->leave($__internal_9ba38fec74b1cc87facb1a0ab4a04915074e158dca7bc47d59d904340199b419_prof);

    }

    // line 5
    public function block_breadcumbs($context, array $blocks = array())
    {
        $__internal_83279a8ea8e44494417afdd8b8c1afcac7baf9f5101b89e0082b1364e25c7ed8 = $this->env->getExtension("native_profiler");
        $__internal_83279a8ea8e44494417afdd8b8c1afcac7baf9f5101b89e0082b1364e25c7ed8->enter($__internal_83279a8ea8e44494417afdd8b8c1afcac7baf9f5101b89e0082b1364e25c7ed8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "breadcumbs"));

        // line 6
        echo "    <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("dao");
        echo "\">Dossier d'appel d'offre</a></li>
    <li><a href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("dao_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\">Lot n° ";
        echo twig_escape_filter($this->env, (isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "html", null, true);
        echo "</a></li>

    ";
        // line 9
        if ($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "valide", array())) {
            // line 10
            echo "        <span class=\"mif-checkmark fg-green place-right\"></span>
    ";
        }
        
        $__internal_83279a8ea8e44494417afdd8b8c1afcac7baf9f5101b89e0082b1364e25c7ed8->leave($__internal_83279a8ea8e44494417afdd8b8c1afcac7baf9f5101b89e0082b1364e25c7ed8_prof);

    }

    // line 15
    public function block_body($context, array $blocks = array())
    {
        $__internal_e4ab4af8f7ec4bf8dad7985efe543dd9f96b00f1fc1def8d11cab169a793816c = $this->env->getExtension("native_profiler");
        $__internal_e4ab4af8f7ec4bf8dad7985efe543dd9f96b00f1fc1def8d11cab169a793816c->enter($__internal_e4ab4af8f7ec4bf8dad7985efe543dd9f96b00f1fc1def8d11cab169a793816c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 17
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "hasRole", array(0 => "ROLE_CI"), "method") && (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "bloc", array()) == null) || ($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "bloc", array()), "numero", array()) != $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "bloc", array()), "numero", array()))))) {
            // line 18
            echo "        ";
            $this->loadTemplate("@LaisoArm/Shared/access_denied.html.twig", "LaisoArmBundle:Lotissement:show.html.twig", 18)->display($context);
            // line 19
            echo "    ";
        } else {
            // line 20
            echo "
        <span class=\"clear-float\"></span>
        <table class=\"table border bordered\">
            <thead>
            <tr>
                <th>Numéro</th>
                <th>Objet</th>
                <th>Classe</th>
                <th>Consistances</th>
                <th>Région</th>
                <th>Bloc</th>
                <th>Délai</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>";
            // line 36
            echo twig_escape_filter($this->env, (isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "html", null, true);
            echo "</td>
                <td>
                    ";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "type", array()), "html", null, true);
            echo " de la ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "axe", array()), "html", null, true);
            echo " - Entre le PK ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "pKDebut", array()), "html", null, true);
            echo " et le PK ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "pKFin", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "localisation", array()), "html", null, true);
            echo "
                    <hr class=\"thin\">
                    <small><strong>Campagne : </strong>";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "campagne", array()), "html", null, true);
            echo "</small>
                </td>
                <td class=\"align-center\">";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "classe", array()), "html", null, true);
            echo "</td>
                <td class=\"align-center\">";
            // line 43
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "type", array()), "abbrege", array()), "html", null, true);
            echo "</td>
                <td class=\"align-center\">";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "region", array()), "html", null, true);
            echo "</td>
                <td class=\"align-center\">";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "bloc", array()), "localisation", array()), "html", null, true);
            echo "</td>
                <td class=\"align-center\">";
            // line 46
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "delai", array()), "html", null, true);
            echo " mois</td>
            </tr>
            </tbody>
            <tbody>
        </table>

        ";
            // line 52
            if ((($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "valide", array()) == false) && ($this->getAttribute($this->getAttribute((isset($context["dqe"]) ? $context["dqe"] : $this->getContext($context, "dqe")), "avenant", array()), "valide", array()) == false))) {
                // line 53
                echo "            <div class=\"place-right\">
                <a class=\"button primary ajax-button\" href=\"";
                // line 54
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("dao_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
                echo "\">Modifier</a>
                <a class=\"button danger ajax-button\" href=\"";
                // line 55
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("dao_ajax_delete", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
                echo "\">Supprimer</a>
            </div>
            <span class=\"clear-float\"></span>
        ";
            } else {
                // line 59
                echo "            <span class=\"place-right fg-red\"><small>Vous ne pouvez plus apporter de modification.</small></span>
            <span class=\"clear-float\"></span>
        ";
            }
            // line 62
            echo "        <br>

        ";
            // line 64
            $context["lotissement"] = (isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity"));
            // line 65
            echo "        ";
            $context["rubrique"] = "dao";
            // line 66
            echo "        ";
            $this->loadTemplate("@LaisoArm/DQE/dqe.html.twig", "LaisoArmBundle:Lotissement:show.html.twig", 66)->display($context);
            // line 67
            echo "    ";
        }
        
        $__internal_e4ab4af8f7ec4bf8dad7985efe543dd9f96b00f1fc1def8d11cab169a793816c->leave($__internal_e4ab4af8f7ec4bf8dad7985efe543dd9f96b00f1fc1def8d11cab169a793816c_prof);

    }

    public function getTemplateName()
    {
        return "LaisoArmBundle:Lotissement:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  191 => 67,  188 => 66,  185 => 65,  183 => 64,  179 => 62,  174 => 59,  167 => 55,  163 => 54,  160 => 53,  158 => 52,  149 => 46,  145 => 45,  141 => 44,  137 => 43,  133 => 42,  128 => 40,  115 => 38,  110 => 36,  92 => 20,  89 => 19,  86 => 18,  84 => 17,  78 => 15,  69 => 10,  67 => 9,  60 => 7,  55 => 6,  49 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block title %}Appel d'offre - Lot n° {{ entity }}{% endblock %}*/
/* */
/* {% block breadcumbs %}*/
/*     <li><a href="{{ path('dao') }}">Dossier d'appel d'offre</a></li>*/
/*     <li><a href="{{ path('dao_show', {'id': entity.id}) }}">Lot n° {{ entity }}</a></li>*/
/* */
/*     {% if entity.valide %}*/
/*         <span class="mif-checkmark fg-green place-right"></span>*/
/*     {% endif %}*/
/* {% endblock %}*/
/* */
/* */
/* {% block body -%}*/
/* */
/*     {% if app.user.hasRole('ROLE_CI') and (app.user.bloc == null or entity.bloc.numero != app.user.bloc.numero) %}*/
/*         {% include ('@LaisoArm/Shared/access_denied.html.twig') %}*/
/*     {% else %}*/
/* */
/*         <span class="clear-float"></span>*/
/*         <table class="table border bordered">*/
/*             <thead>*/
/*             <tr>*/
/*                 <th>Numéro</th>*/
/*                 <th>Objet</th>*/
/*                 <th>Classe</th>*/
/*                 <th>Consistances</th>*/
/*                 <th>Région</th>*/
/*                 <th>Bloc</th>*/
/*                 <th>Délai</th>*/
/*             </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*             <tr>*/
/*                 <td>{{ entity }}</td>*/
/*                 <td>*/
/*                     {{ entity.type }} de la {{ entity.axe }} - Entre le PK {{ entity.pKDebut }} et le PK {{ entity.pKFin }} - {{ entity.localisation }}*/
/*                     <hr class="thin">*/
/*                     <small><strong>Campagne : </strong>{{ entity.campagne }}</small>*/
/*                 </td>*/
/*                 <td class="align-center">{{ entity.classe }}</td>*/
/*                 <td class="align-center">{{ entity.type.abbrege }}</td>*/
/*                 <td class="align-center">{{ entity.region }}</td>*/
/*                 <td class="align-center">{{ entity.bloc.localisation }}</td>*/
/*                 <td class="align-center">{{ entity.delai }} mois</td>*/
/*             </tr>*/
/*             </tbody>*/
/*             <tbody>*/
/*         </table>*/
/* */
/*         {% if entity.valide == false and dqe.avenant.valide == false %}*/
/*             <div class="place-right">*/
/*                 <a class="button primary ajax-button" href="{{ path('dao_edit', { 'id': entity.id }) }}">Modifier</a>*/
/*                 <a class="button danger ajax-button" href="{{ path('dao_ajax_delete', {'id': entity.id}) }}">Supprimer</a>*/
/*             </div>*/
/*             <span class="clear-float"></span>*/
/*         {% else %}*/
/*             <span class="place-right fg-red"><small>Vous ne pouvez plus apporter de modification.</small></span>*/
/*             <span class="clear-float"></span>*/
/*         {% endif %}*/
/*         <br>*/
/* */
/*         {% set lotissement = entity %}*/
/*         {% set rubrique = 'dao' %}*/
/*         {% include "@LaisoArm/DQE/dqe.html.twig" %}*/
/*     {% endif %}*/
/* {% endblock %}*/
/* */
