<?php

/* LaisoArmBundle:Lotissement:index.html.twig */
class __TwigTemplate_951da57eb76b066d4b563b2238251596772c4466545521cb5dec828920da411f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "LaisoArmBundle:Lotissement:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'breadcumbs' => array($this, 'block_breadcumbs'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f73e41f1ce96d739e160636ab621494cd4e2a95474a09fd86733460aa201c79a = $this->env->getExtension("native_profiler");
        $__internal_f73e41f1ce96d739e160636ab621494cd4e2a95474a09fd86733460aa201c79a->enter($__internal_f73e41f1ce96d739e160636ab621494cd4e2a95474a09fd86733460aa201c79a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "LaisoArmBundle:Lotissement:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f73e41f1ce96d739e160636ab621494cd4e2a95474a09fd86733460aa201c79a->leave($__internal_f73e41f1ce96d739e160636ab621494cd4e2a95474a09fd86733460aa201c79a_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_7f9551e44a64918e0fefb75fddd00dd73cb6ea6be685f9344f9edffffcbb2647 = $this->env->getExtension("native_profiler");
        $__internal_7f9551e44a64918e0fefb75fddd00dd73cb6ea6be685f9344f9edffffcbb2647->enter($__internal_7f9551e44a64918e0fefb75fddd00dd73cb6ea6be685f9344f9edffffcbb2647_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Dossier d'appels d'offre";
        
        $__internal_7f9551e44a64918e0fefb75fddd00dd73cb6ea6be685f9344f9edffffcbb2647->leave($__internal_7f9551e44a64918e0fefb75fddd00dd73cb6ea6be685f9344f9edffffcbb2647_prof);

    }

    // line 5
    public function block_breadcumbs($context, array $blocks = array())
    {
        $__internal_56a05b56c2660ee59ea95d570124899d78dce1da7d061258d517f49294e1baef = $this->env->getExtension("native_profiler");
        $__internal_56a05b56c2660ee59ea95d570124899d78dce1da7d061258d517f49294e1baef->enter($__internal_56a05b56c2660ee59ea95d570124899d78dce1da7d061258d517f49294e1baef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "breadcumbs"));

        // line 6
        echo "    <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("dao");
        echo "\">Dossier d'appel d'offre</a></li>
";
        
        $__internal_56a05b56c2660ee59ea95d570124899d78dce1da7d061258d517f49294e1baef->leave($__internal_56a05b56c2660ee59ea95d570124899d78dce1da7d061258d517f49294e1baef_prof);

    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        $__internal_cc60a2d3ad91da3774514c1bbcc438202ae776283c0d56c7e7c094b65fc32627 = $this->env->getExtension("native_profiler");
        $__internal_cc60a2d3ad91da3774514c1bbcc438202ae776283c0d56c7e7c094b65fc32627->enter($__internal_cc60a2d3ad91da3774514c1bbcc438202ae776283c0d56c7e7c094b65fc32627_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 9
        if ((twig_length_filter($this->env, (isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities"))) > 0)) {
            // line 10
            echo "
        ";
            // line 11
            $context["lotissements"] = (isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities"));
            // line 12
            echo "        ";
            $this->loadTemplate("@LaisoArm/Lotissement/includes/list_lotissement.html.twig", "LaisoArmBundle:Lotissement:index.html.twig", 12)->display($context);
            // line 13
            echo "
        ";
            // line 14
            $this->loadTemplate("LaisoArmBundle:Shared:paginator.html.twig", "LaisoArmBundle:Lotissement:index.html.twig", 14)->display($context);
            // line 15
            echo "
        <a class=\"ajax-button button primary\" href=\"";
            // line 16
            echo $this->env->getExtension('routing')->getPath("dao_new");
            echo "\">
            Créer un nouvel appel d'offre
        </a>
    ";
        } else {
            // line 20
            echo "        <div class=\"laiso-bordered margin10 no-margin-left no-margin-right no-margin-bottom padding10 align-center bg-grayLighter\">
            <h3>Aucun appel d'offre.</h3>

            <a class=\"ajax-button button primary\" href=\"";
            // line 23
            echo $this->env->getExtension('routing')->getPath("dao_new");
            echo "\">
                Créer un nouvel appel d'offre
            </a>
        </div>
    ";
        }
        
        $__internal_cc60a2d3ad91da3774514c1bbcc438202ae776283c0d56c7e7c094b65fc32627->leave($__internal_cc60a2d3ad91da3774514c1bbcc438202ae776283c0d56c7e7c094b65fc32627_prof);

    }

    public function getTemplateName()
    {
        return "LaisoArmBundle:Lotissement:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 23,  95 => 20,  88 => 16,  85 => 15,  83 => 14,  80 => 13,  77 => 12,  75 => 11,  72 => 10,  70 => 9,  64 => 8,  54 => 6,  48 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block title %}Dossier d'appels d'offre{% endblock %}*/
/* */
/* {% block breadcumbs %}*/
/*     <li><a href="{{ path('dao') }}">Dossier d'appel d'offre</a></li>*/
/* {% endblock %}*/
/* {% block body -%}*/
/*     {% if entities | length > 0 %}*/
/* */
/*         {% set lotissements = entities %}*/
/*         {% include ('@LaisoArm/Lotissement/includes/list_lotissement.html.twig') %}*/
/* */
/*         {% include ('LaisoArmBundle:Shared:paginator.html.twig') %}*/
/* */
/*         <a class="ajax-button button primary" href="{{ path('dao_new') }}">*/
/*             Créer un nouvel appel d'offre*/
/*         </a>*/
/*     {% else %}*/
/*         <div class="laiso-bordered margin10 no-margin-left no-margin-right no-margin-bottom padding10 align-center bg-grayLighter">*/
/*             <h3>Aucun appel d'offre.</h3>*/
/* */
/*             <a class="ajax-button button primary" href="{{ path('dao_new') }}">*/
/*                 Créer un nouvel appel d'offre*/
/*             </a>*/
/*         </div>*/
/*     {% endif %}*/
/* {% endblock %}*/
/* */
