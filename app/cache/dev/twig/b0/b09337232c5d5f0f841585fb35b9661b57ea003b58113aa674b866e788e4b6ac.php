<?php

/* FOSUserBundle:Profile:show_content.html.twig */
class __TwigTemplate_a25063bb03dcbaa6f1216208a1ba507496763552a6a431c1a0e8798ae761908d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3be5be6cc0ccf10e444aa24d8a499a7a919d65b89c78d49999cc7f7f053b7255 = $this->env->getExtension("native_profiler");
        $__internal_3be5be6cc0ccf10e444aa24d8a499a7a919d65b89c78d49999cc7f7f053b7255->enter($__internal_3be5be6cc0ccf10e444aa24d8a499a7a919d65b89c78d49999cc7f7f053b7255_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show_content.html.twig"));

        // line 2
        echo "
<div class=\"fos_user_user_show\">
    <dl class=\"horizontal padding10 laiso-bordered\">
        <dt>";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("profile.show.username", array(), "FOSUserBundle"), "html", null, true);
        echo " :</dt>
        <dd>";
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "</dd>

        <dt>";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("profile.show.nom", array(), "FOSUserBundle"), "html", null, true);
        echo " :</dt>
        <dd>";
        // line 9
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "nom", array())) > 0)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "nom", array()), "html", null, true);
            echo " ";
        } else {
            echo "-";
        }
        echo "</dd>

        <dt>";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("profile.show.prenom", array(), "FOSUserBundle"), "html", null, true);
        echo " :</dt>
        <dd>";
        // line 12
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "prenom", array())) > 0)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "prenom", array()), "html", null, true);
            echo " ";
        } else {
            echo "-";
        }
        echo "</dd>

        <dt>";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("profile.show.email", array(), "FOSUserBundle"), "html", null, true);
        echo " :</dt>
        <dd>";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array()), "html", null, true);
        echo "</dd>

        <dt>Rôles :</dt>
        <dd>";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "roles", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["role"]) {
            echo twig_escape_filter($this->env, $context["role"], "html", null, true);
            echo " ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['role'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "</dd>

        <dt>Dernière connexion :</dt>
        <dd>";
        // line 21
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "lastLogin", array()), "d M Y - H:i"), "html", null, true);
        echo "</dd>

        <dt>Bloc : </dt>
        <dd>";
        // line 24
        if ( !(null === $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "bloc", array()))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "bloc", array()), "localisation", array()), "html", null, true);
        } else {
            echo "Aucun bloc attribué à votre compte";
        }
        echo "</dd>
    </dl>
</div>
<span class=\"clear-float\"></span>";
        
        $__internal_3be5be6cc0ccf10e444aa24d8a499a7a919d65b89c78d49999cc7f7f053b7255->leave($__internal_3be5be6cc0ccf10e444aa24d8a499a7a919d65b89c78d49999cc7f7f053b7255_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 24,  90 => 21,  76 => 18,  70 => 15,  66 => 14,  55 => 12,  51 => 11,  40 => 9,  36 => 8,  31 => 6,  27 => 5,  22 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* <div class="fos_user_user_show">*/
/*     <dl class="horizontal padding10 laiso-bordered">*/
/*         <dt>{{ 'profile.show.username'|trans }} :</dt>*/
/*         <dd>{{ user.username }}</dd>*/
/* */
/*         <dt>{{ 'profile.show.nom'|trans }} :</dt>*/
/*         <dd>{% if user.nom | length > 0 %} {{ user.nom }} {% else %}-{% endif %}</dd>*/
/* */
/*         <dt>{{ 'profile.show.prenom'|trans }} :</dt>*/
/*         <dd>{% if user.prenom | length > 0 %} {{ user.prenom }} {% else %}-{% endif %}</dd>*/
/* */
/*         <dt>{{ 'profile.show.email'|trans }} :</dt>*/
/*         <dd>{{ user.email }}</dd>*/
/* */
/*         <dt>Rôles :</dt>*/
/*         <dd>{%  for role in user.roles %}{{ role }} {% endfor %}</dd>*/
/* */
/*         <dt>Dernière connexion :</dt>*/
/*         <dd>{{ user.lastLogin | date("d M Y - H:i") }}</dd>*/
/* */
/*         <dt>Bloc : </dt>*/
/*         <dd>{% if user.bloc is not null %} {{ user.bloc.localisation }}{% else %}Aucun bloc attribué à votre compte{% endif %}</dd>*/
/*     </dl>*/
/* </div>*/
/* <span class="clear-float"></span>*/
