<?php

/* FOSUserBundle:Resetting:request_content.html.twig */
class __TwigTemplate_7323e0eb39cfef3bf01bf5798b287702fc5fbfa6a54862a0badc94bb7f7ac0d1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ed101ffe9fd8a977de167857f6d9296163a6e46b3668534e68f6d13d98b3a1d0 = $this->env->getExtension("native_profiler");
        $__internal_ed101ffe9fd8a977de167857f6d9296163a6e46b3668534e68f6d13d98b3a1d0->enter($__internal_ed101ffe9fd8a977de167857f6d9296163a6e46b3668534e68f6d13d98b3a1d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request_content.html.twig"));

        // line 1
        echo "<div class=\"login-form padding20 block-shadow\">
    ";
        // line 3
        echo "    <h1 class=\"text-light\">Réinitialiser mon mot de passe</h1>
    <hr class=\"thin\">
    <br>

    <form action=\"";
        // line 7
        echo $this->env->getExtension('routing')->getPath("fos_user_resetting_send_email");
        echo "\" method=\"POST\" class=\"fos_user_resetting_request\">
        <div>
            ";
        // line 9
        if (array_key_exists("invalid_username", $context)) {
            // line 10
            echo "                <p class=\"fg-orange\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.request.invalid_username", array("%username%" => (isset($context["invalid_username"]) ? $context["invalid_username"] : $this->getContext($context, "invalid_username"))), "FOSUserBundle"), "html", null, true);
            echo "</p>
            ";
        }
        // line 12
        echo "            <label class=\"text-bold\" for=\"username\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.request.username", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>

            <div class=\"input-control text full-size\">
                <input type=\"text\" id=\"username\" name=\"username\" required=\"required\"/>
            </div>
        </div>
        <div>
            <input type=\"submit\" class=\"button primary place-right\" value=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.request.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\"/>
            <span class=\"clear-float\"></span>
        </div>
    </form>
</div>";
        
        $__internal_ed101ffe9fd8a977de167857f6d9296163a6e46b3668534e68f6d13d98b3a1d0->leave($__internal_ed101ffe9fd8a977de167857f6d9296163a6e46b3668534e68f6d13d98b3a1d0_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 19,  44 => 12,  38 => 10,  36 => 9,  31 => 7,  25 => 3,  22 => 1,);
    }
}
/* <div class="login-form padding20 block-shadow">*/
/*     {% trans_default_domain 'FOSUserBundle' %}*/
/*     <h1 class="text-light">Réinitialiser mon mot de passe</h1>*/
/*     <hr class="thin">*/
/*     <br>*/
/* */
/*     <form action="{{ path('fos_user_resetting_send_email') }}" method="POST" class="fos_user_resetting_request">*/
/*         <div>*/
/*             {% if invalid_username is defined %}*/
/*                 <p class="fg-orange">{{ 'resetting.request.invalid_username'|trans({'%username%': invalid_username}) }}</p>*/
/*             {% endif %}*/
/*             <label class="text-bold" for="username">{{ 'resetting.request.username'|trans }}</label>*/
/* */
/*             <div class="input-control text full-size">*/
/*                 <input type="text" id="username" name="username" required="required"/>*/
/*             </div>*/
/*         </div>*/
/*         <div>*/
/*             <input type="submit" class="button primary place-right" value="{{ 'resetting.request.submit'|trans }}"/>*/
/*             <span class="clear-float"></span>*/
/*         </div>*/
/*     </form>*/
/* </div>*/
