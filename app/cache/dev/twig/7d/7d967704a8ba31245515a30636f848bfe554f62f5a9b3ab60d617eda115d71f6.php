<?php

/* @LaisoArm/Lotissement/includes/list_lotissement.html.twig */
class __TwigTemplate_55ae7a82b97faf2de2527275dddf362b816ae0f27bdb338a821a9ad9e09ac8c4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_31ffd3793606143f1ce6322017da6d695a0d52ed72a6ea119897e54d4e39eb53 = $this->env->getExtension("native_profiler");
        $__internal_31ffd3793606143f1ce6322017da6d695a0d52ed72a6ea119897e54d4e39eb53->enter($__internal_31ffd3793606143f1ce6322017da6d695a0d52ed72a6ea119897e54d4e39eb53_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@LaisoArm/Lotissement/includes/list_lotissement.html.twig"));

        // line 1
        echo "<table class=\"table border bordered bg-white\">
    <thead>
    <tr>
        <th style=\"width: 20%;\">Lot</th>
        <th style=\"text-align: center\">Objet</th>
        <th style=\"text-align: center\">Bloc</th>
        <th style=\"width: 80px; text-align: center\">Délai</th>
        <th  style=\"text-align: center\">Classe</th>
        <th style=\"width: 18%; text-align: center;\">Remarques et actions</th>
    </tr>
    </thead>
    <tbody>

    ";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["lotissements"]) ? $context["lotissements"] : $this->getContext($context, "lotissements")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 15
            echo "        <tr>
            <td>
                <h4 ";
            // line 17
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "bloc", array()) && ($this->getAttribute($this->getAttribute($context["entity"], "bloc", array()), "localisation", array()) != $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "bloc", array()), "localisation", array())))) {
                // line 18
                echo "                    class=\"fg-orange\"
                ";
            } elseif ($this->getAttribute(            // line 19
$context["entity"], "valide", array())) {
                // line 20
                echo "                    class=\"fg-darkGreen\"
                ";
            } else {
                // line 22
                echo "                    class=\"fg-darkBlue\"
                        ";
            }
            // line 23
            echo ">

                    <strong>Lot n° ";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "affichage", array()), "html", null, true);
            echo "</strong>

                    ";
            // line 27
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "bloc", array()) && ($this->getAttribute($this->getAttribute($context["entity"], "bloc", array()), "localisation", array()) != $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "bloc", array()), "localisation", array())))) {
                // line 28
                echo "                        <span class=\"mif-warning fg-orange place-right\"></span>
                    ";
            } elseif ($this->getAttribute(            // line 29
$context["entity"], "valide", array())) {
                // line 30
                echo "                        <span class=\"mif-checkmark place-right\"></span>
                    ";
            }
            // line 32
            echo "                </h4>
                <hr class=\"thin\">
                <small>
                    ";
            // line 35
            if ($this->getAttribute($context["entity"], "type", array())) {
                // line 36
                echo "                        ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "type", array()), "html", null, true);
                echo "
                    ";
            } else {
                // line 38
                echo "                        (Aucune consistance)
                    ";
            }
            // line 40
            echo "                </small>
            </td>
            <td>
                ";
            // line 43
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "type", array()), "html", null, true);
            echo " de la ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "axe", array()), "html", null, true);
            echo " entre le PK ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "pKDebut", array()), "html", null, true);
            echo " et le PK ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "pKFin", array()), "html", null, true);
            echo "
                - ";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "localisation", array()), "html", null, true);
            echo "
                <hr class=\"thin\">
                <small><strong>Campagne : </strong>";
            // line 46
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "campagne", array()), "html", null, true);
            echo "</small>
            </td>
            <td class=\"align-center\">";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "region", array()), "nom", array()), "html", null, true);
            echo "</td>
            <td class=\"align-center\">";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "delai", array()), "html", null, true);
            echo " mois</td>
            <td class=\"align-center\">";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "classe", array()), "html", null, true);
            echo "</td>
            <td class=\"align-center\">
                ";
            // line 52
            if ( !$this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($context["entity"], "marche", array()), "avenants", array()), (twig_length_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "marche", array()), "avenants", array())) - 1), array(), "array"), "valide", array())) {
                // line 53
                echo "                    <small class=\"fg-orange\">Les quantités ne sont pas encores validées <br></small>
                ";
            }
            // line 55
            echo "
                ";
            // line 56
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "bloc", array()) && ($this->getAttribute($this->getAttribute($context["entity"], "bloc", array()), "localisation", array()) != $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "bloc", array()), "localisation", array())))) {
                // line 57
                echo "                    <small class=\"fg-orange\">
                        Offre non disponible uniquement dans le bloc ";
                // line 58
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "bloc", array()), "html", null, true);
                echo "
                    </small>
                ";
            } elseif (($this->getAttribute(            // line 60
$context["entity"], "valide", array()) == false)) {
                // line 61
                echo "                    <small class=\"fg-gray\">Offre en attente de validation</small> <br><br>
                    <a href=\"";
                // line 62
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("dao_show", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\">
                        Détails
                    </a> &nbsp;
                    <a class=\"ajax-button\" href=\"";
                // line 65
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("dao_edit", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\">
                        Modifier
                    </a>
                ";
            } else {
                // line 69
                echo "                    <small class=\"fg-gray\">
                        Offre déjà validée : Veuillez la retrouver dans la rubrique <strong> <a
                                    href=\"";
                // line 71
                echo $this->env->getExtension('routing')->getPath("marche");
                echo "\">Marché</a> > Lot ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "affichage", array()), "html", null, true);
                echo "</strong>
                    </small>
                ";
            }
            // line 74
            echo "            </td>
        </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo "    </tbody>
</table>";
        
        $__internal_31ffd3793606143f1ce6322017da6d695a0d52ed72a6ea119897e54d4e39eb53->leave($__internal_31ffd3793606143f1ce6322017da6d695a0d52ed72a6ea119897e54d4e39eb53_prof);

    }

    public function getTemplateName()
    {
        return "@LaisoArm/Lotissement/includes/list_lotissement.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  192 => 77,  184 => 74,  176 => 71,  172 => 69,  165 => 65,  159 => 62,  156 => 61,  154 => 60,  149 => 58,  146 => 57,  144 => 56,  141 => 55,  137 => 53,  135 => 52,  130 => 50,  126 => 49,  122 => 48,  117 => 46,  112 => 44,  102 => 43,  97 => 40,  93 => 38,  87 => 36,  85 => 35,  80 => 32,  76 => 30,  74 => 29,  71 => 28,  69 => 27,  64 => 25,  60 => 23,  56 => 22,  52 => 20,  50 => 19,  47 => 18,  45 => 17,  41 => 15,  37 => 14,  22 => 1,);
    }
}
/* <table class="table border bordered bg-white">*/
/*     <thead>*/
/*     <tr>*/
/*         <th style="width: 20%;">Lot</th>*/
/*         <th style="text-align: center">Objet</th>*/
/*         <th style="text-align: center">Bloc</th>*/
/*         <th style="width: 80px; text-align: center">Délai</th>*/
/*         <th  style="text-align: center">Classe</th>*/
/*         <th style="width: 18%; text-align: center;">Remarques et actions</th>*/
/*     </tr>*/
/*     </thead>*/
/*     <tbody>*/
/* */
/*     {% for entity in lotissements %}*/
/*         <tr>*/
/*             <td>*/
/*                 <h4 {% if app.user.bloc and entity.bloc.localisation != app.user.bloc.localisation %}*/
/*                     class="fg-orange"*/
/*                 {% elseif entity.valide %}*/
/*                     class="fg-darkGreen"*/
/*                 {% else %}*/
/*                     class="fg-darkBlue"*/
/*                         {% endif %}>*/
/* */
/*                     <strong>Lot n° {{ entity.affichage }}</strong>*/
/* */
/*                     {% if app.user.bloc and entity.bloc.localisation != app.user.bloc.localisation %}*/
/*                         <span class="mif-warning fg-orange place-right"></span>*/
/*                     {% elseif entity.valide %}*/
/*                         <span class="mif-checkmark place-right"></span>*/
/*                     {% endif %}*/
/*                 </h4>*/
/*                 <hr class="thin">*/
/*                 <small>*/
/*                     {% if entity.type %}*/
/*                         {{ entity.type }}*/
/*                     {% else %}*/
/*                         (Aucune consistance)*/
/*                     {% endif %}*/
/*                 </small>*/
/*             </td>*/
/*             <td>*/
/*                 {{ entity.type }} de la {{ entity.axe }} entre le PK {{ entity.pKDebut }} et le PK {{ entity.pKFin }}*/
/*                 - {{ entity.localisation }}*/
/*                 <hr class="thin">*/
/*                 <small><strong>Campagne : </strong>{{ entity.campagne }}</small>*/
/*             </td>*/
/*             <td class="align-center">{{ entity.region.nom }}</td>*/
/*             <td class="align-center">{{ entity.delai }} mois</td>*/
/*             <td class="align-center">{{ entity.classe }}</td>*/
/*             <td class="align-center">*/
/*                 {% if not entity.marche.avenants[entity.marche.avenants | length - 1].valide %}*/
/*                     <small class="fg-orange">Les quantités ne sont pas encores validées <br></small>*/
/*                 {% endif %}*/
/* */
/*                 {% if app.user.bloc and entity.bloc.localisation != app.user.bloc.localisation %}*/
/*                     <small class="fg-orange">*/
/*                         Offre non disponible uniquement dans le bloc {{ entity.bloc }}*/
/*                     </small>*/
/*                 {% elseif entity.valide == false %}*/
/*                     <small class="fg-gray">Offre en attente de validation</small> <br><br>*/
/*                     <a href="{{ path('dao_show', { 'id': entity.id }) }}">*/
/*                         Détails*/
/*                     </a> &nbsp;*/
/*                     <a class="ajax-button" href="{{ path('dao_edit', { 'id': entity.id }) }}">*/
/*                         Modifier*/
/*                     </a>*/
/*                 {% else %}*/
/*                     <small class="fg-gray">*/
/*                         Offre déjà validée : Veuillez la retrouver dans la rubrique <strong> <a*/
/*                                     href="{{ path('marche') }}">Marché</a> > Lot {{ entity.affichage }}</strong>*/
/*                     </small>*/
/*                 {% endif %}*/
/*             </td>*/
/*         </tr>*/
/*     {% endfor %}*/
/*     </tbody>*/
/* </table>*/
