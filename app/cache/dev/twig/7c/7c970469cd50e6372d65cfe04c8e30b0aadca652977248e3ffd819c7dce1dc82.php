<?php

/* @LaisoArm/Default/menuAdmin.html.twig */
class __TwigTemplate_ab1ad82fc1f9fd00233c361a2604c6a5d64d1707e7f238e2a7c4b827663cc198 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3de40d35555977475d950e5f1db839cbc42003c670babd3f5c87b17a2817cc16 = $this->env->getExtension("native_profiler");
        $__internal_3de40d35555977475d950e5f1db839cbc42003c670babd3f5c87b17a2817cc16->enter($__internal_3de40d35555977475d950e5f1db839cbc42003c670babd3f5c87b17a2817cc16_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@LaisoArm/Default/menuAdmin.html.twig"));

        // line 1
        echo "<h3 class=\"bg-grayLighter padding10\">Menu principal <span class=\"place-right tag warning\">Super Admin</span></h3>

<div class=\"grid\">
    <div class=\"row cells2\">
        <div class=\"cell\">
            <div class=\"panel collapsible\" data-role=\"panel\">
                <div class=\"heading\">
                    <span class=\"icon mif-cog\"></span>
                    <span class=\"title\">Paramétrage</span>
                </div>
                <div class=\"content padding10\">
                    <div class=\"padding10\">
                        <ul class=\"numeric-list square-marker\">
                            <li><a href=\"";
        // line 14
        echo $this->env->getExtension('routing')->getPath("typeconsistance");
        echo "\">Types de consistance</a></li>
                            <li><a href=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("unitedemesure");
        echo "\">Unités de mesure</a></li>
                            <li><a href=\"";
        // line 16
        echo $this->env->getExtension('routing')->getPath("libelleavenant");
        echo "\">Libellés de marché</a></li>
                            <li><a href=\"";
        // line 17
        echo $this->env->getExtension('routing')->getPath("bloc");
        echo "\">Bloc</a></li>
                            <li><a href=\"";
        // line 18
        echo $this->env->getExtension('routing')->getPath("region");
        echo "\">Régions</a></li>
                            <li><a href=\"";
        // line 19
        echo $this->env->getExtension('routing')->getPath("axe");
        echo "\">Axes</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"cell\">
            <div class=\"panel collapsible\" data-role=\"panel\">
                <div class=\"heading\">
                    <span class=\"icon mif-users\"></span>
                    <span class=\"title\">Gestion des utilisateurs</span>
                </div>
                <div class=\"content padding10\">
                    <ul class=\"numeric-list square-marker\">
                        <li><a href=\"#\">Gestion des utilisateurs</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
";
        
        $__internal_3de40d35555977475d950e5f1db839cbc42003c670babd3f5c87b17a2817cc16->leave($__internal_3de40d35555977475d950e5f1db839cbc42003c670babd3f5c87b17a2817cc16_prof);

    }

    public function getTemplateName()
    {
        return "@LaisoArm/Default/menuAdmin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 19,  53 => 18,  49 => 17,  45 => 16,  41 => 15,  37 => 14,  22 => 1,);
    }
}
/* <h3 class="bg-grayLighter padding10">Menu principal <span class="place-right tag warning">Super Admin</span></h3>*/
/* */
/* <div class="grid">*/
/*     <div class="row cells2">*/
/*         <div class="cell">*/
/*             <div class="panel collapsible" data-role="panel">*/
/*                 <div class="heading">*/
/*                     <span class="icon mif-cog"></span>*/
/*                     <span class="title">Paramétrage</span>*/
/*                 </div>*/
/*                 <div class="content padding10">*/
/*                     <div class="padding10">*/
/*                         <ul class="numeric-list square-marker">*/
/*                             <li><a href="{{ path('typeconsistance') }}">Types de consistance</a></li>*/
/*                             <li><a href="{{ path('unitedemesure') }}">Unités de mesure</a></li>*/
/*                             <li><a href="{{ path('libelleavenant') }}">Libellés de marché</a></li>*/
/*                             <li><a href="{{ path('bloc') }}">Bloc</a></li>*/
/*                             <li><a href="{{ path('region') }}">Régions</a></li>*/
/*                             <li><a href="{{ path('axe') }}">Axes</a></li>*/
/*                         </ul>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         <div class="cell">*/
/*             <div class="panel collapsible" data-role="panel">*/
/*                 <div class="heading">*/
/*                     <span class="icon mif-users"></span>*/
/*                     <span class="title">Gestion des utilisateurs</span>*/
/*                 </div>*/
/*                 <div class="content padding10">*/
/*                     <ul class="numeric-list square-marker">*/
/*                         <li><a href="#">Gestion des utilisateurs</a></li>*/
/*                     </ul>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
