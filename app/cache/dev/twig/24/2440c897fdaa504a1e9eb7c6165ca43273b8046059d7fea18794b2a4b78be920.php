<?php

/* LaisoArmBundle:Bloc/includes:new_bloc.html.twig */
class __TwigTemplate_78cd209bcef2a7f19c14875c72808e45d061acdbd5d8dcdf75628cdec53d1a4a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a5ff91b9bf8bfd48d789c418ad036c57bcad755e49c65e03297710f14afb8c09 = $this->env->getExtension("native_profiler");
        $__internal_a5ff91b9bf8bfd48d789c418ad036c57bcad755e49c65e03297710f14afb8c09->enter($__internal_a5ff91b9bf8bfd48d789c418ad036c57bcad755e49c65e03297710f14afb8c09_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "LaisoArmBundle:Bloc/includes:new_bloc.html.twig"));

        // line 1
        echo "<h1 class=\"fg-darkTeal\">Création de nouveau bloc</h1>
<br>
";
        // line 3
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("data-role" => "validator", "data-show-required-state" => "false", "data-hint-mode" => "line", "data-hint-background" => "bg-red", "data-hint-color" => "fg-white", "data-hide-error" => "5000")));
        // line 10
        echo "
<div class=\"grid laiso-bordered padding10\">
    <div class=\"row cells5\">
        <div class=\"cell\">
            <strong>Numéro du bloc</strong>

            <div class=\"fg-red\">
                ";
        // line 17
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "numero", array()), 'errors');
        echo "
            </div>

            <div class=\"input-control text full-size\">
                ";
        // line 21
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "numero", array()), 'widget');
        echo "
                <span class=\"input-state-error mif-warning\"></span>
                <span class=\"input-state-success mif-checkmark\"></span>
            </div>
        </div>

        <div class=\"cell colspan2\">
            <strong>Emplacement du bloc</strong>

            <div class=\"fg-red\">
                ";
        // line 31
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "localisation", array()), 'errors');
        echo "
            </div>

            <div class=\"input-control text full-size\">
                ";
        // line 35
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "localisation", array()), 'widget');
        echo "
                <span class=\"input-state-error mif-warning\"></span>
                <span class=\"input-state-success mif-checkmark\"></span>
            </div>
        </div>

        <div class=\"cell colspan2\">
            <strong>Siège du bloc</strong>

            <div class=\"fg-red\">
                ";
        // line 45
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "siege", array()), 'errors');
        echo "
            </div>

            <div class=\"input-control text full-size\">
                ";
        // line 49
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "siege", array()), 'widget');
        echo "
                <span class=\"input-state-error mif-warning\"></span>
                <span class=\"input-state-success mif-checkmark\"></span>
            </div>
        </div>
    </div>
</div>
";
        // line 56
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        
        $__internal_a5ff91b9bf8bfd48d789c418ad036c57bcad755e49c65e03297710f14afb8c09->leave($__internal_a5ff91b9bf8bfd48d789c418ad036c57bcad755e49c65e03297710f14afb8c09_prof);

    }

    public function getTemplateName()
    {
        return "LaisoArmBundle:Bloc/includes:new_bloc.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 56,  84 => 49,  77 => 45,  64 => 35,  57 => 31,  44 => 21,  37 => 17,  28 => 10,  26 => 3,  22 => 1,);
    }
}
/* <h1 class="fg-darkTeal">Création de nouveau bloc</h1>*/
/* <br>*/
/* {{ form_start(form, {'attr':{*/
/*     'data-role': 'validator',*/
/*     'data-show-required-state': "false",*/
/*     'data-hint-mode': "line",*/
/*     'data-hint-background': "bg-red",*/
/*     'data-hint-color': "fg-white",*/
/*     'data-hide-error': "5000"*/
/* }}) }}*/
/* <div class="grid laiso-bordered padding10">*/
/*     <div class="row cells5">*/
/*         <div class="cell">*/
/*             <strong>Numéro du bloc</strong>*/
/* */
/*             <div class="fg-red">*/
/*                 {{ form_errors(form.numero) }}*/
/*             </div>*/
/* */
/*             <div class="input-control text full-size">*/
/*                 {{ form_widget(form.numero) }}*/
/*                 <span class="input-state-error mif-warning"></span>*/
/*                 <span class="input-state-success mif-checkmark"></span>*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="cell colspan2">*/
/*             <strong>Emplacement du bloc</strong>*/
/* */
/*             <div class="fg-red">*/
/*                 {{ form_errors(form.localisation) }}*/
/*             </div>*/
/* */
/*             <div class="input-control text full-size">*/
/*                 {{ form_widget(form.localisation) }}*/
/*                 <span class="input-state-error mif-warning"></span>*/
/*                 <span class="input-state-success mif-checkmark"></span>*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="cell colspan2">*/
/*             <strong>Siège du bloc</strong>*/
/* */
/*             <div class="fg-red">*/
/*                 {{ form_errors(form.siege) }}*/
/*             </div>*/
/* */
/*             <div class="input-control text full-size">*/
/*                 {{ form_widget(form.siege) }}*/
/*                 <span class="input-state-error mif-warning"></span>*/
/*                 <span class="input-state-success mif-checkmark"></span>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* {{ form_end(form) }}*/
