<?php

/* LaisoArmBundle:Axe:index.html.twig */
class __TwigTemplate_28df33e31945c2a28c1ed85c3de74e249587d21f75e1ef4b65356298c6940c6f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "LaisoArmBundle:Axe:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'breadcumbs' => array($this, 'block_breadcumbs'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e31448b1c6a19046be9e557305a55e34b13feba45b3c15bc2b9b04f15253af80 = $this->env->getExtension("native_profiler");
        $__internal_e31448b1c6a19046be9e557305a55e34b13feba45b3c15bc2b9b04f15253af80->enter($__internal_e31448b1c6a19046be9e557305a55e34b13feba45b3c15bc2b9b04f15253af80_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "LaisoArmBundle:Axe:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e31448b1c6a19046be9e557305a55e34b13feba45b3c15bc2b9b04f15253af80->leave($__internal_e31448b1c6a19046be9e557305a55e34b13feba45b3c15bc2b9b04f15253af80_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_a50e8d1aaf38b85a74e47e5abb65881f5c9bed9b75ab7310b3f3c9cfb899233b = $this->env->getExtension("native_profiler");
        $__internal_a50e8d1aaf38b85a74e47e5abb65881f5c9bed9b75ab7310b3f3c9cfb899233b->enter($__internal_a50e8d1aaf38b85a74e47e5abb65881f5c9bed9b75ab7310b3f3c9cfb899233b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Admin - Axe";
        
        $__internal_a50e8d1aaf38b85a74e47e5abb65881f5c9bed9b75ab7310b3f3c9cfb899233b->leave($__internal_a50e8d1aaf38b85a74e47e5abb65881f5c9bed9b75ab7310b3f3c9cfb899233b_prof);

    }

    // line 5
    public function block_breadcumbs($context, array $blocks = array())
    {
        $__internal_1256ace0def6cb3e447217fe8f5c7e58f764c29ad0aafd13be58011e3b338320 = $this->env->getExtension("native_profiler");
        $__internal_1256ace0def6cb3e447217fe8f5c7e58f764c29ad0aafd13be58011e3b338320->enter($__internal_1256ace0def6cb3e447217fe8f5c7e58f764c29ad0aafd13be58011e3b338320_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "breadcumbs"));

        // line 6
        echo "    <li><a href=\"#\">Admin</a></li>
    <li><a href=\"";
        // line 7
        echo $this->env->getExtension('routing')->getPath("axe");
        echo "\">Axes</a></li>
";
        
        $__internal_1256ace0def6cb3e447217fe8f5c7e58f764c29ad0aafd13be58011e3b338320->leave($__internal_1256ace0def6cb3e447217fe8f5c7e58f764c29ad0aafd13be58011e3b338320_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_c7f6d40108ba88da3ca2e3b521f9755b8715c6c05001f062f9a823e68fd6a691 = $this->env->getExtension("native_profiler");
        $__internal_c7f6d40108ba88da3ca2e3b521f9755b8715c6c05001f062f9a823e68fd6a691->enter($__internal_c7f6d40108ba88da3ca2e3b521f9755b8715c6c05001f062f9a823e68fd6a691_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 11
        if ((twig_length_filter($this->env, (isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities"))) > 0)) {
            // line 12
            echo "        <table class=\"table border bordered\">
            <thead>
            <tr>
                <th style=\"width: 80%;\">Axe</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            ";
            // line 20
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
            foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
                // line 21
                echo "                <tr>
                    <td>";
                // line 22
                echo twig_escape_filter($this->env, $context["entity"], "html", null, true);
                echo "</td>
                    <td class=\"align-center\">
                        ";
                // line 25
                echo "                        <a class=\"ajax-button\" href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("axe_edit", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\">Modifier</a>
                    </td>
                </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 29
            echo "            </tbody>
        </table>

        ";
            // line 32
            $this->loadTemplate("LaisoArmBundle:Shared:paginator.html.twig", "LaisoArmBundle:Axe:index.html.twig", 32)->display($context);
            // line 33
            echo "        <a class=\"button primary ajax-button\" href=\"";
            echo $this->env->getExtension('routing')->getPath("axe_new");
            echo "\">
            Créer un nouvel axe
        </a>
    ";
        } else {
            // line 37
            echo "        <div class=\"laiso-bordered padding10 align-center margin10 no-margin-left no-margin-right no-margin-bottom\">
            <h3>Aucun axe trouvé</h3>
            <a class=\"button primary ajax-button\" href=\"";
            // line 39
            echo $this->env->getExtension('routing')->getPath("axe_new");
            echo "\">
                Créer un nouvel axe
            </a>
        </div>
    ";
        }
        // line 44
        echo "
";
        
        $__internal_c7f6d40108ba88da3ca2e3b521f9755b8715c6c05001f062f9a823e68fd6a691->leave($__internal_c7f6d40108ba88da3ca2e3b521f9755b8715c6c05001f062f9a823e68fd6a691_prof);

    }

    public function getTemplateName()
    {
        return "LaisoArmBundle:Axe:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 44,  126 => 39,  122 => 37,  114 => 33,  112 => 32,  107 => 29,  96 => 25,  91 => 22,  88 => 21,  84 => 20,  74 => 12,  72 => 11,  66 => 10,  57 => 7,  54 => 6,  48 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block title %}Admin - Axe{% endblock %}*/
/* */
/* {% block breadcumbs %}*/
/*     <li><a href="#">Admin</a></li>*/
/*     <li><a href="{{ path('axe') }}">Axes</a></li>*/
/* {% endblock %}*/
/* */
/* {% block body -%}*/
/*     {% if entities | length > 0 %}*/
/*         <table class="table border bordered">*/
/*             <thead>*/
/*             <tr>*/
/*                 <th style="width: 80%;">Axe</th>*/
/*                 <th>Actions</th>*/
/*             </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*             {% for entity in entities %}*/
/*                 <tr>*/
/*                     <td>{{ entity }}</td>*/
/*                     <td class="align-center">*/
/*                         {#<a href="{{ path('axe_show', { 'id': entity.id }) }}">Détails</a>#}*/
/*                         <a class="ajax-button" href="{{ path('axe_edit', { 'id': entity.id }) }}">Modifier</a>*/
/*                     </td>*/
/*                 </tr>*/
/*             {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/* */
/*         {% include ('LaisoArmBundle:Shared:paginator.html.twig') %}*/
/*         <a class="button primary ajax-button" href="{{ path('axe_new') }}">*/
/*             Créer un nouvel axe*/
/*         </a>*/
/*     {% else %}*/
/*         <div class="laiso-bordered padding10 align-center margin10 no-margin-left no-margin-right no-margin-bottom">*/
/*             <h3>Aucun axe trouvé</h3>*/
/*             <a class="button primary ajax-button" href="{{ path('axe_new') }}">*/
/*                 Créer un nouvel axe*/
/*             </a>*/
/*         </div>*/
/*     {% endif %}*/
/* */
/* {% endblock %}*/
/* */
