<?php

/* LaisoArmBundle:Bloc/includes:edit_bloc.html.twig */
class __TwigTemplate_426760eaf9c668fedacd6db506d0297606c386bbd2c97e8386f1dcb63119a3d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_395de88ca9b4acc2498d4446286908020bc486d0f30c3e9dd8321da047996f00 = $this->env->getExtension("native_profiler");
        $__internal_395de88ca9b4acc2498d4446286908020bc486d0f30c3e9dd8321da047996f00->enter($__internal_395de88ca9b4acc2498d4446286908020bc486d0f30c3e9dd8321da047996f00_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "LaisoArmBundle:Bloc/includes:edit_bloc.html.twig"));

        // line 1
        echo "<h1 class=\"fg-darkTeal\">Modifier le bloc ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "localisation", array()), "html", null, true);
        echo "</h1>
<br>
";
        // line 3
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_start', array("attr" => array("data-role" => "validator", "data-show-required-state" => "false", "data-hint-mode" => "line", "data-hint-background" => "bg-red", "data-hint-color" => "fg-white", "data-hide-error" => "5000")));
        // line 10
        echo "
<div class=\"grid laiso-bordered padding10\">
    <div class=\"row cells5\">
        <div class=\"cell\">
            <strong>Numéro du bloc</strong>

            <div class=\"fg-red\">
                ";
        // line 17
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "numero", array()), 'errors');
        echo "
            </div>

            <div class=\"input-control text full-size\">
                ";
        // line 21
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "numero", array()), 'widget');
        echo "
                <span class=\"input-state-error mif-warning\"></span>
                <span class=\"input-state-success mif-checkmark\"></span>
            </div>
        </div>

        <div class=\"cell colspan2\">
            <strong>Emplacement du bloc</strong>

            <div class=\"fg-red\">
                ";
        // line 31
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "localisation", array()), 'errors');
        echo "
            </div>

            <div class=\"input-control text full-size\">
                ";
        // line 35
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "localisation", array()), 'widget');
        echo "
                <span class=\"input-state-error mif-warning\"></span>
                <span class=\"input-state-success mif-checkmark\"></span>
            </div>
        </div>

        <div class=\"cell colspan2\">
            <strong>Siège du bloc</strong>

            <div class=\"fg-red\">
                ";
        // line 45
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "siege", array()), 'errors');
        echo "
            </div>

            <div class=\"input-control text full-size\">
                ";
        // line 49
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "siege", array()), 'widget');
        echo "
                <span class=\"input-state-error mif-warning\"></span>
                <span class=\"input-state-success mif-checkmark\"></span>
            </div>
        </div>
    </div>
</div>
";
        // line 56
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_end');
        
        $__internal_395de88ca9b4acc2498d4446286908020bc486d0f30c3e9dd8321da047996f00->leave($__internal_395de88ca9b4acc2498d4446286908020bc486d0f30c3e9dd8321da047996f00_prof);

    }

    public function getTemplateName()
    {
        return "LaisoArmBundle:Bloc/includes:edit_bloc.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 56,  86 => 49,  79 => 45,  66 => 35,  59 => 31,  46 => 21,  39 => 17,  30 => 10,  28 => 3,  22 => 1,);
    }
}
/* <h1 class="fg-darkTeal">Modifier le bloc {{ entity.localisation }}</h1>*/
/* <br>*/
/* {{ form_start(edit_form, {'attr':{*/
/*     'data-role': 'validator',*/
/*     'data-show-required-state': "false",*/
/*     'data-hint-mode': "line",*/
/*     'data-hint-background': "bg-red",*/
/*     'data-hint-color': "fg-white",*/
/*     'data-hide-error': "5000"*/
/* }}) }}*/
/* <div class="grid laiso-bordered padding10">*/
/*     <div class="row cells5">*/
/*         <div class="cell">*/
/*             <strong>Numéro du bloc</strong>*/
/* */
/*             <div class="fg-red">*/
/*                 {{ form_errors(edit_form.numero) }}*/
/*             </div>*/
/* */
/*             <div class="input-control text full-size">*/
/*                 {{ form_widget(edit_form.numero) }}*/
/*                 <span class="input-state-error mif-warning"></span>*/
/*                 <span class="input-state-success mif-checkmark"></span>*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="cell colspan2">*/
/*             <strong>Emplacement du bloc</strong>*/
/* */
/*             <div class="fg-red">*/
/*                 {{ form_errors(edit_form.localisation) }}*/
/*             </div>*/
/* */
/*             <div class="input-control text full-size">*/
/*                 {{ form_widget(edit_form.localisation) }}*/
/*                 <span class="input-state-error mif-warning"></span>*/
/*                 <span class="input-state-success mif-checkmark"></span>*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="cell colspan2">*/
/*             <strong>Siège du bloc</strong>*/
/* */
/*             <div class="fg-red">*/
/*                 {{ form_errors(edit_form.siege) }}*/
/*             </div>*/
/* */
/*             <div class="input-control text full-size">*/
/*                 {{ form_widget(edit_form.siege) }}*/
/*                 <span class="input-state-error mif-warning"></span>*/
/*                 <span class="input-state-success mif-checkmark"></span>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* {{ form_end(edit_form) }}*/
