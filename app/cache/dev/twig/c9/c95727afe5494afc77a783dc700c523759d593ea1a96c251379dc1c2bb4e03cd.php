<?php

/* FOSUserBundle:Resetting:checkEmail.html.twig */
class __TwigTemplate_d5751be8b83c02a585ef8ab960a3a818ffda6bab7599b4fa2c1d82b77cb06783 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::login.html.twig", "FOSUserBundle:Resetting:checkEmail.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::login.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3dc99681b777902b236cf3e7b9a6b7dec9d6cc454878839f394746778519705c = $this->env->getExtension("native_profiler");
        $__internal_3dc99681b777902b236cf3e7b9a6b7dec9d6cc454878839f394746778519705c->enter($__internal_3dc99681b777902b236cf3e7b9a6b7dec9d6cc454878839f394746778519705c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:checkEmail.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3dc99681b777902b236cf3e7b9a6b7dec9d6cc454878839f394746778519705c->leave($__internal_3dc99681b777902b236cf3e7b9a6b7dec9d6cc454878839f394746778519705c_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_b0e8175253cd36a598c85811be5893176d283cf9ad78fe3db449e71cc0e9aa32 = $this->env->getExtension("native_profiler");
        $__internal_b0e8175253cd36a598c85811be5893176d283cf9ad78fe3db449e71cc0e9aa32->enter($__internal_b0e8175253cd36a598c85811be5893176d283cf9ad78fe3db449e71cc0e9aa32_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <div class=\"login-form padding20 block-shadow\">
        <h2>Veuillez consulter votre boîte de messagerie</h2>
        <hr class=\"thin\">
        <p>
            ";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.check_email", array("%email%" => (isset($context["email"]) ? $context["email"] : $this->getContext($context, "email"))), "FOSUserBundle"), "html", null, true);
        echo "
        </p>
    </div>
";
        
        $__internal_b0e8175253cd36a598c85811be5893176d283cf9ad78fe3db449e71cc0e9aa32->leave($__internal_b0e8175253cd36a598c85811be5893176d283cf9ad78fe3db449e71cc0e9aa32_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:checkEmail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 10,  40 => 6,  34 => 5,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::login.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/*     <div class="login-form padding20 block-shadow">*/
/*         <h2>Veuillez consulter votre boîte de messagerie</h2>*/
/*         <hr class="thin">*/
/*         <p>*/
/*             {{ 'resetting.check_email'|trans({'%email%': email}) }}*/
/*         </p>*/
/*     </div>*/
/* {% endblock %}*/
/* */
