<?php

/* base.html.twig */
class __TwigTemplate_cdbc71e5e8fb3c2b4d0e2fc2e5c0a02a746b2b9de73d70041d00e45dda133407 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'breadcumbs' => array($this, 'block_breadcumbs'),
            'body' => array($this, 'block_body'),
            'footer' => array($this, 'block_footer'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5fd54fa566a3f54079acda24cad628835fb7a7630fef95ce9437e8cbb73f93ec = $this->env->getExtension("native_profiler");
        $__internal_5fd54fa566a3f54079acda24cad628835fb7a7630fef95ce9437e8cbb73f93ec->enter($__internal_5fd54fa566a3f54079acda24cad628835fb7a7630fef95ce9437e8cbb73f93ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\"/>
    <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo " | ARM::SGPEC</title>

    <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\"/>
    ";
        // line 8
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 17
        echo "</head>
<body> <!--class=\"scroll-y\"--->
<div class=\"container-fluid margin10\">
    <div class=\"bg-grayLighter padding10 laiso-bordered margin10 no-margin-left no-margin-right no-margin-bottom\">
        <h1><a href=\"";
        // line 21
        echo $this->env->getExtension('routing')->getPath("laiso_arm_homepage");
        echo "\" class=\"fg-black\">ARM::SGPEC</a>
            <small> SYSTEME DE GESTION DE PROJET D'ENTRETIENS COURANTS DES ROUTES</small>
        </h1>
    </div>
    <br>

    <div class=\"bg-grayLighter padding10 laiso-bordered\">

        <!-- MENU -->

        ";
        // line 31
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 32
            echo "        <ul class=\"h-menu laiso-bordered bg-white\">
            <li><a href=\"";
            // line 33
            echo $this->env->getExtension('routing')->getPath("laiso_arm_homepage");
            echo "\">Accueil</a></li>
            ";
            // line 34
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "hasRole", array(0 => "ROLE_DTEC"), "method") || $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "hasRole", array(0 => "ROLE_CI"), "method"))) {
                // line 35
                echo "                <li>
                    <a href=\"#\" class=\"dropdown-toggle\">Gestion des DAO</a>
                    <ul class=\"d-menu\" data-role=\"dropdown\">
                        <li>
                            <a href=\"#\" class=\"dropdown-toggle\">Prix</a>
                            <ul class=\"d-menu\" data-role=\"dropdown\">
                                <li><a href=\"";
                // line 41
                echo $this->env->getExtension('routing')->getPath("prix_categorie");
                echo "\">Catégorie</a></li>
                                <li><a href=\"";
                // line 42
                echo $this->env->getExtension('routing')->getPath("prix_serie");
                echo "\">Série de prix</a></li>
                            </ul>
                        </li>
                        <li><a href=\"";
                // line 45
                echo $this->env->getExtension('routing')->getPath("dao");
                echo "\">DAO</a></li>
                    </ul>
                </li>
            ";
            }
            // line 49
            echo "
            ";
            // line 50
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "hasRole", array(0 => "ROLE_DTEC"), "method") || $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "hasRole", array(0 => "ROLE_CI"), "method"))) {
                // line 51
                echo "                <li><a href=\"";
                echo $this->env->getExtension('routing')->getPath("marche");
                echo "\">Gestion des marchés</a></li>
            ";
            }
            // line 53
            echo "
            ";
            // line 54
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "hasRole", array(0 => "ROLE_DTEC"), "method") || $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "hasRole", array(0 => "ROLE_CI"), "method"))) {
                // line 55
                echo "                <li><a href=\"";
                echo $this->env->getExtension('routing')->getPath("entreprise");
                echo "\">Entreprises</a></li>
            ";
            }
            // line 57
            echo "
            ";
            // line 58
            if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "hasRole", array(0 => "ROLE_DTEC"), "method")) {
                // line 59
                echo "                <li>
                    <a href=\"#\" class=\"dropdown-toggle\">Administration</a>
                    <ul class=\"d-menu\" data-role=\"dropdown\">
                        <li><a href=\"";
                // line 62
                echo $this->env->getExtension('routing')->getPath("dashboard_index");
                echo "\">Tableau de bord</a></li>
                        <li><a href=\"";
                // line 63
                echo $this->env->getExtension('routing')->getPath("campagne");
                echo "\">Campagne</a></li>
                    </ul>
                </li>
            ";
            }
            // line 67
            echo "
            ";
            // line 68
            if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "hasRole", array(0 => "ROLE_ADMIN"), "method")) {
                // line 69
                echo "                <li>
                    <a href=\"#\" class=\"dropdown-toggle\">Paramétrage</a>
                    <ul class=\"d-menu\" data-role=\"dropdown\">
                        <li><a href=\"";
                // line 72
                echo $this->env->getExtension('routing')->getPath("typeconsistance");
                echo "\">Types de consistances</a></li>
                        <li><a href=\"";
                // line 73
                echo $this->env->getExtension('routing')->getPath("unitedemesure");
                echo "\">Unités de mesure</a></li>
                        <li><a href=\"";
                // line 74
                echo $this->env->getExtension('routing')->getPath("libelleavenant");
                echo "\">Libellés de marchés</a></li>
                        <li><a href=\"";
                // line 75
                echo $this->env->getExtension('routing')->getPath("bloc");
                echo "\">Bloc</a></li>
                        <li><a href=\"";
                // line 76
                echo $this->env->getExtension('routing')->getPath("region");
                echo "\">Région</a></li>
                        <li><a href=\"";
                // line 77
                echo $this->env->getExtension('routing')->getPath("axe");
                echo "\">Axe</a></li>
                    </ul>
                </li>
                <li>
                    <a href=\"";
                // line 81
                echo $this->env->getExtension('routing')->getPath("users");
                echo "\">Gérer les utlisateurs</a>
                </li>
            ";
            }
            // line 84
            echo "
            <li class=\"place-right\">
                <a href=\"#\" class=\"dropdown-toogle\">
                    ";
            // line 87
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("layout.logged_in_as", array("%username%" => twig_capitalize_string_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array()))), "FOSUserBundle"), "html", null, true);
            echo "
                </a>
                <ul class=\"d-menu align-left\" data-role=\"dropdown\">
                    <li><a href=\"";
            // line 90
            echo $this->env->getExtension('routing')->getPath("fos_user_profile_show");
            echo "\">Mon profil</a></li>
                    <li><a href=\"";
            // line 91
            echo $this->env->getExtension('routing')->getPath("fos_user_profile_edit");
            echo "\">Modifier mon profil</a></li>
                    <li><a href=\"";
            // line 92
            echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
            echo "\">Se déconnecter</a></li>
                </ul>
            </li>
        </ul>

        ";
        }
        // line 98
        echo "        <!--- Fin Menu --->

        ";
        // line 100
        $context["flashNumber"] = 1;
        // line 101
        echo "
        ";
        // line 102
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array())) > 0)) {
            // line 103
            echo "            <div>
                ";
            // line 104
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array(), "method"));
            foreach ($context['_seq'] as $context["key"] => $context["message"]) {
                // line 105
                echo "                    <br>
                    <div class=\"flash-";
                // line 106
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo " padding10\" id=\"flash-";
                echo twig_escape_filter($this->env, (isset($context["flashNumber"]) ? $context["flashNumber"] : $this->getContext($context, "flashNumber")), "html", null, true);
                echo "\">
                        <a href=\"#\" class=\"closeFlash\" flashId=\"flash-";
                // line 107
                echo twig_escape_filter($this->env, (isset($context["flashNumber"]) ? $context["flashNumber"] : $this->getContext($context, "flashNumber")), "html", null, true);
                echo "\">
                            <span class=\"place-right mif-cross fg-grayLight\"></span>
                        </a>
                        ";
                // line 110
                echo $this->getAttribute($context["message"], 0, array(), "array");
                echo "
                    </div>
                    ";
                // line 112
                $context["flashNumber"] = ((isset($context["flashNumber"]) ? $context["flashNumber"] : $this->getContext($context, "flashNumber")) + 1);
                // line 113
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 114
            echo "            </div>
            <br>
        ";
        }
        // line 117
        echo "        <div class=\"laiso-bordered bg-white padding10\">
            <ul class=\"breadcrumbs dark bg-darkCyan\">
                <li><a href=\"";
        // line 119
        echo $this->env->getExtension('routing')->getPath("laiso_arm_homepage");
        echo "\"><span class=\"icon mif-home\"></span> Accueil</a></li>
                ";
        // line 120
        $this->displayBlock('breadcumbs', $context, $blocks);
        // line 121
        echo "            </ul>
            <div>
                ";
        // line 123
        $this->displayBlock('body', $context, $blocks);
        // line 126
        echo "            </div>
            <span class=\"clear-float\"></span>
        </div>
    </div>

    <div class=\"bg-grayLighter grid laiso-bordered padding10 no-padding-bottom\">
        <div class=\"bg-white laiso-bordered\">
            ";
        // line 133
        $this->displayBlock('footer', $context, $blocks);
        // line 162
        echo "            <div data-role=\"dialog\" id=\"loader\" class=\"padding20\" data-overlay=\"true\" data-windows-style=\"true\"
                 data-overlay-color=\"op-dark\">
                <div class=\"align-center container\" id=\"loader-container\">
                    <div data-role=\"preloader\" data-type=\"metro\" data-style=\"color\"></div>
                    <h5>En cours de traitement ...</h5>
                </div>
            </div>

            <div data-role=\"dialog\" id=\"dialog\" class=\"padding20 no-padding-top\" data-close-button=\"true\"
                 data-windows-style=\"true\" data-overlay=\"true\" data-overlay-color=\"op-dark\" data-overlay-click-close=\"true\">
                <div class=\"container\" id=\"dialog-container\">

                </div>
            </div>

            <div data-role=\"dialog\" id=\"confirm\" class=\"padding20 no-padding-top\" data-close-button=\"true\"
                 data-windows-style=\"true\" data-overlay=\"true\" data-overlay-color=\"op-dark\" data-overlay-click-close=\"true\">
                <div class=\"container\" id=\"dialog-container\">
                    <h1>Vous êtes sur le point de valider les modifications. Confirmer?</h1>
                </div>
            </div>
        </div>
        <br>
        ";
        // line 185
        $this->displayBlock('javascripts', $context, $blocks);
        // line 195
        echo "    </div>
</div>
</body>
</html>
";
        
        $__internal_5fd54fa566a3f54079acda24cad628835fb7a7630fef95ce9437e8cbb73f93ec->leave($__internal_5fd54fa566a3f54079acda24cad628835fb7a7630fef95ce9437e8cbb73f93ec_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_377bd053aae9ab54645b58030f12378ae3a8e9db529e493ab4c8b3b2ae9e4129 = $this->env->getExtension("native_profiler");
        $__internal_377bd053aae9ab54645b58030f12378ae3a8e9db529e493ab4c8b3b2ae9e4129->enter($__internal_377bd053aae9ab54645b58030f12378ae3a8e9db529e493ab4c8b3b2ae9e4129_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_377bd053aae9ab54645b58030f12378ae3a8e9db529e493ab4c8b3b2ae9e4129->leave($__internal_377bd053aae9ab54645b58030f12378ae3a8e9db529e493ab4c8b3b2ae9e4129_prof);

    }

    // line 8
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_86db4213c3e3db0ec2b1d535dc4867a548110a2a933e07fdc2e90a564ae1ad84 = $this->env->getExtension("native_profiler");
        $__internal_86db4213c3e3db0ec2b1d535dc4867a548110a2a933e07fdc2e90a564ae1ad84->enter($__internal_86db4213c3e3db0ec2b1d535dc4867a548110a2a933e07fdc2e90a564ae1ad84_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 9
        echo "        <link rel=\"stylesheet\" media=\"all\" type=\"text/css\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/metro-ui/css/metro.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" media=\"all\" type=\"text/css\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/metro-ui/css/metro-responsive.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" media=\"all\" type=\"text/css\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/metro-ui/css/metro-rtl.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" media=\"all\" type=\"text/css\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/metro-ui/css/metro-schemes.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" media=\"all\" type=\"text/css\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/metro-ui/css/metro-icons.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" media=\"all\" type=\"text/css\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/styles/jquery.mCustomScrollbar.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" media=\"all\" type=\"text/css\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/styles/styles.css"), "html", null, true);
        echo "\">
    ";
        
        $__internal_86db4213c3e3db0ec2b1d535dc4867a548110a2a933e07fdc2e90a564ae1ad84->leave($__internal_86db4213c3e3db0ec2b1d535dc4867a548110a2a933e07fdc2e90a564ae1ad84_prof);

    }

    // line 120
    public function block_breadcumbs($context, array $blocks = array())
    {
        $__internal_d0cd8a3903a720f6857e99354fb8bdbab774cda3db730d1d5db5201e5f28e74d = $this->env->getExtension("native_profiler");
        $__internal_d0cd8a3903a720f6857e99354fb8bdbab774cda3db730d1d5db5201e5f28e74d->enter($__internal_d0cd8a3903a720f6857e99354fb8bdbab774cda3db730d1d5db5201e5f28e74d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "breadcumbs"));

        
        $__internal_d0cd8a3903a720f6857e99354fb8bdbab774cda3db730d1d5db5201e5f28e74d->leave($__internal_d0cd8a3903a720f6857e99354fb8bdbab774cda3db730d1d5db5201e5f28e74d_prof);

    }

    // line 123
    public function block_body($context, array $blocks = array())
    {
        $__internal_b4112cc16d9da4c2507e8a331ca57d01c3b462affafa38240c8df2bb577b40fd = $this->env->getExtension("native_profiler");
        $__internal_b4112cc16d9da4c2507e8a331ca57d01c3b462affafa38240c8df2bb577b40fd->enter($__internal_b4112cc16d9da4c2507e8a331ca57d01c3b462affafa38240c8df2bb577b40fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 124
        echo "
                ";
        
        $__internal_b4112cc16d9da4c2507e8a331ca57d01c3b462affafa38240c8df2bb577b40fd->leave($__internal_b4112cc16d9da4c2507e8a331ca57d01c3b462affafa38240c8df2bb577b40fd_prof);

    }

    // line 133
    public function block_footer($context, array $blocks = array())
    {
        $__internal_cbe3c6998b5f3d638181ab85d9786f45e4629ec9b3a180956d603cddb4267eb1 = $this->env->getExtension("native_profiler");
        $__internal_cbe3c6998b5f3d638181ab85d9786f45e4629ec9b3a180956d603cddb4267eb1->enter($__internal_cbe3c6998b5f3d638181ab85d9786f45e4629ec9b3a180956d603cddb4267eb1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 134
        echo "                <div class=\"align-center\">
                    <h6> &copy; ARM ";
        // line 135
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo "</h6>

                    <div class=\"align-center\">
                        <small>
                            ";
        // line 139
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 140
            echo "                                ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("layout.logged_in_as", array("%username%" => twig_capitalize_string_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array()))), "FOSUserBundle"), "html", null, true);
            echo "
                                ";
            // line 141
            echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "hasRole", array(0 => "ROLE_CI"), "method")) ? ((("(Bloc " . $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "bloc", array())) . ")")) : ("")), "html", null, true);
            echo "
                                <br>
                                Rôles:
                                ";
            // line 144
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "roles", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["role"]) {
                // line 145
                echo "                                    ";
                echo (((($context["role"] == "ROLE_ARM") || ($context["role"] == "ROLE_USER"))) ? ("") : ((("<span class=\"tag success\">" . $context["role"]) . "</span>")));
                echo "
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['role'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 147
            echo "                                <br>
                                <a href=\"";
            // line 148
            echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("layout.logout", array(), "FOSUserBundle"), "html", null, true);
            echo "</a>
                                |
                                <a href=\"";
            // line 150
            echo $this->env->getExtension('routing')->getPath("fos_user_profile_edit");
            echo "\">Modifier mon profil</a>
                                |
                                <a href=\"";
            // line 152
            echo $this->env->getExtension('routing')->getPath("fos_user_change_password");
            echo "\">Changer mon mot de passe</a>
                            ";
        } else {
            // line 154
            echo "                                <a class=\"button primary\"
                                   href=\"";
            // line 155
            echo $this->env->getExtension('routing')->getPath("fos_user_security_login");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("layout.login", array(), "FOSUserBundle"), "html", null, true);
            echo "</a>
                            ";
        }
        // line 157
        echo "                        </small>
                    </div>
                    <br>
                </div>
            ";
        
        $__internal_cbe3c6998b5f3d638181ab85d9786f45e4629ec9b3a180956d603cddb4267eb1->leave($__internal_cbe3c6998b5f3d638181ab85d9786f45e4629ec9b3a180956d603cddb4267eb1_prof);

    }

    // line 185
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_de4763a0aae60542654c6a33d188d2b1a8af7c94eefefc5b9170bb4e5a189537 = $this->env->getExtension("native_profiler");
        $__internal_de4763a0aae60542654c6a33d188d2b1a8af7c94eefefc5b9170bb4e5a189537->enter($__internal_de4763a0aae60542654c6a33d188d2b1a8af7c94eefefc5b9170bb4e5a189537_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 186
        echo "            <script language=\"JavaScript\" type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/scripts/jquery-2.1.3.min.js"), "html", null, true);
        echo "\"></script>
            <script language=\"JavaScript\" type=\"text/javascript\" src=\"";
        // line 187
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/scripts/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
            <script language=\"JavaScript\" type=\"text/javascript\" src=\"";
        // line 188
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/metro-ui/js/metro.min.js"), "html", null, true);
        echo "\"></script>
            <script language=\"JavaScript\" type=\"text/javascript\" src=\"";
        // line 189
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/scripts/select2.min.js"), "html", null, true);
        echo "\"></script>
            <script language=\"JavaScript\" type=\"text/javascript\" src=\"";
        // line 190
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/scripts/highcharts.js"), "html", null, true);
        echo "\"></script>
            <script language=\"JavaScript\" type=\"text/javascript\" src=\"";
        // line 191
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/scripts/jquery.mCustomScrollbar.min.js"), "html", null, true);
        echo "\"></script>
            <script language=\"JavaScript\" type=\"text/javascript\" src=\"";
        // line 192
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/scripts/functions.js"), "html", null, true);
        echo "\"></script>
            <script language=\"JavaScript\" type=\"text/javascript\" src=\"";
        // line 193
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/scripts/wizard.js"), "html", null, true);
        echo "\"></script>
        ";
        
        $__internal_de4763a0aae60542654c6a33d188d2b1a8af7c94eefefc5b9170bb4e5a189537->leave($__internal_de4763a0aae60542654c6a33d188d2b1a8af7c94eefefc5b9170bb4e5a189537_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  524 => 193,  520 => 192,  516 => 191,  512 => 190,  508 => 189,  504 => 188,  500 => 187,  495 => 186,  489 => 185,  478 => 157,  471 => 155,  468 => 154,  463 => 152,  458 => 150,  451 => 148,  448 => 147,  439 => 145,  435 => 144,  429 => 141,  424 => 140,  422 => 139,  415 => 135,  412 => 134,  406 => 133,  398 => 124,  392 => 123,  381 => 120,  372 => 15,  368 => 14,  364 => 13,  360 => 12,  356 => 11,  352 => 10,  347 => 9,  341 => 8,  330 => 5,  319 => 195,  317 => 185,  292 => 162,  290 => 133,  281 => 126,  279 => 123,  275 => 121,  273 => 120,  269 => 119,  265 => 117,  260 => 114,  254 => 113,  252 => 112,  247 => 110,  241 => 107,  235 => 106,  232 => 105,  228 => 104,  225 => 103,  223 => 102,  220 => 101,  218 => 100,  214 => 98,  205 => 92,  201 => 91,  197 => 90,  191 => 87,  186 => 84,  180 => 81,  173 => 77,  169 => 76,  165 => 75,  161 => 74,  157 => 73,  153 => 72,  148 => 69,  146 => 68,  143 => 67,  136 => 63,  132 => 62,  127 => 59,  125 => 58,  122 => 57,  116 => 55,  114 => 54,  111 => 53,  105 => 51,  103 => 50,  100 => 49,  93 => 45,  87 => 42,  83 => 41,  75 => 35,  73 => 34,  69 => 33,  66 => 32,  64 => 31,  51 => 21,  45 => 17,  43 => 8,  39 => 7,  34 => 5,  28 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/* <head>*/
/*     <meta charset="UTF-8"/>*/
/*     <title>{% block title %}{% endblock %} | ARM::SGPEC</title>*/
/* */
/*     <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}"/>*/
/*     {% block stylesheets %}*/
/*         <link rel="stylesheet" media="all" type="text/css" href="{{ asset('public/metro-ui/css/metro.min.css') }}">*/
/*         <link rel="stylesheet" media="all" type="text/css" href="{{ asset('public/metro-ui/css/metro-responsive.min.css') }}">*/
/*         <link rel="stylesheet" media="all" type="text/css" href="{{ asset('public/metro-ui/css/metro-rtl.min.css') }}">*/
/*         <link rel="stylesheet" media="all" type="text/css" href="{{ asset('public/metro-ui/css/metro-schemes.min.css') }}">*/
/*         <link rel="stylesheet" media="all" type="text/css" href="{{ asset('public/metro-ui/css/metro-icons.min.css') }}">*/
/*         <link rel="stylesheet" media="all" type="text/css" href="{{ asset('public/styles/jquery.mCustomScrollbar.min.css') }}">*/
/*         <link rel="stylesheet" media="all" type="text/css" href="{{ asset('public/styles/styles.css') }}">*/
/*     {% endblock %}*/
/* </head>*/
/* <body> <!--class="scroll-y"--->*/
/* <div class="container-fluid margin10">*/
/*     <div class="bg-grayLighter padding10 laiso-bordered margin10 no-margin-left no-margin-right no-margin-bottom">*/
/*         <h1><a href="{{ path('laiso_arm_homepage') }}" class="fg-black">ARM::SGPEC</a>*/
/*             <small> SYSTEME DE GESTION DE PROJET D'ENTRETIENS COURANTS DES ROUTES</small>*/
/*         </h1>*/
/*     </div>*/
/*     <br>*/
/* */
/*     <div class="bg-grayLighter padding10 laiso-bordered">*/
/* */
/*         <!-- MENU -->*/
/* */
/*         {% if is_granted("IS_AUTHENTICATED_REMEMBERED") %}*/
/*         <ul class="h-menu laiso-bordered bg-white">*/
/*             <li><a href="{{ path('laiso_arm_homepage') }}">Accueil</a></li>*/
/*             {% if app.user.hasRole('ROLE_DTEC') or app.user.hasRole('ROLE_CI') %}*/
/*                 <li>*/
/*                     <a href="#" class="dropdown-toggle">Gestion des DAO</a>*/
/*                     <ul class="d-menu" data-role="dropdown">*/
/*                         <li>*/
/*                             <a href="#" class="dropdown-toggle">Prix</a>*/
/*                             <ul class="d-menu" data-role="dropdown">*/
/*                                 <li><a href="{{ path('prix_categorie') }}">Catégorie</a></li>*/
/*                                 <li><a href="{{ path('prix_serie') }}">Série de prix</a></li>*/
/*                             </ul>*/
/*                         </li>*/
/*                         <li><a href="{{ path('dao') }}">DAO</a></li>*/
/*                     </ul>*/
/*                 </li>*/
/*             {% endif %}*/
/* */
/*             {% if app.user.hasRole('ROLE_DTEC') or app.user.hasRole('ROLE_CI') %}*/
/*                 <li><a href="{{ path('marche') }}">Gestion des marchés</a></li>*/
/*             {% endif %}*/
/* */
/*             {% if app.user.hasRole('ROLE_DTEC') or app.user.hasRole('ROLE_CI') %}*/
/*                 <li><a href="{{ path('entreprise') }}">Entreprises</a></li>*/
/*             {% endif %}*/
/* */
/*             {% if app.user.hasRole('ROLE_DTEC') %}*/
/*                 <li>*/
/*                     <a href="#" class="dropdown-toggle">Administration</a>*/
/*                     <ul class="d-menu" data-role="dropdown">*/
/*                         <li><a href="{{ path('dashboard_index') }}">Tableau de bord</a></li>*/
/*                         <li><a href="{{ path('campagne') }}">Campagne</a></li>*/
/*                     </ul>*/
/*                 </li>*/
/*             {% endif %}*/
/* */
/*             {% if app.user.hasRole('ROLE_ADMIN') %}*/
/*                 <li>*/
/*                     <a href="#" class="dropdown-toggle">Paramétrage</a>*/
/*                     <ul class="d-menu" data-role="dropdown">*/
/*                         <li><a href="{{ path('typeconsistance') }}">Types de consistances</a></li>*/
/*                         <li><a href="{{ path('unitedemesure') }}">Unités de mesure</a></li>*/
/*                         <li><a href="{{ path('libelleavenant') }}">Libellés de marchés</a></li>*/
/*                         <li><a href="{{ path('bloc') }}">Bloc</a></li>*/
/*                         <li><a href="{{ path('region') }}">Région</a></li>*/
/*                         <li><a href="{{ path('axe') }}">Axe</a></li>*/
/*                     </ul>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="{{ path('users') }}">Gérer les utlisateurs</a>*/
/*                 </li>*/
/*             {% endif %}*/
/* */
/*             <li class="place-right">*/
/*                 <a href="#" class="dropdown-toogle">*/
/*                     {{ 'layout.logged_in_as'|trans({'%username%': app.user.username | capitalize}, 'FOSUserBundle') }}*/
/*                 </a>*/
/*                 <ul class="d-menu align-left" data-role="dropdown">*/
/*                     <li><a href="{{ path('fos_user_profile_show') }}">Mon profil</a></li>*/
/*                     <li><a href="{{ path('fos_user_profile_edit') }}">Modifier mon profil</a></li>*/
/*                     <li><a href="{{ path('fos_user_security_logout') }}">Se déconnecter</a></li>*/
/*                 </ul>*/
/*             </li>*/
/*         </ul>*/
/* */
/*         {% endif %}*/
/*         <!--- Fin Menu --->*/
/* */
/*         {% set flashNumber = 1 %}*/
/* */
/*         {% if app.session.flashbag | length > 0 %}*/
/*             <div>*/
/*                 {% for key, message in app.session.flashbag() %}*/
/*                     <br>*/
/*                     <div class="flash-{{ key }} padding10" id="flash-{{ flashNumber }}">*/
/*                         <a href="#" class="closeFlash" flashId="flash-{{ flashNumber }}">*/
/*                             <span class="place-right mif-cross fg-grayLight"></span>*/
/*                         </a>*/
/*                         {{ message[0] | raw }}*/
/*                     </div>*/
/*                     {% set flashNumber = flashNumber + 1 %}*/
/*                 {% endfor %}*/
/*             </div>*/
/*             <br>*/
/*         {% endif %}*/
/*         <div class="laiso-bordered bg-white padding10">*/
/*             <ul class="breadcrumbs dark bg-darkCyan">*/
/*                 <li><a href="{{ path('laiso_arm_homepage') }}"><span class="icon mif-home"></span> Accueil</a></li>*/
/*                 {% block breadcumbs %}{% endblock %}*/
/*             </ul>*/
/*             <div>*/
/*                 {% block body %}*/
/* */
/*                 {% endblock %}*/
/*             </div>*/
/*             <span class="clear-float"></span>*/
/*         </div>*/
/*     </div>*/
/* */
/*     <div class="bg-grayLighter grid laiso-bordered padding10 no-padding-bottom">*/
/*         <div class="bg-white laiso-bordered">*/
/*             {% block footer %}*/
/*                 <div class="align-center">*/
/*                     <h6> &copy; ARM {{ "now"|date('Y') }}</h6>*/
/* */
/*                     <div class="align-center">*/
/*                         <small>*/
/*                             {% if is_granted("IS_AUTHENTICATED_REMEMBERED") %}*/
/*                                 {{ 'layout.logged_in_as'|trans({'%username%': app.user.username | capitalize}, 'FOSUserBundle') }}*/
/*                                 {{ app.user.hasRole('ROLE_CI') ? '(Bloc ' ~ app.user.bloc ~')' : '' }}*/
/*                                 <br>*/
/*                                 Rôles:*/
/*                                 {% for role in app.user.roles %}*/
/*                                     {{ (role == 'ROLE_ARM' or role == 'ROLE_USER') ? '' : ('<span class="tag success">' ~ role ~'</span>') | raw }}*/
/*                                 {% endfor %}*/
/*                                 <br>*/
/*                                 <a href="{{ path('fos_user_security_logout') }}">{{ 'layout.logout'|trans({}, 'FOSUserBundle') }}</a>*/
/*                                 |*/
/*                                 <a href="{{ path('fos_user_profile_edit') }}">Modifier mon profil</a>*/
/*                                 |*/
/*                                 <a href="{{ path('fos_user_change_password') }}">Changer mon mot de passe</a>*/
/*                             {% else %}*/
/*                                 <a class="button primary"*/
/*                                    href="{{ path('fos_user_security_login') }}">{{ 'layout.login'|trans({}, 'FOSUserBundle') }}</a>*/
/*                             {% endif %}*/
/*                         </small>*/
/*                     </div>*/
/*                     <br>*/
/*                 </div>*/
/*             {% endblock %}*/
/*             <div data-role="dialog" id="loader" class="padding20" data-overlay="true" data-windows-style="true"*/
/*                  data-overlay-color="op-dark">*/
/*                 <div class="align-center container" id="loader-container">*/
/*                     <div data-role="preloader" data-type="metro" data-style="color"></div>*/
/*                     <h5>En cours de traitement ...</h5>*/
/*                 </div>*/
/*             </div>*/
/* */
/*             <div data-role="dialog" id="dialog" class="padding20 no-padding-top" data-close-button="true"*/
/*                  data-windows-style="true" data-overlay="true" data-overlay-color="op-dark" data-overlay-click-close="true">*/
/*                 <div class="container" id="dialog-container">*/
/* */
/*                 </div>*/
/*             </div>*/
/* */
/*             <div data-role="dialog" id="confirm" class="padding20 no-padding-top" data-close-button="true"*/
/*                  data-windows-style="true" data-overlay="true" data-overlay-color="op-dark" data-overlay-click-close="true">*/
/*                 <div class="container" id="dialog-container">*/
/*                     <h1>Vous êtes sur le point de valider les modifications. Confirmer?</h1>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         <br>*/
/*         {% block javascripts %}*/
/*             <script language="JavaScript" type="text/javascript" src="{{ asset('public/scripts/jquery-2.1.3.min.js') }}"></script>*/
/*             <script language="JavaScript" type="text/javascript" src="{{ asset('public/scripts/jquery.dataTables.min.js') }}"></script>*/
/*             <script language="JavaScript" type="text/javascript" src="{{ asset('public/metro-ui/js/metro.min.js') }}"></script>*/
/*             <script language="JavaScript" type="text/javascript" src="{{ asset('public/scripts/select2.min.js') }}"></script>*/
/*             <script language="JavaScript" type="text/javascript" src="{{ asset('public/scripts/highcharts.js') }}"></script>*/
/*             <script language="JavaScript" type="text/javascript" src="{{ asset('public/scripts/jquery.mCustomScrollbar.min.js') }}"></script>*/
/*             <script language="JavaScript" type="text/javascript" src="{{ asset('public/scripts/functions.js') }}"></script>*/
/*             <script language="JavaScript" type="text/javascript" src="{{ asset('public/scripts/wizard.js') }}"></script>*/
/*         {% endblock %}*/
/*     </div>*/
/* </div>*/
/* </body>*/
/* </html>*/
/* */
