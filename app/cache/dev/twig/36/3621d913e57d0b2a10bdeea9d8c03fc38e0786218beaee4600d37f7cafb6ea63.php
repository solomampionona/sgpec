<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_74652eccd6a2c58d2b70b09dee39c2df99b7dd99522802e8f7d6861c4fb48513 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b5a1b6efb521c10298e75935ff65dbf1d5a931f13f8999377acd7ebca70ab9da = $this->env->getExtension("native_profiler");
        $__internal_b5a1b6efb521c10298e75935ff65dbf1d5a931f13f8999377acd7ebca70ab9da->enter($__internal_b5a1b6efb521c10298e75935ff65dbf1d5a931f13f8999377acd7ebca70ab9da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b5a1b6efb521c10298e75935ff65dbf1d5a931f13f8999377acd7ebca70ab9da->leave($__internal_b5a1b6efb521c10298e75935ff65dbf1d5a931f13f8999377acd7ebca70ab9da_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_e346716c8c6f40393dbbbff8afcf3c37ef566eaf8b28affc87722b8e7a731182 = $this->env->getExtension("native_profiler");
        $__internal_e346716c8c6f40393dbbbff8afcf3c37ef566eaf8b28affc87722b8e7a731182->enter($__internal_e346716c8c6f40393dbbbff8afcf3c37ef566eaf8b28affc87722b8e7a731182_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_e346716c8c6f40393dbbbff8afcf3c37ef566eaf8b28affc87722b8e7a731182->leave($__internal_e346716c8c6f40393dbbbff8afcf3c37ef566eaf8b28affc87722b8e7a731182_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_e09562ec56cb9045830c58c9a1f80713296d4cae70383b3109b843ddb346df7c = $this->env->getExtension("native_profiler");
        $__internal_e09562ec56cb9045830c58c9a1f80713296d4cae70383b3109b843ddb346df7c->enter($__internal_e09562ec56cb9045830c58c9a1f80713296d4cae70383b3109b843ddb346df7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_e09562ec56cb9045830c58c9a1f80713296d4cae70383b3109b843ddb346df7c->leave($__internal_e09562ec56cb9045830c58c9a1f80713296d4cae70383b3109b843ddb346df7c_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_5ff2e4218b2e6348ea36d9624283fb768c18bee0a0b87ae089949de7fe6a2525 = $this->env->getExtension("native_profiler");
        $__internal_5ff2e4218b2e6348ea36d9624283fb768c18bee0a0b87ae089949de7fe6a2525->enter($__internal_5ff2e4218b2e6348ea36d9624283fb768c18bee0a0b87ae089949de7fe6a2525_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("TwigBundle:Exception:exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_5ff2e4218b2e6348ea36d9624283fb768c18bee0a0b87ae089949de7fe6a2525->leave($__internal_5ff2e4218b2e6348ea36d9624283fb768c18bee0a0b87ae089949de7fe6a2525_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'TwigBundle::layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include 'TwigBundle:Exception:exception.html.twig' %}*/
/* {% endblock %}*/
/* */
