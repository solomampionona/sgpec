<?php

/* LaisoArmBundle:DQE/dao:dqe_dao.html.twig */
class __TwigTemplate_95bd747a5e2d8e87c836cc830682d8283eae0f41b5be5f5eccd2f7bba26b62bf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_93b885cd46b4e1b668d4dde06e1e796c599e23e32688cc71ff8f4267f65710a2 = $this->env->getExtension("native_profiler");
        $__internal_93b885cd46b4e1b668d4dde06e1e796c599e23e32688cc71ff8f4267f65710a2->enter($__internal_93b885cd46b4e1b668d4dde06e1e796c599e23e32688cc71ff8f4267f65710a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "LaisoArmBundle:DQE/dao:dqe_dao.html.twig"));

        // line 1
        if ((($this->getAttribute((isset($context["lotissement"]) ? $context["lotissement"] : $this->getContext($context, "lotissement")), "valide", array()) || $this->getAttribute((isset($context["lotissement"]) ? $context["lotissement"] : $this->getContext($context, "lotissement")), "puValide", array())) || $this->getAttribute((isset($context["avenant"]) ? $context["avenant"] : $this->getContext($context, "avenant")), "valide", array()))) {
            // line 2
            echo "    ";
            // line 8
            echo "

    <div class=\"align-center  margin10 no-margin-top no-margin-right no-margin-left\">
        <div class=\"bg-lightGreen padding5\">
            <h3>";
            // line 12
            echo twig_escape_filter($this->env, twig_upper_filter($this->env, "Détail quantitatif et Estimatif"), "html", null, true);
            echo "</h3>
        </div>
    </div>

    ";
            // line 16
            $this->loadTemplate("LaisoArmBundle:DQE/dao:lotissement_valide.html.twig", "LaisoArmBundle:DQE/dao:dqe_dao.html.twig", 16)->display($context);
            // line 17
            echo "    <small class=\"place-right fg-red\">Vous ne pouvez plus apporter de modification.</small>
";
        } else {
            // line 19
            echo "    ";
            // line 24
            echo "
    <div class=\"align-center  margin10 no-margin-top no-margin-right no-margin-left\">
        <div class=\"bg-grayLight fg-white padding5\">
            <h3>";
            // line 27
            echo twig_escape_filter($this->env, twig_upper_filter($this->env, "Détail quantitatif et Estimatif"), "html", null, true);
            echo "</h3>
        </div>
    </div>

    ";
            // line 31
            $this->loadTemplate("LaisoArmBundle:DQE/dao:default.html.twig", "LaisoArmBundle:DQE/dao:dqe_dao.html.twig", 31)->display($context);
        }
        
        $__internal_93b885cd46b4e1b668d4dde06e1e796c599e23e32688cc71ff8f4267f65710a2->leave($__internal_93b885cd46b4e1b668d4dde06e1e796c599e23e32688cc71ff8f4267f65710a2_prof);

    }

    public function getTemplateName()
    {
        return "LaisoArmBundle:DQE/dao:dqe_dao.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 31,  52 => 27,  47 => 24,  45 => 19,  41 => 17,  39 => 16,  32 => 12,  26 => 8,  24 => 2,  22 => 1,);
    }
}
/* {% if lotissement.valide or lotissement.puValide or avenant.valide %}*/
/*     {#*/
/*         Aucune modifification autorisée sur les quantités initiales.*/
/*             * lotissement.valide: marché*/
/*             * lotissement.PuValide: prix unitaire fixé*/
/*             * avenant.valide : quantités fixées.*/
/*     #}*/
/* */
/* */
/*     <div class="align-center  margin10 no-margin-top no-margin-right no-margin-left">*/
/*         <div class="bg-lightGreen padding5">*/
/*             <h3>{{ 'Détail quantitatif et Estimatif' |upper }}</h3>*/
/*         </div>*/
/*     </div>*/
/* */
/*     {% include ('LaisoArmBundle:DQE/dao:lotissement_valide.html.twig') %}*/
/*     <small class="place-right fg-red">Vous ne pouvez plus apporter de modification.</small>*/
/* {% else %}*/
/*     {#*/
/*         Affichage par défaut d'un DQE, toute modification est autorisée: ajout ou*/
/*         suppression de ligne, de consistance, de localisation modification de*/
/*         quantité, validation des quantités saisies.*/
/*     #}*/
/* */
/*     <div class="align-center  margin10 no-margin-top no-margin-right no-margin-left">*/
/*         <div class="bg-grayLight fg-white padding5">*/
/*             <h3>{{ 'Détail quantitatif et Estimatif' |upper }}</h3>*/
/*         </div>*/
/*     </div>*/
/* */
/*     {% include ('LaisoArmBundle:DQE/dao:default.html.twig') %}*/
/* {% endif %}*/
/* */
