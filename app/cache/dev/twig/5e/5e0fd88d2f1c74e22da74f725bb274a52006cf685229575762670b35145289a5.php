<?php

/* FOSUserBundle:Profile:show.html.twig */
class __TwigTemplate_2c98a9a8ea9f716dbd5172c9b2ca01f7680e98cbd8dd97ed239356e87a20e2bb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Profile:show.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'breadcumbs' => array($this, 'block_breadcumbs'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_84ed6ae7c4000ebebaf66146d291ceedd0c2cc6076fd05be82d2570427044c14 = $this->env->getExtension("native_profiler");
        $__internal_84ed6ae7c4000ebebaf66146d291ceedd0c2cc6076fd05be82d2570427044c14->enter($__internal_84ed6ae7c4000ebebaf66146d291ceedd0c2cc6076fd05be82d2570427044c14_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_84ed6ae7c4000ebebaf66146d291ceedd0c2cc6076fd05be82d2570427044c14->leave($__internal_84ed6ae7c4000ebebaf66146d291ceedd0c2cc6076fd05be82d2570427044c14_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_fa6ca50b74934838e6bb97e18b96eace67c0d0978dd8a64b76583bf3559d6e3e = $this->env->getExtension("native_profiler");
        $__internal_fa6ca50b74934838e6bb97e18b96eace67c0d0978dd8a64b76583bf3559d6e3e->enter($__internal_fa6ca50b74934838e6bb97e18b96eace67c0d0978dd8a64b76583bf3559d6e3e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "nom", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "prenom", array()), "html", null, true);
        echo " ";
        
        $__internal_fa6ca50b74934838e6bb97e18b96eace67c0d0978dd8a64b76583bf3559d6e3e->leave($__internal_fa6ca50b74934838e6bb97e18b96eace67c0d0978dd8a64b76583bf3559d6e3e_prof);

    }

    // line 5
    public function block_breadcumbs($context, array $blocks = array())
    {
        $__internal_d6c3eab6e948a1482c3d4059697fca91aa93d04190788c4fa1f69d359d10fbdd = $this->env->getExtension("native_profiler");
        $__internal_d6c3eab6e948a1482c3d4059697fca91aa93d04190788c4fa1f69d359d10fbdd->enter($__internal_d6c3eab6e948a1482c3d4059697fca91aa93d04190788c4fa1f69d359d10fbdd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "breadcumbs"));

        // line 6
        echo "    <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("fos_user_profile_show");
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "nom", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "prenom", array()), "html", null, true);
        echo "</a></li>
";
        
        $__internal_d6c3eab6e948a1482c3d4059697fca91aa93d04190788c4fa1f69d359d10fbdd->leave($__internal_d6c3eab6e948a1482c3d4059697fca91aa93d04190788c4fa1f69d359d10fbdd_prof);

    }

    // line 8
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_48807a4ebd69f1b2d01b60419550d62d28fbc9ed6793753f06f1bf4c2bdf14a3 = $this->env->getExtension("native_profiler");
        $__internal_48807a4ebd69f1b2d01b60419550d62d28fbc9ed6793753f06f1bf4c2bdf14a3->enter($__internal_48807a4ebd69f1b2d01b60419550d62d28fbc9ed6793753f06f1bf4c2bdf14a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 9
        $this->loadTemplate("FOSUserBundle:Profile:show_content.html.twig", "FOSUserBundle:Profile:show.html.twig", 9)->display($context);
        
        $__internal_48807a4ebd69f1b2d01b60419550d62d28fbc9ed6793753f06f1bf4c2bdf14a3->leave($__internal_48807a4ebd69f1b2d01b60419550d62d28fbc9ed6793753f06f1bf4c2bdf14a3_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 9,  72 => 8,  58 => 6,  52 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block title %} {{ app.user.nom }} {{ app.user.prenom }} {% endblock %}*/
/* */
/* {% block breadcumbs %}*/
/*     <li><a href="{{ path('fos_user_profile_show') }}">{{ app.user.nom }} {{ app.user.prenom }}</a></li>*/
/* {% endblock %}*/
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Profile:show_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
