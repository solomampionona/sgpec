<?php

/* ::base.html.twig */
class __TwigTemplate_d2ceb14981f3c7dc9a31acc84226313070962048fb2abfa75460c7957826ff0b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'breadcumbs' => array($this, 'block_breadcumbs'),
            'body' => array($this, 'block_body'),
            'footer' => array($this, 'block_footer'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_262ce4860b30a10f2067f87fa5432dfe8c148f08b0097f933bcc60599e09cf6e = $this->env->getExtension("native_profiler");
        $__internal_262ce4860b30a10f2067f87fa5432dfe8c148f08b0097f933bcc60599e09cf6e->enter($__internal_262ce4860b30a10f2067f87fa5432dfe8c148f08b0097f933bcc60599e09cf6e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\"/>
    <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo " | ARM::SGPEC</title>

    <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\"/>
    ";
        // line 8
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 17
        echo "</head>
<body> <!--class=\"scroll-y\"--->
<div class=\"container-fluid margin10\">
    <div class=\"bg-grayLighter padding10 laiso-bordered margin10 no-margin-left no-margin-right no-margin-bottom\">
        <h1><a href=\"";
        // line 21
        echo $this->env->getExtension('routing')->getPath("laiso_arm_homepage");
        echo "\" class=\"fg-black\">ARM::SGPEC</a>
            <small> SYSTEME DE GESTION DE PROJET D'ENTRETIENS COURANTS DES ROUTES</small>
        </h1>
    </div>
    <br>

    <div class=\"bg-grayLighter padding10 laiso-bordered\">

        <!-- MENU -->

        ";
        // line 31
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 32
            echo "        <ul class=\"h-menu laiso-bordered bg-white\">
            <li><a href=\"";
            // line 33
            echo $this->env->getExtension('routing')->getPath("laiso_arm_homepage");
            echo "\">Accueil</a></li>
            ";
            // line 34
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "hasRole", array(0 => "ROLE_DTEC"), "method") || $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "hasRole", array(0 => "ROLE_CI"), "method"))) {
                // line 35
                echo "                <li>
                    <a href=\"#\" class=\"dropdown-toggle\">Gestion des DAO</a>
                    <ul class=\"d-menu\" data-role=\"dropdown\">
                        <li>
                            <a href=\"#\" class=\"dropdown-toggle\">Prix</a>
                            <ul class=\"d-menu\" data-role=\"dropdown\">
                                <li><a href=\"";
                // line 41
                echo $this->env->getExtension('routing')->getPath("prix_categorie");
                echo "\">Catégorie</a></li>
                                <li><a href=\"";
                // line 42
                echo $this->env->getExtension('routing')->getPath("prix_serie");
                echo "\">Série de prix</a></li>
                            </ul>
                        </li>
                        <li><a href=\"";
                // line 45
                echo $this->env->getExtension('routing')->getPath("dao");
                echo "\">DAO</a></li>
                    </ul>
                </li>
            ";
            }
            // line 49
            echo "
            ";
            // line 50
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "hasRole", array(0 => "ROLE_DTEC"), "method") || $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "hasRole", array(0 => "ROLE_CI"), "method"))) {
                // line 51
                echo "                <li><a href=\"";
                echo $this->env->getExtension('routing')->getPath("marche");
                echo "\">Gestion des marchés</a></li>
            ";
            }
            // line 53
            echo "
            ";
            // line 54
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "hasRole", array(0 => "ROLE_DTEC"), "method") || $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "hasRole", array(0 => "ROLE_CI"), "method"))) {
                // line 55
                echo "                <li><a href=\"";
                echo $this->env->getExtension('routing')->getPath("entreprise");
                echo "\">Entreprises</a></li>
            ";
            }
            // line 57
            echo "
            ";
            // line 58
            if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "hasRole", array(0 => "ROLE_DTEC"), "method")) {
                // line 59
                echo "                <li>
                    <a href=\"#\" class=\"dropdown-toggle\">Administration</a>
                    <ul class=\"d-menu\" data-role=\"dropdown\">
                        <li><a href=\"";
                // line 62
                echo $this->env->getExtension('routing')->getPath("dashboard_index");
                echo "\">Tableau de bord</a></li>
                        <li><a href=\"";
                // line 63
                echo $this->env->getExtension('routing')->getPath("campagne");
                echo "\">Campagne</a></li>
                    </ul>
                </li>
            ";
            }
            // line 67
            echo "
            ";
            // line 68
            if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "hasRole", array(0 => "ROLE_ADMIN"), "method")) {
                // line 69
                echo "                <li>
                    <a href=\"#\" class=\"dropdown-toggle\">Paramétrage</a>
                    <ul class=\"d-menu\" data-role=\"dropdown\">
                        <li><a href=\"";
                // line 72
                echo $this->env->getExtension('routing')->getPath("typeconsistance");
                echo "\">Types de consistances</a></li>
                        <li><a href=\"";
                // line 73
                echo $this->env->getExtension('routing')->getPath("unitedemesure");
                echo "\">Unités de mesure</a></li>
                        <li><a href=\"";
                // line 74
                echo $this->env->getExtension('routing')->getPath("libelleavenant");
                echo "\">Libellés de marchés</a></li>
                        <li><a href=\"";
                // line 75
                echo $this->env->getExtension('routing')->getPath("bloc");
                echo "\">Bloc</a></li>
                        <li><a href=\"";
                // line 76
                echo $this->env->getExtension('routing')->getPath("region");
                echo "\">Région</a></li>
                        <li><a href=\"";
                // line 77
                echo $this->env->getExtension('routing')->getPath("axe");
                echo "\">Axe</a></li>
                    </ul>
                </li>
                <li>
                    <a href=\"";
                // line 81
                echo $this->env->getExtension('routing')->getPath("users");
                echo "\">Gérer les utlisateurs</a>
                </li>
            ";
            }
            // line 84
            echo "
            <li class=\"place-right\">
                <a href=\"#\" class=\"dropdown-toogle\">
                    ";
            // line 87
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("layout.logged_in_as", array("%username%" => twig_capitalize_string_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array()))), "FOSUserBundle"), "html", null, true);
            echo "
                </a>
                <ul class=\"d-menu align-left\" data-role=\"dropdown\">
                    <li><a href=\"";
            // line 90
            echo $this->env->getExtension('routing')->getPath("fos_user_profile_show");
            echo "\">Mon profil</a></li>
                    <li><a href=\"";
            // line 91
            echo $this->env->getExtension('routing')->getPath("fos_user_profile_edit");
            echo "\">Modifier mon profil</a></li>
                    <li><a href=\"";
            // line 92
            echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
            echo "\">Se déconnecter</a></li>
                </ul>
            </li>
        </ul>

        ";
        }
        // line 98
        echo "        <!--- Fin Menu --->

        ";
        // line 100
        $context["flashNumber"] = 1;
        // line 101
        echo "
        ";
        // line 102
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array())) > 0)) {
            // line 103
            echo "            <div>
                ";
            // line 104
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array(), "method"));
            foreach ($context['_seq'] as $context["key"] => $context["message"]) {
                // line 105
                echo "                    <br>
                    <div class=\"flash-";
                // line 106
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo " padding10\" id=\"flash-";
                echo twig_escape_filter($this->env, (isset($context["flashNumber"]) ? $context["flashNumber"] : $this->getContext($context, "flashNumber")), "html", null, true);
                echo "\">
                        <a href=\"#\" class=\"closeFlash\" flashId=\"flash-";
                // line 107
                echo twig_escape_filter($this->env, (isset($context["flashNumber"]) ? $context["flashNumber"] : $this->getContext($context, "flashNumber")), "html", null, true);
                echo "\">
                            <span class=\"place-right mif-cross fg-grayLight\"></span>
                        </a>
                        ";
                // line 110
                echo $this->getAttribute($context["message"], 0, array(), "array");
                echo "
                    </div>
                    ";
                // line 112
                $context["flashNumber"] = ((isset($context["flashNumber"]) ? $context["flashNumber"] : $this->getContext($context, "flashNumber")) + 1);
                // line 113
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 114
            echo "            </div>
            <br>
        ";
        }
        // line 117
        echo "        <div class=\"laiso-bordered bg-white padding10\">
            <ul class=\"breadcrumbs dark bg-darkCyan\">
                <li><a href=\"";
        // line 119
        echo $this->env->getExtension('routing')->getPath("laiso_arm_homepage");
        echo "\"><span class=\"icon mif-home\"></span> Accueil</a></li>
                ";
        // line 120
        $this->displayBlock('breadcumbs', $context, $blocks);
        // line 121
        echo "            </ul>
            <div>
                ";
        // line 123
        $this->displayBlock('body', $context, $blocks);
        // line 126
        echo "            </div>
            <span class=\"clear-float\"></span>
        </div>
    </div>

    <div class=\"bg-grayLighter grid laiso-bordered padding10 no-padding-bottom\">
        <div class=\"bg-white laiso-bordered\">
            ";
        // line 133
        $this->displayBlock('footer', $context, $blocks);
        // line 162
        echo "            <div data-role=\"dialog\" id=\"loader\" class=\"padding20\" data-overlay=\"true\" data-windows-style=\"true\"
                 data-overlay-color=\"op-dark\">
                <div class=\"align-center container\" id=\"loader-container\">
                    <div data-role=\"preloader\" data-type=\"metro\" data-style=\"color\"></div>
                    <h5>En cours de traitement ...</h5>
                </div>
            </div>

            <div data-role=\"dialog\" id=\"dialog\" class=\"padding20 no-padding-top\" data-close-button=\"true\"
                 data-windows-style=\"true\" data-overlay=\"true\" data-overlay-color=\"op-dark\" data-overlay-click-close=\"true\">
                <div class=\"container\" id=\"dialog-container\">

                </div>
            </div>

            <div data-role=\"dialog\" id=\"confirm\" class=\"padding20 no-padding-top\" data-close-button=\"true\"
                 data-windows-style=\"true\" data-overlay=\"true\" data-overlay-color=\"op-dark\" data-overlay-click-close=\"true\">
                <div class=\"container\" id=\"dialog-container\">
                    <h1>Vous êtes sur le point de valider les modifications. Confirmer?</h1>
                </div>
            </div>
        </div>
        <br>
        ";
        // line 185
        $this->displayBlock('javascripts', $context, $blocks);
        // line 195
        echo "    </div>
</div>
</body>
</html>
";
        
        $__internal_262ce4860b30a10f2067f87fa5432dfe8c148f08b0097f933bcc60599e09cf6e->leave($__internal_262ce4860b30a10f2067f87fa5432dfe8c148f08b0097f933bcc60599e09cf6e_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_3b381a77bc33b1121b002585b4f6c9c098e3487dc315f63afe508022e4d72cc2 = $this->env->getExtension("native_profiler");
        $__internal_3b381a77bc33b1121b002585b4f6c9c098e3487dc315f63afe508022e4d72cc2->enter($__internal_3b381a77bc33b1121b002585b4f6c9c098e3487dc315f63afe508022e4d72cc2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_3b381a77bc33b1121b002585b4f6c9c098e3487dc315f63afe508022e4d72cc2->leave($__internal_3b381a77bc33b1121b002585b4f6c9c098e3487dc315f63afe508022e4d72cc2_prof);

    }

    // line 8
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_6721d278dd694d0c4c5a614b6da5a53a7dfce8097346ee461c8d7a6f0ed258d0 = $this->env->getExtension("native_profiler");
        $__internal_6721d278dd694d0c4c5a614b6da5a53a7dfce8097346ee461c8d7a6f0ed258d0->enter($__internal_6721d278dd694d0c4c5a614b6da5a53a7dfce8097346ee461c8d7a6f0ed258d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 9
        echo "        <link rel=\"stylesheet\" media=\"all\" type=\"text/css\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/metro-ui/css/metro.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" media=\"all\" type=\"text/css\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/metro-ui/css/metro-responsive.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" media=\"all\" type=\"text/css\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/metro-ui/css/metro-rtl.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" media=\"all\" type=\"text/css\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/metro-ui/css/metro-schemes.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" media=\"all\" type=\"text/css\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/metro-ui/css/metro-icons.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" media=\"all\" type=\"text/css\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/styles/jquery.mCustomScrollbar.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" media=\"all\" type=\"text/css\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/styles/styles.css"), "html", null, true);
        echo "\">
    ";
        
        $__internal_6721d278dd694d0c4c5a614b6da5a53a7dfce8097346ee461c8d7a6f0ed258d0->leave($__internal_6721d278dd694d0c4c5a614b6da5a53a7dfce8097346ee461c8d7a6f0ed258d0_prof);

    }

    // line 120
    public function block_breadcumbs($context, array $blocks = array())
    {
        $__internal_46724efdc6cdb266dc82e4ff27ae7d590cedf484e4a159c79678590c5ad756c0 = $this->env->getExtension("native_profiler");
        $__internal_46724efdc6cdb266dc82e4ff27ae7d590cedf484e4a159c79678590c5ad756c0->enter($__internal_46724efdc6cdb266dc82e4ff27ae7d590cedf484e4a159c79678590c5ad756c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "breadcumbs"));

        
        $__internal_46724efdc6cdb266dc82e4ff27ae7d590cedf484e4a159c79678590c5ad756c0->leave($__internal_46724efdc6cdb266dc82e4ff27ae7d590cedf484e4a159c79678590c5ad756c0_prof);

    }

    // line 123
    public function block_body($context, array $blocks = array())
    {
        $__internal_54ca5e469185413b6ed5e1a7c8f4db49c6e70847275ca0b3fd2df6b44cd75750 = $this->env->getExtension("native_profiler");
        $__internal_54ca5e469185413b6ed5e1a7c8f4db49c6e70847275ca0b3fd2df6b44cd75750->enter($__internal_54ca5e469185413b6ed5e1a7c8f4db49c6e70847275ca0b3fd2df6b44cd75750_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 124
        echo "
                ";
        
        $__internal_54ca5e469185413b6ed5e1a7c8f4db49c6e70847275ca0b3fd2df6b44cd75750->leave($__internal_54ca5e469185413b6ed5e1a7c8f4db49c6e70847275ca0b3fd2df6b44cd75750_prof);

    }

    // line 133
    public function block_footer($context, array $blocks = array())
    {
        $__internal_6c7c7dc1433010543e855e31c8a5bf32ab43c7a2c5b3430e9d72fdf3d0618c97 = $this->env->getExtension("native_profiler");
        $__internal_6c7c7dc1433010543e855e31c8a5bf32ab43c7a2c5b3430e9d72fdf3d0618c97->enter($__internal_6c7c7dc1433010543e855e31c8a5bf32ab43c7a2c5b3430e9d72fdf3d0618c97_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 134
        echo "                <div class=\"align-center\">
                    <h6> &copy; ARM ";
        // line 135
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo "</h6>

                    <div class=\"align-center\">
                        <small>
                            ";
        // line 139
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 140
            echo "                                ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("layout.logged_in_as", array("%username%" => twig_capitalize_string_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array()))), "FOSUserBundle"), "html", null, true);
            echo "
                                ";
            // line 141
            echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "hasRole", array(0 => "ROLE_CI"), "method")) ? ((("(Bloc " . $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "bloc", array())) . ")")) : ("")), "html", null, true);
            echo "
                                <br>
                                Rôles:
                                ";
            // line 144
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "roles", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["role"]) {
                // line 145
                echo "                                    ";
                echo (((($context["role"] == "ROLE_ARM") || ($context["role"] == "ROLE_USER"))) ? ("") : ((("<span class=\"tag success\">" . $context["role"]) . "</span>")));
                echo "
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['role'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 147
            echo "                                <br>
                                <a href=\"";
            // line 148
            echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("layout.logout", array(), "FOSUserBundle"), "html", null, true);
            echo "</a>
                                |
                                <a href=\"";
            // line 150
            echo $this->env->getExtension('routing')->getPath("fos_user_profile_edit");
            echo "\">Modifier mon profil</a>
                                |
                                <a href=\"";
            // line 152
            echo $this->env->getExtension('routing')->getPath("fos_user_change_password");
            echo "\">Changer mon mot de passe</a>
                            ";
        } else {
            // line 154
            echo "                                <a class=\"button primary\"
                                   href=\"";
            // line 155
            echo $this->env->getExtension('routing')->getPath("fos_user_security_login");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("layout.login", array(), "FOSUserBundle"), "html", null, true);
            echo "</a>
                            ";
        }
        // line 157
        echo "                        </small>
                    </div>
                    <br>
                </div>
            ";
        
        $__internal_6c7c7dc1433010543e855e31c8a5bf32ab43c7a2c5b3430e9d72fdf3d0618c97->leave($__internal_6c7c7dc1433010543e855e31c8a5bf32ab43c7a2c5b3430e9d72fdf3d0618c97_prof);

    }

    // line 185
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_bd4db2a896a0357c7793dceefc831caf407ff5cb151fe3ebced818384e7d373e = $this->env->getExtension("native_profiler");
        $__internal_bd4db2a896a0357c7793dceefc831caf407ff5cb151fe3ebced818384e7d373e->enter($__internal_bd4db2a896a0357c7793dceefc831caf407ff5cb151fe3ebced818384e7d373e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 186
        echo "            <script language=\"JavaScript\" type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/scripts/jquery-2.1.3.min.js"), "html", null, true);
        echo "\"></script>
            <script language=\"JavaScript\" type=\"text/javascript\" src=\"";
        // line 187
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/scripts/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
            <script language=\"JavaScript\" type=\"text/javascript\" src=\"";
        // line 188
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/metro-ui/js/metro.min.js"), "html", null, true);
        echo "\"></script>
            <script language=\"JavaScript\" type=\"text/javascript\" src=\"";
        // line 189
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/scripts/select2.min.js"), "html", null, true);
        echo "\"></script>
            <script language=\"JavaScript\" type=\"text/javascript\" src=\"";
        // line 190
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/scripts/highcharts.js"), "html", null, true);
        echo "\"></script>
            <script language=\"JavaScript\" type=\"text/javascript\" src=\"";
        // line 191
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/scripts/jquery.mCustomScrollbar.min.js"), "html", null, true);
        echo "\"></script>
            <script language=\"JavaScript\" type=\"text/javascript\" src=\"";
        // line 192
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/scripts/functions.js"), "html", null, true);
        echo "\"></script>
            <script language=\"JavaScript\" type=\"text/javascript\" src=\"";
        // line 193
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/scripts/wizard.js"), "html", null, true);
        echo "\"></script>
        ";
        
        $__internal_bd4db2a896a0357c7793dceefc831caf407ff5cb151fe3ebced818384e7d373e->leave($__internal_bd4db2a896a0357c7793dceefc831caf407ff5cb151fe3ebced818384e7d373e_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  524 => 193,  520 => 192,  516 => 191,  512 => 190,  508 => 189,  504 => 188,  500 => 187,  495 => 186,  489 => 185,  478 => 157,  471 => 155,  468 => 154,  463 => 152,  458 => 150,  451 => 148,  448 => 147,  439 => 145,  435 => 144,  429 => 141,  424 => 140,  422 => 139,  415 => 135,  412 => 134,  406 => 133,  398 => 124,  392 => 123,  381 => 120,  372 => 15,  368 => 14,  364 => 13,  360 => 12,  356 => 11,  352 => 10,  347 => 9,  341 => 8,  330 => 5,  319 => 195,  317 => 185,  292 => 162,  290 => 133,  281 => 126,  279 => 123,  275 => 121,  273 => 120,  269 => 119,  265 => 117,  260 => 114,  254 => 113,  252 => 112,  247 => 110,  241 => 107,  235 => 106,  232 => 105,  228 => 104,  225 => 103,  223 => 102,  220 => 101,  218 => 100,  214 => 98,  205 => 92,  201 => 91,  197 => 90,  191 => 87,  186 => 84,  180 => 81,  173 => 77,  169 => 76,  165 => 75,  161 => 74,  157 => 73,  153 => 72,  148 => 69,  146 => 68,  143 => 67,  136 => 63,  132 => 62,  127 => 59,  125 => 58,  122 => 57,  116 => 55,  114 => 54,  111 => 53,  105 => 51,  103 => 50,  100 => 49,  93 => 45,  87 => 42,  83 => 41,  75 => 35,  73 => 34,  69 => 33,  66 => 32,  64 => 31,  51 => 21,  45 => 17,  43 => 8,  39 => 7,  34 => 5,  28 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/* <head>*/
/*     <meta charset="UTF-8"/>*/
/*     <title>{% block title %}{% endblock %} | ARM::SGPEC</title>*/
/* */
/*     <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}"/>*/
/*     {% block stylesheets %}*/
/*         <link rel="stylesheet" media="all" type="text/css" href="{{ asset('public/metro-ui/css/metro.min.css') }}">*/
/*         <link rel="stylesheet" media="all" type="text/css" href="{{ asset('public/metro-ui/css/metro-responsive.min.css') }}">*/
/*         <link rel="stylesheet" media="all" type="text/css" href="{{ asset('public/metro-ui/css/metro-rtl.min.css') }}">*/
/*         <link rel="stylesheet" media="all" type="text/css" href="{{ asset('public/metro-ui/css/metro-schemes.min.css') }}">*/
/*         <link rel="stylesheet" media="all" type="text/css" href="{{ asset('public/metro-ui/css/metro-icons.min.css') }}">*/
/*         <link rel="stylesheet" media="all" type="text/css" href="{{ asset('public/styles/jquery.mCustomScrollbar.min.css') }}">*/
/*         <link rel="stylesheet" media="all" type="text/css" href="{{ asset('public/styles/styles.css') }}">*/
/*     {% endblock %}*/
/* </head>*/
/* <body> <!--class="scroll-y"--->*/
/* <div class="container-fluid margin10">*/
/*     <div class="bg-grayLighter padding10 laiso-bordered margin10 no-margin-left no-margin-right no-margin-bottom">*/
/*         <h1><a href="{{ path('laiso_arm_homepage') }}" class="fg-black">ARM::SGPEC</a>*/
/*             <small> SYSTEME DE GESTION DE PROJET D'ENTRETIENS COURANTS DES ROUTES</small>*/
/*         </h1>*/
/*     </div>*/
/*     <br>*/
/* */
/*     <div class="bg-grayLighter padding10 laiso-bordered">*/
/* */
/*         <!-- MENU -->*/
/* */
/*         {% if is_granted("IS_AUTHENTICATED_REMEMBERED") %}*/
/*         <ul class="h-menu laiso-bordered bg-white">*/
/*             <li><a href="{{ path('laiso_arm_homepage') }}">Accueil</a></li>*/
/*             {% if app.user.hasRole('ROLE_DTEC') or app.user.hasRole('ROLE_CI') %}*/
/*                 <li>*/
/*                     <a href="#" class="dropdown-toggle">Gestion des DAO</a>*/
/*                     <ul class="d-menu" data-role="dropdown">*/
/*                         <li>*/
/*                             <a href="#" class="dropdown-toggle">Prix</a>*/
/*                             <ul class="d-menu" data-role="dropdown">*/
/*                                 <li><a href="{{ path('prix_categorie') }}">Catégorie</a></li>*/
/*                                 <li><a href="{{ path('prix_serie') }}">Série de prix</a></li>*/
/*                             </ul>*/
/*                         </li>*/
/*                         <li><a href="{{ path('dao') }}">DAO</a></li>*/
/*                     </ul>*/
/*                 </li>*/
/*             {% endif %}*/
/* */
/*             {% if app.user.hasRole('ROLE_DTEC') or app.user.hasRole('ROLE_CI') %}*/
/*                 <li><a href="{{ path('marche') }}">Gestion des marchés</a></li>*/
/*             {% endif %}*/
/* */
/*             {% if app.user.hasRole('ROLE_DTEC') or app.user.hasRole('ROLE_CI') %}*/
/*                 <li><a href="{{ path('entreprise') }}">Entreprises</a></li>*/
/*             {% endif %}*/
/* */
/*             {% if app.user.hasRole('ROLE_DTEC') %}*/
/*                 <li>*/
/*                     <a href="#" class="dropdown-toggle">Administration</a>*/
/*                     <ul class="d-menu" data-role="dropdown">*/
/*                         <li><a href="{{ path('dashboard_index') }}">Tableau de bord</a></li>*/
/*                         <li><a href="{{ path('campagne') }}">Campagne</a></li>*/
/*                     </ul>*/
/*                 </li>*/
/*             {% endif %}*/
/* */
/*             {% if app.user.hasRole('ROLE_ADMIN') %}*/
/*                 <li>*/
/*                     <a href="#" class="dropdown-toggle">Paramétrage</a>*/
/*                     <ul class="d-menu" data-role="dropdown">*/
/*                         <li><a href="{{ path('typeconsistance') }}">Types de consistances</a></li>*/
/*                         <li><a href="{{ path('unitedemesure') }}">Unités de mesure</a></li>*/
/*                         <li><a href="{{ path('libelleavenant') }}">Libellés de marchés</a></li>*/
/*                         <li><a href="{{ path('bloc') }}">Bloc</a></li>*/
/*                         <li><a href="{{ path('region') }}">Région</a></li>*/
/*                         <li><a href="{{ path('axe') }}">Axe</a></li>*/
/*                     </ul>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="{{ path('users') }}">Gérer les utlisateurs</a>*/
/*                 </li>*/
/*             {% endif %}*/
/* */
/*             <li class="place-right">*/
/*                 <a href="#" class="dropdown-toogle">*/
/*                     {{ 'layout.logged_in_as'|trans({'%username%': app.user.username | capitalize}, 'FOSUserBundle') }}*/
/*                 </a>*/
/*                 <ul class="d-menu align-left" data-role="dropdown">*/
/*                     <li><a href="{{ path('fos_user_profile_show') }}">Mon profil</a></li>*/
/*                     <li><a href="{{ path('fos_user_profile_edit') }}">Modifier mon profil</a></li>*/
/*                     <li><a href="{{ path('fos_user_security_logout') }}">Se déconnecter</a></li>*/
/*                 </ul>*/
/*             </li>*/
/*         </ul>*/
/* */
/*         {% endif %}*/
/*         <!--- Fin Menu --->*/
/* */
/*         {% set flashNumber = 1 %}*/
/* */
/*         {% if app.session.flashbag | length > 0 %}*/
/*             <div>*/
/*                 {% for key, message in app.session.flashbag() %}*/
/*                     <br>*/
/*                     <div class="flash-{{ key }} padding10" id="flash-{{ flashNumber }}">*/
/*                         <a href="#" class="closeFlash" flashId="flash-{{ flashNumber }}">*/
/*                             <span class="place-right mif-cross fg-grayLight"></span>*/
/*                         </a>*/
/*                         {{ message[0] | raw }}*/
/*                     </div>*/
/*                     {% set flashNumber = flashNumber + 1 %}*/
/*                 {% endfor %}*/
/*             </div>*/
/*             <br>*/
/*         {% endif %}*/
/*         <div class="laiso-bordered bg-white padding10">*/
/*             <ul class="breadcrumbs dark bg-darkCyan">*/
/*                 <li><a href="{{ path('laiso_arm_homepage') }}"><span class="icon mif-home"></span> Accueil</a></li>*/
/*                 {% block breadcumbs %}{% endblock %}*/
/*             </ul>*/
/*             <div>*/
/*                 {% block body %}*/
/* */
/*                 {% endblock %}*/
/*             </div>*/
/*             <span class="clear-float"></span>*/
/*         </div>*/
/*     </div>*/
/* */
/*     <div class="bg-grayLighter grid laiso-bordered padding10 no-padding-bottom">*/
/*         <div class="bg-white laiso-bordered">*/
/*             {% block footer %}*/
/*                 <div class="align-center">*/
/*                     <h6> &copy; ARM {{ "now"|date('Y') }}</h6>*/
/* */
/*                     <div class="align-center">*/
/*                         <small>*/
/*                             {% if is_granted("IS_AUTHENTICATED_REMEMBERED") %}*/
/*                                 {{ 'layout.logged_in_as'|trans({'%username%': app.user.username | capitalize}, 'FOSUserBundle') }}*/
/*                                 {{ app.user.hasRole('ROLE_CI') ? '(Bloc ' ~ app.user.bloc ~')' : '' }}*/
/*                                 <br>*/
/*                                 Rôles:*/
/*                                 {% for role in app.user.roles %}*/
/*                                     {{ (role == 'ROLE_ARM' or role == 'ROLE_USER') ? '' : ('<span class="tag success">' ~ role ~'</span>') | raw }}*/
/*                                 {% endfor %}*/
/*                                 <br>*/
/*                                 <a href="{{ path('fos_user_security_logout') }}">{{ 'layout.logout'|trans({}, 'FOSUserBundle') }}</a>*/
/*                                 |*/
/*                                 <a href="{{ path('fos_user_profile_edit') }}">Modifier mon profil</a>*/
/*                                 |*/
/*                                 <a href="{{ path('fos_user_change_password') }}">Changer mon mot de passe</a>*/
/*                             {% else %}*/
/*                                 <a class="button primary"*/
/*                                    href="{{ path('fos_user_security_login') }}">{{ 'layout.login'|trans({}, 'FOSUserBundle') }}</a>*/
/*                             {% endif %}*/
/*                         </small>*/
/*                     </div>*/
/*                     <br>*/
/*                 </div>*/
/*             {% endblock %}*/
/*             <div data-role="dialog" id="loader" class="padding20" data-overlay="true" data-windows-style="true"*/
/*                  data-overlay-color="op-dark">*/
/*                 <div class="align-center container" id="loader-container">*/
/*                     <div data-role="preloader" data-type="metro" data-style="color"></div>*/
/*                     <h5>En cours de traitement ...</h5>*/
/*                 </div>*/
/*             </div>*/
/* */
/*             <div data-role="dialog" id="dialog" class="padding20 no-padding-top" data-close-button="true"*/
/*                  data-windows-style="true" data-overlay="true" data-overlay-color="op-dark" data-overlay-click-close="true">*/
/*                 <div class="container" id="dialog-container">*/
/* */
/*                 </div>*/
/*             </div>*/
/* */
/*             <div data-role="dialog" id="confirm" class="padding20 no-padding-top" data-close-button="true"*/
/*                  data-windows-style="true" data-overlay="true" data-overlay-color="op-dark" data-overlay-click-close="true">*/
/*                 <div class="container" id="dialog-container">*/
/*                     <h1>Vous êtes sur le point de valider les modifications. Confirmer?</h1>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         <br>*/
/*         {% block javascripts %}*/
/*             <script language="JavaScript" type="text/javascript" src="{{ asset('public/scripts/jquery-2.1.3.min.js') }}"></script>*/
/*             <script language="JavaScript" type="text/javascript" src="{{ asset('public/scripts/jquery.dataTables.min.js') }}"></script>*/
/*             <script language="JavaScript" type="text/javascript" src="{{ asset('public/metro-ui/js/metro.min.js') }}"></script>*/
/*             <script language="JavaScript" type="text/javascript" src="{{ asset('public/scripts/select2.min.js') }}"></script>*/
/*             <script language="JavaScript" type="text/javascript" src="{{ asset('public/scripts/highcharts.js') }}"></script>*/
/*             <script language="JavaScript" type="text/javascript" src="{{ asset('public/scripts/jquery.mCustomScrollbar.min.js') }}"></script>*/
/*             <script language="JavaScript" type="text/javascript" src="{{ asset('public/scripts/functions.js') }}"></script>*/
/*             <script language="JavaScript" type="text/javascript" src="{{ asset('public/scripts/wizard.js') }}"></script>*/
/*         {% endblock %}*/
/*     </div>*/
/* </div>*/
/* </body>*/
/* </html>*/
/* */
