<?php

/* LaisoArmBundle:Bloc:index.html.twig */
class __TwigTemplate_0fd851235e363856c09a6f0f8e18536f3a692a501d34c3c02a2a1b19249a4136 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "LaisoArmBundle:Bloc:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'breadcumbs' => array($this, 'block_breadcumbs'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2f200ea3beac4dd1d67d0ab7285dc5607889265cc7ea7c1d2c7f172703032ae0 = $this->env->getExtension("native_profiler");
        $__internal_2f200ea3beac4dd1d67d0ab7285dc5607889265cc7ea7c1d2c7f172703032ae0->enter($__internal_2f200ea3beac4dd1d67d0ab7285dc5607889265cc7ea7c1d2c7f172703032ae0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "LaisoArmBundle:Bloc:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2f200ea3beac4dd1d67d0ab7285dc5607889265cc7ea7c1d2c7f172703032ae0->leave($__internal_2f200ea3beac4dd1d67d0ab7285dc5607889265cc7ea7c1d2c7f172703032ae0_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_39017c270f72f443d71b4e608c35d7a4b6c1fe43bc4efc284c78fe1551e567c6 = $this->env->getExtension("native_profiler");
        $__internal_39017c270f72f443d71b4e608c35d7a4b6c1fe43bc4efc284c78fe1551e567c6->enter($__internal_39017c270f72f443d71b4e608c35d7a4b6c1fe43bc4efc284c78fe1551e567c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Admin - Bloc";
        
        $__internal_39017c270f72f443d71b4e608c35d7a4b6c1fe43bc4efc284c78fe1551e567c6->leave($__internal_39017c270f72f443d71b4e608c35d7a4b6c1fe43bc4efc284c78fe1551e567c6_prof);

    }

    // line 5
    public function block_breadcumbs($context, array $blocks = array())
    {
        $__internal_2ce27e7fae2023d75f80bb8450aaa3bc034bb914d7ed91d76b1f71edee3e2661 = $this->env->getExtension("native_profiler");
        $__internal_2ce27e7fae2023d75f80bb8450aaa3bc034bb914d7ed91d76b1f71edee3e2661->enter($__internal_2ce27e7fae2023d75f80bb8450aaa3bc034bb914d7ed91d76b1f71edee3e2661_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "breadcumbs"));

        // line 6
        echo "    <li><a href=\"#\">Admin</a></li>
    <li><a href=\"";
        // line 7
        echo $this->env->getExtension('routing')->getPath("bloc");
        echo "\">Bloc</a></li>
";
        
        $__internal_2ce27e7fae2023d75f80bb8450aaa3bc034bb914d7ed91d76b1f71edee3e2661->leave($__internal_2ce27e7fae2023d75f80bb8450aaa3bc034bb914d7ed91d76b1f71edee3e2661_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_b6a3c2b4c27e2462faefcd16bed9ea362c2446cfaf2eef58fec220b8f29ce701 = $this->env->getExtension("native_profiler");
        $__internal_b6a3c2b4c27e2462faefcd16bed9ea362c2446cfaf2eef58fec220b8f29ce701->enter($__internal_b6a3c2b4c27e2462faefcd16bed9ea362c2446cfaf2eef58fec220b8f29ce701_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 11
        if ((twig_length_filter($this->env, (isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities"))) > 0)) {
            // line 12
            echo "        <table class=\"table border bordered\">
            <thead>
            <tr>
                <th style=\"width: 10%;\">Numéro</th>
                <th>Localisation</th>
                <td>Siège</td>
                <th style=\"width: 20%\">Actions</th>
            </tr>
            </thead>
            <tbody>
            ";
            // line 22
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
            foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
                // line 23
                echo "                <tr>
                    <td>";
                // line 24
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "numero", array()), "html", null, true);
                echo "</td>
                    <td>";
                // line 25
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "localisation", array()), "html", null, true);
                echo "</td>
                    <td>";
                // line 26
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "siege", array()), "html", null, true);
                echo "</td>
                    <td class=\"align-center\">
                        ";
                // line 32
                echo "                        &nbsp;
                        <a class=\"ajax-button\" href=\"";
                // line 33
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("bloc_edit", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\"><span
                                    data-role=\"hint\" data-hint-background=\"bg-darkTeal\" data-hint-color=\"fg-white\"
                                    data-hint=\"Modifier|Modifier le bloc  ";
                // line 35
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "localisation", array()), "html", null, true);
                echo "\">Modifier</span></a>
                    </td>
                </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 39
            echo "            </tbody>
        </table>

        ";
            // line 42
            $this->loadTemplate("LaisoArmBundle:Shared:paginator.html.twig", "LaisoArmBundle:Bloc:index.html.twig", 42)->display($context);
            // line 43
            echo "
        <a class=\"button primary ajax-button\" id=\"new\" href=\"";
            // line 44
            echo $this->env->getExtension('routing')->getPath("bloc_new");
            echo "\">Créer un nouveau bloc</a>
    ";
        } else {
            // line 46
            echo "        <div class=\"align-center margin10 no-margin-left no-margin-right no-margin-bottom laiso-bordered padding10\">
            <h3>Aucun bloc</h3>
            <a class=\"button primary ajax-button\" id=\"new\" href=\"";
            // line 48
            echo $this->env->getExtension('routing')->getPath("bloc_new");
            echo "\">Créer un nouveau bloc</a>
        </div>
    ";
        }
        
        $__internal_b6a3c2b4c27e2462faefcd16bed9ea362c2446cfaf2eef58fec220b8f29ce701->leave($__internal_b6a3c2b4c27e2462faefcd16bed9ea362c2446cfaf2eef58fec220b8f29ce701_prof);

    }

    public function getTemplateName()
    {
        return "LaisoArmBundle:Bloc:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  143 => 48,  139 => 46,  134 => 44,  131 => 43,  129 => 42,  124 => 39,  114 => 35,  109 => 33,  106 => 32,  101 => 26,  97 => 25,  93 => 24,  90 => 23,  86 => 22,  74 => 12,  72 => 11,  66 => 10,  57 => 7,  54 => 6,  48 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block title %}Admin - Bloc{% endblock %}*/
/* */
/* {% block breadcumbs %}*/
/*     <li><a href="#">Admin</a></li>*/
/*     <li><a href="{{ path('bloc') }}">Bloc</a></li>*/
/* {% endblock %}*/
/* */
/* {% block body -%}*/
/*     {% if entities | length > 0 %}*/
/*         <table class="table border bordered">*/
/*             <thead>*/
/*             <tr>*/
/*                 <th style="width: 10%;">Numéro</th>*/
/*                 <th>Localisation</th>*/
/*                 <td>Siège</td>*/
/*                 <th style="width: 20%">Actions</th>*/
/*             </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*             {% for entity in entities %}*/
/*                 <tr>*/
/*                     <td>{{ entity.numero }}</td>*/
/*                     <td>{{ entity.localisation }}</td>*/
/*                     <td>{{ entity.siege }}</td>*/
/*                     <td class="align-center">*/
/*                         {#<a href="{{ path('bloc_show', { 'id': entity.id }) }}">*/
/*                             <span data-role="hint" data-hint-background="bg-darkTeal" data-hint-color="fg-white"*/
/*                                   data-hint="Détailler|Détailler le bloc {{ entity.localisation }}">Détails</span>*/
/*                         </a>#}*/
/*                         &nbsp;*/
/*                         <a class="ajax-button" href="{{ path('bloc_edit', { 'id': entity.id }) }}"><span*/
/*                                     data-role="hint" data-hint-background="bg-darkTeal" data-hint-color="fg-white"*/
/*                                     data-hint="Modifier|Modifier le bloc  {{ entity.localisation }}">Modifier</span></a>*/
/*                     </td>*/
/*                 </tr>*/
/*             {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/* */
/*         {% include ('LaisoArmBundle:Shared:paginator.html.twig') %}*/
/* */
/*         <a class="button primary ajax-button" id="new" href="{{ path('bloc_new') }}">Créer un nouveau bloc</a>*/
/*     {% else %}*/
/*         <div class="align-center margin10 no-margin-left no-margin-right no-margin-bottom laiso-bordered padding10">*/
/*             <h3>Aucun bloc</h3>*/
/*             <a class="button primary ajax-button" id="new" href="{{ path('bloc_new') }}">Créer un nouveau bloc</a>*/
/*         </div>*/
/*     {% endif %}*/
/* {% endblock %}*/
/* */
