<?php

/* FOSUserBundle:Resetting:request.html.twig */
class __TwigTemplate_dbaa9f1d0a8b7c5de644c059c62c853d5df3a7bea588329f074643babb63bd87 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::login.html.twig", "FOSUserBundle:Resetting:request.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::login.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_93e35b3ef02f8f78eb8d1ceadc6307ed647d7f89164d2d83d675c00a2112b4bb = $this->env->getExtension("native_profiler");
        $__internal_93e35b3ef02f8f78eb8d1ceadc6307ed647d7f89164d2d83d675c00a2112b4bb->enter($__internal_93e35b3ef02f8f78eb8d1ceadc6307ed647d7f89164d2d83d675c00a2112b4bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_93e35b3ef02f8f78eb8d1ceadc6307ed647d7f89164d2d83d675c00a2112b4bb->leave($__internal_93e35b3ef02f8f78eb8d1ceadc6307ed647d7f89164d2d83d675c00a2112b4bb_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_8f89ec94f06e967029fcbb3f378046d06309ac586704199b9107d18931b555f6 = $this->env->getExtension("native_profiler");
        $__internal_8f89ec94f06e967029fcbb3f378046d06309ac586704199b9107d18931b555f6->enter($__internal_8f89ec94f06e967029fcbb3f378046d06309ac586704199b9107d18931b555f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Resetting:request_content.html.twig", "FOSUserBundle:Resetting:request.html.twig", 4)->display($context);
        
        $__internal_8f89ec94f06e967029fcbb3f378046d06309ac586704199b9107d18931b555f6->leave($__internal_8f89ec94f06e967029fcbb3f378046d06309ac586704199b9107d18931b555f6_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::login.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Resetting:request_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
