<?php

/* FOSUserBundle:Resetting:email.txt.twig */
class __TwigTemplate_9f6067a39df5635f21fc4c96315edeef9967b469aa56313655bc1d766eeed070 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f6d9ad1a8b56ae85ade5d73ad6b88406094ec2b658e18654052ffbd8f2ac76b7 = $this->env->getExtension("native_profiler");
        $__internal_f6d9ad1a8b56ae85ade5d73ad6b88406094ec2b658e18654052ffbd8f2ac76b7->enter($__internal_f6d9ad1a8b56ae85ade5d73ad6b88406094ec2b658e18654052ffbd8f2ac76b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        $this->displayBlock('body_text', $context, $blocks);
        // line 12
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_f6d9ad1a8b56ae85ade5d73ad6b88406094ec2b658e18654052ffbd8f2ac76b7->leave($__internal_f6d9ad1a8b56ae85ade5d73ad6b88406094ec2b658e18654052ffbd8f2ac76b7_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_04b31ff82cddc2a90a84177e8df79f160b75a94d2958a5faa7f47541ff5e354d = $this->env->getExtension("native_profiler");
        $__internal_04b31ff82cddc2a90a84177e8df79f160b75a94d2958a5faa7f47541ff5e354d->enter($__internal_04b31ff82cddc2a90a84177e8df79f160b75a94d2958a5faa7f47541ff5e354d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('translator')->trans("resetting.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array())), "FOSUserBundle");
        echo "
";
        
        $__internal_04b31ff82cddc2a90a84177e8df79f160b75a94d2958a5faa7f47541ff5e354d->leave($__internal_04b31ff82cddc2a90a84177e8df79f160b75a94d2958a5faa7f47541ff5e354d_prof);

    }

    // line 7
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_0786e3c7a3353654b4a4000b31c4a81ca613287a5c6349e80708a501746a671a = $this->env->getExtension("native_profiler");
        $__internal_0786e3c7a3353654b4a4000b31c4a81ca613287a5c6349e80708a501746a671a->enter($__internal_0786e3c7a3353654b4a4000b31c4a81ca613287a5c6349e80708a501746a671a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 9
        echo $this->env->getExtension('translator')->trans("resetting.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_0786e3c7a3353654b4a4000b31c4a81ca613287a5c6349e80708a501746a671a->leave($__internal_0786e3c7a3353654b4a4000b31c4a81ca613287a5c6349e80708a501746a671a_prof);

    }

    // line 12
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_ed4aecbea5718970e749f71d5b436a9b0051de08d96314c72eefc5ceae4363d3 = $this->env->getExtension("native_profiler");
        $__internal_ed4aecbea5718970e749f71d5b436a9b0051de08d96314c72eefc5ceae4363d3->enter($__internal_ed4aecbea5718970e749f71d5b436a9b0051de08d96314c72eefc5ceae4363d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_ed4aecbea5718970e749f71d5b436a9b0051de08d96314c72eefc5ceae4363d3->leave($__internal_ed4aecbea5718970e749f71d5b436a9b0051de08d96314c72eefc5ceae4363d3_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  66 => 12,  57 => 9,  51 => 7,  42 => 4,  36 => 2,  29 => 12,  27 => 7,  25 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* {% block subject %}*/
/* {% autoescape false %}*/
/* {{ 'resetting.email.subject'|trans({'%username%': user.username}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_text %}*/
/* {% autoescape false %}*/
/* {{ 'resetting.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_html %}{% endblock %}*/
/* */
