<?php

/* KnpPaginatorBundle:Pagination:metro_ui_circle_pagination.html.twig */
class __TwigTemplate_36f45282bbee82ccadd57325be6e440fbb3c96aab94f024487748bb0d72b2286 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_451742f103f1e0345ea823cfad5397fd6525dee858a699da3b9596993ed8d729 = $this->env->getExtension("native_profiler");
        $__internal_451742f103f1e0345ea823cfad5397fd6525dee858a699da3b9596993ed8d729->enter($__internal_451742f103f1e0345ea823cfad5397fd6525dee858a699da3b9596993ed8d729_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "KnpPaginatorBundle:Pagination:metro_ui_circle_pagination.html.twig"));

        // line 1
        echo "

<div data-role=\"group\" data-group-type=\"multi-state\" data-button-style=\"class\">
";
        // line 4
        if (((isset($context["pageCount"]) ? $context["pageCount"] : $this->getContext($context, "pageCount")) > 1)) {
            // line 5
            echo "
    ";
            // line 6
            if (array_key_exists("previous", $context)) {
                // line 7
                echo "        <a class=\"button cycle-button\" href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["route"]) ? $context["route"] : $this->getContext($context, "route")), twig_array_merge((isset($context["query"]) ? $context["query"] : $this->getContext($context, "query")), array((isset($context["pageParameterName"]) ? $context["pageParameterName"] : $this->getContext($context, "pageParameterName")) => (isset($context["previous"]) ? $context["previous"] : $this->getContext($context, "previous"))))), "html", null, true);
                echo "\"><span class=\"icon mif-arrow-left\"></span></a>
    ";
            } else {
                // line 9
                echo "        <button class=\"button cycle-button\" disabled><span class=\"icon mif-arrow-left\"></span></button>
    ";
            }
            // line 11
            echo "
    ";
            // line 12
            if (((isset($context["startPage"]) ? $context["startPage"] : $this->getContext($context, "startPage")) > 1)) {
                // line 13
                echo "        <a class=\"button cycle-button\" href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["route"]) ? $context["route"] : $this->getContext($context, "route")), twig_array_merge((isset($context["query"]) ? $context["query"] : $this->getContext($context, "query")), array((isset($context["pageParameterName"]) ? $context["pageParameterName"] : $this->getContext($context, "pageParameterName")) => 1))), "html", null, true);
                echo "\">1</a>
        ";
                // line 14
                if (((isset($context["startPage"]) ? $context["startPage"] : $this->getContext($context, "startPage")) == 3)) {
                    // line 15
                    echo "            <a class=\"button cycle-button\" href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["route"]) ? $context["route"] : $this->getContext($context, "route")), twig_array_merge((isset($context["query"]) ? $context["query"] : $this->getContext($context, "query")), array((isset($context["pageParameterName"]) ? $context["pageParameterName"] : $this->getContext($context, "pageParameterName")) => 2))), "html", null, true);
                    echo "\">2</a>
        ";
                } elseif ((                // line 16
(isset($context["startPage"]) ? $context["startPage"] : $this->getContext($context, "startPage")) != 2)) {
                    // line 17
                    echo "        <button disabled class=\"button cycle-button\"> <span>&hellip;</span> </button>
        ";
                }
                // line 19
                echo "    ";
            }
            // line 20
            echo "
    ";
            // line 21
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["pagesInRange"]) ? $context["pagesInRange"] : $this->getContext($context, "pagesInRange")));
            foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                // line 22
                echo "        ";
                if (($context["page"] != (isset($context["current"]) ? $context["current"] : $this->getContext($context, "current")))) {
                    // line 23
                    echo "            <a class=\"button cycle-button\" href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["route"]) ? $context["route"] : $this->getContext($context, "route")), twig_array_merge((isset($context["query"]) ? $context["query"] : $this->getContext($context, "query")), array((isset($context["pageParameterName"]) ? $context["pageParameterName"] : $this->getContext($context, "pageParameterName")) => $context["page"]))), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $context["page"], "html", null, true);
                    echo "</a>
        ";
                } else {
                    // line 25
                    echo "                <button class=\"button cycle-button primary fg-white\">";
                    echo twig_escape_filter($this->env, $context["page"], "html", null, true);
                    echo "</button>
        ";
                }
                // line 27
                echo "
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 29
            echo "
    ";
            // line 30
            if (((isset($context["pageCount"]) ? $context["pageCount"] : $this->getContext($context, "pageCount")) > (isset($context["endPage"]) ? $context["endPage"] : $this->getContext($context, "endPage")))) {
                // line 31
                echo "        ";
                if (((isset($context["pageCount"]) ? $context["pageCount"] : $this->getContext($context, "pageCount")) > ((isset($context["endPage"]) ? $context["endPage"] : $this->getContext($context, "endPage")) + 1))) {
                    // line 32
                    echo "            ";
                    if (((isset($context["pageCount"]) ? $context["pageCount"] : $this->getContext($context, "pageCount")) > ((isset($context["endPage"]) ? $context["endPage"] : $this->getContext($context, "endPage")) + 2))) {
                        // line 33
                        echo "                <button disabled class=\"button cycle-button\">
                    <span>&hellip;</span>

                </button>
            ";
                    } else {
                        // line 38
                        echo "                <a class=\"button cycle-button\" href=\"";
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["route"]) ? $context["route"] : $this->getContext($context, "route")), twig_array_merge((isset($context["query"]) ? $context["query"] : $this->getContext($context, "query")), array((isset($context["pageParameterName"]) ? $context["pageParameterName"] : $this->getContext($context, "pageParameterName")) => ((isset($context["pageCount"]) ? $context["pageCount"] : $this->getContext($context, "pageCount")) - 1)))), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, ((isset($context["pageCount"]) ? $context["pageCount"] : $this->getContext($context, "pageCount")) - 1), "html", null, true);
                        echo "</a>
            ";
                    }
                    // line 40
                    echo "        ";
                }
                // line 41
                echo "        <a class=\"button cycle-button\" href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["route"]) ? $context["route"] : $this->getContext($context, "route")), twig_array_merge((isset($context["query"]) ? $context["query"] : $this->getContext($context, "query")), array((isset($context["pageParameterName"]) ? $context["pageParameterName"] : $this->getContext($context, "pageParameterName")) => (isset($context["pageCount"]) ? $context["pageCount"] : $this->getContext($context, "pageCount"))))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, (isset($context["pageCount"]) ? $context["pageCount"] : $this->getContext($context, "pageCount")), "html", null, true);
                echo "</a>
    ";
            }
            // line 43
            echo "
    ";
            // line 44
            if (array_key_exists("next", $context)) {
                // line 45
                echo "        <a class=\"button cycle-button\" href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["route"]) ? $context["route"] : $this->getContext($context, "route")), twig_array_merge((isset($context["query"]) ? $context["query"] : $this->getContext($context, "query")), array((isset($context["pageParameterName"]) ? $context["pageParameterName"] : $this->getContext($context, "pageParameterName")) => (isset($context["next"]) ? $context["next"] : $this->getContext($context, "next"))))), "html", null, true);
                echo "\"><span class=\"icon mif-arrow-right\"></span></a>
    ";
            } else {
                // line 47
                echo "        <button class=\"button cycle-button\" disabled>
            <span class=\"icon mif-arrow-right\"></span>
        </button>
    ";
            }
            // line 51
            echo "    </ul>
";
        }
        // line 53
        echo "</div>";
        
        $__internal_451742f103f1e0345ea823cfad5397fd6525dee858a699da3b9596993ed8d729->leave($__internal_451742f103f1e0345ea823cfad5397fd6525dee858a699da3b9596993ed8d729_prof);

    }

    public function getTemplateName()
    {
        return "KnpPaginatorBundle:Pagination:metro_ui_circle_pagination.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  159 => 53,  155 => 51,  149 => 47,  143 => 45,  141 => 44,  138 => 43,  130 => 41,  127 => 40,  119 => 38,  112 => 33,  109 => 32,  106 => 31,  104 => 30,  101 => 29,  94 => 27,  88 => 25,  80 => 23,  77 => 22,  73 => 21,  70 => 20,  67 => 19,  63 => 17,  61 => 16,  56 => 15,  54 => 14,  49 => 13,  47 => 12,  44 => 11,  40 => 9,  34 => 7,  32 => 6,  29 => 5,  27 => 4,  22 => 1,);
    }
}
/* */
/* */
/* <div data-role="group" data-group-type="multi-state" data-button-style="class">*/
/* {% if pageCount > 1 %}*/
/* */
/*     {% if previous is defined %}*/
/*         <a class="button cycle-button" href="{{ path(route, query|merge({(pageParameterName): previous})) }}"><span class="icon mif-arrow-left"></span></a>*/
/*     {% else %}*/
/*         <button class="button cycle-button" disabled><span class="icon mif-arrow-left"></span></button>*/
/*     {% endif %}*/
/* */
/*     {% if startPage > 1 %}*/
/*         <a class="button cycle-button" href="{{ path(route, query|merge({(pageParameterName): 1})) }}">1</a>*/
/*         {% if startPage == 3 %}*/
/*             <a class="button cycle-button" href="{{ path(route, query|merge({(pageParameterName): 2})) }}">2</a>*/
/*         {% elseif startPage != 2 %}*/
/*         <button disabled class="button cycle-button"> <span>&hellip;</span> </button>*/
/*         {% endif %}*/
/*     {% endif %}*/
/* */
/*     {% for page in pagesInRange %}*/
/*         {% if page != current %}*/
/*             <a class="button cycle-button" href="{{ path(route, query|merge({(pageParameterName): page})) }}">{{ page }}</a>*/
/*         {% else %}*/
/*                 <button class="button cycle-button primary fg-white">{{ page }}</button>*/
/*         {% endif %}*/
/* */
/*     {% endfor %}*/
/* */
/*     {% if pageCount > endPage %}*/
/*         {% if pageCount > (endPage + 1) %}*/
/*             {% if pageCount > (endPage + 2) %}*/
/*                 <button disabled class="button cycle-button">*/
/*                     <span>&hellip;</span>*/
/* */
/*                 </button>*/
/*             {% else %}*/
/*                 <a class="button cycle-button" href="{{ path(route, query|merge({(pageParameterName): (pageCount - 1)})) }}">{{ pageCount -1 }}</a>*/
/*             {% endif %}*/
/*         {% endif %}*/
/*         <a class="button cycle-button" href="{{ path(route, query|merge({(pageParameterName): pageCount})) }}">{{ pageCount }}</a>*/
/*     {% endif %}*/
/* */
/*     {% if next is defined %}*/
/*         <a class="button cycle-button" href="{{ path(route, query|merge({(pageParameterName): next})) }}"><span class="icon mif-arrow-right"></span></a>*/
/*     {% else %}*/
/*         <button class="button cycle-button" disabled>*/
/*             <span class="icon mif-arrow-right"></span>*/
/*         </button>*/
/*     {% endif %}*/
/*     </ul>*/
/* {% endif %}*/
/* </div>*/
