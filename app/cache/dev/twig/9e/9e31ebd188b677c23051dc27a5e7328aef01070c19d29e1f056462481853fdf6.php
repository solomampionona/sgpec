<?php

/* LaisoArmBundle:Default:index.html.twig */
class __TwigTemplate_3450a0bf177e1794ebdf1fcb8cc95acc7730fbd696c686dd427cd35db2df691d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "LaisoArmBundle:Default:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4f6b01bcab39e98a58b80631a940a35575959379c14e094c693d3ad0f1016301 = $this->env->getExtension("native_profiler");
        $__internal_4f6b01bcab39e98a58b80631a940a35575959379c14e094c693d3ad0f1016301->enter($__internal_4f6b01bcab39e98a58b80631a940a35575959379c14e094c693d3ad0f1016301_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "LaisoArmBundle:Default:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4f6b01bcab39e98a58b80631a940a35575959379c14e094c693d3ad0f1016301->leave($__internal_4f6b01bcab39e98a58b80631a940a35575959379c14e094c693d3ad0f1016301_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_e89a781eb9853af333facae4dcdace30982bf8130d82f71ab8eed7317b11d830 = $this->env->getExtension("native_profiler");
        $__internal_e89a781eb9853af333facae4dcdace30982bf8130d82f71ab8eed7317b11d830->enter($__internal_e89a781eb9853af333facae4dcdace30982bf8130d82f71ab8eed7317b11d830_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Accueil";
        
        $__internal_e89a781eb9853af333facae4dcdace30982bf8130d82f71ab8eed7317b11d830->leave($__internal_e89a781eb9853af333facae4dcdace30982bf8130d82f71ab8eed7317b11d830_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_6407b7b94bf12b9492f1cb5478baf89e451f305015bffd428d958d4e1d86e532 = $this->env->getExtension("native_profiler");
        $__internal_6407b7b94bf12b9492f1cb5478baf89e451f305015bffd428d958d4e1d86e532->enter($__internal_6407b7b94bf12b9492f1cb5478baf89e451f305015bffd428d958d4e1d86e532_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"laiso-bordered padding10 no-padding-bottom margin10 no-margin-bottom no-margin-left no-margin-right\">
        <div class=\"align-center\">
            <h3>
                Bienvenue sur la plateforme SGPEC: <span class=\"fg-darkTeal\">Système de Gestion de Projet d'Entretiens Courants des routes</span>
                de l'Autorité Routière de Madagascar
            </h3>
            <hr class=\"thin\" width=\"30%\">
        </div>

        ";
        // line 15
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "hasRole", array(0 => "ROLE_ADMIN"), "method")) {
            // line 16
            echo "            ";
            $this->loadTemplate("@LaisoArm/Default/menuAdmin.html.twig", "LaisoArmBundle:Default:index.html.twig", 16)->display($context);
            // line 17
            echo "        ";
        } elseif ((($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "hasRole", array(0 => "ROLE_DTEC"), "method") || $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "hasRole", array(0 => "ROLE_CI"), "method")) || $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "hasRole", array(0 => "ROLE_ASSIST"), "method"))) {
            // line 18
            echo "            ";
            $this->loadTemplate("@LaisoArm/Default/menuDtec.html.twig", "LaisoArmBundle:Default:index.html.twig", 18)->display($context);
            // line 19
            echo "        ";
        }
        // line 20
        echo "    </div>
";
        
        $__internal_6407b7b94bf12b9492f1cb5478baf89e451f305015bffd428d958d4e1d86e532->leave($__internal_6407b7b94bf12b9492f1cb5478baf89e451f305015bffd428d958d4e1d86e532_prof);

    }

    public function getTemplateName()
    {
        return "LaisoArmBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 20,  75 => 19,  72 => 18,  69 => 17,  66 => 16,  64 => 15,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }
}
/* {% extends "::base.html.twig" %}*/
/* */
/* {% block title %}Accueil{% endblock %}*/
/* */
/* {% block body %}*/
/*     <div class="laiso-bordered padding10 no-padding-bottom margin10 no-margin-bottom no-margin-left no-margin-right">*/
/*         <div class="align-center">*/
/*             <h3>*/
/*                 Bienvenue sur la plateforme SGPEC: <span class="fg-darkTeal">Système de Gestion de Projet d'Entretiens Courants des routes</span>*/
/*                 de l'Autorité Routière de Madagascar*/
/*             </h3>*/
/*             <hr class="thin" width="30%">*/
/*         </div>*/
/* */
/*         {% if app.user.hasRole('ROLE_ADMIN') %}*/
/*             {% include '@LaisoArm/Default/menuAdmin.html.twig' %}*/
/*         {% elseif app.user.hasRole('ROLE_DTEC') or app.user.hasRole('ROLE_CI') or app.user.hasRole('ROLE_ASSIST') %}*/
/*             {% include '@LaisoArm/Default/menuDtec.html.twig' %}*/
/*         {% endif %}*/
/*     </div>*/
/* {% endblock %}*/
