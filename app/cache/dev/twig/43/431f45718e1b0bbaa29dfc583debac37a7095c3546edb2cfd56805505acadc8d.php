<?php

/* FOSUserBundle:Resetting:reset.html.twig */
class __TwigTemplate_696e1f22396a3318ee51c1799bfeb39ec27342674f0674b109764d958e10175b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::login.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::login.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8423aacdac0d512e98659e294309c493f004fdec0b0e795d4731023aa1ed5984 = $this->env->getExtension("native_profiler");
        $__internal_8423aacdac0d512e98659e294309c493f004fdec0b0e795d4731023aa1ed5984->enter($__internal_8423aacdac0d512e98659e294309c493f004fdec0b0e795d4731023aa1ed5984_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8423aacdac0d512e98659e294309c493f004fdec0b0e795d4731023aa1ed5984->leave($__internal_8423aacdac0d512e98659e294309c493f004fdec0b0e795d4731023aa1ed5984_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_3c25cc2762608c7b02e2b64642ee20da2ad5352b6bc28cd42ff783537f5ad8fb = $this->env->getExtension("native_profiler");
        $__internal_3c25cc2762608c7b02e2b64642ee20da2ad5352b6bc28cd42ff783537f5ad8fb->enter($__internal_3c25cc2762608c7b02e2b64642ee20da2ad5352b6bc28cd42ff783537f5ad8fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Resetting:reset_content.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 4)->display($context);
        
        $__internal_3c25cc2762608c7b02e2b64642ee20da2ad5352b6bc28cd42ff783537f5ad8fb->leave($__internal_3c25cc2762608c7b02e2b64642ee20da2ad5352b6bc28cd42ff783537f5ad8fb_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::login.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Resetting:reset_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
