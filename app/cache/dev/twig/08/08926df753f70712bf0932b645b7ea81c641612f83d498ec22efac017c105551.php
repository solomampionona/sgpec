<?php

/* LaisoArmBundle:Shared:paginator.html.twig */
class __TwigTemplate_64124592d859f1fde73b7d75e79414e08a48d8fd22b70a25333ddfabd5b8bb61 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1fc3ade76d94e39fd27d6eacfb62ce3ec52f57fa63542061b8188189dd04a260 = $this->env->getExtension("native_profiler");
        $__internal_1fc3ade76d94e39fd27d6eacfb62ce3ec52f57fa63542061b8188189dd04a260->enter($__internal_1fc3ade76d94e39fd27d6eacfb62ce3ec52f57fa63542061b8188189dd04a260_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "LaisoArmBundle:Shared:paginator.html.twig"));

        // line 1
        echo "<div class=\"button-group place-right\">
    ";
        // line 2
        echo $this->env->getExtension('knp_pagination')->render($this->env, (isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        echo "
</div>";
        
        $__internal_1fc3ade76d94e39fd27d6eacfb62ce3ec52f57fa63542061b8188189dd04a260->leave($__internal_1fc3ade76d94e39fd27d6eacfb62ce3ec52f57fa63542061b8188189dd04a260_prof);

    }

    public function getTemplateName()
    {
        return "LaisoArmBundle:Shared:paginator.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* <div class="button-group place-right">*/
/*     {{ knp_pagination_render(entities) }}*/
/* </div>*/
