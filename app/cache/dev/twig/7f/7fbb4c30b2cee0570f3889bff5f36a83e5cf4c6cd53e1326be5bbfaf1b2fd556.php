<?php

/* FOSUserBundle:Resetting:reset_content.html.twig */
class __TwigTemplate_e24be63782e684d6a34a62def2917c958441be229773b93769108fab81860e3d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e24901b85d0dac42dfad12732d237fa9c03c9d164e5cc1d07c9ed46b5538825e = $this->env->getExtension("native_profiler");
        $__internal_e24901b85d0dac42dfad12732d237fa9c03c9d164e5cc1d07c9ed46b5538825e->enter($__internal_e24901b85d0dac42dfad12732d237fa9c03c9d164e5cc1d07c9ed46b5538825e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset_content.html.twig"));

        // line 1
        echo "<div class=\"login-form padding20 block-shadow\">
    ";
        // line 3
        echo "    <h1 class=\"text-light\">Enter votre nouveau mot de passe</h1>
    <hr class=\"thin\">
    <br>
    ";
        // line 6
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("action" => $this->env->getExtension('routing')->getPath("fos_user_resetting_reset", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))), "attr" => array("class" => "fos_user_resetting_reset")));
        echo "
    <strong>Entrer le nouveau mot de passe</strong>
    <div class=\"input-control password full-size\" data-role=\"input\">
        ";
        // line 9
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "first", array(), "array"), 'widget');
        echo "
        <button class=\"button helper-button reveal\"><span class=\"mif-looks\"></span></button>
    </div>
    <strong>Confirmer le mot de passe</strong>
    <div class=\"input-control password full-size\" data-role=\"input\">
        ";
        // line 14
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "second", array(), "array"), 'widget');
        echo "
        <button class=\"button helper-button reveal\"><span class=\"mif-looks\"></span></button>
    </div>
    <div>
        <input class=\"button primary place-right\" type=\"submit\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.reset.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\"/>
    </div>
    <span class=\"clear-float\"></span>
    ";
        // line 21
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
</div>";
        
        $__internal_e24901b85d0dac42dfad12732d237fa9c03c9d164e5cc1d07c9ed46b5538825e->leave($__internal_e24901b85d0dac42dfad12732d237fa9c03c9d164e5cc1d07c9ed46b5538825e_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:reset_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 21,  51 => 18,  44 => 14,  36 => 9,  30 => 6,  25 => 3,  22 => 1,);
    }
}
/* <div class="login-form padding20 block-shadow">*/
/*     {% trans_default_domain 'FOSUserBundle' %}*/
/*     <h1 class="text-light">Enter votre nouveau mot de passe</h1>*/
/*     <hr class="thin">*/
/*     <br>*/
/*     {{ form_start(form, { 'action': path('fos_user_resetting_reset', {'token': token}), 'attr': { 'class': 'fos_user_resetting_reset' } }) }}*/
/*     <strong>Entrer le nouveau mot de passe</strong>*/
/*     <div class="input-control password full-size" data-role="input">*/
/*         {{ form_widget(form.plainPassword['first']) }}*/
/*         <button class="button helper-button reveal"><span class="mif-looks"></span></button>*/
/*     </div>*/
/*     <strong>Confirmer le mot de passe</strong>*/
/*     <div class="input-control password full-size" data-role="input">*/
/*         {{ form_widget(form.plainPassword['second']) }}*/
/*         <button class="button helper-button reveal"><span class="mif-looks"></span></button>*/
/*     </div>*/
/*     <div>*/
/*         <input class="button primary place-right" type="submit" value="{{ 'resetting.reset.submit'|trans }}"/>*/
/*     </div>*/
/*     <span class="clear-float"></span>*/
/*     {{ form_end(form) }}*/
/* </div>*/
