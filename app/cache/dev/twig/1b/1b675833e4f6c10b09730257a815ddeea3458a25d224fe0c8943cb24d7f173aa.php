<?php

/* FOSUserBundle:Security:login.html.twig */
class __TwigTemplate_002a9eaf2746f26ef109fe5aae2a6d3a8ca6f59424728b30ab84f6ce335a602a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::login.html.twig", "FOSUserBundle:Security:login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::login.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_52be1810632e5c9c7fdad5e2aef75b1a105ebbc3bf8195bdc62b4a722e00475c = $this->env->getExtension("native_profiler");
        $__internal_52be1810632e5c9c7fdad5e2aef75b1a105ebbc3bf8195bdc62b4a722e00475c->enter($__internal_52be1810632e5c9c7fdad5e2aef75b1a105ebbc3bf8195bdc62b4a722e00475c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Security:login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_52be1810632e5c9c7fdad5e2aef75b1a105ebbc3bf8195bdc62b4a722e00475c->leave($__internal_52be1810632e5c9c7fdad5e2aef75b1a105ebbc3bf8195bdc62b4a722e00475c_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_748c909ad19cc327b0efe56b730eaf962271ff743be8b6551f21ca7ddeb7170b = $this->env->getExtension("native_profiler");
        $__internal_748c909ad19cc327b0efe56b730eaf962271ff743be8b6551f21ca7ddeb7170b->enter($__internal_748c909ad19cc327b0efe56b730eaf962271ff743be8b6551f21ca7ddeb7170b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "

    <form action=\"";
        // line 8
        echo $this->env->getExtension('routing')->getPath("fos_user_security_check");
        echo "\" method=\"post\">
        <div class=\"login-form padding20 block-shadow\">
            <h1 class=\"text-light\">Authentification</h1>
            <hr class=\"thin\"/>

            ";
        // line 13
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 14
            echo "                <div class=\"fg-red\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "</div>
                <br>
            ";
        }
        // line 17
        echo "            <br>

            <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 19
        echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : $this->getContext($context, "csrf_token")), "html", null, true);
        echo "\"/>

            <div class=\"input-control text full-size\" data-role=\"input\">
                <label for=\"username\">";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("security.login.username", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
                <input type=\"text\" id=\"username\" name=\"_username\" value=\"";
        // line 23
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\" required=\"required\"/>
                <button class=\"button helper-button clear\"><span class=\"mif-cross\"></span></button>
            </div>
            <br/>
            <br/>

            <div class=\"input-control password full-size\" data-role=\"input\">
                <label for=\"password\">";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("security.login.password", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
                <input type=\"password\" id=\"password\" name=\"_password\" required=\"required\"/>
                <button class=\"button helper-button reveal\"><span class=\"mif-looks\"></span></button>
            </div>
            <br><br>
            <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\"/>
            <label for=\"remember_me\">";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("security.login.remember_me", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
            <br/>
            <br/>

            <hr class=\"thin\">
            <div class=\"form-actions\">
                <input type=\"submit\" class=\"button primary place-right\" id=\"_submit\" name=\"_submit\"
                       value=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("security.login.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\"/>
            </div>
            <div>
                <a class=\"button place-left\" href=\"";
        // line 46
        echo $this->env->getExtension('routing')->getPath("fos_user_resetting_request");
        echo "\">Mot de passe oublié?</a>
            </div>
            <span class='clear-float'></span>
        </div>
    </form>
";
        
        $__internal_748c909ad19cc327b0efe56b730eaf962271ff743be8b6551f21ca7ddeb7170b->leave($__internal_748c909ad19cc327b0efe56b730eaf962271ff743be8b6551f21ca7ddeb7170b_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 46,  104 => 43,  94 => 36,  85 => 30,  75 => 23,  71 => 22,  65 => 19,  61 => 17,  54 => 14,  52 => 13,  44 => 8,  40 => 6,  34 => 5,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::login.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/* */
/* */
/*     <form action="{{ path("fos_user_security_check") }}" method="post">*/
/*         <div class="login-form padding20 block-shadow">*/
/*             <h1 class="text-light">Authentification</h1>*/
/*             <hr class="thin"/>*/
/* */
/*             {% if error %}*/
/*                 <div class="fg-red">{{ error.messageKey|trans(error.messageData, 'security') }}</div>*/
/*                 <br>*/
/*             {% endif %}*/
/*             <br>*/
/* */
/*             <input type="hidden" name="_csrf_token" value="{{ csrf_token }}"/>*/
/* */
/*             <div class="input-control text full-size" data-role="input">*/
/*                 <label for="username">{{ 'security.login.username'|trans }}</label>*/
/*                 <input type="text" id="username" name="_username" value="{{ last_username }}" required="required"/>*/
/*                 <button class="button helper-button clear"><span class="mif-cross"></span></button>*/
/*             </div>*/
/*             <br/>*/
/*             <br/>*/
/* */
/*             <div class="input-control password full-size" data-role="input">*/
/*                 <label for="password">{{ 'security.login.password'|trans }}</label>*/
/*                 <input type="password" id="password" name="_password" required="required"/>*/
/*                 <button class="button helper-button reveal"><span class="mif-looks"></span></button>*/
/*             </div>*/
/*             <br><br>*/
/*             <input type="checkbox" id="remember_me" name="_remember_me" value="on"/>*/
/*             <label for="remember_me">{{ 'security.login.remember_me'|trans }}</label>*/
/*             <br/>*/
/*             <br/>*/
/* */
/*             <hr class="thin">*/
/*             <div class="form-actions">*/
/*                 <input type="submit" class="button primary place-right" id="_submit" name="_submit"*/
/*                        value="{{ 'security.login.submit'|trans }}"/>*/
/*             </div>*/
/*             <div>*/
/*                 <a class="button place-left" href="{{ path('fos_user_resetting_request') }}">Mot de passe oublié?</a>*/
/*             </div>*/
/*             <span class='clear-float'></span>*/
/*         </div>*/
/*     </form>*/
/* {% endblock fos_user_content %}*/
/* */
