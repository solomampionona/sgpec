<?php

/* @LaisoArm/DQE/dqe.html.twig */
class __TwigTemplate_2d8b7f35bf3b2f6e914c7cf793e9dc9d0cc548044af132ef5beaf9424f070ac3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ac1be0c5091c4cebab1515875bc15da27e1109dad526234f21cd8bb200006516 = $this->env->getExtension("native_profiler");
        $__internal_ac1be0c5091c4cebab1515875bc15da27e1109dad526234f21cd8bb200006516->enter($__internal_ac1be0c5091c4cebab1515875bc15da27e1109dad526234f21cd8bb200006516_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@LaisoArm/DQE/dqe.html.twig"));

        // line 17
        echo "
";
        // line 18
        $context["consistances"] = $this->getAttribute((isset($context["dqe"]) ? $context["dqe"] : $this->getContext($context, "dqe")), "consistances", array());
        // line 19
        $context["avenant"] = $this->getAttribute((isset($context["dqe"]) ? $context["dqe"] : $this->getContext($context, "dqe")), "avenant", array());
        // line 20
        echo "
";
        // line 21
        if (((isset($context["rubrique"]) ? $context["rubrique"] : $this->getContext($context, "rubrique")) == "dao")) {
            // line 22
            echo "    ";
            $this->loadTemplate("LaisoArmBundle:DQE/dao:dqe_dao.html.twig", "@LaisoArm/DQE/dqe.html.twig", 22)->display($context);
        } elseif ((        // line 23
(isset($context["rubrique"]) ? $context["rubrique"] : $this->getContext($context, "rubrique")) == "marche")) {
            // line 24
            echo "    ";
            $this->loadTemplate("LaisoArmBundle:DQE/marche:dqe_marche.html.twig", "@LaisoArm/DQE/dqe.html.twig", 24)->display($context);
        }
        
        $__internal_ac1be0c5091c4cebab1515875bc15da27e1109dad526234f21cd8bb200006516->leave($__internal_ac1be0c5091c4cebab1515875bc15da27e1109dad526234f21cd8bb200006516_prof);

    }

    public function getTemplateName()
    {
        return "@LaisoArm/DQE/dqe.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 24,  37 => 23,  34 => 22,  32 => 21,  29 => 20,  27 => 19,  25 => 18,  22 => 17,);
    }
}
/* {#*/
/*     Variables nécessaires pour afficher le DQE :*/
/*         * Laiso\ArmBundle\UtilsDQE dqe*/
/*         * Laiso\ArmBundle\Entity\Lotissement lotissement*/
/* */
/*     Afin de garder la lisibilité du code, on a séparé les différents modes d'affichage*/
/*     du DQE (avec ou sans modification autorisées) selon les critères de validité du*/
/*     marché, du lotissement et de l'avenant.*/
/* */
/*     REMARQUES IMPORTANTES:*/
/*         * lotissement.PuValide  : Prix unitaires valides **.*/
/*         * lotissement.valide    : Un marché. Ne peut être mis à true si lotissement.PuValide est encore à false*/
/*         * avenant.valide        : Quantités de travaux valides **.*/
/* */
/*     ** Les quantités et prix unitaires validés ne sont plus modifiables.*/
/* #}*/
/* */
/* {% set consistances = dqe.consistances %}*/
/* {% set avenant = dqe.avenant %}*/
/* */
/* {% if rubrique == 'dao' %}*/
/*     {% include ('LaisoArmBundle:DQE/dao:dqe_dao.html.twig') %}*/
/* {% elseif rubrique == 'marche' %}*/
/*     {% include ('LaisoArmBundle:DQE/marche:dqe_marche.html.twig') %}*/
/* {% endif %}*/
/* */
