<?php

/* LaisoArmBundle:DQE/dao:lotissement_valide.html.twig */
class __TwigTemplate_a82cca006f36c6d8daf04e75130f91c95184077f49b2e0ddd6d69da241c750a1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9b24c4f9d425475834fd3615f1bec1f1c5c8e2ad52b391ac0dbd56001ca2f892 = $this->env->getExtension("native_profiler");
        $__internal_9b24c4f9d425475834fd3615f1bec1f1c5c8e2ad52b391ac0dbd56001ca2f892->enter($__internal_9b24c4f9d425475834fd3615f1bec1f1c5c8e2ad52b391ac0dbd56001ca2f892_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "LaisoArmBundle:DQE/dao:lotissement_valide.html.twig"));

        // line 5
        echo "
";
        // line 6
        if ((twig_length_filter($this->env, (isset($context["consistances"]) ? $context["consistances"] : $this->getContext($context, "consistances"))) > 0)) {
            // line 7
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["consistances"]) ? $context["consistances"] : $this->getContext($context, "consistances")));
            foreach ($context['_seq'] as $context["_key"] => $context["consistance"]) {
                // line 8
                echo "        <div class=\"laiso-bordered padding10 bg-grayLighter margin10 no-margin-top no-margin-right no-margin-left\">
            <div class=\"laiso-bordered padding10 align-center bg-white\">
                <h4>
                    ";
                // line 11
                echo twig_escape_filter($this->env, $this->getAttribute($context["consistance"], "type", array()), "html", null, true);
                echo "
                </h4>
                <small><strong>Sites</strong></small>
                <br>
                ";
                // line 15
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["consistance"], "localisations", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["site"]) {
                    // line 16
                    echo "                    <small>Axe ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lotissement"]) ? $context["lotissement"] : $this->getContext($context, "lotissement")), "axe", array()), "html", null, true);
                    echo " - Entre le PK ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["site"], "pKDebut", array()), "html", null, true);
                    echo " et le PK ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["site"], "pKFin", array()), "html", null, true);
                    echo " - ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["site"], "localisation", array()), "html", null, true);
                    echo "</small>
                    <br>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['site'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 19
                echo "                <strong>Lot n° ";
                echo twig_escape_filter($this->env, (isset($context["lotissement"]) ? $context["lotissement"] : $this->getContext($context, "lotissement")), "html", null, true);
                echo "</strong>
                <br>
                ";
                // line 21
                echo twig_escape_filter($this->env, (( !(null === $this->getAttribute($context["consistance"], "libelle", array()))) ? (($this->getAttribute($context["consistance"], "libelle", array()) . "1")) : ("")), "html", null, true);
                echo "
            </div>
            ";
                // line 23
                if ((twig_length_filter($this->env, $this->getAttribute($context["consistance"], "prix", array())) > 0)) {
                    // line 24
                    echo "                <table class=\"bordered border table bg-white\">
                    <thead>
                    <tr>
                        <th style=\"width: 10%;\">Numéro</th>
                        <th>Désignation</th>
                        <th style=\"width: 10%;\">Unité</th>
                        <th style=\"width: 10%;\">Quantité</th>
                        <th style=\"width: 13%;\">Prix Unitaire</th>
                        <th style=\"width: 13%;\">Montant</th>
                    </tr>
                    </thead>
                    <tbody>
                    ";
                    // line 36
                    $context["current"] = "";
                    // line 37
                    echo "                    ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["consistance"], "prix", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["prix"]) {
                        // line 38
                        echo "                        ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["prix"], "lignesDqe", array()));
                        foreach ($context['_seq'] as $context["_key"] => $context["ligne"]) {
                            // line 39
                            echo "                            ";
                            if (($this->getAttribute($this->getAttribute($context["ligne"], "avenant", array()), "id", array()) == $this->getAttribute((isset($context["avenant"]) ? $context["avenant"] : $this->getContext($context, "avenant")), "id", array()))) {
                                // line 40
                                echo "                                ";
                                $context["serie"] = $this->getAttribute($this->getAttribute($context["ligne"], "prix", array()), "serie", array());
                                // line 41
                                echo "                                ";
                                $context["categorie"] = $this->getAttribute((isset($context["serie"]) ? $context["serie"] : $this->getContext($context, "serie")), "categorie", array());
                                // line 42
                                echo "                                ";
                                if (((isset($context["current"]) ? $context["current"] : $this->getContext($context, "current")) != $this->getAttribute((isset($context["categorie"]) ? $context["categorie"] : $this->getContext($context, "categorie")), "designation", array()))) {
                                    // line 43
                                    echo "                                    <tr class=\"bg-grayLighter\">
                                        <td><strong>";
                                    // line 44
                                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["categorie"]) ? $context["categorie"] : $this->getContext($context, "categorie")), "numero", array()), "html", null, true);
                                    echo "</strong></td>
                                        <td colspan=\"5\"><strong>";
                                    // line 45
                                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["categorie"]) ? $context["categorie"] : $this->getContext($context, "categorie")), "designation", array()), "html", null, true);
                                    echo "</strong></td>
                                    </tr>
                                    ";
                                    // line 47
                                    $context["current"] = $this->getAttribute((isset($context["categorie"]) ? $context["categorie"] : $this->getContext($context, "categorie")), "designation", array());
                                    // line 48
                                    echo "                                ";
                                }
                                // line 49
                                echo "                                <tr>
                                    <td>";
                                // line 50
                                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["serie"]) ? $context["serie"] : $this->getContext($context, "serie")), "numero", array()), "html", null, true);
                                echo "</td>
                                    <td>";
                                // line 51
                                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["serie"]) ? $context["serie"] : $this->getContext($context, "serie")), "designation", array()), "html", null, true);
                                echo "</td>
                                    <td class=\"align-center\">";
                                // line 52
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["serie"]) ? $context["serie"] : $this->getContext($context, "serie")), "unite", array()), "abbrege", array()), "html", null, true);
                                echo "</td>
                                    <td class=\"align-right\">";
                                // line 53
                                echo twig_escape_filter($this->env, $this->env->getExtension('sgpec_extension')->separer($this->getAttribute($context["ligne"], "quantite", array())), "html", null, true);
                                echo "</td>
                                    <td class=\"align-right\">
                                        ";
                                // line 55
                                echo twig_escape_filter($this->env, $this->env->getExtension('sgpec_extension')->separer($this->getAttribute($this->getAttribute($context["ligne"], "prix", array()), "prixUnitaire", array())), "html", null, true);
                                echo "
                                    </td>
                                    <td class=\"align-right\">
                                        ";
                                // line 58
                                if ($this->getAttribute($this->getAttribute($context["ligne"], "prix", array()), "prixUnitaire", array())) {
                                    // line 59
                                    echo "                                            ";
                                    echo twig_escape_filter($this->env, $this->env->getExtension('sgpec_extension')->separer(($this->getAttribute($this->getAttribute($context["ligne"], "prix", array()), "prixUnitaire", array()) * $this->getAttribute($context["ligne"], "quantite", array()))), "html", null, true);
                                    echo "
                                        ";
                                }
                                // line 61
                                echo "                                    </td>
                                </tr>
                            ";
                            }
                            // line 64
                            echo "                        ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ligne'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 65
                        echo "                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['prix'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 66
                    echo "                    </tbody>
                </table>
            ";
                } else {
                    // line 69
                    echo "                <div class=\"align-center laiso-bordered padding10 margin10 no-margin-bottom no-margin-right no-margin-left bg-white\">
                    <h3>Aucune ligne trouvée dans cette consistance.</h3>
                </div>
            ";
                }
                // line 73
                echo "        </div>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['consistance'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } else {
            // line 76
            echo "    <div class=\"align-center laiso-bordered padding10 margin10 no-margin-top no-margin-right no-margin-left\">
        <h3>Aucune consistance trouvée dans ce lot.</h3>
    </div>
";
        }
        
        $__internal_9b24c4f9d425475834fd3615f1bec1f1c5c8e2ad52b391ac0dbd56001ca2f892->leave($__internal_9b24c4f9d425475834fd3615f1bec1f1c5c8e2ad52b391ac0dbd56001ca2f892_prof);

    }

    public function getTemplateName()
    {
        return "LaisoArmBundle:DQE/dao:lotissement_valide.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  202 => 76,  194 => 73,  188 => 69,  183 => 66,  177 => 65,  171 => 64,  166 => 61,  160 => 59,  158 => 58,  152 => 55,  147 => 53,  143 => 52,  139 => 51,  135 => 50,  132 => 49,  129 => 48,  127 => 47,  122 => 45,  118 => 44,  115 => 43,  112 => 42,  109 => 41,  106 => 40,  103 => 39,  98 => 38,  93 => 37,  91 => 36,  77 => 24,  75 => 23,  70 => 21,  64 => 19,  48 => 16,  44 => 15,  37 => 11,  32 => 8,  27 => 7,  25 => 6,  22 => 5,);
    }
}
/* {#*/
/*     Lotissement validé:*/
/*         Aucune modification ne peut être apportée*/
/* #}*/
/* */
/* {% if consistances | length > 0 %}*/
/*     {% for consistance in consistances %}*/
/*         <div class="laiso-bordered padding10 bg-grayLighter margin10 no-margin-top no-margin-right no-margin-left">*/
/*             <div class="laiso-bordered padding10 align-center bg-white">*/
/*                 <h4>*/
/*                     {{ consistance.type }}*/
/*                 </h4>*/
/*                 <small><strong>Sites</strong></small>*/
/*                 <br>*/
/*                 {% for site in consistance.localisations %}*/
/*                     <small>Axe {{ lotissement.axe }} - Entre le PK {{ site.pKDebut }} et le PK {{ site.pKFin }} - {{ site.localisation }}</small>*/
/*                     <br>*/
/*                 {% endfor %}*/
/*                 <strong>Lot n° {{ lotissement }}</strong>*/
/*                 <br>*/
/*                 {{ consistance.libelle is not null ? consistance.libelle ~"1" : "" }}*/
/*             </div>*/
/*             {% if consistance.prix | length > 0 %}*/
/*                 <table class="bordered border table bg-white">*/
/*                     <thead>*/
/*                     <tr>*/
/*                         <th style="width: 10%;">Numéro</th>*/
/*                         <th>Désignation</th>*/
/*                         <th style="width: 10%;">Unité</th>*/
/*                         <th style="width: 10%;">Quantité</th>*/
/*                         <th style="width: 13%;">Prix Unitaire</th>*/
/*                         <th style="width: 13%;">Montant</th>*/
/*                     </tr>*/
/*                     </thead>*/
/*                     <tbody>*/
/*                     {% set current = "" %}*/
/*                     {% for prix in consistance.prix %}*/
/*                         {% for ligne in prix.lignesDqe %}*/
/*                             {% if ligne.avenant.id == avenant.id %}*/
/*                                 {% set serie = ligne.prix.serie %}*/
/*                                 {% set categorie = serie.categorie %}*/
/*                                 {% if current != categorie.designation %}*/
/*                                     <tr class="bg-grayLighter">*/
/*                                         <td><strong>{{ categorie.numero }}</strong></td>*/
/*                                         <td colspan="5"><strong>{{ categorie.designation }}</strong></td>*/
/*                                     </tr>*/
/*                                     {% set current = categorie.designation %}*/
/*                                 {% endif %}*/
/*                                 <tr>*/
/*                                     <td>{{ serie.numero }}</td>*/
/*                                     <td>{{ serie.designation }}</td>*/
/*                                     <td class="align-center">{{ serie.unite.abbrege }}</td>*/
/*                                     <td class="align-right">{{ ligne.quantite | separer }}</td>*/
/*                                     <td class="align-right">*/
/*                                         {{ ligne.prix.prixUnitaire | separer }}*/
/*                                     </td>*/
/*                                     <td class="align-right">*/
/*                                         {% if ligne.prix.prixUnitaire %}*/
/*                                             {{ (ligne.prix.prixUnitaire * ligne.quantite) | separer }}*/
/*                                         {% endif %}*/
/*                                     </td>*/
/*                                 </tr>*/
/*                             {% endif %}*/
/*                         {% endfor %}*/
/*                     {% endfor %}*/
/*                     </tbody>*/
/*                 </table>*/
/*             {% else %}*/
/*                 <div class="align-center laiso-bordered padding10 margin10 no-margin-bottom no-margin-right no-margin-left bg-white">*/
/*                     <h3>Aucune ligne trouvée dans cette consistance.</h3>*/
/*                 </div>*/
/*             {% endif %}*/
/*         </div>*/
/*     {% endfor %}*/
/* {% else %}*/
/*     <div class="align-center laiso-bordered padding10 margin10 no-margin-top no-margin-right no-margin-left">*/
/*         <h3>Aucune consistance trouvée dans ce lot.</h3>*/
/*     </div>*/
/* {% endif %}*/
