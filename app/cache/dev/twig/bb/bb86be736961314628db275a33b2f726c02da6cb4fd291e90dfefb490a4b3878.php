<?php

/* FOSUserBundle::layout.html.twig */
class __TwigTemplate_fe3fb7fdb0c1646aa2861068f5cb9030251389e05de9e892e30e56abdc547e1d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "FOSUserBundle::layout.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e343e95c0b10c8882d300a089d19c45234d0f85590d66d7a532ca63262355c02 = $this->env->getExtension("native_profiler");
        $__internal_e343e95c0b10c8882d300a089d19c45234d0f85590d66d7a532ca63262355c02->enter($__internal_e343e95c0b10c8882d300a089d19c45234d0f85590d66d7a532ca63262355c02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle::layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e343e95c0b10c8882d300a089d19c45234d0f85590d66d7a532ca63262355c02->leave($__internal_e343e95c0b10c8882d300a089d19c45234d0f85590d66d7a532ca63262355c02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_a8a0e64a0b4a10a0ff8388138f5e3abb2e003c2b66c63ab5f2d0cf16779fa49d = $this->env->getExtension("native_profiler");
        $__internal_a8a0e64a0b4a10a0ff8388138f5e3abb2e003c2b66c63ab5f2d0cf16779fa49d->enter($__internal_a8a0e64a0b4a10a0ff8388138f5e3abb2e003c2b66c63ab5f2d0cf16779fa49d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "all", array()));
        foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
            // line 4
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["messages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 5
                echo "            <div class=\"";
                echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                echo "\">
                ";
                // line 6
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($context["message"], array(), "FOSUserBundle"), "html", null, true);
                echo "
            </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 9
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        echo "
    <div>
        ";
        // line 12
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 14
        echo "    </div>
";
        
        $__internal_a8a0e64a0b4a10a0ff8388138f5e3abb2e003c2b66c63ab5f2d0cf16779fa49d->leave($__internal_a8a0e64a0b4a10a0ff8388138f5e3abb2e003c2b66c63ab5f2d0cf16779fa49d_prof);

    }

    // line 12
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_84a900633ad527e0f4bebefccfb12ac7511fdee7e505e76c473b9c7e1027ec79 = $this->env->getExtension("native_profiler");
        $__internal_84a900633ad527e0f4bebefccfb12ac7511fdee7e505e76c473b9c7e1027ec79->enter($__internal_84a900633ad527e0f4bebefccfb12ac7511fdee7e505e76c473b9c7e1027ec79_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 13
        echo "        ";
        
        $__internal_84a900633ad527e0f4bebefccfb12ac7511fdee7e505e76c473b9c7e1027ec79->leave($__internal_84a900633ad527e0f4bebefccfb12ac7511fdee7e505e76c473b9c7e1027ec79_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 13,  85 => 12,  77 => 14,  75 => 12,  71 => 10,  65 => 9,  56 => 6,  51 => 5,  46 => 4,  41 => 3,  35 => 2,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/*     {% for type, messages in app.session.flashBag.all %}*/
/*         {% for message in messages %}*/
/*             <div class="{{ type }}">*/
/*                 {{ message|trans({}, 'FOSUserBundle') }}*/
/*             </div>*/
/*         {% endfor %}*/
/*     {% endfor %}*/
/* */
/*     <div>*/
/*         {% block fos_user_content %}*/
/*         {% endblock fos_user_content %}*/
/*     </div>*/
/* {% endblock %}*/
/* */
