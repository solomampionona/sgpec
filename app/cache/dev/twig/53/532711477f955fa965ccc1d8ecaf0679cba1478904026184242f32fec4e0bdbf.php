<?php

/* FOSUserBundle::login.html.twig */
class __TwigTemplate_6f1d008319e38134d77a3274c3ebb2e2adf2af4db36d115c8d918933466a99fc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ccf1a6f810380c581812709769f6f5ed065bd68a294a0a4386659bed38b0138d = $this->env->getExtension("native_profiler");
        $__internal_ccf1a6f810380c581812709769f6f5ed065bd68a294a0a4386659bed38b0138d->enter($__internal_ccf1a6f810380c581812709769f6f5ed065bd68a294a0a4386659bed38b0138d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle::login.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\">
    <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\"/>
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\">

    <title>SGPEC ARM :: Authentification</title>

    <link rel=\"stylesheet\" media=\"all\" type=\"text/css\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/metro-ui/css/metro.min.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" media=\"all\" type=\"text/css\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/metro-ui/css/metro-responsive.min.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" media=\"all\" type=\"text/css\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/metro-ui/css/metro-rtl.min.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" media=\"all\" type=\"text/css\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/metro-ui/css/metro-schemes.min.css"), "html", null, true);
        echo "\">
    <link rel=\"stylesheet\" media=\"all\" type=\"text/css\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/metro-ui/css/metro-icons.min.css"), "html", null, true);
        echo "\">


    <script language=\"JavaScript\" type=\"text/javascript\" src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/scripts/jquery-2.1.3.min.js"), "html", null, true);
        echo "\"></script>
    <script language=\"JavaScript\" type=\"text/javascript\" src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("public/metro-ui/js/metro.min.js"), "html", null, true);
        echo "\"></script>

    <style>
        .login-form {
            width: 29rem;
            /*position: fixed;
            top: 50%;
            margin-top: -15rem;
            left: 50%;
            margin-left: -12.5rem;*/
            margin: 10em auto 0 auto;
            background-color: #ffffff;
        }
    </style>
</head>
<body class=\"bg-grayDark\">

";
        // line 36
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 39
        echo "<span class='clear-float'></span>
</body>
</html>";
        
        $__internal_ccf1a6f810380c581812709769f6f5ed065bd68a294a0a4386659bed38b0138d->leave($__internal_ccf1a6f810380c581812709769f6f5ed065bd68a294a0a4386659bed38b0138d_prof);

    }

    // line 36
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_0133a9308108021f69b7cd1831519661f815b2b0cbf0758a71f98ca9908fa661 = $this->env->getExtension("native_profiler");
        $__internal_0133a9308108021f69b7cd1831519661f815b2b0cbf0758a71f98ca9908fa661->enter($__internal_0133a9308108021f69b7cd1831519661f815b2b0cbf0758a71f98ca9908fa661_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 37
        echo "
";
        
        $__internal_0133a9308108021f69b7cd1831519661f815b2b0cbf0758a71f98ca9908fa661->leave($__internal_0133a9308108021f69b7cd1831519661f815b2b0cbf0758a71f98ca9908fa661_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle::login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 37,  95 => 36,  86 => 39,  84 => 36,  64 => 19,  60 => 18,  54 => 15,  50 => 14,  46 => 13,  42 => 12,  38 => 11,  29 => 5,  23 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/* <head>*/
/*     <meta charset="UTF-8">*/
/*     <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}"/>*/
/*     <meta http-equiv="X-UA-Compatible" content="IE=edge">*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">*/
/* */
/*     <title>SGPEC ARM :: Authentification</title>*/
/* */
/*     <link rel="stylesheet" media="all" type="text/css" href="{{ asset('public/metro-ui/css/metro.min.css') }}">*/
/*     <link rel="stylesheet" media="all" type="text/css" href="{{ asset('public/metro-ui/css/metro-responsive.min.css') }}">*/
/*     <link rel="stylesheet" media="all" type="text/css" href="{{ asset('public/metro-ui/css/metro-rtl.min.css') }}">*/
/*     <link rel="stylesheet" media="all" type="text/css" href="{{ asset('public/metro-ui/css/metro-schemes.min.css') }}">*/
/*     <link rel="stylesheet" media="all" type="text/css" href="{{ asset('public/metro-ui/css/metro-icons.min.css') }}">*/
/* */
/* */
/*     <script language="JavaScript" type="text/javascript" src="{{ asset('public/scripts/jquery-2.1.3.min.js') }}"></script>*/
/*     <script language="JavaScript" type="text/javascript" src="{{ asset('public/metro-ui/js/metro.min.js') }}"></script>*/
/* */
/*     <style>*/
/*         .login-form {*/
/*             width: 29rem;*/
/*             /*position: fixed;*/
/*             top: 50%;*/
/*             margin-top: -15rem;*/
/*             left: 50%;*/
/*             margin-left: -12.5rem;*//* */
/*             margin: 10em auto 0 auto;*/
/*             background-color: #ffffff;*/
/*         }*/
/*     </style>*/
/* </head>*/
/* <body class="bg-grayDark">*/
/* */
/* {% block fos_user_content %}*/
/* */
/* {% endblock %}*/
/* <span class='clear-float'></span>*/
/* </body>*/
/* </html>*/
