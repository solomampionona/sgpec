<?php

namespace Laiso\ArmBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Laiso\ArmBundle\Entity\Intervenant;
use Laiso\ArmBundle\Form\IntervenantType;

/**
 * Intervenant controller.
 *
 */
class IntervenantController extends Controller
{
    private function verifyAccess()
    {
        if (!($this->getUser()->hasRole('ROLE_DTEC') || $this->getUser()->hasRole('ROLE_CI') || $this->getUser()->hasRole('ROLE_ASSIST')))
            throw new AccessDeniedHttpException("Vous n'avez pas le droit d'accéder à ce ressource");
    }

    /**
     * Lists all Intervenant entities.
     *
     */
    public function indexAction()
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LaisoArmBundle:Intervenant')->findAll();

        return $this->render('LaisoArmBundle:Intervenant:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Intervenant entity.
     *
     */
    public function createAction(Request $request)
    {
        $this->verifyAccess();

        $entity = new Intervenant();
        $returnUrl = $request->query->get('returnUrl');

        $form = $this->createCreateForm($entity, $returnUrl);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Intervenant ajouté avec succès');

            return $returnUrl ? $this->redirect($returnUrl) : $this->redirect($this->generateUrl('intervenant'));
        }

        return $this->render('LaisoArmBundle:Intervenant:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Intervenant entity.
     *
     * @param Intervenant $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Intervenant $entity, $returnUrl)
    {
        $form = $this->createForm(new IntervenantType(), $entity, array(
            'action' => $this->generateUrl(
                'intervenant_create', array(
                    'returnUrl' => $returnUrl
                )
            ),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Enregistrer', 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }

    /**
     * Displays a form to create a new Intervenant entity.
     *
     */
    public function newAction(Request $request)
    {
        $this->verifyAccess();

        $entity = new Intervenant();
        $returnUrl = $request->query->get('returnUrl');

        $form   = $this->createCreateForm($entity, $returnUrl);

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:Intervenant/includes:new_intervenant.html.twig', array(
                'entity' => $entity,
                'form'   => $form->createView(),
            ));

        return $this->render('LaisoArmBundle:Intervenant:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Intervenant entity.
     *
     */
    public function showAction($id)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:Intervenant')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Intervenant entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('LaisoArmBundle:Intervenant:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Intervenant entity.
     *
     */
    public function editAction(Request $request, $id)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:Intervenant')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Intervenant entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);


        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:Intervenant/includes:edit_intervenant.html.twig', array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));

        return $this->render('LaisoArmBundle:Intervenant:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Intervenant entity.
    *
    * @param Intervenant $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Intervenant $entity)
    {
        $form = $this->createForm(new IntervenantType(), $entity, array(
            'action' => $this->generateUrl('intervenant_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Mettre à jour', 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }
    /**
     * Edits an existing Intervenant entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:Intervenant')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Intervenant entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('intervenant_edit', array('id' => $id)));
        }

        return $this->render('LaisoArmBundle:Intervenant:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Intervenant entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $this->verifyAccess();
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('LaisoArmBundle:Intervenant')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Intervenant entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('intervenant'));
    }

    /**
     * Creates a form to delete a Intervenant entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('intervenant_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Supprimer', 'attr' => array(
                'class' => 'button danger place-right'
            )))
            ->getForm()
        ;
    }
}
