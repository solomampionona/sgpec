<?php

namespace Laiso\ArmBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Laiso\ArmBundle\Entity\LigneSuivi;
use Laiso\ArmBundle\Form\LigneSuiviType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * LigneSuivi controller.
 *
 */
class LigneSuiviController extends Controller
{

    /**
     * Lists all LigneSuivi entities.
     *
     */
    public function indexAction($marcheId, $decompteId)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LaisoArmBundle:LigneSuivi')->findBy(array(
            'decompte' => $decompteId
        ));

        /** @var \Laiso\ArmBundle\Entity\Decompte $decompte */
        $decompte = $em->getRepository('LaisoArmBundle:Decompte')->find($decompteId);

        if (!$decompte)
            throw new NotFoundHttpException('Unable to find Decompte Entity');

        return $this->render('LaisoArmBundle:LigneSuivi:index.html.twig', array(
            'entities' => $entities,
            'decompte' => $decompte,
        ));
    }

    /**
     * Creates a new LigneSuivi entity.
     *
     */
    public function createAction(Request $request, $marcheId, $decompteId)
    {
        $entity = new LigneSuivi();
        $em = $this->getDoctrine()->getManager();
        $decompte = $em->getRepository('LaisoArmBundle:Decompte')->find($decompteId);

        if(!$decompte)
            throw new NotFoundHttpException('Unable to find Decompte entity');

        $entity->setDecompte($decompte);

        $form = $this->createCreateForm($entity, $marcheId, $decompteId);
        $form->handleRequest($request);

        /** @var \Laiso\ArmBundle\Entity\LigneSuivi $last */
        $last = $decompte->getLigneSuivis()[count($decompte->getLigneSuivis()) - 1];

        if ($form->isValid()) {
            if($last != null && $entity->getDateEntree() < $last->getDateSortie()){
                $this->addFlash('error', 'La date d\'entrée doit être supérieure à la date de sortie de la dernière intervention');
            }
            elseif ($entity->getDateEntree() > $entity->getDateSortie()) {
                $this->addFlash('error', 'La date de sortie doit être supérieure ou égale à la date d\'entrée');
            }
            else{
                $entity->setNumero($last == null ? 1 : $last->getNumero() + 1);
                $entity->setValide(false);

                $em->persist($entity);
                $em->flush();

                $this->addFlash('success', 'Intervention ajoutée avec succès');

                return $this->redirect($this->generateUrl('suivi', array(
                    'decompteId' => $decompteId,
                    'marcheId' => $marcheId
                )));
            }
        }

        return $this->render('LaisoArmBundle:LigneSuivi:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a LigneSuivi entity.
     *
     * @param LigneSuivi $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(LigneSuivi $entity, $marcheId, $decompteId)
    {
        $form = $this->createForm(new LigneSuiviType(), $entity, array(
            'action' => $this->generateUrl('suivi_create', array(
                'decompteId' => $decompteId,
                'marcheId' => $marcheId
            )),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Enregistrer', 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }

    /**
     * Displays a form to create a new LigneSuivi entity.
     *
     */
    public function newAction(Request $request, $marcheId, $decompteId)
    {
        $entity = new LigneSuivi();

        /** @var \Laiso\ArmBundle\Entity\Decompte $decompte */
        $decompte = $this->getDoctrine()->getRepository('LaisoArmBundle:Decompte')->find($decompteId);

        if (!$decompte)
            throw new NotFoundHttpException('Unable to find Decompte Entity');
        $entity->setDecompte($decompte);

        $allowed = $decompte->isFerme();

        $form = $this->createCreateForm($entity, $marcheId, $decompteId);

        if ($request->isXmlHttpRequest()){
            if(!$allowed)
                return new Response('<h3 class="align-center fg-orange">Vous ne pouvez pas remplir la fiche de suivi tant que le décompte ne soit pas clôturé.</h3>', 403);

            return $this->render('LaisoArmBundle:LigneSuivi/includes:new_ligneSuivi.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView(),
            ));
        }

        if(!$allowed){
            $this->addFlash('error', 'Vous ne pouvez pas remplir la fiche de suivi tant que le décompte ne soit pas clôturé.');

            return $this->redirect($this->generateUrl('decompte_show', array(
                'decompteId' => $decompteId,
                'marcheId' => $marcheId,
            )));
        }

        return $this->render('LaisoArmBundle:LigneSuivi:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing LigneSuivi entity.
     *
     */
    public function editAction(Request $request, $id, $marcheId, $decompteId)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:LigneSuivi')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find LigneSuivi entity.');
        }

        $editForm = $this->createEditForm($entity, $marcheId, $decompteId);
        $deleteForm = $this->createDeleteForm($id, $marcheId, $decompteId);

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:LigneSuivi/includes:edit_ligneSuivi.html.twig', array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));

        return $this->render('LaisoArmBundle:LigneSuivi:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a LigneSuivi entity.
     *
     * @param LigneSuivi $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(LigneSuivi $entity, $marcheId, $decompteId)
    {
        $form = $this->createForm(new LigneSuiviType(), $entity, array(
            'action' => $this->generateUrl('suivi_update', array(
                'id' => $entity->getId(),
                'marcheId' => $marcheId,
                'decompteId' => $decompteId
            )),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Mettre à jour', 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }

    /**
     * Edits an existing LigneSuivi entity.
     *
     */
    public function updateAction(Request $request, $id, $marcheId, $decompteId)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:LigneSuivi')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find LigneSuivi entity.');
        }

        $deleteForm = $this->createDeleteForm($id, $marcheId, $decompteId);
        $editForm = $this->createEditForm($entity, $marcheId, $decompteId);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('suivi_edit', array(
                'id' => $id,
                'marcheId' => $marcheId,
                'decompteId' => $decompteId
            )));
        }

        return $this->render('LaisoArmBundle:LigneSuivi:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a LigneSuivi entity.
     *
     */
    public function deleteAction(Request $request, $id, $marcheId, $decompteId)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('LaisoArmBundle:LigneSuivi')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find LigneSuivi entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('suivi', array(
            'marcheId' => $marcheId,
            'decompteId' => $decompteId
        )));
    }

    /**
     * Creates a form to delete a LigneSuivi entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id, $marcheId, $decompteId)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('suivi_delete', array(
                'id' => $id,
                'marcheId' => $marcheId,
                'decompteId' => $decompteId
            )))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Supprimer'))
            ->getForm();
    }
}
