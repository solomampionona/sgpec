<?php

namespace Laiso\ArmBundle\Controller;

use Laiso\ArmBundle\Entity\Avenant;
use Laiso\ArmBundle\Entity\Axe;
use Laiso\ArmBundle\Entity\Marche;
use Laiso\ArmBundle\Utils\DQE;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Laiso\ArmBundle\Entity\Lotissement;
use Laiso\ArmBundle\Form\LotissementType;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Lotissement controller.
 *
 */
class LotissementController extends Controller
{
    private function verifyAccess()
    {
        if (!($this->getUser()->hasRole('ROLE_DTEC') || $this->getUser()->hasRole('ROLE_CI') || $this->getUser()->hasRole('ROLE_ASSIST')))
            throw new AccessDeniedHttpException("Vous n'avez pas le droit d'accéder à ce ressource");
    }

    /**
     * Lists all Lotissement entities.
     *
     */
    public function indexAction(Request $request)
    {
        $this->verifyAccess();

        $entities = $this->getDoctrine()->getRepository("LaisoArmBundle:Lotissement")->findAll();
        $paginator = $this->get('knp_paginator');

        return $this->render('LaisoArmBundle:Lotissement:index.html.twig', array(
            'entities' => $paginator->paginate($entities, $request->query->getInt('page', 1), 10)
        ));
    }

    /**
     * Creates a new Lotissement entity.
     *
     *
     */
    public function createAction(Request $request)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();
        $entity = new Lotissement();

        $marche = new Marche();
        $avenant = new Avenant();

        $libelleZero = $em->getRepository("LaisoArmBundle:LibelleAvenant")->findOneByNumero(0);

        $avenant->setLibelle($libelleZero);
        $avenant->setMarche($marche);
        $avenant->setMotif("Marché initial");
        $avenant->setDelai($entity->getDelai());

        $entity->setValide(false);
        $entity->setPuValide(false);
        $avenant->setValide(false);


        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);


        if ($form->isValid()) {
            $em->persist($entity);

            $avenant->setDelai($entity->getDelai());
            $marche->setLotissement($entity);
            $marche->setEntrepriseAssujettie(false);

            $em->persist($marche);
            $em->persist($avenant);

            $em->flush();

            return $this->redirect($this->generateUrl('dao_show', array('id' => $entity->getId())));
        }

        return $this->render('LaisoArmBundle:Lotissement:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Lotissement entity.
     *
     * @param Lotissement $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Lotissement $entity)
    {
        $this->verifyAccess();

        $form = $this->createForm(new LotissementType(), $entity, array(
            'action' => $this->generateUrl('dao_create'),
            'method' => 'POST',
        ));

        $form
            ->add('submit', 'submit', array('label' => 'Enregistrer', 'attr' => array(
                'class' => 'button success place-right'
            )));

        return $form;
    }

    /**
     * Get max value of numero
     *
     * @return int
     */
    private function getMaxnumero()
    {
        $all = $this->getDoctrine()->getRepository("LaisoArmBundle:Lotissement")->findAll();
        $max = 0;
        foreach ($all as $e) {
            if ($e->getNumero() > $max)
                $max = $e->getNumero();
        }
        return $max;
    }

    /**
     * @return mixed
     */
    private function getBloc()
    {
        return $this->getUser()->getBloc();
    }

    /**
     * Displays a form to create a new Lotissement entity.
     *
     */
    public function newAction(Request $request)
    {

        $this->verifyAccess();

        $entity = new Lotissement();

        $entity->setNumero($this->getMaxnumero() + 1);

        $form = $this->createCreateForm($entity);

        if ($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:Lotissement/includes:new_lotissement.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView(),
                'page' => 'ajax'
            ));

        return $this->render('LaisoArmBundle:Lotissement:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Lotissement entity.
     *
     */
    public function showAction(Request $request, Lotissement $entity)
    {
        $this->verifyAccess();

        $deleteForm = $this->createDeleteForm($entity->getId());

        $dqe = new DQE();
        $dqe->setAvenant($this->getDoctrine()->getRepository("LaisoArmBundle:Avenant")->findBy(
            array('marche' => $this->getDoctrine()->getRepository("LaisoArmBundle:Marche")->findBy(
                array('lotissement' => $entity->getId())
            ))
        )[0]);
        $dqe->setConsistances($entity->getConsistances());

        return $this->render('LaisoArmBundle:Lotissement:show.html.twig', array(
            'entity' => $entity,
            'dqe' => $dqe,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function validateAction(Request $request, Lotissement $lotissement)
    {
        $this->verifyAccess();

        $lotissement->setValide(false);
        $this->getDoctrine()->getEntityManager()->persist($lotissement);
        $this->getDoctrine()->getEntityManager()->flush();


        if ($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:Lotissement/includes:dqe_lotissement_sans_prix.html.twig', array(
                'lotissement' => $lotissement,
            ));

        return $this->redirect($this->generateUrl('dao_show', array(
            'id' => $lotissement->getId(),
        )));
    }

    public function validateQteAction(Request $request, Lotissement $lotissement)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();
        $avenant = $em->getRepository('LaisoArmBundle:Avenant')->findOneBy(array(
            'libelle' => $em->getRepository('LaisoArmBundle:LibelleAvenant')->findOneBy(array(
                'numero' => 0,
            )),
            'marche' => $em->getRepository('LaisoArmBundle:Marche')->findOneBy(array(
                'lotissement' => $lotissement,
            )),
        ));

        $avenant->setValide(true);
        $em->persist($avenant);
        $em->flush();
        
        return $this->redirect($this->generateUrl('dao_show', array('id' => $lotissement->getId())));
    }

    /**
     * Displays a form to edit an existing Lotissement entity.
     *
     */
    public function editAction(Request $request, Lotissement $entity)
    {
        $this->verifyAccess();

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($entity->getId());

        if ($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:Lotissement/includes:edit_lotissement.html.twig', array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
                'page' => 'ajax',
            ));

        return $this->render('LaisoArmBundle:Lotissement:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Lotissement entity.
     *
     * @param Lotissement $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Lotissement $entity)
    {
        $this->verifyAccess();

        $form = $this->createForm(new LotissementType(), $entity, array(
            'action' => $this->generateUrl('dao_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Mettre à  jour', 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }

    /**
     * Edits an existing Lotissement entity.
     *
     */
    public function updateAction(Request $request, Lotissement $entity)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();

        $deleteForm = $this->createDeleteForm($entity->getId());
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('dao'));
        }

        return $this->render('LaisoArmBundle:Lotissement:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Lotissement entity.
     *
     */
    public function deleteAction(Request $request, Lotissement $entity)
    {
        $this->verifyAccess();

        $form = $this->createDeleteForm($entity->getId());
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('dao'));
    }

    /**
     * Creates a form to delete a Lotissement entity by id.
     *
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('dao_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Supprimer', 'attr' => array(
                'class' => 'button danger place-right'
            )))
            ->getForm();
    }

    /**
     * Handle Ajax request for deletion
     *
     * (c) Laiso
     *
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteAjaxAction(Request $request, $id)
    {
        $this->verifyAccess();

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        $entity = $this->getDoctrine()->getRepository('LaisoArmBundle:Lotissement')->find($id);
        if (!$entity)
            return $this->createNotFoundException("Lotissement introuvable");

        if ($request->isXmlHttpRequest())
            return $this->render("LaisoArmBundle:Shared:delete_ajax.html.twig", array(
                'delete_form' => $form->createView(),
                'title' => "Supprimer le lot n°" . $entity
            ));
        else return $this->redirectToRoute("dao");
    }
}
