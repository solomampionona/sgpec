<?php

namespace Laiso\ArmBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Laiso\ArmBundle\Entity\Marche;
use Laiso\ArmBundle\Form\MarcheType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Marche controller.
 *
 */
class MarcheController extends Controller
{

    private function verifyAccess()
    {
        if (!($this->getUser()->hasRole('ROLE_DTEC') || $this->getUser()->hasRole('ROLE_CI') || $this->getUser()->hasRole('ROLE_ASSIST')))
            throw new AccessDeniedHttpException("Vous n'avez pas le droit d'accéder à ce ressource");
    }

    /**
     * Lists all Marche entities.
     *
     */
    public function indexAction(Request $request)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LaisoArmBundle:Marche')->findBy(array(
            'lotissement' => $em->getRepository('LaisoArmBundle:Lotissement')->findBy(array(
                'valide' => true,
            ))
        ));

        $paginator = $this->get('knp_paginator');

        return $this->render('LaisoArmBundle:Marche:index.html.twig', array(
            'entities' => $paginator->paginate($entities, $request->query->getInt('page', 1), 10)
        ));
    }
    /**
     * Creates a new Marche entity.
     *
     */
    public function createAction(Request $request)
    {
        $this->verifyAccess();

        $entity = new Marche();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            /** @var \Laiso\armBundle\Entity\Marche $data */
            $data = $form->getData();

            $lotissement = $data->getLotissement();

            if($lotissement->getValide() == true)
                throw new AccessDeniedException('Lotissement déjà validé');

            $marche = $this->getDoctrine()->getRepository('LaisoArmBundle:Marche')->findOneBy(array(
                'lotissement' => $lotissement->getId()
            ));

            // Avenant ZERO valide: Quantités initiales valides.
            if($marche->getAvenants()[0]->getValide()==true) {

                $marche->setEntreprise($data->getEntreprise());
                $marche->setNumero($data->getNumero());

                $marche->getLotissement()->setValide(true); // Marché
                $marche->getLotissement()->setPuValide(false); // Prix Unitaires
                $marche->setEntrepriseAssujettie($entity->getEntrepriseAssujettie());
                
                $em->persist($marche->getLotissement());
                $em->persist($marche);
                $em->flush();

                return $this->redirect($this->generateUrl('marche_show', array('id' => $marche->getId())));
            }else{
                $this->addFlash(
                    'error',
                    'Les quantités saisies dans le lot choisi ne sont pas encore validés'
                );
            }
        }

        return $this->render('LaisoArmBundle:Marche:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Marche entity.
     *
     * @param Marche $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Marche $entity)
    {
        $form = $this->createForm(new MarcheType(), $entity, array(
            'action' => $this->generateUrl('marche_create'),
            'method' => 'POST',
        ));

        $form
            ->add('submit', 'submit', array('label' => 'Enregistrer', 'attr' => array(
            'class' => 'button success place-right')))
        ;

        return $form;
    }

    /**
     * Displays a form to create a new Marche entity.
     *
     */
    public function newAction(Request $request)
    {
        $this->verifyAccess();

        $entity = new Marche();
        $form   = $this->createCreateForm($entity);

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:Marche/includes:new_marche.html.twig', array(
                'entity' => $entity,
                'form'   => $form->createView(),
            ));

        return $this->render('LaisoArmBundle:Marche:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Marche entity.
     *
     */
    public function showAction(Marche $marche)
    {
        $this->verifyAccess();

        $deleteForm = $this->createDeleteForm($marche->getId());

        if($marche->getLotissement()->getValide() == false)
            throw new NotFoundHttpException('Marché non trouvé');

        return $this->render('LaisoArmBundle:Marche:show.html.twig', array(
            'entity'      => $marche,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     *
     *
     * @param Marche $marche
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showDqeAction(Marche $marche){
        $this->verifyAccess();

        $consistances = array();

        $nbAvenants = sizeof($marche->getAvenants());

        /** @var \Laiso\ArmBundle\Entity\Consistance $consistance */
        foreach($marche->getLotissement()->getConsistances() as $consistance){
            $tempConsistance['description'] = $consistance->getLibelle();
            $tempConsistance['localisations'] = $consistance->getLocalisations();
            $tempConsistance['type'] = $consistance->getType();

            $prices = $consistance->getPrix();

            $tempLignes = array();

            /** @var \Laiso\ArmBundle\Entity\AffectationPrix $prix */
            foreach($prices as $prix) {
                $tempLigne = array();
                $avenants = array();
                $lignes = $prix->getLignesDqe();

                $tempLigne['serie'] = $prix->getSerie();
                $tempLigne['prixUnitaire'] = $prix->getPrixUnitaire();

                for ($i = 0; $i < $nbAvenants; $i++) {
                    $tempAvenant = null;
                    /** @var \Laiso\ArmBundle\Entity\LigneDQE $ligne */
                    foreach ($lignes as $ligne) {
                        if ($ligne->getAvenant()->getId() == $marche->getAvenants()[$i]->getId()) {
                            $tempAvenant = array(
                                'numero' => $ligne->getAvenant()->getLibelle()->getNumero(),
                                'quantite' => $ligne->getQuantite(),
                            );
                        }
                    }
                    array_push($avenants, $tempAvenant);
                }
                $tempLigne['avenants'] = $avenants;
                array_push($tempLignes, $tempLigne);
            }
            $tempConsistance['lignes'] = $tempLignes;

            array_push($consistances, $tempConsistance);
        }

        return $this->render('@LaisoArm/Marche/dqe.html.twig', array(
            'libelle' => $marche->getAvenants()[sizeof($marche->getAvenants()) - 1]->getLibelle()->getLibelle(),
            'marche' => $marche,
            'consistances' => $consistances
        ));
    }

    /**
     *
     * @param Marche $marche
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function validatePuAction(Marche $marche)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();

        /** @var \Laiso\ArmBundle\Entity\Avenant $avenant */
        $avenant = $marche->getAvenants()[count($marche->getAvenants()) - 1];

        if(!$avenant)
            throw new NotFoundHttpException('Unable to find Avenant entity');

        $valide = true;

        foreach($avenant->getLignes() as $ligne){
            if($ligne->getPrix()->getPrixUnitaire() == null || $ligne->getPrix()->getPrixUnitaire() == 0){
                $valide = false;
                $this->addFlash('error', 'Veuillez remplir tous les prix unitaires avant de valider.');
                break;
            }
        }
        if($valide) {
            $lotissement = $marche->getLotissement()->setPuValide(true);

            // Mettre à jour le montant du marché (marche::montant)
            $montant = 0.0;

            /** @var \Laiso\ArmBundle\Entity\LigneDQE $ligne */
            foreach ($avenant->getLignes() as $ligne) {
                $montant += $ligne->getQuantite() * $ligne->getPrix()->getPrixUnitaire();
            }

            $marche->setMontant($montant);
            $em->persist($marche);
            $em->persist($lotissement);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('marche_show', array(
            'id' => $marche->getId(),
        )));
    }

    /**
     * Displays a form to edit an existing Marche entity.
     *
     */
    public function editAction(Request $request, Marche $marche)
    {
        $this->verifyAccess();

        if($marche->getLotissement()->getValide())
            throw new AccessDeniedException('Impossible de modifier un marché validé');

        $editForm = $this->createEditForm($marche);
        $deleteForm = $this->createDeleteForm($marche->getId());

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:Marche/includes:edit_marche.html.twig', array(
                'entity'      => $marche,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));

        return $this->render('LaisoArmBundle:Marche:edit.html.twig', array(
            'entity'      => $marche,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Marche entity.
    *
    * @param Marche $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Marche $entity)
    {
        $form = $this->createForm(new MarcheType(), $entity, array(
            'action' => $this->generateUrl('marche_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Metter à jour', 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }
    /**
     * Edits an existing Marche entity.
     *
     */
    public function updateAction(Request $request, Marche $marche)
    {
        $this->verifyAccess();

        $deleteForm = $this->createDeleteForm($marche->getId());
        $editForm = $this->createEditForm($marche);
        $editForm->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('marche_show', array('id' => $marche->getId())));
        }

        return $this->render('LaisoArmBundle:Marche:edit.html.twig', array(
            'entity'      => $marche,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Marche entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $this->verifyAccess();

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('LaisoArmBundle:Marche')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Marche entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('marche'));
    }

    /**
     * Creates a form to delete a Marche entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('marche_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Supprimer', 'attr' => array(
                'class' => 'button danger place-right   '
            )))
            ->getForm()
        ;
    }
}
