<?php

namespace Laiso\ArmBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Laiso\ArmBundle\Entity\Axe;
use Laiso\ArmBundle\Form\AxeType;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Axe controller.
 *
 */
class AxeController extends Controller
{

    private function denyAccess(){
        throw new AccessDeniedHttpException("Vous n'avez pas le droit d'accéder à ce ressource");
    }

    /**
     * Lists all Axe entities.
     *
     */
    public function indexAction(Request $request)
    {
        if(!$this->getUser()->hasRole('ROLE_ADMIN'))
            $this->denyAccess();

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LaisoArmBundle:Axe')->findAll();
        $paginator = $this->get('knp_paginator');

        return $this->render('LaisoArmBundle:Axe:index.html.twig', array(
            'entities' => $paginator->paginate($entities, $request->query->getInt('page', 1), 10)
        ));
    }
    /**
     * Creates a new Axe entity.
     *
     */
    public function createAction(Request $request)
    {
        if(!$this->getUser()->hasRole('ROLE_ADMIN'))
            $this->denyAccess();

        $entity = new Axe();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('axe'));
        }

        return $this->render('LaisoArmBundle:Axe:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Axe entity.
     *
     * @param Axe $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Axe $entity)
    {
        if(!$this->getUser()->hasRole('ROLE_ADMIN'))
            $this->denyAccess();

        $form = $this->createForm(new AxeType(), $entity, array(
            'action' => $this->generateUrl('axe_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Enregistrer', 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }

    /**
     * Displays a form to create a new Axe entity.
     *
     */
    public function newAction(Request $request)
    {
        if(!$this->getUser()->hasRole('ROLE_ADMIN'))
            $this->denyAccess();

        $entity = new Axe();
        $form   = $this->createCreateForm($entity);

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:Axe/includes:new_axe.html.twig', array(
                'entity' => $entity,
                'form'   => $form->createView(),
            ));

        return $this->render('LaisoArmBundle:Axe:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Axe entity.
     *
     */
    public function showAction($id)
    {
        if(!$this->getUser()->hasRole('ROLE_ADMIN'))
            $this->denyAccess();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:Axe')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Axe entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('LaisoArmBundle:Axe:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Axe entity.
     *
     */
    public function editAction(Request $request, $id)
    {
        if(!$this->getUser()->hasRole('ROLE_ADMIN'))
            $this->denyAccess();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:Axe')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Axe entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:Axe/includes:edit_axe.html.twig', array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));

        return $this->render('LaisoArmBundle:Axe:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Axe entity.
    *
    * @param Axe $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Axe $entity)
    {
        if(!$this->getUser()->hasRole('ROLE_ADMIN'))
            $this->denyAccess();

        $form = $this->createForm(new AxeType(), $entity, array(
            'action' => $this->generateUrl('axe_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Mettre à jour', 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }
    /**
     * Edits an existing Axe entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        if(!$this->getUser()->hasRole('ROLE_ADMIN'))
            $this->denyAccess();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:Axe')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Axe entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('axe_edit', array('id' => $id)));
        }

        return $this->render('LaisoArmBundle:Axe:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Axe entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        if(!$this->getUser()->hasRole('ROLE_ADMIN'))
            $this->denyAccess();

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('LaisoArmBundle:Axe')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Axe entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('axe'));
    }

    /**
     * Creates a form to delete a Axe entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        if(!$this->getUser()->hasRole('ROLE_ADMIN'))
            $this->denyAccess();

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('axe_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Supprimer', 'attr' => array(
                'class' => 'button danger place-right'
            )))
            ->getForm()
        ;
    }
}
