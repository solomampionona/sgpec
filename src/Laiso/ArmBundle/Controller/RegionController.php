<?php

namespace Laiso\ArmBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Laiso\ArmBundle\Entity\Region;
use Laiso\ArmBundle\Form\RegionType;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Region controller.
 *
 */
class RegionController extends Controller
{
    private function denyAccess(){
        throw new AccessDeniedHttpException("Vous n'avez pas le droit d'accéder à ce ressource");
    }

    /**
     * Lists all Region entities.
     *
     */
    public function indexAction(Request $request)
    {
        if(!$this->getUser()->hasRole('ROLE_ADMIN'))
            $this->denyAccess();

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LaisoArmBundle:Region')->findAll();
        $paginator = $this->get('knp_paginator');

        return $this->render('LaisoArmBundle:Region:index.html.twig', array(
            'entities' => $paginator->paginate($entities, $request->query->getInt('page', 1), 10)
        ));
    }
    /**
     * Creates a new Region entity.
     *
     */
    public function createAction(Request $request)
    {
        if(!$this->getUser()->hasRole('ROLE_ADMIN'))
            $this->denyAccess();

        $entity = new Region();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('region'));
        }

        return $this->render('LaisoArmBundle:Region:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Region entity.
     *
     * @param Region $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Region $entity)
    {
        if(!$this->getUser()->hasRole('ROLE_ADMIN'))
            $this->denyAccess();

        $form = $this->createForm(new RegionType(), $entity, array(
            'action' => $this->generateUrl('region_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Enregistrer', 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }

    /**
     * Displays a form to create a new Region entity.
     *
     */
    public function newAction(Request $request)
    {
        if(!$this->getUser()->hasRole('ROLE_ADMIN'))
            $this->denyAccess();

        $entity = new Region();
        $form   = $this->createCreateForm($entity);

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:Region/includes:new_region.html.twig', array(
                'entity' => $entity,
                'form'   => $form->createView(),
            ));

        return $this->render('LaisoArmBundle:Region:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Region entity.
     *
     */
    public function showAction($id)
    {
        if(!$this->getUser()->hasRole('ROLE_ADMIN'))
            $this->denyAccess();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:Region')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Region entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('LaisoArmBundle:Region:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Region entity.
     *
     */
    public function editAction(Request $request, $id)
    {
        if(!$this->getUser()->hasRole('ROLE_ADMIN'))
            $this->denyAccess();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:Region')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Region entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:Region/includes:edit_region.html.twig', array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));

        return $this->render('LaisoArmBundle:Region:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Region entity.
    *
    * @param Region $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Region $entity)
    {
        if(!$this->getUser()->hasRole('ROLE_ADMIN'))
            $this->denyAccess();

        $form = $this->createForm(new RegionType(), $entity, array(
            'action' => $this->generateUrl('region_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Mettre à jour', 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }
    /**
     * Edits an existing Region entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        if(!$this->getUser()->hasRole('ROLE_ADMIN'))
            $this->denyAccess();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:Region')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Region entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('region'));
        }

        return $this->render('LaisoArmBundle:Region:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Region entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        if(!$this->getUser()->hasRole('ROLE_ADMIN'))
            $this->denyAccess();

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('LaisoArmBundle:Region')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Region entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('region'));
    }

    /**
     * Creates a form to delete a Region entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        if(!$this->getUser()->hasRole('ROLE_ADMIN'))
            $this->denyAccess();

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('region_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Supprimer' , 'attr' => array(
                'class' => 'button danger place-right'
            )))
            ->getForm()
        ;
    }

    /**
     * Handle Ajax request for deletion
     *
     * (c) Laiso
     *
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function ajaxDeleteAction(Request $request, $id)
    {
        if(!$this->getUser()->hasRole('ROLE_ADMIN'))
            $this->denyAccess();

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        $entity = $this->getDoctrine()->getRepository('LaisoArmBundle:Region')->find($id);
        if(!$entity)
            return $this->createNotFoundException("Région introuvable");

        if ($request->isXmlHttpRequest())
            return $this->render("LaisoArmBundle:Shared:delete_ajax.html.twig", array(
                'delete_form' => $form->createView(),
                'title' => "Supprimer la région \"". $entity->getNom() . "\""
            ));
        else return $this->redirectToRoute("region_show", array('id' => $id));
    }
}
