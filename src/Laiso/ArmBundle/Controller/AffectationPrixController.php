<?php

namespace Laiso\ArmBundle\Controller;

use Laiso\ArmBundle\Entity\AffectationPrix;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AffectationPrixController extends Controller
{
    /**
     * @param Request $request
     * @param $numero
     * @param $marcheId
     * @param $consistanceId
     * @return Response
     */
    public function addPrixUnitaireAction(Request $request, $numero, $marcheId, $consistanceId)
    {
        $affectationPrix = $this->getAffectationPrix($consistanceId, $numero);

        if(!$affectationPrix)
            throw new NotFoundHttpException('Affectation Prix non trouvé');

        return
            $request->isXmlHttpRequest() ?
                $this->render("@LaisoArm/AffectationPrix/content.html.twig", array(
                    'form' => $this->getForm($marcheId, $consistanceId, $numero)->createView(),
                    'entity' => $affectationPrix
                )) :
                $this->render("@LaisoArm/AffectationPrix/addPrixUnitaire.html.twig", array(
                    'form' => $this->getForm($marcheId, $consistanceId, $numero)->createView(),
                    'entity' => $affectationPrix
                ));
    }

    /**
     * @param $marcheid
     * @param $consistanceId
     * @param $numero
     * @return \Symfony\Component\Form\Form
     */
    private function getForm($marcheid, $consistanceId, $numero)
    {
        $builder = $this->get('form.factory')->createBuilder('form');
        $builder->setMethod('POST');
        $builder->setAction($this->generateUrl('prix_unitaire_update', array(
            'consistanceId' => $consistanceId,
            'marcheId' => $marcheid,
            'numero' => $numero,
        )));
        $builder->add('prixUnitaire', null, array(
            'attr' => array(
                'placeholder' => 'Prix unitaire'
            )
        ));
        $builder->add('submit', 'submit', array(
            'label' => 'Valider',
            'attr' => array(
                'class' => 'button place-right success'
            )
        ));
        return $builder->getForm();
    }

    /**
     * @param Request $request
     * @param $numero
     * @param $marcheId
     * @param $consistanceId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction(Request $request, $numero, $marcheId, $consistanceId)
    {
        $affectationPrix = $this->getAffectationPrix($consistanceId, $numero);

        if(!$affectationPrix)
            throw new NotFoundHttpException('Affectation Prix non trouvé');

        $form = $this->getForm($marcheId, $consistanceId, $numero);
        $form->handleRequest($request);

        if($form->getData()['prixUnitaire'] != $affectationPrix->getPrixUnitaire()){
            $pu = str_replace(" ", '', $form->getData()['prixUnitaire']);
            $pu = str_replace(',', '.', $pu);
            $affectationPrix->setPrixUnitaire($pu);
            $em = $this->getDoctrine()->getManager();

            $em->persist($affectationPrix);
            $em->flush();

            if($request->isXmlHttpRequest())
                return new Response(json_encode(array(
                    'status' => 1,
                    'montant' => number_format($affectationPrix->getPrixUnitaire() * $affectationPrix->getLignesDqe()[0]->getQuantite(),2, '.', ' '),
                )));
            return $this->redirect($this->generateUrl('avenant_show', array(
                'numero' => 0,
                'marcheId' => $marcheId,
            )));
        }
        return
            $request->isXmlHttpRequest() ?
                new Response(json_encode(array(
                    'status' => 0
                ))) :
                $this->render("@LaisoArm/AffectationPrix/addPrixUnitaire.html.twig", array(
                    'form' => $this->getForm($marcheId, $consistanceId, $numero)->createView(),
                    'entity' => $affectationPrix
                ));
    }

    /**
     * @param $consistanceId
     * @param $numero
     * @return AffectationPrix|null|object
     */
    private function getAffectationPrix($consistanceId, $numero)
    {
        $em = $this->getDoctrine()->getManager();
        return $affectationPrix = $em->getRepository('LaisoArmBundle:AffectationPrix')->findOneBy(array(
            'consistance' => $consistanceId,
            'serie' => $em->getRepository('LaisoArmBundle:SerieDePrix')->findOneBy(array(
                'numero' => $numero,
            )),
        ));
    }
}
