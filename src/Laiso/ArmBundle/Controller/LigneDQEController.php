<?php

namespace Laiso\ArmBundle\Controller;

use Laiso\ArmBundle\Entity\AffectationPrix;
use Laiso\ArmBundle\Form\AffectationPrixType;
use Laiso\ArmBundle\Form\LigneDQEZeroType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Laiso\ArmBundle\Entity\LigneDQE;
use Symfony\Component\HttpFoundation\Response;


/**
 * LigneDQE controller.
 *
 */
class LigneDQEController extends Controller
{

    /**
     * Creates a new LigneDQE entity.
     *
     */
    public function createAction(Request $request, $daoId, $consistanceId)
    {
        $form = $this->createCreateForm($daoId, $consistanceId);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $form->getData();

            $affectationPrix = new AffectationPrix();
            $ligne = new LigneDQE();

            $marche = $em->getRepository("LaisoArmBundle:Marche")->findOneBy(array("lotissement" => $daoId));
            $avenants = $em->getRepository("LaisoArmBundle:Avenant")->findBy(array('marche' => $marche->getId()));

            $affectationPrix->setSerie($data['prix']);
            $affectationPrix->setConsistance($em->getRepository("LaisoArmBundle:Consistance")->find($consistanceId));
            $ligne->setQuantite($data['quantite']);
            $ligne->setPrix($affectationPrix);
            $ligne->setAvenant($avenants[0]);

            $em->persist($affectationPrix);
            $em->persist($ligne);
            $em->flush();

            if(null != $request->query->get('returnUrl'))
                return $this->redirect($request->query->get('returnUrl'));

            if($request->isXmlHttpRequest()){
                return $this->redirect($this->generateUrl('dao_show', array(
                    'id' => $daoId,
                    'action' => 'dqe'
                )));
            }

            return $this->redirect($this->generateUrl('dao_show', array('id' => $daoId)));
        }

        return $this->render('LaisoArmBundle:LigneDQE:new.html.twig', array(
            'form'   => $form->createView(),
        ));
    }


    public function createCreateForm($daoId, $consistanceId, $url = null)
    {
        $builder = $this->get('form.factory')->createBuilder('form');

        $builder->setAction($this->generateUrl('dqe_create', array(
            'consistanceId' => $consistanceId,
            'daoId' => $daoId,
            'returnUrl' => $url
        )));

        $builder->setMethod("post");

        $builder
            ->add('quantite', null, array(
                'attr' => array(
                    'placeholder' => 'Quantité prévue'
                )
            ))
            ->add('prix', 'entity', array(
                'class' => 'Laiso\ArmBundle\Entity\SerieDePrix',
                'group_by' => 'categorie'
            ))
        ;
        return $builder->getForm();
    }
    
    /**
     * Displays a form to create a new LigneDQE entity.
     *
     */
    public function newAction(Request $request, $daoId, $consistanceId)
    {
        $returnUrl = $request->query->get('returnUrl');
        $form   = $this->createCreateForm($daoId, $consistanceId, $returnUrl);

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:LigneDQE/includes:new_ligneDqe.html.twig', array(
                'form'   => $form->createView(),
                'daoId' => $daoId,
                'consistanceId' => $consistanceId,
            ));

        return $this->render('LaisoArmBundle:LigneDQE:new.html.twig', array(
            'form'   => $form->createView(),
            'daoId' => $daoId,
            'consistanceId' => $consistanceId,
        ));
    }


    /**
     * Displays a form to edit an existing LigneDQE entity.
     *
     */
    public function editAction(Request $request, $id, $daoId, $consistanceId)
    {
        $em = $this->getDoctrine()->getManager();
        $returnUrl = $request->query->get('returnUrl');
        $entity = $em->getRepository('LaisoArmBundle:LigneDQE')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find LigneDQE entity.');
        }

        if(null == $returnUrl)
            $editForm = $this->createEditForm($entity, $daoId, $consistanceId);
        else
            $editForm = $this->createEditForm($entity, $daoId, $consistanceId);
        $deleteForm = $this->createDeleteForm($id,$daoId, $consistanceId);

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:LigneDQE/includes:edit_ligneDqe.html.twig', array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));

        return $this->render('LaisoArmBundle:LigneDQE:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a LigneDQE entity.
    *
    * @param LigneDQE $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(LigneDQE $entity, $daoId, $consistanceId)
    {
        $form = $this->createForm(new LigneDQEZeroType(), $entity, array(
            'action' => $this->generateUrl('dqe_update', array(
                'id' => $entity->getId(),
                'daoId' => $daoId,
                'consistanceId' => $consistanceId,
            )),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Mettre à jour','attr' => array(
            'class' => 'button success place-right ligneDqe-submit'
        )));

        return $form;
    }
    /**
     * Edits an existing LigneDQE entity.
     *
     */
    public function updateAction(Request $request, $id, $daoId, $consistanceId)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:LigneDQE')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find LigneDQE entity.');
        }

        $deleteForm = $this->createDeleteForm($id, $daoId, $consistanceId);
        $editForm = $this->createEditForm($entity, $daoId, $consistanceId);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('dao_show', array(
                'id' => $daoId,
            )));
        }

        return $this->render('LaisoArmBundle:LigneDQE:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a LigneDQE entity.
     *
     */
    public function deleteAction(Request $request, $id, $daoId, $consistanceId)
    {
        $form = $this->createDeleteForm($id, $daoId, $consistanceId);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('LaisoArmBundle:LigneDQE')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find LigneDQE entity.');
            }

            // Essayer de comprendre ;) ;)
            if(sizeof($entity->getPrix()->getLignesDqe()) == 1)
                $em->remove($entity->getPrix());

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('dao_show', array(
            'id' => $daoId,
            )));
    }

    /**
     * Creates a form to delete a LigneDQE entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id, $daoId, $consistanceId)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('dqe_delete', array(
                'id' => $id,
                'daoId' => $daoId,
                'consistanceId' => $consistanceId
            )))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Supprimer', 'attr' => array(
                'class' => 'button place-right danger'
            )))
            ->getForm()
        ;
    }
    /**
     * Handle Ajax request for deletion
     *
     * (c) Laiso
     *
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function ajaxDeleteAction(Request $request, $id, $daoId, $consistanceId)
    {
        $form = $this->createDeleteForm($id,$daoId, $consistanceId);
        $form->handleRequest($request);

        $entity = $this->getDoctrine()->getRepository('LaisoArmBundle:LigneDQE')->find($id);
        if(!$entity)
            return $this->createNotFoundException("Ligne du DQE introuvable");

        if ($request->isXmlHttpRequest())
            return $this->render("LaisoArmBundle:Shared:delete_ajax.html.twig", array(
                'delete_form' => $form->createView(),
                'title' => "Supprimer la ligne \"". $entity . "\""
            ));
        else return $this->redirectToRoute("dao_show", array('id' => $id));
    }
}
