<?php

namespace Laiso\ArmBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Laiso\ArmBundle\Entity\Consistance;
use Laiso\ArmBundle\Form\ConsistanceType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Consistance controller.
 *
 */
class ConsistanceController extends Controller
{
    /**
     * Creates a new Consistance entity.
     *
     */
    public function createAction(Request $request, $daoId)
    {
        $entity = new Consistance();
        $dao = $this->getDoctrine()->getRepository("LaisoArmBundle:Lotissement")->find($daoId);
        $entity->setLotissement($dao);

        $form = $this->createCreateForm($entity, $daoId);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('dao_show', array('id' => $daoId)));
        }

        return $this->render('LaisoArmBundle:Consistance:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Consistance entity.
     *
     * @param Consistance $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Consistance $entity, $daoId)
    {
        $form = $this->createForm(new ConsistanceType(), $entity, array(
            'action' => $this->generateUrl('consistance_create', array('daoId' => $daoId)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Enregistrer', 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }

    /**
     * Displays a form to create a new Consistance entity.
     *
     */
    public function newAction(Request $request, $daoId)
    {
        $entity = new Consistance();

        $lotissement = $this->getDoctrine()->getRepository("LaisoArmBundle:Lotissement")->find($daoId);

        if($lotissement == null)
            throw new NotFoundHttpException('Unable to find Lotissement entity');

        $entity->setLotissement($lotissement);
        $entity->setType($lotissement->getType());

        $form   = $this->createCreateForm($entity, $daoId);

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:Consistance/includes:new_consistance.html.twig', array(
                'entity' => $entity,
                'form'   => $form->createView(),
            ));

        return $this->render('LaisoArmBundle:Consistance:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Consistance entity.
     *
     */
    public function showAction($daoId, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:Consistance')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Consistance entity.');
        }

        $deleteForm = $this->createDeleteForm($id, $daoId);

        return $this->render('LaisoArmBundle:Consistance:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Consistance entity.
     *
     */
    public function editAction(Request $request, $id, $daoId)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:Consistance')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Consistance entity.');
        }

        $editForm = $this->createEditForm($entity, $daoId);
        $deleteForm = $this->createDeleteForm($id, $daoId);

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:Consistance/includes:edit_consistance.html.twig', array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));

        return $this->render('LaisoArmBundle:Consistance:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Consistance entity.
    *
    * @param Consistance $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Consistance $entity, $daoId)
    {
        $form = $this->createForm(new ConsistanceType(), $entity, array(
            'action' => $this->generateUrl('consistance_update', array('id' => $entity->getId(), 'daoId' => $daoId)),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Mettre à jour', 'attr' => array(
            'class' => 'success button place-right'
        )));

        return $form;
    }
    /**
     * Edits an existing Consistance entity.
     *
     */
    public function updateAction(Request $request, $id, $daoId)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:Consistance')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Consistance entity.');
        }

        $deleteForm = $this->createDeleteForm($id, $daoId);
        $editForm = $this->createEditForm($entity, $daoId);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('dao_show', array(
                'id' => $daoId
            )));
        }

        return $this->render('LaisoArmBundle:Consistance:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Consistance entity.
     *
     */
    public function deleteAction(Request $request, $id, $daoId)
    {
        $form = $this->createDeleteForm($id, $daoId);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('LaisoArmBundle:Consistance')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Consistance entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('dao_show', array(
            'daoId' => $daoId
        )));
    }

    /**
     * Creates a form to delete a Consistance entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id, $daoId)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('consistance_delete', array(
                'id' => $id,
                'daoId' => $daoId
            )))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Supprimer', 'attr' => array(
                'class' => 'button danger place-right'
            )))
            ->getForm()
        ;
    }

    /**
     * Handle Ajax request for deletion
     *
     * (c) Laiso
     *
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function ajaxDeleteAction(Request $request, $id, $daoId)
    {
        $form = $this->createDeleteForm($id, $daoId);
        $form->handleRequest($request);

        $entity = $this->getDoctrine()->getRepository('LaisoArmBundle:Consistance')->find($id);
        if(!$entity)
            return $this->createNotFoundException("Consistance introuvable");

        if ($request->isXmlHttpRequest())
            return $this->render("LaisoArmBundle:Shared:delete_ajax.html.twig", array(
                'delete_form' => $form->createView(),
                'title' => "Supprimer la consistance \"". $entity->getLibelle() . "\""
            ));
        else return $this->redirectToRoute("dao_show_show", array('daoId' => $daoId));
    }
}
