<?php

namespace Laiso\ArmBundle\Controller;

use Laiso\ArmBundle\Entity\ConsistanceAttachement;
use Laiso\ArmBundle\Entity\Decompte;
use Laiso\ArmBundle\Entity\Marche;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Laiso\ArmBundle\Entity\Attachement;
use Laiso\ArmBundle\Entity\LigneAttachement;
use Laiso\ArmBundle\Form\AttachementType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Attachement controller.
 *
 */
class AttachementController extends Controller
{

    /**
     * Lists all Attachement entities.
     *
     */
    public function indexAction($marcheId, $decompteId)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LaisoArmBundle:Attachement')->findBy(
            array(
                'decompte' => $decompteId
            )
        );

        return $this->render('LaisoArmBundle:Attachement:index.html.twig', array(
            'entities' => $entities,
            'marcheId' => $marcheId,
            'decompteId' => $decompteId,
        ));
    }

    /**
     * Creates a new Attachement entity.
     *
     */
    public function createAction(Request $request, $marcheId, $decompteId)
    {
        $entity = new Attachement();
        $em = $this->getDoctrine()->getManager();
        $decompte = $em->getRepository('LaisoArmBundle:Decompte')->find($decompteId);
        if (!$decompte)
            throw new NotFoundHttpException('Unable to find Decompte entity');

        $form = $this->createCreateForm($entity, $marcheId, $decompteId);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entity->setDecompte($decompte);
            $entity->setValide(false);
            $entity->setType('TRAVAUX');

            $entity->setMontantHTVA(0.0);

            $em->persist($entity);

            /** @var \Laiso\ArmBundle\Entity\Marche $marche */
            $marche = $decompte->getAvenant()->getMarche();

            /** @var \Laiso\ArmBundle\Entity\Avenant $avenant */
            $avenant = $marche->getAvenants()[sizeof($marche->getAvenants()) - 1];

            $consistances = $marche->getLotissement()->getConsistances();

            // Remplir l'attachement

            /** @var \laiso\ArmBundle\Entity\Consistance $consistance */
            foreach ($consistances as $consistance) {
                $consistanceAttachement = new ConsistanceAttachement();
                $consistanceAttachement->setAttachement($entity);
                $consistanceAttachement->setType($consistance->getType());
                $consistanceAttachement->setLibelle($consistance->getLibelle());

                $lignesDqe = $em->getRepository('LaisoArmBundle:LigneDQE')->findBy(array(
                    'avenant' => $avenant->getId(),
                    'prix' => $em->getRepository('LaisoArmBundle:AffectationPrix')->findBy(array(
                        'consistance' => $em->getRepository('LaisoArmBundle:Consistance')->find($consistance->getId())
                    )),
                ));

                $em->persist($consistanceAttachement);

                /** @var \Laiso\ArmBundle\Entity\LigneDQE $ligneDQE */
                foreach ($lignesDqe as $ligneDQE) {
                    $ligneAttachement = new LigneAttachement();
                    $ligneAttachement->setQuantite(0.0);
                    $ligneAttachement->setLigneDqe($ligneDQE);
                    $quantiteAnterieure = 0.0;

                    /** @var \Laiso\ArmBundle\Entity\Avenant $avenant */
                    foreach ($marche->getAvenants() as $avenant) {
                        /** @var \Laiso\ArmBundle\Entity\Decompte $decompte */
                        foreach ($avenant->getDecomptes() as $decompte) {
                            /** @var \Laiso\ArmBundle\Entity\Attachement $attachement */
                            foreach ($decompte->getAttachements() as $attachement) {
                                if ($attachement->isTravaux()) {
                                    /** @var \Laiso\ArmBundle\Entity\ConsistanceAttachement $c */
                                    foreach ($attachement->getConsistances() as $c) {
                                        /** @var \Laiso\ArmBundle\Entity\LigneAttachement $ligne */
                                        foreach ($c->getLignes() as $ligne) {
                                            if ($ligne->getLigneDqe()->getPrix()->getSerie()->getNumero() == $ligneDQE->getPrix()->getSerie()->getNumero())
                                                $quantiteAnterieure += $ligne->getQuantite();
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $ligneAttachement->setQuantiteAnterieure($quantiteAnterieure);

                    $ligneAttachement->setConsistance($consistanceAttachement);

                    $em->persist($ligneAttachement);
                }
            }

            $em->flush();

            $entity = $entity->selfUpdate();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('attachement_show', array(
                'id' => $entity->getId(),
                'marcheId' => $marcheId,
                'decompteId' => $decompteId,
            )));
        }

        return $this->render('LaisoArmBundle:Attachement:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Attachement entity.
     *
     * @param Attachement $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Attachement $entity, $marcheId, $decompteId)
    {
        $form = $this->createForm(new AttachementType(), $entity, array(
            'action' => $this->generateUrl('attachement_create', array(
                'marcheId' => $marcheId,
                'decompteId' => $decompteId,
            )),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Enregistrer', 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }

    /**
     * Displays a form to create a new Attachement entity.
     *
     */
    public function newAction(Request $request, $marcheId, $decompteId)
    {
        $entity = new Attachement();

        $em = $this->getDoctrine()->getManager();

        /** @var \Laiso\ArmBundle\Entity\Decompte $decompte */
        $decompte = $em->getRepository('LaisoArmBundle:Decompte')->find($decompteId);

        /** @var \Laiso\ArmBundle\Entity\Marche $marche */
        $marche = $em->getRepository('LaisoArmBundle:Marche')->find($marcheId);

        /** @var \Laiso\ArmBundle\Entity\Attachement $attachement */
        $attachement = null;

        for ($k = count($marche->getAvenants()) - 1; $k >= 0 && $attachement == null; $k--) {
            /** @var \Laiso\ArmBundle\Entity\Avenant $avenant */
            $avenant = $marche->getAvenants()[$k];

            /** @var \Laiso\ArmBundle\Entity\Decompte $decompte */
            foreach ($avenant->getDecomptes() as $decompte) {
                /** @var \Laiso\ArmBundle\Entity\Attachement $att */
                foreach ($decompte->getAttachements() as $att) {
                    $attachement = $att;
                }
            }
        }

        $numero = $attachement == null ? 1 : $attachement->getNumero() + 1;

        $entity->setNumero($numero);
        $entity->setDateAttachement($decompte->getDateDecompte());
        $form = $this->createCreateForm($entity, $marcheId, $decompteId);

        if ($attachement != null && $attachement->getValide() == false) {
            if ($request->isXmlHttpRequest())
                return new Response("<h3 class='fg-red align-center'>Vous devez valider l'attachement précédent afin de pouvoir ajouter un nouvel attachement.</h3>", 403);

            $this->addFlash(
                'error',
                'Vous devez valider l\'attachement précédent afin de pouvoir ajouter un nouvel attachement.'
            );

            return $this->redirect($this->generateUrl('decompte_show', array(
                'decompteId' => $decompteId,
                'marcheId' => $marcheId
            )));
        }

        if ($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:Attachement/includes:new_attachement.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView(),
            ));

        return $this->render('LaisoArmBundle:Attachement:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Attachement entity.
     *
     */
    public function showAction($id, $marcheId, $decompteId)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:Attachement')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Attachement entity.');
        }

        $avance = 0.0; $montant = 0;

        if($entity->isRemboursement()) {
            $marche = $entity->getDecompte()->getAvenant()->getMarche();
            $montant = $entity->getDecompte()->getMontantsBrutsPrecedentsHTVA() + $entity->getDecompte()->getAttachements()[0]->getMontantHTVA();
            if ($marche->hasAvance())
                $avance = $marche->getMontantInitial() * $marche->getLotissement()->getCampagne()->getTauxAvance() / 100;
        }

        return $this->render('LaisoArmBundle:Attachement:show.html.twig', array(
            'entity' => $entity,
            'avance' => $avance,
            'montant' => $montant,
        ));
    }

    /**
     * Displays a form to edit an existing Attachement entity.
     *
     */
    public function editAction(Request $request, $id, $marcheId, $decompteId)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:Attachement')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Attachement entity.');
        }

        $editForm = $this->createEditForm($entity, $marcheId, $decompteId);
        $deleteForm = $this->createDeleteForm($id, $marcheId, $decompteId);

        if ($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:Attachement/includes:edit_attachement.html.twig', array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));

        return $this->render('LaisoArmBundle:Attachement:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Attachement entity.
     *
     * @param Attachement $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Attachement $entity, $marcheId, $decompteId)
    {
        $form = $this->createForm(new AttachementType(), $entity, array(
            'action' => $this->generateUrl('attachement_update', array(
                'id' => $entity->getId(),
                'marcheId' => $marcheId,
                'decompteId' => $decompteId,
            )),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Mettre à jour', 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }

    /**
     * Edits an existing Attachement entity.
     *
     */
    public function updateAction(Request $request, $id, $marcheId, $decompteId)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:Attachement')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Attachement entity.');
        }

        $deleteForm = $this->createDeleteForm($id, $marcheId, $decompteId);
        $editForm = $this->createEditForm($entity, $marcheId, $decompteId);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('attachement_show', array(
                'id' => $id,
                'marcheId' => $marcheId,
                'decompteId' => $decompteId,
            )));
        }

        return $this->render('LaisoArmBundle:Attachement:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Attachement entity.
     *
     */
    public function deleteAction(Request $request, $id, $marcheId, $decompteId)
    {
        $form = $this->createDeleteForm($id, $marcheId, $decompteId);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('LaisoArmBundle:Attachement')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Attachement entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('attachement', array(
            'marcheId' => $marcheId,
            'decompteId' => $decompteId,
        )));
    }

    /**
     * Creates a form to delete a Attachement entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id, $marcheId, $decompteId)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('attachement_delete', array(
                'id' => $id,
                'marcheId' => $marcheId,
                'decompteId' => $decompteId
            )))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Supprimer', 'attr' => array(
                'class' => 'button danger place-right'
            )))
            ->getForm();
    }

    /**
     * @param LigneAttachement $ligneAttachement
     * @param $id
     * @param $marcheId
     * @param $decompteId
     * @return \Symfony\Component\Form\Form
     */
    private function createEditQuantiteForm(LigneAttachement $ligneAttachement, $id, $marcheId, $decompteId)
    {
        $builder = $this->get('form.factory')->createBuilder('form');
        $builder->setMethod('post');
        $builder->setAction($this->generateUrl('updateQuantite', array(
            'marcheId' => $marcheId,
            'decompteId' => $decompteId,
            'attachementId' => $id,
            'ligneId' => $ligneAttachement->getId()
        )));
        $builder->add('quantite', 'number', array(
            'data' => $ligneAttachement->getQuantite()
        ));
        $builder->add('submit', 'submit', array(
            'label' => 'Valider', 'attr' => array(
                'class' => 'button success place-right'
            )
        ));

        return $builder->getForm();
    }

    /**
     * Update quantite
     * @param Request $request
     * @param $ligneId
     * @param $attachementId
     * @param $marcheId
     * @param $decompteId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function updateQuantiteAction(Request $request, $ligneId, $attachementId, $marcheId, $decompteId)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var \Laiso\ArmBundle\Entity\LigneAttachement $ligne */
        $ligne = $em->getRepository('LaisoArmBundle:LigneAttachement')->find($ligneId);
        $quantite = $ligne->getQuantite();

        if (!$ligne)
            throw new NotFoundHttpException('Unable to find LigneAttachement entity');
        $form = $this->createEditQuantiteForm($ligne, $ligneId, $marcheId, $decompteId);

        $form->handleRequest($request);

        if ($form->getData()['quantite'] != $quantite) {
            $decompte = $ligne->getConsistance()->getAttachement()->getDecompte();
            $attachement = $ligne->getConsistance()->getAttachement();
            $ligne->setQuantite(str_replace(' ', '', $form->getData()['quantite']));

            $attachement = $attachement->selfUpdate();
            $decompte = $decompte->selfUpdate();

            $em->persist($attachement);
            $em->persist($decompte);

            $em->flush();

            if ($request->isXmlHttpRequest())
                return new Response(json_encode(array(
                    'status' => 1,
                    'montant' => number_format(($ligne->getQuantite() + $ligne->getQuantiteAnterieure()) * $ligne->getLigneDqe()->getPrix()->getPrixUnitaire(), 2, '.', ' '),
                )));

            return $this->redirect($this->generateUrl(
                'attachement_show', array(
                    'id' => $attachementId,
                    'marcheId' => $marcheId,
                    'decompteId' => $decompteId,
                )
            ));
        }
        if ($request->isXmlHttpRequest())
            return new Response(json_encode(array(
                'status' => 0
            )));
        $this->addFlash('error', 'Données non enregistrées');
        return $this->redirect($this->generateUrl('attachement_show', array(
                'id' => $attachementId,
                'marcheId' => $marcheId,
                'decompteId' => $decompteId,
            )
        ));
    }

    /**
     * PDF
     * @param $attachementId
     * @param $marcheId
     * @param $decompteId
     * @return Response
     */
    public function generatePdfAction($attachementId, $marcheId, $decompteId)
    {
        $attachement = $this->getDoctrine()->getRepository('LaisoArmBundle:Attachement')->find($attachementId);
        if (!$attachement)
            throw new NotFoundHttpException('Unable to find Attachement entity');

        $avance = 0.0; $montant = 0;

        if($attachement->isRemboursement()) {
            $marche = $attachement->getDecompte()->getAvenant()->getMarche();
            $montant = $attachement->getDecompte()->getMontantsBrutsPrecedentsHTVA() + $attachement->getDecompte()->getAttachements()[0]->getMontantHTVA();
            if ($marche->hasAvance())
                $avance = $marche->getMontantInitial() * $marche->getLotissement()->getCampagne()->getTauxAvance() / 100;
        }

        $html = $this->renderView('LaisoArmBundle:Attachement:export.html.twig', array(
            'entity' => $attachement,
            'avance' => $avance,
            'montant' => $montant,
        ));

        $fileName = $attachement->getDecompte()->getAvenant()->getMarche() . "-attachement-" . $attachement->getNumero() . '-' . $attachement->getDateAttachement()->format('d-M-Y');

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="' . $fileName . '.pdf"'
            )
        );
    }

    /**
     * Edit quantite
     * @param Request $request
     * @param $ligneId
     * @param $attachementId
     * @param $marcheId
     * @param $decompteId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editQuantiteAction(Request $request, $ligneId, $attachementId, $marcheId, $decompteId)
    {
        $em = $this->getDoctrine()->getManager();
        $ligne = $em->getRepository('LaisoArmBundle:LigneAttachement')->find($ligneId);
        if (!$ligne)
            throw new NotFoundHttpException('Unable to find LigneAttachement Entity');

        $form = $this->createEditQuantiteForm($ligne, $ligneId, $marcheId, $decompteId);

        return
            $request->isXmlHttpRequest() ?
                $this->render('LaisoArmBundle:Attachement/includes:edit_quantiteAttachement.html.twig', array(
                    'form' => $form->createView(),
                    'entity' => $ligne
                )) :
                $this->render('LaisoArmBundle:Attachement:editQuantite.html.twig', array(
                    'form' => $form->createView(),
                    'entity' => $ligne
                ));
    }

    /**
     * @param $attachementId
     * @param $marcheId
     * @param $decompteId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function validateAction($attachementId, $marcheId, $decompteId)
    {
        $em = $this->getDoctrine()->getManager();

        $successFlash = "";

        /** @var \Laiso\ArmBundle\Entity\Marche $marche */
        $marche = $em->getRepository('LaisoArmBundle:Marche')->find($marcheId);

        if (!$marche)
            throw new NotFoundHttpException('Unable to find Marche entity');

        /** @var \Laiso\ArmBundle\Entity\Attachement $attachement */
        $attachement = $em->getRepository('LaisoArmBundle:Attachement')->find($attachementId);

        if (!$attachement)
            throw new NotFoundHttpException('Unable to find Attachement entity');

        if ($attachement->getRetenue())
            $attachement->getRetenue()->setValide(true);

        $attachement->setValide(true);
        $decompte = $attachement->getDecompte();

        $montantInitial = $marche->getMontantInitial();
        $montantDecompteHTVA = $decompte->getMontantsBrutsPrecedentsHTVA() + $decompte->getMontantBrutHTVA() ;
        $avancement = ($montantDecompteHTVA * 100 / $montantInitial);

        $marche->setAvancementFinancier($avancement);
        $marche->setAvancementPhysique($avancement);

        $successFlash = $successFlash . "Marché à jour <br>";

        $attachement = $attachement->selfUpdate();

        // Créer attachement RETENUE de GARANTIE
        if ($attachement->hasRetenue()) {
            $retenue = $this->handleRetenue($attachement);
            $successFlash .= "Attahement de retenue de garantie créé <br>";
        }

        $em->persist($marche);
        $em->persist($attachement);
        $em->flush();

        $decompte = $decompte->selfUpdate();
        $em->persist($decompte);

        $successFlash .= "Decompte à jour <br>";

        $em->flush();

        if($this->hasAvance($marche)) {
            $avance = $montantInitial * $decompte->getAvenant()->getMarche()->getLotissement()->getCampagne()->getTauxAvance() / 100;

            /** @var \Laiso\ArmBundle\Entity\Avenant $avenant */
            foreach ($marche->getAvenants() as $avenant) {
                /** @var \Laiso\ArmBundle\Entity\Decompte $decompte */
                foreach ($avenant->getDecomptes() as $decompte) {
                    $avance -= $decompte->getMontantAvance();
                }
            }
            if ($avancement >= 30) {

                $montantDecompteHTVA = $decompte->getMontantsBrutsPrecedentsHTVA() + $decompte->getMontantBrutHTVA();

                $montantRemboursement = ($montantDecompteHTVA * 0.2 > $avance) ? $avance : $montantDecompteHTVA * 0.2;

                if ($montantRemboursement > 0.01) {
                    $attachementRemboursement = new Attachement();
                    $attachementRemboursement->setType('REMBOURSEMENT');
                    $attachementRemboursement->setDecompte($decompte);
                    $attachementRemboursement->setMontantHTVA($montantRemboursement);
                    $attachementRemboursement->setValide(true);
                    $attachementRemboursement->setNumero(($attachement->getRetenue() != null) ? $attachement->getRetenue()->getNumero() + 1 : $attachement->getNumero() + 1);
                    $attachementRemboursement->setDateAttachement($attachement->getDateAttachement());

                    $decompte->setMontantAvance($montantRemboursement);

                    $successFlash .= "Attachement de remboursement de retenue de garantie créé <br>";

                    $em->persist($decompte);
                    $em->persist($attachementRemboursement);
                    $em->flush();

                    $this->addFlash('success', 'Attachement de remboursement d\'avance de démarrage créé avec succès');
                }
            }
        }

        $this->addFlash('success', $successFlash);

        return $this->redirect($this->generateUrl('attachement_show', array(
            'id' => $attachementId, 'marcheId' => $marcheId, 'decompteId' => $decompteId
        )));
    }

    /**
     *
     * @param Marche $marche
     * @return bool
     */
    private function hasAvance(Marche $marche)
    {
        return $this->getDoctrine()->getRepository('LaisoArmBundle:Decompte')->findOneBy(array(
            'numero' => 0,
            'avenant' => $marche->getAvenants()[0]->getId()
        )) != null;
    }

    /**
     * @param Attachement $attachement
     * @return Attachement
     */
    private function handleRetenue($attachement)
    {
        $retenue = new Attachement();
        $retenue->setNumero($attachement->getNumero() + 1);
        $retenue->setDateAttachement($attachement->getDateAttachement());
        $retenue->setDecompte($attachement->getDecompte());
        $retenue->setType('RETENUE');
        $retenue->setValide(true);

        $montantPartiel = 0.0;

        $em = $this->getDoctrine()->getManager();

        $em->persist($retenue);

        $attachement->setRetenue($retenue);

        /** @var \Laiso\ArmBundle\Entity\ConsistanceAttachement $consistance */
        foreach ($attachement->getConsistances() as $consistance) {
            $consistanceAttachement = new ConsistanceAttachement();
            $consistanceAttachement->setAttachement($retenue);
            $consistanceAttachement->setLibelle($consistance->getLibelle());
            $consistanceAttachement->setType($consistance->getType());

            $em->persist($consistanceAttachement);

            /** @var \laiso\ArmBundle\Entity\LigneAttachement $ligne */
            foreach ($consistance->getLignes() as $ligne) {
                if ($ligne->getLigneDqe()->getPrix()->getSerie()->getCategorie()->getNumero() > 100) {
                    $ligneAttachement = new LigneAttachement();
                    $ligneAttachement->setLigneDqe($ligne->getLigneDqe());
                    $ligneAttachement->setConsistance($consistanceAttachement);
                    $ligneAttachement->setQuantite($ligne->getQuantite());
                    $ligneAttachement->setQuantiteAnterieure($ligne->getQuantiteAnterieure());
                    $m = $ligneAttachement->getQuantite() * $ligneAttachement->getLigneDqe()->getPrix()->getPrixUnitaire();
                    $montantPartiel += $m;

                    $em->persist($ligneAttachement);
                }
            }
        }

        $retenue->setMontantHTVA($montantPartiel / 10.0);

        $this->addFlash('success', 'Retenue de garantie créée avec succès');
        $em->flush();
        return $retenue;
    }
}
