<?php

namespace Laiso\ArmBundle\Controller;

use Laiso\ArmBundle\Entity\Attachement;
use Laiso\ArmBundle\Entity\Avance;
use Laiso\ArmBundle\Form\AvanceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Laiso\ArmBundle\Utils\CPA;
use Laiso\ArmBundle\Utils\DP;
use Laiso\ArmBundle\Entity\Decompte;
use Laiso\ArmBundle\Form\DecompteType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Decompte controller.
 *
 */
class DecompteController extends Controller
{

    private function verifyAccess()
    {
        if(!($this->getUser()->hasRole('ROLE_CI') || $this->getUser()->hasRole('ROLE_DTEC') || $this->getUser()->hasRole('ROLE_ASSIST')))
            throw new AccessDeniedHttpException("Vous n'avez pas le droit d'accéder à ce ressource");
    }
    /**
     * Lists all Decompte entities.
     *
     */
    public function indexAction($marcheId)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();

        /** @var \Laiso\ArmBundle\Entity\Marche $marche */
        $marche = $em->getRepository('LaisoArmBundle:Marche')->find($marcheId);

        $entities = array();
        /** @var \Laiso\ArmBundle\Entity\Avenant $avenant */
        foreach ($marche->getAvenants() as $avenant) {
            foreach ($avenant->getDecomptes() as $decompte) {
                array_push($entities, $decompte);
            }
        }

        return $this->render('LaisoArmBundle:Decompte:index.html.twig', array(
            'entities' => $entities,
            'marche' => $marche,
            'marcheId' => $marcheId,
            'delai' => ceil($marche->getAvenants()[count($marche->getAvenants()) - 1]->getDelai()),
        ));
    }

    /**
     * Creates a new Decompte entity.
     *
     */
    public function createAction(Request $request, $marcheId)
    {
        $this->verifyAccess();

        $entity = new Decompte();
        $em = $this->getDoctrine()->getManager();

        /** @var \Laiso\ArmBundle\Entity\Marche $marche */
        $marche = $em->getRepository('LaisoArmBundle:Marche')->find($marcheId);

        if (!$marche) throw new NotFoundHttpException('Unable to find Marche entity');

        /** @var \Laiso\ArmBundle\Entity\Decompte $dernierDecompte */
        $dernierDecompte = null;

        for ($k = count($marche->getAvenants()) - 1; $dernierDecompte == null && $k >= 0; $k--) {
            /** @var \Laiso\ArmBundle\Entity\Avenant $avenant */
            $avenant = $marche->getAvenants()[$k];
            if ($avenant && count($avenant->getDecomptes()) > 0)
                $dernierDecompte = $avenant->getDecomptes()[count($avenant->getDecomptes()) - 1];
        }

        /** @var \Laiso\ArmBundle\Entity\Avenant $avenant */
        $avenant = $marche->getAvenants()[count($marche->getAvenants()) - 1];
        $entity->setAvenant($avenant);

        $form = $this->createCreateForm($entity, $marcheId);

        $form->handleRequest($request);

        if ($form->isValid()) {

            if($dernierDecompte && $dernierDecompte->getDateDecompte() >= $entity->getDateDecompte()){
                $this->addFlash('error', 'La date du décompte n\'est pas valide');
            }else {
                $entity->setFerme(false);
                $entity->setMontantAvance(0);
                $entity->setMontantBrutHTVA(0);
                $entity->setMontantRetenueHTVA(0);
                $entity->setMontantAcompteHTVA(0);

                $em->persist($entity);

                $em->flush();

                return $this->redirect($this->generateUrl('decompte_show', array(
                    'decompteId' => $entity->getId(),
                    'marcheId' => $marcheId
                )));
            }
        }

        return $this->render('LaisoArmBundle:Decompte:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Apercu avant impression
     * @param $decompteId
     * @param $marcheId
     * @return Response
     */
    public function dpRectoAction($decompteId, $marcheId)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();
        /** @var \Laiso\ArmBundle\Entity\Marche $marche */
        $marche = $em->getRepository('LaisoArmBundle:Marche')->find($marcheId);
        if (!$marche)
            throw new NotFoundHttpException('Unable to find Marché entity');
        $decompte = $em->getRepository('LaisoArmBundle:Decompte')->find($decompteId);
        if (!$decompte)
            throw new NotFoundHttpException('Unable to find Decompte entity');

        $dp = $this->getDP($decompte, $marche);

        return $this->render('@LaisoArm/Decompte/preview.html.twig', array(
            'attachements' => $dp->getAttachements(),
            'wNonTermines' => $dp->getWNonTermines(),
            'wTermines' => $dp->getWTermines(),
            'retenues' => $dp->getRetenues(),
            'avance' => $dp->getAvance(),
            'remboursements' => $dp->getRemboursements(),
            'restitutions' => $dp->getRestitutions(),
            'entity' => $dp->getEntity(),
            'marche' => $dp->getMarche(),
            'resteAPayer' => $dp->getResteAPayer(),
            'final' => $decompte->getNumero() == ceil($decompte->getAvenant()->getDelai())
        ));
    }

    /**
     * @param $decompteId
     * @param $marcheId
     * @return Response
     */
    public function printDpRectoAction($decompteId, $marcheId)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();
        /** @var \Laiso\ArmBundle\Entity\Marche $marche */
        $marche = $em->getRepository('LaisoArmBundle:Marche')->find($marcheId);
        if (!$marche)
            throw new NotFoundHttpException('Unable to find Marché entity');
        $decompte = $em->getRepository('LaisoArmBundle:Decompte')->find($decompteId);
        if (!$decompte)
            throw new NotFoundHttpException('Unable to find Decompte entity');

        $dp = $this->getDP($decompte, $marche);

        $html = $this->renderView('@LaisoArm/Decompte/print.html.twig', array(
            'attachements' => $dp->getAttachements(),
            'wNonTermines' => $dp->getWNonTermines(),
            'wTermines' => $dp->getWTermines(),
            'retenues' => $dp->getRetenues(),
            'avance' => $dp->getAvance(),
            'remboursements' => $dp->getRemboursements(),
            'restitutions' => $dp->getRestitutions(),
            'entity' => $dp->getEntity(),
            'marche' => $dp->getMarche(),
            'resteAPayer' => $dp->getResteAPayer(),
            'final' => $decompte->getNumero() == ceil($decompte->getAvenant()->getDelai())
        ));

        $fileName = $decompte->getAvenant()->getMarche() . "-DP-" . $decompte->getNumero();

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="' . $fileName . '.pdf"'
            )
        );
    }

    /**
     * @param $decompte
     * @param $marche
     * @return DP
     *
     */
    private function getDP($decompte, $marche)
    {
        $wNonTermines = array();
        $avance = null;
        $wTermines = array();
        $retenues = array();
        $remboursements = array();
        $restitution = array();

        $cumulTermine = 0.0;
        $cumulRetenue = 0.0;
        $cumulRemboursement = 0.0;
        $cumulRestitution = 0.0;
        $cumulRemboursementAvance = 0.0;

        /** @var \Laiso\ArmBundle\Entity\Attachement $attachement */
        foreach ($decompte->getAttachements() as $attachement) {
            if ($attachement->isTravaux())
                $cumulTermine += $attachement->getMontantHTVA();
            elseif ($attachement->isRemboursement())
                $cumulRemboursementAvance += $attachement->getMontantHTVA();
            elseif ($attachement->isRetenue())
                $cumulRetenue += $attachement->getMontantHTVA();
            elseif ($attachement->isRemboursement())
                $cumulRemboursement += $attachement->getMontantHTVA();
            elseif ($attachement->isRestitution())
                $cumulRestitution + $attachement->getMontantHTVA();
        }

        $attachements = $this->getAllAttachement($marche, $decompte);

        /** @var \Laiso\ArmBundle\Entity\Attachement $attachement */
        foreach ($attachements as $attachement) {
            if ($attachement->isAvance())
                $avance = $attachement;
            elseif ($attachement->isTravaux())
                array_push($wNonTermines, $attachement);
            elseif ($attachement->isRetenue())
                array_push($retenues, $attachement);
            elseif ($attachement->isRemboursement())
                array_push($remboursements, $attachement);
            elseif ($attachement->isRestitution())
                array_push($restitution, $attachement);
        }

        $resteApayer = $cumulTermine - $cumulRetenue + $cumulRestitution - $cumulRemboursementAvance;
        $resteApayer = $decompte->getNumero() == 0 ? $decompte->getMontantBrutHTVA() : $resteApayer;

        return new DP($attachements, $wNonTermines, $wTermines, $retenues, $avance, $remboursements, $restitution, $decompte, $marche, $resteApayer);
    }


    public function recapitulationAction($marcheId, $decompteId)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();
        $decompte = $em->getRepository('LaisoArmBundle:Decompte')->find($decompteId);
        $marche = $em->getRepository('LaisoArmBundle:Marche')->find($marcheId);

        if (!$marche || !$decompte)
            throw new NotFoundHttpException('Unable to find Decompte or Marche entities');

        $avance = 0.0;
        $travaux = 0.0;
        $penalites = 0.0;
        $retenuesDeGaranties = 0.0;
        $restitutionRetenues = 0.0;
        $remboursement = 0.0;

        /** @var \Laiso\ArmBundle\Entity\Attachement $attachement */
        $dernierAttachement = null;

        $tva = $marche->getEntrepriseAssujettie() ? $marche->getLotissement()->getCampagne()->getTVA() : 0;

        $acomptesDelivres = 0.0;
        /** @var \Laiso\ArmBundle\Entity\Avenant $avenant */
        foreach ($marche->getAvenants() as $avenant) {
            /** @var \Laiso\ArmBundle\Entity\Decompte $dp */
            foreach ($avenant->getDecomptes() as $dp) {
                if ($dp->getNumero() == 0) {
                    $avance = $dp->getMontantBrutHTVA();
                }
                if ($dp->getNumero() <= $decompte->getNumero()) {
                    if ($dp->getNumero() < $decompte->getNumero())
                        $acomptesDelivres += $dp->getMontantAcompteHTVA();

                    /** @var \Laiso\ArmBundle\Entity\Attachement $attachement */
                    foreach ($dp->getAttachements() as $attachement) {
                        if ($attachement->isTravaux()) {
                            $travaux += $attachement->getMontantHTVA();
                            $dernierAttachement = $attachement;
                        } elseif ($attachement->isRemboursement())
                            $remboursement += $attachement->getMontantHTVA();
                        elseif ($attachement->isRetenue())
                            $retenuesDeGaranties += $attachement->getMontantHTVA();
                        elseif ($attachement->isRestitution())
                            $restitutionRetenues += $attachement->getMontantHTVA();
                        elseif ($attachement->isPenalite())
                            $penalites += $attachement->getMontantHTVA();
                    }
                }
            }
        }

        return $this->render('@LaisoArm/Decompte/recapitulation.html.twig', array(
            'entity' => $decompte,
            'marche' => $marche,
            'termine' => $decompte->getNumero() == ceil($decompte->getAvenant()->getDelai()),
            'avance' => $avance,
            'tva' => $tva,
            'travaux' => $travaux,
            'penalite' => $penalites,
            'montantBrut' => $dernierAttachement ? $dernierAttachement->getMontantHTVA() : 0,
            'remboursement' => $remboursement,
            'restitution' => $restitutionRetenues,
            'acomptesDelivres' => $acomptesDelivres,
            'retenuesCumulees' => $retenuesDeGaranties,
        ));
    }


    public function exportRecapitulationAction($marcheId, $decompteId)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();
        $decompte = $em->getRepository('LaisoArmBundle:Decompte')->find($decompteId);
        $marche = $em->getRepository('LaisoArmBundle:Marche')->find($marcheId);

        if (!$marche || !$decompte)
            throw new NotFoundHttpException('Unable to find Decompte or Marche entities');

        $avance = 0.0;
        $travaux = 0.0;
        $penalites = 0.0;
        $retenuesDeGaranties = 0.0;
        $restitutionRetenues = 0.0;
        $remboursement = 0.0;

        /** @var \Laiso\ArmBundle\Entity\Attachement $attachement */
        $dernierAttachement = null;

        $tva = $marche->getEntrepriseAssujettie() ? $marche->getLotissement()->getCampagne()->getTVA() : 0;

        $acomptesDelivres = 0.0;
        /** @var \Laiso\ArmBundle\Entity\Avenant $avenant */
        foreach ($marche->getAvenants() as $avenant) {
            /** @var \Laiso\ArmBundle\Entity\Decompte $dp */
            foreach ($avenant->getDecomptes() as $dp) {
                if ($dp->getNumero() == 0) {
                    $avance = $dp->getMontantBrutHTVA();
                }
                if ($dp->getNumero() <= $decompte->getNumero()) {
                    if ($dp->getNumero() < $decompte->getNumero())
                        $acomptesDelivres += $dp->getMontantAcompteHTVA();

                    /** @var \Laiso\ArmBundle\Entity\Attachement $attachement */
                    foreach ($dp->getAttachements() as $attachement) {
                        if ($attachement->isTravaux()) {
                            $travaux += $attachement->getMontantHTVA();
                            $dernierAttachement = $attachement;
                        } elseif ($attachement->isRemboursement())
                            $remboursement += $attachement->getMontantHTVA();
                        elseif ($attachement->isRetenue())
                            $retenuesDeGaranties += $attachement->getMontantHTVA();
                        elseif ($attachement->isRestitution())
                            $restitutionRetenues += $attachement->getMontantHTVA();
                        elseif ($attachement->isPenalite())
                            $penalites += $attachement->getMontantHTVA();
                    }
                }
            }
        }

        $html = $this->renderView('@LaisoArm/Decompte/recapitulation.export.html.twig', array(
            'entity' => $decompte,
            'marche' => $marche,
            'termine' => $decompte->getNumero() == ceil($decompte->getAvenant()->getDelai()),
            'avance' => $avance,
            'tva' => $tva,
            'travaux' => $travaux,
            'penalite' => $penalites,
            'montantBrut' => $dernierAttachement ? $dernierAttachement->getMontantHTVA() : 0,
            'remboursement' => $remboursement,
            'restitution' => $restitutionRetenues,
            'acomptesDelivres' => $acomptesDelivres,
            'retenuesCumulees' => $retenuesDeGaranties,
        ));

        $fileName = $decompte->getAvenant()->getMarche() . "-DP-VERSO-" . $decompte->getNumero() . '-' . $decompte->getDateDecompte()->format('d-M-Y');

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="' . $fileName . '.pdf"'
            )
        );
    }

    /**
     * Creates a form to create a Decompte entity.
     *
     * @param Decompte $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Decompte $entity, $marcheId)
    {
        $form = $this->createForm(new DecompteType(), $entity, array(
            'action' => $this->generateUrl('decompte_create', array(
                'marcheId' => $marcheId,
            )),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Valider', 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }

    /**
     * Displays a form to create a new Decompte entity.
     *
     */
    public function newAction(Request $request, $marcheId)
    {
        $this->verifyAccess();

        $entity = new Decompte();
        $allowed = true;

        $marche = $this->getDoctrine()->getRepository('LaisoArmBundle:Marche')->find($marcheId);

        if (!$marche) throw new NotFoundHttpException('Unable to find Marche entity');

        /** @var \Laiso\ArmBundle\Entity\Decompte $decompte */
        $decompte = null;

        /** @var \Laiso\ArmBundle\Entity\Avenant $avenant */
        $avenant = null;


        /** @var \Laiso\ArmBundle\Entity\Avenant $av */
        foreach ($marche->getAvenants() as $av) {
            $avenant = $av;
            /** @var \Laiso\ArmBundle\Entity\Decompte $dp */
            foreach ($av->getDecomptes() as $dp) {
                $decompte = $dp;
            }
        }

        if ($decompte) {
            $entity->setNumero($decompte->getNumero() + 1);
            if (!$decompte->isFerme())
                $allowed = false;
        } else
            $entity->setNumero(1);

        $entity->setAvenant($avenant);
        $form = $this->createCreateForm($entity, $marcheId);

        if ($request->isXmlHttpRequest()) {
            $response = $this->render('LaisoArmBundle:Decompte/includes:new_decompte.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView(),
                'marcheId' => $marcheId
            ));

            if ($allowed == false)
                $response = new Response("<h3 class='align-center fg-orange'>Veuillez clôturer le dernier décompte avant d'en créer un nouveau.</h3>", 403);

            return $response;
        }

        $response = $this->render('LaisoArmBundle:Decompte:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
            'marcheId' => $marcheId
        ));

        if ($allowed)
            return $response;
        else {
            $this->addFlash('error', 'Vous devez clôturer le dernier décompte avant d\'en créer un nouveau.');
            return $this->redirect($this->generateUrl('decompte', array(
                'marcheId' => $marcheId,
            )));
        }

    }

    /**
     * Affichage d'un formulaire de création d'une avance de démarrage
     *
     * @param Request $request
     * @param $marcheId
     * @return Response
     */
    public function zeroAction(Request $request, $marcheId)
    {
        $this->verifyAccess();

        $entity = new Avance();
        $marche = $this->getDoctrine()->getRepository('LaisoArmBundle:Marche')->find($marcheId);

        if (!$marche)
            throw new NotFoundHttpException('Unable to find Marché entity');

        $taux = $marche->getLotissement()->getCampagne()->getTauxAvance();
        $entity->setMarche($marche);
        $entity->setMontant($marche->getMontant() * $taux / 100);

        $form = $this->createZeroForm($entity, $marcheId);

        if ($request->isXmlHttpRequest())
            return $this->render("LaisoArmBundle:Avance/includes:confirm.html.twig", array(
                'entity' => $entity,
                'taux' => $taux,
                'form' => $form->createView(),
            ));

        return $this->render("LaisoArmBundle:Avance:confirm.html.twig", array(
            'entity' => $entity,
            'taux' => $taux,
            'form' => $form->createView(),
        ));
    }

    /**
     * Création d'un formulaire de création d'un avance de démarrage
     *
     * @param Avance $entity
     * @param $marcheId
     * @return \Symfony\Component\Form\Form
     */
    private function createZeroForm(Avance $entity, $marcheId)
    {
        $form = $this->createForm(new AvanceType(), $entity, array(
            'action' => $this->generateUrl('decompte_create_zero', array(
                'marcheId' => $marcheId
            )),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Valider', 'attr' => array(
            'class' => 'button warning place-right'
        )));

        return $form;
    }

    /**
     * Persistance d'une avance de démarrage
     *
     * @param Request $request
     * @param $marcheId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createZeroAction(Request $request, $marcheId)
    {
        $entity = new Avance();
        $em = $this->getDoctrine()->getManager();
        $marche = $this->getDoctrine()->getRepository('LaisoArmBundle:Marche')->find($marcheId);

        if (!$marche)
            throw new NotFoundHttpException('Unable to find Marché entity');

        $form = $this->createZeroForm($entity, $marcheId);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $taux = $marche->getLotissement()->getCampagne()->getTauxAvance();

            $montant = $marche->getMontantInitial() * $taux / 100;

            $attachement = new Attachement();
            $decompte = new Decompte();
            $decompte->setNumero(0);
            $decompte->setAvenant($marche->getAvenants()[count($marche->getAvenants()) - 1]);
            $decompte->setMontantBrutHTVA($montant);
            $decompte->setMontantAcompteHTVA($montant);
            $decompte->setMontantRetenueHTVA(0);
            $decompte->setDateDecompte($entity->getDate());
            $decompte->setFerme(true);
            $decompte->setMontantAvance(0);

            $attachement->setNumero(0);
            $attachement->setType('AVANCE');
            $attachement->setValide(true);
            $attachement->setDateAttachement($entity->getDate());
            $attachement->setDecompte($decompte);
            $attachement->setMontantHTVA($montant);

            $em->persist($decompte);
            $em->persist($attachement);

            /*$em->persist($entity);*/

            $em->flush();
        }

        return $this->redirect($this->generateUrl('decompte', array('marcheId' => $marcheId)));
    }

    /**
     * Finds and displays a Decompte entity.
     *
     */
    public function showAction($decompteId, $marcheId)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:Decompte')->find($decompteId);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Decompte entity.');
        }

        $resteAPayer = $entity->getMontantsBrutsPrecedentsHTVA() + $entity->getMontantBrutHTVA();

        return $this->render('LaisoArmBundle:Decompte:show.html.twig', array(
            'entity' => $entity,
            'resteAPayer' => $resteAPayer,
        ));
    }

    /**
     *
     * @param $decompteId
     * @param $marcheId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function cpaAction($decompteId, $marcheId)
    {
        $this->verifyAccess();

        $entity = $this->getDoctrine()->getRepository('LaisoArmBundle:Decompte')->find($decompteId);
        if (!$entity)
            throw new NotFoundHttpException('Unable to find Decompte entity');

        $cpa = $this->getCPA($entity);

        return $this->render('LaisoArmBundle:Decompte:cpa.html.twig', array(
            'entity' => $cpa->getEntity(),
            'resteAPayer' => $cpa->getResteAPayer(),
            'tva' => $cpa->getTva(),
            'cumulRetenue' => $cpa->getCumulRetenue(),
            'montantRemboursementAvance' => $cpa->getMontantRemboursementAvance(),
            'restitutionRetenue' => $cpa->getRestitutionRetenue(),
            'acompte' => $cpa->getAcompte(),
            'numero' => $cpa->getNumero(),
        ));
    }

    /**
     * @param \Laiso\ArmBundle\Entity\Decompte $entity
     * @return CPA
     */
    private function getCPA($entity)
    {
        $marche = $entity->getAvenant()->getMarche();

        $numero = $entity->getAvenant()->getMarche()->hasAvance() ? $entity->getNumero() + 1 : $entity->getNumero();
        $tva = $entity->getAvenant()->getMarche()->getEntrepriseAssujettie() ? $entity->getAvenant()->getMarche()->getLotissement()->getCampagne()->getTVA() : 0;

        $cumulRetenue = 0.0;
        $montantRemboursementAvance = 0.0;
        $restitutionRetenue = 0.0;
        $acompte = 0.0;


        $resteAPayer = $entity->getMontantsBrutsPrecedentsHTVA();

        if($entity->getNumero() > 0)
            $resteAPayer  += $entity->getMontantBrutHTVA();

        /** @var \Laiso\ArmBundle\Entity\Avenant $avenant */
        foreach ($marche->getAvenants() as $avenant) {
            /** @var \Laiso\ArmBundle\Entity\Decompte $decompte */
            foreach ($avenant->getDecomptes() as $decompte) {
                if($decompte->getNumero() == 0){
                    $resteAPayer += $decompte->getMontantBrutHTVA();
                }
                if ($decompte->getNumero() <= $entity->getNumero()) {
                    $cumulRetenue += $decompte->getMontantRetenueHTVA();
                    $montantRemboursementAvance += $decompte->getMontantAvance();
                    $acompte += $decompte->getMontantAcompteHTVA();

                    /** @var \Laiso\ArmBundle\Entity\Attachement $attachement */
                    foreach ($decompte->getAttachements() as $attachement) {
                        if($attachement->isRestitution()){
                            $restitutionRetenue += $attachement->getMontantHTVA();
                        }
                    }
                }
            }
        }

        $acompte -= $entity->getMontantAcompteHTVA();

        return new CPA($entity, $resteAPayer, $tva, $cumulRetenue, $montantRemboursementAvance, $restitutionRetenue, $acompte, $numero);
    }

    /**
     * @param $decompteId
     * @param $marcheId
     * @return Response
     */
    public function exportCpaAction($decompteId, $marcheId)
    {
        $this->verifyAccess();

        $entity = $this->getDoctrine()->getRepository('LaisoArmBundle:Decompte')->find($decompteId);
        if (!$entity)
            throw new NotFoundHttpException('Unable to find Decompte entity');

        $cpa = $this->getCPA($entity);

        $html = $this->renderView('LaisoArmBundle:Decompte:cpa.export.html.twig', array(
            'entity' => $cpa->getEntity(),
            'resteAPayer' => $cpa->getResteAPayer(),
            'tva' => $cpa->getTva(),
            'cumulRetenue' => $cpa->getCumulRetenue(),
            'montantRemboursementAvance' => $cpa->getMontantRemboursementAvance(),
            'restitutionRetenue' => $cpa->getRestitutionRetenue(),
            'acompte' => $cpa->getAcompte(),
            'numero' => $cpa->getNumero(),
        ));

        $fileName = $entity->getAvenant()->getMarche() . "-CPA-" . $entity->getNumero();

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="' . $fileName . '.pdf"'
            )
        );
    }

    /**
     * Displays a form to edit an existing Decompte entity.
     *
     */
    public function editAction(Request $request, $decompteId, $marcheId)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:Decompte')->find($decompteId);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Decompte entity.');
        }

        $editForm = $this->createEditForm($entity, $marcheId);
        $deleteForm = $this->createDeleteForm($decompteId, $marcheId);

        if ($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:Decompte/includes:edit_decompte.html.twig', array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));

        return $this->render('LaisoArmBundle:Decompte:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Decompte entity.
     *
     * @param Decompte $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Decompte $entity, $marcheId)
    {
        $form = $this->createForm(new DecompteType(), $entity, array(
            'action' => $this->generateUrl('decompte_update', array(
                'decompteId' => $entity->getId(),
                'marcheId' => $marcheId,
            )),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Mettre à jour', 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }

    /**
     * Edits an existing Decompte entity.
     *
     */
    public function updateAction(Request $request, $decompteId, $marcheId)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:Decompte')->find($decompteId);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Decompte entity.');
        }

        $deleteForm = $this->createDeleteForm($decompteId, $marcheId);
        $editForm = $this->createEditForm($entity, $marcheId);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('decompte_show', array(
                'decompteId' => $decompteId,
                'marcheId' => $marcheId
            )));
        }

        return $this->render('LaisoArmBundle:Decompte:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Decompte entity.
     *
     */
    public function deleteAction(Request $request, $decompteId, $marcheId)
    {
        $this->verifyAccess();

        $form = $this->createDeleteForm($decompteId, $marcheId);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('LaisoArmBundle:Decompte')->find($decompteId);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Decompte entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('decompte', array('marcheId' => $marcheId)));
    }

    /**
     * Creates a form to delete a Decompte entity by id.
     *
     * @param mixed $decompteId The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($decompteId, $marcheId)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('decompte_delete', array(
                'decompteId' => $decompteId,
                'marcheId' => $marcheId
            )))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Supprimer', 'attr' => array(
                'class' => 'danger place-right button'
            )))
            ->getForm();
    }

    /**
     * @param \Laiso\ArmBundle\Entity\Marche $marche
     * @param \Laiso\ArmBundle\Entity\Decompte $dp
     * @return array
     */
    private function getAllAttachement($marche, $dp)
    {
        $attachements = array();

        /** @var \Laiso\ArmBundle\Entity\Avenant $avenant */
        foreach ($marche->getAvenants() as $avenant) {
            /** @var \Laiso\ArmBundle\Entity\Decompte $decompte */
            foreach ($avenant->getDecomptes() as $decompte) {
                if ($decompte->getNumero() <= $dp->getNumero())
                    /** @var \Laiso\ArmBundle\Entity\Attachement $attachement */
                    foreach ($decompte->getAttachements() as $attachement) {
                        array_push($attachements, $attachement);
                    }
            }
        }
        return $attachements;
    }

    /**
     * Clôture d'un decompte
     *
     * @param $marcheId
     * @param $decompteId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function clotureAction($marcheId, $decompteId)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();
        $decompte = $em->getRepository('LaisoArmBundle:Decompte')->find($decompteId);
        if (!$decompte)
            throw new NotFoundHttpException('Unable to find Decompte entity');

        $allowed = true;
        if (count($decompte->getAttachements()) == 0)
            $allowed = false;

        for ($k = sizeof($decompte->getAttachements()) - 1; $k >= 0 && $allowed = true; $k--) {
            if ($attachement = $decompte->getAttachements()[$k]->getValide() == false)
                $allowed = false;
        }

        if ($allowed) {
            $decompte->setFerme(true);
            $em->persist($decompte);
            $em->flush();

            $delai = $decompte->getAvenant()->getDelai();

            if ($decompte->getNumero() == ceil($delai)) {
                /** @var \Laiso\ArmBundle\Entity\Attachement $dernierAttachement */
                $dernierAttachement = $decompte->getAttachements()[count($decompte->getAttachements()) - 1];

                $restitution = new Attachement();
                $restitution->setType('RESTITUTION');
                $restitution->setDateAttachement($decompte->getDateDecompte());
                $restitution->setNumero($dernierAttachement->getNumero() + 1);
                $restitution->setDecompte($decompte);
                $restitution->setValide(true);

                $retenues = array();
                $montant = 0.0;
                $attachements = $this->getAllAttachement($decompte->getAvenant()->getMarche(), $decompte);
                /** @var \Laiso\ArmBundle\Entity\Attachement $attachement */
                foreach ($attachements as $attachement) {
                    if ($attachement->isRetenue()) {
                        array_push($retenues, $attachement);
                        $montant += $attachement->getMontantHTVA();
                    }
                }

                $restitution->setMontantHTVA($montant);

                /** @var \Laiso\ArmBundle\Entity\Attachement $retenue */
                foreach ($retenues as $retenue) {
                    $retenue->setAttachement($restitution);
                    $em->persist($retenue);
                }
                $em->persist($restitution);
                $em->flush();

                $this->addFlash('success', 'Attachement de restitution de retenues de garanties créé avec succès');
            }
        } else {
            $this->addFlash('error', 'Veuillez valider tous les attachements du décompte n° ' . $decompte->getNumero() . ' avant de le clôturer.');
        }


        $decompte = $decompte->selfUpdate();
        $em->persist($decompte);
        $em->flush();

        return $this->redirect($this->generateUrl('decompte_show', array(
            'decompteId' => $decompteId,
            'marcheId' => $marcheId,
        )));

    }

    public function apercuAction($marcheId)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();
        $marche = $em->getRepository('LaisoArmBundle:Marche')->find($marcheId);
        if (!$marche)
            throw new NotFoundHttpException('Unable to find Marché entity');
        return new Response('<h4 class="fg-orange"> TODO: <br> okay - marché ' . $marche . '</h4>');
    }
}
