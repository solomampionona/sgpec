<?php

namespace Laiso\ArmBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class DashboardController extends Controller
{
    public function indexAction()
    {
        if(!$this->getUser()->hasRole('ROLE_DTEC'))
            throw new AccessDeniedHttpException('Vous n\'avez pas le dreoit d\'accéder à ce ressource');

        $em = $this->getDoctrine()->getRepository('LaisoArmBundle:Marche');
        $marches = $em->findAll();
        return $this->render("LaisoArmBundle:Dashboard:index.html.twig", array(
            'marches' => $marches,
        ));
    }
}
