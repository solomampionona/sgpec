<?php

namespace Laiso\ArmBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Laiso\ArmBundle\Entity\Campagne;
use Laiso\ArmBundle\Form\CampagneType;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Campagne controller.
 *
 */
class CampagneController extends Controller
{

    private function verifyAccess()
    {
        if (!$this->getUser()->hasRole('ROLE_DTEC', 'ROLE_CI', 'ROLE_ASSIST'))
            throw new AccessDeniedHttpException("Vous n'avez pas le droit d'accéder à ce ressource");
    }

    /**
     * Lists all Campagne entities.
     *
     */
    public function indexAction(Request $request)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LaisoArmBundle:Campagne')->findAll();
        $paginator = $this->get('knp_paginator');

        return $this->render('LaisoArmBundle:Campagne:index.html.twig', array(
            'entities' => $paginator->paginate($entities, $request->query->getInt('page', 1), 10)
        ));
    }
    /**
     * Creates a new Campagne entity.
     *
     */
    public function createAction(Request $request)
    {
        $this->verifyAccess();

        $entity = new Campagne();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Campagne créée avec succès');

            return $this->redirect($this->generateUrl('campagne_show', array('id' => $entity->getId())));
        }

        return $this->render('LaisoArmBundle:Campagne:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Campagne entity.
     *
     * @param Campagne $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Campagne $entity)
    {
        $this->verifyAccess();

        $form = $this->createForm(new CampagneType(), $entity, array(
            'action' => $this->generateUrl('campagne_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Enregistrer', 'attr' => array(
            'class' => 'button success place-right',
        )));

        return $form;
    }

    /**
     * Displays a form to create a new Campagne entity.
     *
     */
    public function newAction(Request $request)
    {
        $this->verifyAccess();

        $entity = new Campagne();
        $form   = $this->createCreateForm($entity);

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:Campagne/includes:new_campagne.html.twig', array(
                'entity' => $entity,
                'form'   => $form->createView(),
            ));

        return $this->render('LaisoArmBundle:Campagne:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Campagne entity.
     *
     */
    public function showAction($id)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:Campagne')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Campagne entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('LaisoArmBundle:Campagne:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Campagne entity.
     *
     */
    public function editAction(Request $request, $id)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:Campagne')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Campagne entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:Campagne/includes:edit_campagne.html.twig', array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));

        return $this->render('LaisoArmBundle:Campagne:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Campagne entity.
    *
    * @param Campagne $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Campagne $entity)
    {
        $form = $this->createForm(new CampagneType(), $entity, array(
            'action' => $this->generateUrl('campagne_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Mettre à jour', 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }
    /**
     * Edits an existing Campagne entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:Campagne')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Campagne entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Campagne mise à jour avec succès');

            return $this->redirect($this->generateUrl('campagne_show', array('id' => $id)));
        }

        return $this->render('LaisoArmBundle:Campagne:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Campagne entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $this->verifyAccess();

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('LaisoArmBundle:Campagne')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Campagne entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('campagne'));
    }

    /**
     * Creates a form to delete a Campagne entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('campagne_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Supprimer', 'attr' => array(
                'class' => 'button danger place-right'
            )))
            ->getForm()
        ;
    }
}
