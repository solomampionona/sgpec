<?php

namespace Laiso\ArmBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('LaisoArmBundle:Default:index.html.twig');
    }

    public function uiAction()
    {
        return $this->render('LaisoArmBundle:Default:menuAdmin.html.twig');
    }

    public function dtecAction()
    {
        return $this->render('LaisoArmBundle:Default:menuDtec.html.twig');
    }
}
