<?php

namespace Laiso\ArmBundle\Controller;

use Laiso\ArmBundle\Entity\AffectationPrix;
use Laiso\ArmBundle\Entity\LigneDQE;
use Laiso\ArmBundle\Form\LigneDQE2Type;
use Laiso\ArmBundle\Form\LigneDQEType;
use Laiso\ArmBundle\Utils\DQE;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Laiso\ArmBundle\Entity\Avenant;
use Laiso\ArmBundle\Form\AvenantType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Avenant controller.
 *
 */
class AvenantController extends Controller
{

    private function verifyAccess()
    {
        if (!($this->getUser()->hasRole('ROLE_DTEC') || $this->getUser()->hasRole('ROLE_CI')))
            throw new AccessDeniedHttpException("Vous n'avez pas le droit d'accéder à ce ressource");
    }

    /**
     * Lists all Avenant entities.
     *
     */
    public function indexAction($marcheId)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LaisoArmBundle:Avenant')->findBy(array(
            'marche' => $marcheId
        ));

        return $this->render('LaisoArmBundle:Avenant:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new Avenant entity.
     *
     */
    public function createAction(Request $request, $marcheId)
    {
        $this->verifyAccess();

        $entity = new Avenant();
        $form = $this->createCreateForm($entity, $marcheId);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $entity->setMarche($em->getRepository("LaisoArmBundle:Marche")->find($marcheId));
            $avenants = $em->getRepository("LaisoArmBundle:Avenant")->findBy(array(
                'marche' => $marcheId
            ));

            $max = 0;

            foreach ($avenants as $avenant) {
                if ($avenant->getLibelle()->getNumero() > $max)
                    $max = $avenant->getLibelle()->getNumero();
            }

            $entity->setLibelle($em->getRepository("LaisoArmBundle:LibelleAvenant")->findOneByNumero($max + 1));
            $entity->setValide(false);

            $em->persist($entity);

            $em->flush();

            $this->defineDqe($entity);

            return $this->redirect($this->generateUrl('avenant_show', array(
                'numero' => $entity->getLibelle()->getNumero(),
                'marcheId' => $marcheId
            )));
        }

        return $this->render('LaisoArmBundle:Avenant:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Avenant entity.
     *
     * @param Avenant $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Avenant $entity, $marcheId)
    {
        $form = $this->createForm(new AvenantType(), $entity, array(
            'action' => $this->generateUrl('avenant_create', array(
                'marcheId' => $marcheId
            )),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Enregistrer', 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }

    /**
     * Displays a form to create a new Avenant entity.
     *
     */
    public function newAction(Request $request, $marcheId)
    {
        $this->verifyAccess();

        $entity = new Avenant();
        $avenants = $this->getDoctrine()->getRepository("LaisoArmBundle:Avenant")->findBy(array(
            'marche' => $marcheId
        ));

        $delai = 0; $numero = 0;
        foreach($avenants as $avenant){
            $delai = $avenant->getDelai();
            $numero++;
        }

        $entity->setDelai($delai);
        $libelle = $this->getDoctrine()->getRepository("LaisoArmBundle:LibelleAvenant")->findOneByNumero($numero);

        $entity->setLibelle($libelle);
        $entity->setMarche($this->getDoctrine()->getRepository('LaisoArmBundle:Marche')->find($marcheId));

        $form   = $this->createCreateForm($entity, $marcheId);

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:Avenant/includes:new_avenant.html.twig', array(
                'entity' => $entity,
                'form'   => $form->createView(),
            ));

        return $this->render('LaisoArmBundle:Avenant:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Avenant entity.
     *
     */
    public function showAction($marcheId, $numero)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:Avenant')->findOneBy(
            array(
                'marche' => $marcheId,
                'libelle' => $em->getRepository('LaisoArmBundle:LibelleAvenant')->findBy(array(
                    'numero' => $numero
                ))
            )
        );
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Avenant entity.');
        }

        if($entity->getMarche()->getLotissement()->getValide() == false)
            throw new NotFoundHttpException('Marché non trouvé');

        $deleteForm = $this->createDeleteForm($numero, $marcheId);

        $dqe = new DQE();

        $dqe->setAvenant($entity);
        $dqe->setConsistances($entity->getMarche()->getLotissement()->getConsistances());

        return $this->render('LaisoArmBundle:Avenant:show.html.twig', array(
            'entity'      => $entity,
            'dqe'         => $dqe,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Avenant entity.
     *
     */
    public function editAction(Request $request, $marcheId, $numero)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:Avenant')->findOneBy(array(
            'marche' => $marcheId,
            'libelle' => $em->getRepository('LaisoArmBundle:LibelleAvenant')->findOneBy(array(
                'numero' => $numero
            )),
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Avenant entity.');
        }
        if($entity->getLibelle()->getNumero() == 0){
            $entity->setDelai($entity->getMarche()->getLotissement()->getDelai());
        }

        $editForm = $this->createEditForm($entity, $marcheId);
        $deleteForm = $this->createDeleteForm($numero, $marcheId);

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:Avenant/includes:edit_avenant.html.twig', array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));

        return $this->render('LaisoArmBundle:Avenant:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Avenant entity.
    *
    * @param Avenant $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Avenant $entity, $marcheId)
    {
        $form = $this->createForm(new AvenantType(), $entity, array(
            'action' => $this->generateUrl('avenant_update', array(
                'numero' => $entity->getLibelle()->getNumero(),
                'marcheId' => $marcheId
            )),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Mettre à jour', 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }
    /**
     * Edits an existing Avenant entity.
     *
     */
    public function updateAction(Request $request, $numero, $marcheId)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:Avenant')->findOneBy(array(
            'marche' => $marcheId,
            'libelle' => $em->getRepository('LaisoArmBundle:LibelleAvenant')->findOneBy(array(
                'numero' => $numero
            ))->getId()
        ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Avenant entity.');
        }

        $deleteForm = $this->createDeleteForm($numero, $marcheId);
        $editForm = $this->createEditForm($entity, $marcheId);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $dao = $entity->getMarche()->getLotissement();
            $dao->setDelai($entity->getDelai());
            $em->persist($dao);

            $em->flush();

            if($entity->getLibelle()->getNumero() == 0)
                return $this->redirect($this->generateUrl('marche_show', array(
                    'id' => $marcheId
                )));

            return $this->redirect($this->generateUrl('avenant', array(
                'marcheId' => $marcheId
            )));
        }

        return $this->render('LaisoArmBundle:Avenant:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * 
     * @param Request $request
     * @param $marcheId
     * @param $consistance
     * @param $numero
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createLigneAction(Request $request, $marcheId, $consistance, $numero)
    {
        $em = $this->getDoctrine()->getManager();
        $avenant = $em->getRepository('LaisoArmBundle:Avenant')->findOneBy(array(
            'marche' => $marcheId,
            'libelle' => $em->getRepository('LaisoArmBundle:LibelleAvenant')->findOneBy(array(
                'numero' => $numero,
            )),
        ));
        $form = $this->createDqeCreateForm($avenant, $consistance, $request->query->get('returnUrl'));
        $form->handleRequest($request);
        if($form->isValid()){
            $ligneDqe = $form->getData();
            $serie = $ligneDqe->getPrix();

            $do = true;

            foreach($avenant->getLignes() as $dqe){
                if($dqe->getPrix()->getSerie()->getId() == $serie->getId()){
                    $do = false; break;
                }
            }

            if($do) {
                $affectationPrix = new AffectationPrix();
                $affectationPrix->setConsistance($em->getRepository('LaisoArmBundle:Consistance')->find($consistance));
                $affectationPrix->setPrixUnitaire($serie->getPrixUnitaire());
                $affectationPrix->setSerie($serie->getSerie());

                $ligne = new LigneDQE();
                $ligne->setAvenant($avenant);
                $ligne->setQuantite($ligneDqe->getQuantite());
                $ligne->setPrix($affectationPrix);

                $em->persist($affectationPrix);
                $em->persist($ligne);
                $em->flush();

                return $this->redirect($this->generateUrl('avenant_show', array(
                    'numero' => $numero,
                    'marcheId' => $marcheId
                )));
            }
        }
        return $this->render('LaisoArmBundle:Avenant:new_ligne.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     *
     * @param Avenant $avenant
     * @param $consistance
     * @param $returnUrl
     * @return \Symfony\Component\Form\Form
     */
    public function createDqeCreateForm(Avenant $avenant, $consistance, $returnUrl)
    {
        $form = $this->createForm(new LigneDQEType(), new LigneDQE(), array(
            'action' => $this->generateUrl('avenant_create_line', array(
                'numero' => $avenant->getLibelle()->getNumero(),
                'marcheId' => $avenant->getMarche()->getId(),
                'consistance' => $consistance,
                'returnUrl' => $returnUrl,
            )),
            'method' => 'post',
        ));
        $form->add('submit', 'submit', array('label' => 'Créer','attr' => array(
            'class' => 'button success place-right ligneDqe-submit'
        )));
        return $form;
    }
    
    public function addLigneAction(Request $request, $marcheId, $consistance, $numero)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();
        $avenant = $em->getRepository('LaisoArmBundle:Avenant')->findOneBy(array(
            'marche' => $marcheId,
            'libelle' => $em->getRepository('LaisoArmBundle:LibelleAvenant')->findBy(array(
                'numero' => $numero
            )),
        ));
        $returnUrl = $request->query->get('returnUrl');
        $form = $this->createDqeCreateForm($avenant, $consistance, $returnUrl);

        if($request->isXmlHttpRequest())
            return $this->render('@LaisoArm/Avenant/includes/new_ligneDqe.html.twig', array(
                'form' => $form->createView(),
            ));

        return $this->render('LaisoArmBundle:Avenant:new_ligne.html.twig', array(
            'form' => $form->createView(),
        ));
    }


    /**
     * Create a LigneDQE edit form
     *
     * @param LigneDQE $ligneDQE
     * @param $returnUrl
     * @return \Symfony\Component\Form\Form
     */
    public function createDqeEditForm(LigneDQE $ligneDQE, $marcheId, $consistnce, $numero, $serie, $returnUrl = null)
    {
        $form = $this->createForm(new LigneDQE2Type(), $ligneDQE, array(
            'action' => $this->generateUrl('avenant_update_ligne', array(
                'marcheId' => $marcheId,
                'consistance' => $consistnce,
                'numero' => $numero,
                'serie' => $serie,
                'returnUrl' => $returnUrl,
            )),
            'method' => 'post',
        ));
        $form->add('submit', 'submit', array('label' => 'Mettre à jour','attr' => array(
            'class' => 'button success place-right ligneDqe-submit'
        )));

        return $form;
    }

    /**
     * @param Request $request
     * @param $numero
     * @param $consistance
     * @param $serie
     * @param $marcheId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function updateLigneAction(Request $request, $numero, $consistance, $serie, $marcheId)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();

        $ligne = $em->getRepository('LaisoArmBundle:LigneDQE')->findOneBy(array(
            'avenant' => $em->getRepository('LaisoArmBundle:Avenant')->findOneBy(array(
                'marche' => $marcheId,
                'libelle' => $em->getRepository('LaisoArmBundle:LibelleAvenant')->findOneBy(array(
                    'numero' => $numero
                )))
            ),
            'prix' => $em->getRepository('LaisoArmBundle:AffectationPrix')->findOneBy(array(
                'consistance' => $consistance,
                'serie' => $em->getRepository('LaisoArmBundle:SerieDePrix')->findOneBy(array(
                    'numero' => $serie,
                )),
            )),
        ));
        if(!$ligne)
            throw new NotFoundHttpException('LigneDQE non trouvé');

        $quantite = $ligne->getQuantite();

        $form = $this->createDqeEditForm($ligne, $marcheId, $consistance, $numero, $serie);

        $form->handleRequest($request);

        if($quantite != $form->getData()->getQuantite()){

            /** @var \Laiso\ArmBundle\Entity\LigneDQE $data */
            $data = $form->getData();
            $ligne->setQuantite($data->getQuantite());

            $em->persist($ligne);
            $em->flush();

            if($request->isXmlHttpRequest())
                return new Response(json_encode(array(
                    'status' => 1,
                    'montant' => number_format($ligne->getQuantite() * $ligne->getPrix()->getPrixUnitaire(),2, '.', ' ')
                )));

            return $this->redirect($this->generateUrl('avenant_show', array(
                'numero'=> $numero,
                'marcheId'=> $marcheId
            )));
        }

        if($request->isXmlHttpRequest())
            return new Response(json_encode(array(
                'status' => 0,
            )));

        return $this->render('LaisoArmBundle:Avenant:edit_ligne.html.twig', array(
            'entity' => $ligne,
            'edit_form' => $form->createView(),
        ));
    }

    /**
     * Show edit form :)
     *
     * @param Request $request
     * @param $marcheId
     * @param $consistance
     * @param $numero
     * @param $serie
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editLigneAction(Request $request, $marcheId, $consistance, $numero, $serie)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();
        $avenant = $em->getRepository('LaisoArmBundle:Avenant')->findOneBy(array(
            'marche' => $marcheId,
            'libelle' => $em->getRepository('LaisoArmBundle:LibelleAvenant')->findOneBy(array(
                'numero' => $numero
            ))
        ));

        $sp = $em->getRepository('LaisoArmBundle:SerieDePrix')->findOneBy(array(
            'numero' => $serie,
        ));

        if(!$sp)
            throw new NotFoundHttpException('Serie de prix non trouvé');

        if(!$avenant)
            throw new NotFoundHttpException('Avenant non trouvé');

        $ligne = $em->getRepository('LaisoArmBundle:LigneDQE')->findOneBy(array(
            'prix' => $em->getRepository('LaisoArmBundle:AffectationPrix')->findOneBy(array(
                'serie' => $sp,
                'consistance' => $consistance,
            )),
            'avenant' => $avenant->getId(),
        ));

        if(!$ligne)
            throw new NotFoundHttpException('Ligne non trouvée');

        $form = $this->createDqeEditForm($ligne, $marcheId, $consistance, $numero, $serie);

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:Avenant/includes:edit_ligneDqe.html.twig', array(
                'edit_form' => $form->createView(),
                'entity' => $ligne
            ));

        return $this->render('LaisoArmBundle:Avenant:edit_ligne.html.twig', array(
            'edit_form' => $form->createView(),
            'entity' => $ligne
        ));
    }

    /**
     * Deletes an Avenant entity.
     *
     */
    public function deleteAction(Request $request, $numero, $marcheId)
    {
        $this->verifyAccess();

        $form = $this->createDeleteForm($numero, $marcheId);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('LaisoArmBundle:Avenant')->findOneBy(array(
                'marche' => $marcheId,
                'libelle' => $em->getRepository('LaisoArmBundle:LibelleAvenant')->findOneBy(array(
                    'numero' => $numero,
                ))->getId()
            ));

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Avenant entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('avenant', array('marcheId' => $marcheId)));
    }

    /**
     * Creates a form to delete a Avenant entity by numero and marcheId.
     *
     * @param mixed $numero The entity->libelle->numero
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($numero, $marcheId)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('avenant_delete', array(
                'numero' => $numero,
                'marcheId' => $marcheId
            )))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Supprimer', 'attr' => array(
                'class' => 'button danger place-right'
            )))
            ->getForm()
        ;
    }

    /**
     * Réécrire les lignes DQE
     *
     * @param Avenant $avenant
     *
     */
    private function defineDqe(Avenant $avenant)
    {
        $em = $this->getDoctrine()->getManager();

        $avenantPrecedent = $em->getRepository("LaisoArmBundle:Avenant")->findOneBy(
            array(
                'marche' => $avenant->getMarche(),
                'libelle' => $em->getRepository("LaisoArmBundle:LibelleAvenant")->findOneBy(
                                array(
                                    'numero' => $avenant->getLibelle()->getNumero() - 1,
                                )
                            )->getId(),
            )
        );

        foreach ($avenantPrecedent->getLignes() as $ligne) {
            $ligneDqe = new LigneDQE();
            $ligneDqe->setQuantite($ligne->getQuantite());
            $ligneDqe->setPrix($ligne->getprix());
            $ligneDqe->setAvenant($avenant);
            $em->persist($ligneDqe);
            $em->flush();
        }
    }


    /**
     * Afin de bloquer toute modification sur les quantités saisies.
     *
     * REMARQUE IMPORTANTE: Pour l'avenant ZERO (Marché initial),
     *                      vérifier si les PU ont été déjà validés
     *
     * Mettre à jour le montant du marché
     *
     * @param Request $request
     * @param $numero
     * @param $marcheId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function validateAction(Request $request, $numero, $marcheId)
    {
        $this->verifyAccess();

        $em = $this->getDoctrine()->getManager();
        $avenant = $em->getRepository('LaisoArmBundle:Avenant')->findOneBy(array(
            'marche' => $marcheId,
            'libelle' => $this->getDoctrine()->getRepository('LaisoArmBundle:LibelleAvenant')->findOneBy(array(
                'numero' => $numero,
            ))->getId()
        ));

        $puValide = true;
        /** @var \Laiso\ArmBundle\Entity\Marche $marche */
        $marche = $avenant->getMarche();

        if ($numero == 0)
            if ($marche->getLotissement()->getPuValide() == false) {
                $puValide = false;
                $this->addFlash('error', 'Les prix unitaires ne sont pas encore validés.');
            }

        if ($puValide) {
            // Mettre à jour le montant du marché (marche::montant)

            $montant = 0.0;

            /** @var \Laiso\ArmBundle\Entity\LigneDQE $ligne */
            foreach ($avenant->getLignes() as $ligne) {
                $montant += $ligne->getQuantite() * $ligne->getPrix()->getPrixUnitaire();
            }

            $marche->setMontant($montant);
            $avenant->setValide(true);
            $em->persist($marche);
            $em->persist($avenant);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('avenant_show', array(
            'numero' => $numero,
            'marcheId' => $marcheId
        )));
    }
}
