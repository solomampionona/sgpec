<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Lotissement
 *
 * Entité matérialisant un appel d'offre
 *
 * REMARQUE IMPORTANT:
 *  Un lotissement ne peut avoir qu'un et un seul DQE.
 *  C'est à dire un lotissement correspond déjà, depuis sa création, à un marché vide et
 *  un avenant ZERO qui comporte un DQE (DQE initial).
 *
 * @ORM\Table("dao")
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\LotissementRepository")
 */
class Lotissement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="Numero", type="integer", nullable=false)
     */
    private $numero;

    /**
     * @var integer
     *
     * @ORM\Column(name="Delai", type="float")
     */
    private $delai;

    /**
     * @var float
     *
     * @ORM\Column(name="PKDebut", type="float")
     */
    private $pKDebut;

    /**
     * @var float
     *
     * @ORM\Column(name="PKFin", type="float")
     */
    private $pKFin;

    /**
     * @var string
     *
     * @ORM\Column(name="Localisation", type="string", length=200)
     */
    private $localisation;

    /**
     * @var boolean
     *
     * Afin de bloquer les modifications sur les PrixUnitaires
     *
     * @ORM\Column(name="PuValide", type="boolean")
     */
    private $PuValide;

    /**
     * @var boolean
     *
     * Un lotissement valide => Un marché
     *
     * Si true : - On ne va plus l'afficher dans la rubrique DAO (list, show, edit, remove)
     *           - Impossible de remettre à false. (car sinon: our system has been HACKED huhuhu)
     *
     * A ne pas confondre avec Avenant::valide
     *
     * cf Avenant::valide
     *
     * @ORM\Column(name="Valide", type="boolean")
     */
    private $valide;

    /**
     * @var string
     *
     * @ORM\Column(name="Classe", type="string", length=5)
     */
    private $classe;

    /***********************************************
     *                   ASSOCIATIONS
     *
     *    Ne surtout pas modifier les annotations
     *      sauf en cas de modification du modèle
     *
     *                  (c) Laiso
     ***********************************************/


    /**
     * Un lot appartient seulement à un et un seul bloc, mais un bloc peut
     * en contenir plusieurs
     *
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\Bloc", inversedBy="lotissements")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     */
    private $bloc;

    /**
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\Axe", inversedBy="lotissements")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     */
    private $axe;

    /**
     * Un lot appartient seulement à une et une seule region, mais un bloc peut
     * en contenir plusieurs
     *
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\Region", inversedBy="lotissements")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     */
    private $region;

    /**
     * Groupe de travaux
     *
     * @var
     * @ORM\OneToMany(targetEntity="Laiso\ArmBundle\Entity\Consistance", mappedBy="lotissement")
     */
    private $consistances;

    /**
     * @var
     * @ORM\OneToOne(targetEntity="Laiso\ArmBundle\Entity\Marche", mappedBy="lotissement")
     */
    private $marche;

    /**
     * Proxy TypeConsistance
     *
     * @var
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\TypeConsistance")
     */
    private $type;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\Campagne", inversedBy="lotissements")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     */
    private $campagne;

    public function getObjet()
    {
        return $this->getType() . " de la " . $this->getAxe() .
                " entre le PK " . $this->getPKDebut() . " et le PK " . $this->getPKFin() .
                " ( " . $this->getLocalisation() . " )";
    }

    public function __construct()
    {
        $this->consistances = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    

    /**
     * Set delai
     *
     * @param integer $delai
     *
     * @return Lotissement
     */
    public function setDelai($delai)
    {
        $this->delai = $delai;

        return $this;
    }

    /**
     * Get delai
     *
     * @return integer
     */
    public function getDelai()
    {
        return $this->delai;
    }

    /**
     * Set axe
     *
     * @param string $axe
     *
     * @return Lotissement
     */
    public function setAxe($axe)
    {
        $this->axe = $axe;

        return $this;
    }

    /**
     * Get axe
     *
     * @return string
     */
    public function getAxe()
    {
        return $this->axe;
    }

    /**
     * Set pKDebut
     *
     * @param float $pKDebut
     *
     * @return Lotissement
     */
    public function setPKDebut($pKDebut)
    {
        $this->pKDebut = $pKDebut;

        return $this;
    }

    /**
     * Get pKDebut
     *
     * @return float
     */
    public function getPKDebut()
    {
        return $this->pKDebut;
    }

    /**
     * Set pKFin
     *
     * @param float $pKFin
     *
     * @return Lotissement
     */
    public function setPKFin($pKFin)
    {
        $this->pKFin = $pKFin;

        return $this;
    }

    /**
     * Get pKFin
     *
     * @return float
     */
    public function getPKFin()
    {
        return $this->pKFin;
    }

    /**
     * Set localisation
     *
     * @param string $localisation
     *
     * @return Lotissement
     */
    public function setLocalisation($localisation)
    {
        $this->localisation = $localisation;

        return $this;
    }

    /**
     * Get localisation
     *
     * @return string
     */
    public function getLocalisation()
    {
        return $this->localisation;
    }

    /**
     * Set bloc
     *
     * @param \Laiso\ArmBundle\Entity\Bloc $bloc
     *
     * @return Lotissement
     */
    public function setBloc(\Laiso\ArmBundle\Entity\Bloc $bloc)
    {
        $this->bloc = $bloc;

        return $this;
    }

    /**
     * Get bloc
     *
     * @return \Laiso\ArmBundle\Entity\Bloc
     */
    public function getBloc()
    {
        return $this->bloc;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $lot = $this->numero;
        if($this->numero > 9 && $this->numero < 99){
            $lot = "0".$lot;
        }elseif($this->numero <= 9) {
            $lot = "00".$lot;
        }

        $bloc = $this->bloc->getNumero();
        if($bloc < 9)
            $bloc = "0".$bloc;
        return $lot."-B".$bloc;
    }


    public function getAffichage()
    {
        $lot = $this->numero;
        if($this->numero > 9 && $this->numero < 99){
            $lot = "0".$lot;
        }elseif($this->numero <= 9) {
            $lot = "00".$lot;
        }
        $bloc = $this->bloc->getNumero();
        if($bloc < 9)
            $bloc = "0".$bloc;
        return $lot."-B".$bloc;
    }

    /**
     * @param TypeConsistance $consistance
     * @return $this
     */
    public function addConsistance(\Laiso\ArmBundle\Entity\TypeConsistance $consistance)
    {
        $this->consistances[] = $consistance;

        return $this;
    }

    /**
     * Remove consistance
     *
     * @param \Laiso\ArmBundle\Entity\Consistance $consistance
     */
    public function removeConsistance(\Laiso\ArmBundle\Entity\Consistance $consistance)
    {
        $this->consistances->removeElement($consistance);
    }

    /**
     * Get consistances
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConsistances()
    {
        return $this->consistances;
    }


    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return Lotissement
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set region
     *
     * @param \Laiso\ArmBundle\Entity\Region $region
     *
     * @return Lotissement
     */
    public function setRegion(\Laiso\ArmBundle\Entity\Region $region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \Laiso\ArmBundle\Entity\Region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set valide
     *
     * @param boolean $valide
     *
     * @return Lotissement
     */
    public function setValide($valide)
    {
        $this->valide = $valide;

        return $this;
    }

    /**
     * Get valide
     *
     * @return boolean
     */
    public function getValide()
    {
        return $this->valide;
    }

    /**
     * Set puValide
     *
     * @param boolean $puValide
     *
     * @return Lotissement
     */
    public function setPuValide($puValide)
    {
        $this->PuValide = $puValide;

        return $this;
    }

    /**
     * Get puValide
     *
     * @return boolean
     */
    public function getPuValide()
    {
        return $this->PuValide;
    }

    /**
     * Set campagne
     *
     * @param \Laiso\ArmBundle\Entity\Campagne $campagne
     *
     * @return Lotissement
     */
    public function setCampagne(\Laiso\ArmBundle\Entity\Campagne $campagne)
    {
        $this->campagne = $campagne;

        return $this;
    }

    /**
     * Get campagne
     *
     * @return \Laiso\ArmBundle\Entity\Campagne
     */
    public function getCampagne()
    {
        return $this->campagne;
    }

     /**
     * Set classe
     *
     * @param string $classe
     *
     * @return Lotissement
     */
    public function setClasse($classe)
    {
        $this->classe = $classe;

        return $this;
    }

    /**
     * Get classe
     *
     * @return string
     */
    public function getClasse()
    {
        return $this->classe;
    }

    /**
     * Set type
     *
     * @param \Laiso\ArmBundle\Entity\TypeConsistance $type
     *
     * @return Lotissement
     */
    public function setType(\Laiso\ArmBundle\Entity\TypeConsistance $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \Laiso\ArmBundle\Entity\TypeConsistance
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set marche
     *
     * @param \Laiso\ArmBundle\Entity\Marche $marche
     *
     * @return Lotissement
     */
    public function setMarche(\Laiso\ArmBundle\Entity\Marche $marche = null)
    {
        $this->marche = $marche;

        return $this;
    }

    /**
     * Get marche
     *
     * @return \Laiso\ArmBundle\Entity\Marche
     */
    public function getMarche()
    {
        return $this->marche;
    }
}
