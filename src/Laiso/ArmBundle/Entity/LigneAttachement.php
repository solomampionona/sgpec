<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LigneAttachement
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\LigneAttachementRepository")
 */
class LigneAttachement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Quantite", type="float")
     */
    private $quantite;

    /**
     * @var string
     *
     * @ORM\Column(name="QuantiteAnterieure", type="float")
     */
    private $quantiteAnterieure;

    /***********************************************
     *                   ASSOCIATIONS
     *
     *    Ne surtout pas modifier les annotations
     *      sauf en cas de modification du modèle
     *
     *                  (c) Laiso
     ***********************************************/

    /**
     *
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\LigneDQE")
     * @ORM\JoinColumn(nullable=true)
     */
    private $ligneDqe;

    /**
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\ConsistanceAttachement", inversedBy="lignes")
     */
    private $consistance;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantite
     *
     * @param float $quantite
     *
     * @return LigneAttachement
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return float
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set quantiteAnterieure
     *
     * @param float $quantiteAnterieure
     *
     * @return LigneAttachement
     */
    public function setQuantiteAnterieure($quantiteAnterieure)
    {
        $this->quantiteAnterieure = $quantiteAnterieure;

        return $this;
    }

    /**
     * Get quantiteAnterieure
     *
     * @return float
     */
    public function getQuantiteAnterieure()
    {
        return $this->quantiteAnterieure;
    }

    /**
     * Set ligneDqe
     *
     * @param \Laiso\ArmBundle\Entity\LigneDQE $ligneDqe
     *
     * @return LigneAttachement
     */
    public function setLigneDqe(\Laiso\ArmBundle\Entity\LigneDQE $ligneDqe = null)
    {
        $this->ligneDqe = $ligneDqe;

        return $this;
    }

    /**
     * Get ligneDqe
     *
     * @return \Laiso\ArmBundle\Entity\LigneDQE
     */
    public function getLigneDqe()
    {
        return $this->ligneDqe;
    }

    /**
     * Set consistance
     *
     * @param \Laiso\ArmBundle\Entity\ConsistanceAttachement $consistance
     *
     * @return LigneAttachement
     */
    public function setConsistance(\Laiso\ArmBundle\Entity\ConsistanceAttachement $consistance = null)
    {
        $this->consistance = $consistance;

        return $this;
    }

    /**
     * Get consistance
     *
     * @return \Laiso\ArmBundle\Entity\ConsistanceAttachement
     */
    public function getConsistance()
    {
        return $this->consistance;
    }
}
