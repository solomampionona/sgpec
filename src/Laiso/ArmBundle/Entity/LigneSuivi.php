<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LigneSuivi
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\LigneSuiviRepository")
 */
class LigneSuivi
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="Numero", type="integer")
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="LieuEntree", type="string", length=30)
     */
    private $lieuEntree;

    /**
     * @var string
     *
     * @ORM\Column(name="LieuSortie", type="string", length=30)
     */
    private $lieuSortie;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateEntree", type="date")
     */
    private $dateEntree;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateSortie", type="date")
     */
    private $dateSortie;

    /**
     * @var string
     *
     * @ORM\Column(name="Observation", type="text")
     */
    private $observation;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Valide", type="boolean")
     */
    private $valide;

    /***********************************************
     *                   ASSOCIATIONS
     *
     *    Ne surtout pas modifier les annotations
     *      sauf en cas de modification du modèle
     *
     *                  (c) Laiso
     ***********************************************/

    /**
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\Decompte", inversedBy="ligneSuivis")
     */
    private $decompte;

    /**
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\Intervenant", inversedBy="ligneSuivis")
     */
    private $intervenant;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lieuEntree
     *
     * @param string $lieuEntree
     *
     * @return LigneSuivi
     */
    public function setLieuEntree($lieuEntree)
    {
        $this->lieuEntree = $lieuEntree;

        return $this;
    }

    /**
     * Get lieuEntree
     *
     * @return string
     */
    public function getLieuEntree()
    {
        return $this->lieuEntree;
    }

    /**
     * Set lieuSortie
     *
     * @param string $lieuSortie
     *
     * @return LigneSuivi
     */
    public function setLieuSortie($lieuSortie)
    {
        $this->lieuSortie = $lieuSortie;

        return $this;
    }

    /**
     * Get lieuSortie
     *
     * @return string
     */
    public function getLieuSortie()
    {
        return $this->lieuSortie;
    }

    /**
     * Set dateEntree
     *
     * @param \DateTime $dateEntree
     *
     * @return LigneSuivi
     */
    public function setDateEntree($dateEntree)
    {
        $this->dateEntree = $dateEntree;

        return $this;
    }

    /**
     * Get dateEntree
     *
     * @return \DateTime
     */
    public function getDateEntree()
    {
        return $this->dateEntree;
    }

    /**
     * Set dateSortie
     *
     * @param \DateTime $dateSortie
     *
     * @return LigneSuivi
     */
    public function setDateSortie($dateSortie)
    {
        $this->dateSortie = $dateSortie;

        return $this;
    }

    /**
     * Get dateSortie
     *
     * @return \DateTime
     */
    public function getDateSortie()
    {
        return $this->dateSortie;
    }

    /**
     * Set observation
     *
     * @param string $observation
     *
     * @return LigneSuivi
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set valide
     *
     * @param boolean $valide
     *
     * @return LigneSuivi
     */
    public function setValide($valide)
    {
        $this->valide = $valide;

        return $this;
    }

    /**
     * Get valide
     *
     * @return boolean
     */
    public function getValide()
    {
        return $this->valide;
    }

    /**
     * Set decompte
     *
     * @param \Laiso\ArmBundle\Entity\Decompte $decompte
     *
     * @return LigneSuivi
     */
    public function setDecompte(\Laiso\ArmBundle\Entity\Decompte $decompte = null)
    {
        $this->decompte = $decompte;

        return $this;
    }

    /**
     * Get decompte
     *
     * @return \Laiso\ArmBundle\Entity\Decompte
     */
    public function getDecompte()
    {
        return $this->decompte;
    }

    /**
     * Set intervenant
     *
     * @param \Laiso\ArmBundle\Entity\Intervenant $intervenant
     *
     * @return LigneSuivi
     */
    public function setIntervenant(\Laiso\ArmBundle\Entity\Intervenant $intervenant = null)
    {
        $this->intervenant = $intervenant;

        return $this;
    }

    /**
     * Get intervenant
     *
     * @return \Laiso\ArmBundle\Entity\Intervenant
     */
    public function getIntervenant()
    {
        return $this->intervenant;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return LigneSuivi
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero()
    {
        return $this->numero;
    }
}
