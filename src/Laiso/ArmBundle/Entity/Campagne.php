<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Campagne
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\CampagneRepository")
 */
class Campagne
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Exercice", type="string", length=20)
     */
    private $exercice;

    /**
     * @var string
     *
     * @ORM\Column(name="Gestion", type="string", length=4)
     */
    private $gestion;

    /**
     * @var string
     *
     * @ORM\Column(name="Financement", type="string", length=100)
     */
    private $financement;

    /**
     * @var string
     *
     * @ORM\Column(name="FinancementAbbrege", type="string", length=4)
     */
    private $financementAbbr;

    /**
     * @var float
     *
     * @ORM\Column(name="TVA", type="float")
     */
    private $tVA;

    /**
     * @var float
     *
     * @ORM\Column(name="Avance", type="float")
     */
    private $tauxAvance;

    /**
     * @var float
     *
     * @ORM\Column(name="PenaliteRetardJour", type="float")
     */
    private $penaliteRetardJour;

    /**
     * @var float
     *
     * @ORM\Column(name="PlafondPenalite", type="float")
     */
    private $plafondPenalite;

    /**
     * @var string
     *
     * @ORM\Column(name="Numerotation", type="string", length=100)
     */
    private $numerotation;

    /***********************************************
     *                   ASSOCIATIONS
     *
     *    Ne surtout pas modifier les annotations
     *      sauf en cas de modification du modèle
     *
     *                  (c) Laiso
     ***********************************************/

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="Laiso\ArmBundle\Entity\Lotissement", mappedBy="campagne")
     */
    private $lotissements;

    public function __toString()
    {
        return "Campagne du " . $this->exercice . " - " . $this->financement . " - " . $this->numerotation;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set exercice
     *
     * @param string $exercice
     *
     * @return Campagne
     */
    public function setExercice($exercice)
    {
        $this->exercice = $exercice;

        return $this;
    }

    /**
     * Get exercice
     *
     * @return string
     */
    public function getExercice()
    {
        return $this->exercice;
    }

    /**
     * Set financement
     *
     * @param string $financement
     *
     * @return Campagne
     */
    public function setFinancement($financement)
    {
        $this->financement = $financement;

        return $this;
    }

    /**
     * Get financement
     *
     * @return string
     */
    public function getFinancement()
    {
        return $this->financement;
    }

    /**
     * Set tVA
     *
     * @param float $tVA
     *
     * @return Campagne
     */
    public function setTVA($tVA)
    {
        $this->tVA = $tVA;

        return $this;
    }

    /**
     * Get tVA
     *
     * @return float
     */
    public function getTVA()
    {
        return $this->tVA;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->lotissements = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add lotissement
     *
     * @param \Laiso\ArmBundle\Entity\Lotissement $lotissement
     *
     * @return Campagne
     */
    public function addLotissement(\Laiso\ArmBundle\Entity\Lotissement $lotissement)
    {
        $this->lotissements[] = $lotissement;

        return $this;
    }

    /**
     * Remove lotissement
     *
     * @param \Laiso\ArmBundle\Entity\Lotissement $lotissement
     */
    public function removeLotissement(\Laiso\ArmBundle\Entity\Lotissement $lotissement)
    {
        $this->lotissements->removeElement($lotissement);
    }

    /**
     * Get lotissements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLotissements()
    {
        return $this->lotissements;
    }

    /**
     * Set numerotation
     *
     * @param string $numerotation
     *
     * @return Campagne
     */
    public function setNumerotation($numerotation)
    {
        $this->numerotation = $numerotation;

        return $this;
    }

    /**
     * Get numerotation
     *
     * @return string
     */
    public function getNumerotation()
    {
        return $this->numerotation;
    }

    /**
     * Set tauxAvance
     *
     * @param float $tauxAvance
     *
     * @return Campagne
     */
    public function setTauxAvance($tauxAvance)
    {
        $this->tauxAvance = $tauxAvance;

        return $this;
    }

    /**
     * Get tauxAvance
     *
     * @return float
     */
    public function getTauxAvance()
    {
        return $this->tauxAvance;
    }

    /**
     * Set penaliteRetardJour
     *
     * @param float $penaliteRetardJour
     *
     * @return Campagne
     */
    public function setPenaliteRetardJour($penaliteRetardJour)
    {
        $this->penaliteRetardJour = $penaliteRetardJour;

        return $this;
    }

    /**
     * Get penaliteRetardJour
     *
     * @return float
     */
    public function getPenaliteRetardJour()
    {
        return $this->penaliteRetardJour;
    }

    /**
     * Set plafondPenalite
     *
     * @param float $plafondPenalite
     *
     * @return Campagne
     */
    public function setPlafondPenalite($plafondPenalite)
    {
        $this->plafondPenalite = $plafondPenalite;

        return $this;
    }

    /**
     * Get plafondPenalite
     *
     * @return float
     */
    public function getPlafondPenalite()
    {
        return $this->plafondPenalite;
    }

    /**
     * @return string
     */
    public function getGestion()
    {
        return $this->gestion;
    }

    /**
     * @param string $gestion
     */
    public function setGestion($gestion)
    {
        $this->gestion = $gestion;
    }

    /**
     * @return string
     */
    public function getFinancementAbbr()
    {
        return $this->financementAbbr;
    }

    /**
     * @param string $financementAbbr
     */
    public function setFinancementAbbr($financementAbbr)
    {
        $this->financementAbbr = $financementAbbr;
    }

}
