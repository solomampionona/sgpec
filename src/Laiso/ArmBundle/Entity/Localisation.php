<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Localisation
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\LocalisationRepository")
 */
class Localisation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var float
     *
     * @ORM\Column(name="PKDebut", type="float")
     */
    private $pKDebut;

    /**
     * @var float
     *
     * @ORM\Column(name="PKFin", type="float")
     */
    private $pKFin;

    /**
     * @var string
     *
     * @ORM\Column(name="Localisation", type="string", length=100)
     */
    private $localisation;


    /***********************************************
     *                   ASSOCIATIONS
     *
     *    Ne surtout pas modifier les annotations
     *      sauf en cas de modification du modèle
     *
     *                  (c) Laiso
     ***********************************************/


    /**
     * Un marché est exercé par une et une seule entreprise,
     * mais une entreprise peut exercer plusieurs marchés
     *
     * Nullable à true pour la création de l'offre
     *
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\Consistance", inversedBy="localisations")
     * @ORM\JoinColumn(nullable=true, referencedColumnName="id")
     */
    private $consistance;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set pKDebut
     *
     * @param float $pKDebut
     *
     * @return Localisation
     */
    public function setPKDebut($pKDebut)
    {
        $this->pKDebut = $pKDebut;

        return $this;
    }

    /**
     * Get pKDebut
     *
     * @return float
     */
    public function getPKDebut()
    {
        return $this->pKDebut;
    }

    /**
     * Set pKFin
     *
     * @param float $pKFin
     *
     * @return Localisation
     */
    public function setPKFin($pKFin)
    {
        $this->pKFin = $pKFin;

        return $this;
    }

    /**
     * Get pKFin
     *
     * @return float
     */
    public function getPKFin()
    {
        return $this->pKFin;
    }

    /**
     * Set localisation
     *
     * @param string $localisation
     *
     * @return Localisation
     */
    public function setLocalisation($localisation)
    {
        $this->localisation = $localisation;

        return $this;
    }

    /**
     * Get localisation
     *
     * @return string
     */
    public function getLocalisation()
    {
        return $this->localisation;
    }

    /**
     * Set consistance
     *
     * @param \Laiso\ArmBundle\Entity\Consistance $consistance
     *
     * @return Localisation
     */
    public function setConsistance(\Laiso\ArmBundle\Entity\Consistance $consistance = null)
    {
        $this->consistance = $consistance;

        return $this;
    }

    /**
     * Get consistance
     *
     * @return \Laiso\ArmBundle\Entity\Consistance
     */
    public function getConsistance()
    {
        return $this->consistance;
    }
}
