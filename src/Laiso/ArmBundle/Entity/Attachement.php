<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Attachement
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\AttachementRepository")
 */
class Attachement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="Numero", type="integer")
     */
    private $numero;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateAttachement", type="date")
     */
    private $dateAttachement;

    /**
     * @var boolean
     *
     * Afin de bloquer les modifications sur les Quantités
     *
     * @ORM\Column(name="valide", type="boolean")
     */
    private $valide;

    /**
     * @var string
     *
     * @ORM\Column(name="Type", type="string", length=30)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="MontantHTVA", type="float", nullable=true)
     */
    private $montantHTVA;

    /**
     *
     * @return bool
     */
    public function isTravaux()
    {
        return $this->type == 'TRAVAUX';
    }

    /**
     * @return bool
     */
    public function isRetenue()
    {
        return $this->type == 'RETENUE';
    }

    /**
     * @return bool
     */
    public function isRemboursement()
    {
        return $this->type == "REMBOURSEMENT";
    }

    /**
     * @return bool
     */
    public function isRestitution()
    {
        return $this->type == 'RESTITUTION';
    }

    public function isAvance()
    {
        return $this->type == 'AVANCE';
    }

    public function isPenalite()
    {
        return $this->type == 'PENALITE';
    }

    /**
     *
     * @return Attachement|null
     */
    public function getAttachementPrecedent()
    {
        $attachementPrecedent = null;

        /** @var \Laiso\ArmBundle\Entity\Marche $marche */
        $marche = $this->getDecompte()->getAvenant()->getMarche();

        for($av = count($marche->getAvenants()) - 1; $av >= 0 && $attachementPrecedent == null; $av--){
            /** @var \Laiso\ArmBundle\Entity\Avenant $avenant */
            $avenant = $marche->getAvenants()[$av];

            for($d = count($avenant->getDecomptes()) - 1; $d >= 0 && $attachementPrecedent == null; $d--){
                /** @var \Laiso\ArmBundle\Entity\Decompte $decompte */
                $decompte = $avenant->getDecomptes()[$d];

                for($at = count($decompte->getAttachements()) - 1; $at >= 0 && $attachementPrecedent == null; $at --){
                    /** @var \Laiso\ArmBundle\Entity\Attachement $att */
                    $att = $decompte->getAttachements()[$at];

                    if($att->getType() == $this->getType() && $this->getNumero() > $att->getNumero())
                        $attachementPrecedent = $att;
                }
            }
        }
        return $attachementPrecedent;
    }


    /**
     * @return $this
     */
    public function selfUpdate()
    {
        $montantPartiel = 0.0;

        /** @var \Laiso\ArmBundle\Entity\ConsistanceAttachement $consistance */
        foreach ($this->getConsistances() as $consistance) {
            /** @var \Laiso\ArmBundle\Entity\LigneAttachement $ligne */
            foreach ($consistance->getLignes() as $ligne) {
                $montantPartiel += $ligne->getQuantite() * $ligne->getLigneDqe()->getPrix()->getPrixUnitaire();
            }
        }

        if ($this->isRetenue())
            $montantPartiel /= 10.0;

        $this->setMontantHTVA($montantPartiel);
        return $this;
    }

    /**
     * Test if the current attachement has a Retenue
     * @return bool
     */
    public function hasRetenue()
    {
        if($this->getRetenue() != null)
            return false;

        /** @var \Laiso\ArmBundle\Entity\ConsistanceAttachement $consistance */
        foreach ($this->getConsistances() as $consistance) {
            /** @var \Laiso\ArmBundle\Entity\LigneAttachement $ligne */
            foreach ($consistance->getLignes() as $ligne) {
                if ($ligne->getQuantite() > 0 && $ligne->getLigneDqe()->getPrix()->getSerie()->getCategorie()->getNumero() > 100) return true;
            }
        }
        return false;
    }

    /***********************************************
     *                   ASSOCIATIONS
     *
     *    Ne surtout pas modifier les annotations
     *      sauf en cas de modification du modèle
     *
     *                  (c) Laiso
     ***********************************************/


    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\Decompte", inversedBy="attachements")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     */
    private $decompte;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="Laiso\ArmBundle\Entity\ConsistanceAttachement", mappedBy="attachement")
     */
    private $consistances;

    /**
     * @ORM\OneToOne(targetEntity="Laiso\ArmBundle\Entity\Attachement", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $retenue;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="Laiso\ArmBundle\Entity\Attachement", mappedBy="attachement")
     */
    private $attachements;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\Attachement", inversedBy="attachements")
     * @ORM\JoinColumn(nullable=true, referencedColumnName="id")
     */
    private $attachement;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->consistances = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return Attachement
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set dateAttachement
     *
     * @param \DateTime $dateAttachement
     *
     * @return Attachement
     */
    public function setDateAttachement($dateAttachement)
    {
        $this->dateAttachement = $dateAttachement;

        return $this;
    }

    /**
     * Get dateAttachement
     *
     * @return \DateTime
     */
    public function getDateAttachement()
    {
        return $this->dateAttachement;
    }

    /**
     * Set valide
     *
     * @param boolean $valide
     *
     * @return Attachement
     */
    public function setValide($valide)
    {
        $this->valide = $valide;

        return $this;
    }

    /**
     * Get valide
     *
     * @return boolean
     */
    public function getValide()
    {
        return $this->valide;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Attachement
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set decompte
     *
     * @param \Laiso\ArmBundle\Entity\Decompte $decompte
     *
     * @return Attachement
     */
    public function setDecompte(\Laiso\ArmBundle\Entity\Decompte $decompte)
    {
        $this->decompte = $decompte;

        return $this;
    }

    /**
     * Get decompte
     *
     * @return \Laiso\ArmBundle\Entity\Decompte
     */
    public function getDecompte()
    {
        return $this->decompte;
    }

    /**
     * Add consistance
     *
     * @param \Laiso\ArmBundle\Entity\ConsistanceAttachement $consistance
     *
     * @return Attachement
     */
    public function addConsistance(\Laiso\ArmBundle\Entity\ConsistanceAttachement $consistance)
    {
        $this->consistances[] = $consistance;

        return $this;
    }

    /**
     * Remove consistance
     *
     * @param \Laiso\ArmBundle\Entity\ConsistanceAttachement $consistance
     */
    public function removeConsistance(\Laiso\ArmBundle\Entity\ConsistanceAttachement $consistance)
    {
        $this->consistances->removeElement($consistance);
    }

    /**
     * Get consistances
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConsistances()
    {
        return $this->consistances;
    }

    /**
     * Set retenue
     *
     * @param \Laiso\ArmBundle\Entity\Attachement $retenue
     *
     * @return Attachement
     */
    public function setRetenue(\Laiso\ArmBundle\Entity\Attachement $retenue = null)
    {
        $this->retenue = $retenue;

        return $this;
    }

    /**
     * Get retenue
     *
     * @return \Laiso\ArmBundle\Entity\Attachement
     */
    public function getRetenue()
    {
        return $this->retenue;
    }

    /**
     * @return string
     */
    public function getMontantHTVA()
    {
        return $this->montantHTVA;
    }

    /**
     * @param string $montantHTVA
     */
    public function setMontantHTVA($montantHTVA)
    {
        $this->montantHTVA = $montantHTVA;
    }


    /**
     * Add attachement
     *
     * @param \Laiso\ArmBundle\Entity\Attachement $attachement
     *
     * @return Attachement
     */
    public function addAttachement(\Laiso\ArmBundle\Entity\Attachement $attachement)
    {
        $this->attachements[] = $attachement;

        return $this;
    }

    /**
     * Remove attachement
     *
     * @param \Laiso\ArmBundle\Entity\Attachement $attachement
     */
    public function removeAttachement(\Laiso\ArmBundle\Entity\Attachement $attachement)
    {
        $this->attachements->removeElement($attachement);
    }

    /**
     * Get attachements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttachements()
    {
        return $this->attachements;
    }

    /**
     * Set attachement
     *
     * @param \Laiso\ArmBundle\Entity\Attachement $attachement
     *
     * @return Attachement
     */
    public function setAttachement(\Laiso\ArmBundle\Entity\Attachement $attachement = null)
    {
        $this->attachement = $attachement;

        return $this;
    }

    /**
     * Get attachement
     *
     * @return \Laiso\ArmBundle\Entity\Attachement
     */
    public function getAttachement()
    {
        return $this->attachement;
    }
}
