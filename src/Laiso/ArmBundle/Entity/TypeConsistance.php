<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TypeConsistance
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\TypeConsistanceRepository")
 */
class TypeConsistance
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Type", type="string", length=255)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="Abbrege", type="string", length=20)
     */
    private $abbrege;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return TypeConsistance
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set abbrege
     *
     * @param string $abbrege
     *
     * @return TypeConsistance
     */
    public function setAbbrege($abbrege)
    {
        $this->abbrege = $abbrege;

        return $this;
    }

    /**
     * Get abbrege
     *
     * @return string
     */
    public function getAbbrege()
    {
        return $this->abbrege;
    }

    public function __toString()
    {
        return $this->type;
    }
}
