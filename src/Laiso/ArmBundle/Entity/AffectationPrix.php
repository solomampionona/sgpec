<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AffectationPrix
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\AffectationPrixRepository")
 */
class AffectationPrix
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="PrixUnitaire", type="float", nullable=true)
     */
    private $prixUnitaire;

    /***********************************************
     *                   ASSOCIATIONS
     *
     *    Ne surtout pas modifier les annotations
     *      sauf en cas de modification du modèle
     *
     *                  (c) Laiso
     ***********************************************/


    /**
     *
     * Consistance (marché), Serie Des Prix ----> Prix Unitaire
     *
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\Consistance", inversedBy="prix", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */

    private $consistance;

    /**
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\SerieDePrix")
     * @ORM\JoinColumn(nullable=false)
     */

    private $serie;

    /**
     * @ORM\OneToMany(targetEntity="Laiso\ArmBundle\Entity\LigneDQE", mappedBy="prix")
     * @ORM\JoinColumn(nullable=false)
     */

    private $lignesDqe;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set prixUnitaire
     *
     * @param float $prixUnitaire
     *
     * @return AffectationPrix
     */
    public function setPrixUnitaire($prixUnitaire)
    {
        $this->prixUnitaire = $prixUnitaire;

        return $this;
    }

    /**
     * Get prixUnitaire
     *
     * @return float
     */
    public function getPrixUnitaire()
    {
        return $this->prixUnitaire;
    }
    

    /**
     * Set serie
     *
     * @param \Laiso\ArmBundle\Entity\SerieDePrix $serie
     *
     * @return AffectationPrix
     */
    public function setSerie(\Laiso\ArmBundle\Entity\SerieDePrix $serie)
    {
        $this->serie = $serie;

        return $this;
    }

    /**
     * Get serie
     *
     * @return \Laiso\ArmBundle\Entity\SerieDePrix
     */
    public function getSerie()
    {
        return $this->serie;
    }

    /**
     * Set consistance
     *
     * @param \Laiso\ArmBundle\Entity\Consistance $consistance
     *
     * @return AffectationPrix
     */
    public function setConsistance(\Laiso\ArmBundle\Entity\Consistance $consistance)
    {
        $this->consistance = $consistance;

        return $this;
    }

    /**
     * Get consistance
     *
     * @return \Laiso\ArmBundle\Entity\Consistance
     */
    public function getConsistance()
    {
        return $this->consistance;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->lignesDqe = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add lignesDqe
     *
     * @param \Laiso\ArmBundle\Entity\LigneDQE $lignesDqe
     *
     * @return AffectationPrix
     */
    public function addLignesDqe(\Laiso\ArmBundle\Entity\LigneDQE $lignesDqe)
    {
        $this->lignesDqe[] = $lignesDqe;

        return $this;
    }

    /**
     * Remove lignesDqe
     *
     * @param \Laiso\ArmBundle\Entity\LigneDQE $lignesDqe
     */
    public function removeLignesDqe(\Laiso\ArmBundle\Entity\LigneDQE $lignesDqe)
    {
        $this->lignesDqe->removeElement($lignesDqe);
    }

    /**
     * Get lignesDqe
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLignesDqe()
    {
        return $this->lignesDqe;
    }

    function __toString()
    {
        return $this->id . " " . $this->prixUnitaire."";
    }


}
