<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Avance
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\AvanceRepository")
 */
class Avance
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date", type="date")
     */
    private $date;

    /**
     * @var float
     *
     * @ORM\Column(name="Montant", type="float")
     */
    private $montant;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\Marche", inversedBy="avances")
     * @ORM\JoinColumn(nullable=true, referencedColumnName="id")
     */
    private $marche;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Avance
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set montant
     *
     * @param float $montant
     *
     * @return Avance
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Get montant
     *
     * @return float
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Set marche
     *
     * @param \Laiso\ArmBundle\Entity\Marche $marche
     *
     * @return Avance
     */
    public function setMarche(\Laiso\ArmBundle\Entity\Marche $marche = null)
    {
        $this->marche = $marche;

        return $this;
    }

    /**
     * Get marche
     *
     * @return \Laiso\ArmBundle\Entity\Marche
     */
    public function getMarche()
    {
        return $this->marche;
    }
}
