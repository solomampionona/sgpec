<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Marche
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\MarcheRepository")
 */
class Marche
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="Numero", type="integer", nullable=true)
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="Montant", type="float", nullable=true)
     */
    private $montant;

    /**
     * @var string
     *
     * @ORM\Column(name="AvancementPhysique", type="float", nullable=true)
     */
    private $avancementPhysique;

    /**
     * @var string
     *
     * @ORM\Column(name="AvancementFinancier", type="float", nullable=true)
     */
    private $avancementFinancier;

    /**
     * @var boolean
     *
     * Taxé ou pas! :)
     *
     * @ORM\Column(name="EntrepriseAssujettie", type="boolean")
     */
    private $entrepriseAssujettie;

    /***********************************************
     *                   ASSOCIATIONS
     *
     *    Ne surtout pas modifier les annotations
     *      sauf en cas de modification du modèle
     *
     *                  (c) Laiso
     ***********************************************/


    /**
     * Un marché est exercé par une et une seule entreprise,
     * mais une entreprise peut exercer plusieurs marchés
     *
     * Nullable à true pour la création de l'offre
     *
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\Entreprise", inversedBy="marches")
     * @ORM\JoinColumn(nullable=true, referencedColumnName="id")
     */
    private $entreprise;

    /**
     * Un marché est associeé à un seul lotissement (DAO)
     *
     * @ORM\OneToOne(targetEntity="Laiso\ArmBundle\Entity\Lotissement", inversedBy="marche")
     */
    private $lotissement;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="Laiso\ArmBundle\Entity\Avenant", mappedBy="marche")
     */
    private $avenants;
    /**
     * @var
     * @ORM\OneToMany(targetEntity="Laiso\ArmBundle\Entity\Avance", mappedBy="marche")
     */
    private $avances;


    /**
     * @return string
     */
    public function getObjet()
    {
        return $this->getLotissement()->getObjet();
    }

    /**
     * Count decompte
     * @return int
     */
    public function getDecomptes()
    {
        $c = 0;
        /** @var \Laiso\ArmBundle\Entity\Avenant $avenant */
        foreach ($this->avenants as $avenant) {
            $c = $c + count($avenant->getDecomptes());
        }
        return $c;
    }

    public function hasAvance()
    {
        /** @var \Laiso\ArmBundle\Entity\Avenant $avenant */
        foreach ($this->getAvenants() as $avenant) {
            /** @var \Laiso\ArmBundle\Entity\Decompte $decompte */
            foreach ($avenant->getDecomptes() as $decompte) {
                return $decompte->getNumero() == 0;
            }
        }
    }

    /**
     *
     * @return int|string
     */
    public function getMontantInitial(){
        /** @var \Laiso\ArmBundle\Entity\Avenant $avenant */
        $avenant = $this->avenants[0];
        $montant = 0;
        /** @var \Laiso\ArmBundle\Entity\LigneDQE $ligne */
        foreach ($avenant->getLignes() as $ligne) {
            $montant += $ligne->getQuantite() * $ligne->getPrix()->getPrixUnitaire();
        }
        return $montant;
    }

    /**
     * Marche constructor.
     */
    public function __construct()
    {
        $this->avenants = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return Marche
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set entreprise
     *
     * @param \Laiso\ArmBundle\Entity\Entreprise $entreprise
     *
     * @return Marche
     */
    public function setEntreprise(\Laiso\ArmBundle\Entity\Entreprise $entreprise = null)
    {
        $this->entreprise = $entreprise;

        return $this;
    }

    /**
     * Get entreprise
     *
     * @return \Laiso\ArmBundle\Entity\Entreprise
     */
    public function getEntreprise()
    {
        return $this->entreprise;
    }


    function __toString()
    {
        return $this->numero . " - " . $this->lotissement->getCampagne()->getNumerotation();
    }

    /**
     * Set lotissement
     *
     * @param \Laiso\ArmBundle\Entity\Lotissement $lotissement
     *
     * @return Marche
     */
    public function setLotissement(\Laiso\ArmBundle\Entity\Lotissement $lotissement = null)
    {
        $this->lotissement = $lotissement;

        return $this;
    }

    /**
     * Get lotissement
     *
     * @return \Laiso\ArmBundle\Entity\Lotissement
     */
    public function getLotissement()
    {
        return $this->lotissement;
    }

    /**
     * Add avenant
     *
     * @param \Laiso\ArmBundle\Entity\Avenant $avenant
     *
     * @return Marche
     */
    public function addAvenant(\Laiso\ArmBundle\Entity\Avenant $avenant)
    {
        $this->avenants[] = $avenant;

        return $this;
    }

    /**
     * Remove avenant
     *
     * @param \Laiso\ArmBundle\Entity\Avenant $avenant
     */
    public function removeAvenant(\Laiso\ArmBundle\Entity\Avenant $avenant)
    {
        $this->avenants->removeElement($avenant);
    }

    /**
     * Get avenants
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAvenants()
    {
        return $this->avenants;
    }

    /**
     * Add avance
     *
     * @param \Laiso\ArmBundle\Entity\Avance $avance
     *
     * @return Marche
     */
    public function addAvance(\Laiso\ArmBundle\Entity\Avance $avance)
    {
        $this->avances[] = $avance;

        return $this;
    }

    /**
     * Remove avance
     *
     * @param \Laiso\ArmBundle\Entity\Avance $avance
     */
    public function removeAvance(\Laiso\ArmBundle\Entity\Avance $avance)
    {
        $this->avances->removeElement($avance);
    }

    /**
     * Get avances
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAvances()
    {
        return $this->avances;
    }

    /**
     * Set montant
     *
     * @param float $montant
     *
     * @return Marche
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Get montant
     *
     * @return float
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Set avancementPhysique
     *
     * @param float $avancementPhysique
     *
     * @return Marche
     */
    public function setAvancementPhysique($avancementPhysique)
    {
        $this->avancementPhysique = $avancementPhysique;

        return $this;
    }

    /**
     * Get avancementPhysique
     *
     * @return float
     */
    public function getAvancementPhysique()
    {
        return $this->avancementPhysique;
    }

    /**
     * Set avancementFinancier
     *
     * @param float $avancementFinancier
     *
     * @return Marche
     */
    public function setAvancementFinancier($avancementFinancier)
    {
        $this->avancementFinancier = $avancementFinancier;

        return $this;
    }

    /**
     * Get avancementFinancier
     *
     * @return float
     */
    public function getAvancementFinancier()
    {
        return $this->avancementFinancier;
    }

    /**
     * Set entrepriseAssujettie
     *
     * @param boolean $entrepriseAssujettie
     *
     * @return Marche
     */
    public function setEntrepriseAssujettie($entrepriseAssujettie)
    {
        $this->entrepriseAssujettie = $entrepriseAssujettie;

        return $this;
    }

    /**
     * Get entrepriseAssujettie
     *
     * @return boolean
     */
    public function getEntrepriseAssujettie()
    {
        return $this->entrepriseAssujettie;
    }
}
