<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Consistance
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\ConsistanceRepository")
 */
class Consistance
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Libelle", type="string", length=200, nullable=true)
     */
    private $libelle;


    /***********************************************
     *                   ASSOCIATIONS
     *
     *    Ne surtout pas modifier les annotations
     *      sauf en cas de modification du modèle
     *
     *                  (c) Laiso
     ***********************************************/


    /**
     *
     *
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\Lotissement", inversedBy="consistances")
     * @ORM\JoinColumn(nullable=true, referencedColumnName="id")
     */
    private $lotissement;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\TypeConsistance")
     */
    private $type;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="Laiso\ArmBundle\Entity\Localisation", mappedBy="consistance")
     */
    private $localisations;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="Laiso\ArmBundle\Entity\AffectationPrix", mappedBy="consistance")
     */
    private $prix;

    public function __construct()
    {
        $this->localisations = new ArrayCollection();
        $this->prix = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set lotissement
     *
     * @param \Laiso\ArmBundle\Entity\Lotissement $lotissement
     *
     * @return Consistance
     */
    public function setLotissement(\Laiso\ArmBundle\Entity\Lotissement $lotissement = null)
    {
        $this->lotissement = $lotissement;

        return $this;
    }

    /**
     * Get lotissement
     *
     * @return \Laiso\ArmBundle\Entity\Lotissement
     */
    public function getLotissement()
    {
        return $this->lotissement;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return "Consistance n°" . $this->id . " - " . $this->getLotissement()->__toString();
    }

    /**
     * Add localisation
     *
     * @param \Laiso\ArmBundle\Entity\Localisation $localisation
     *
     * @return Consistance
     */
    public function addLocalisation(\Laiso\ArmBundle\Entity\Localisation $localisation)
    {
        $this->localisations[] = $localisation;

        return $this;
    }

    /**
     * Remove localisation
     *
     * @param \Laiso\ArmBundle\Entity\Localisation $localisation
     */
    public function removeLocalisation(\Laiso\ArmBundle\Entity\Localisation $localisation)
    {
        $this->localisations->removeElement($localisation);
    }

    /**
     * Get localisations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLocalisations()
    {
        return $this->localisations;
    }

    /**
     * Add prix
     *
     * @param \Laiso\ArmBundle\Entity\AffectationPrix $prix
     *
     * @return Consistance
     */
    public function addPrix(\Laiso\ArmBundle\Entity\AffectationPrix $prix)
    {
        $this->prix[] = $prix;

        return $this;
    }

    /**
     * Remove prix
     *
     * @param \Laiso\ArmBundle\Entity\AffectationPrix $prix
     */
    public function removePrix(\Laiso\ArmBundle\Entity\AffectationPrix $prix)
    {
        $this->prix->removeElement($prix);
    }

    /**
     * Get prix
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return Consistance
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set type
     *
     * @param \Laiso\ArmBundle\Entity\TypeConsistance $type
     *
     * @return Consistance
     */
    public function setType(\Laiso\ArmBundle\Entity\TypeConsistance $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \Laiso\ArmBundle\Entity\TypeConsistance
     */
    public function getType()
    {
        return $this->type;
    }
}
