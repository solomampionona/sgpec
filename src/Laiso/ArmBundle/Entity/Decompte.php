<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Decompte
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\DecompteRepository")
 */
class Decompte
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="Numero", type="integer")
     */
    private $numero;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateDecompte", type="date")
     */
    private $dateDecompte;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Ferme", type="boolean")
     */
    private $ferme;

    /**
     * @var string
     *
     * @ORM\Column(name="montantAvance", type="float")
     */
    private $montantAvance;

    /**
     * @var string
     *
     * @ORM\Column(name="montantBrutHTVA", type="float")
     */
    private $montantBrutHTVA;

    /**
     * @var string
     *
     * @ORM\Column(name="montantRetenueHTVA", type="float")
     */
    private $montantRetenueHTVA;

    /**
     * @var string
     *
     * @ORM\Column(name="montantAcompteHTVA", type="float")
     */
    private $montantAcompteHTVA;

    function __toString()
    {
        return $this->numero ."";
    }

    /**
     * @return Decompte
     */
    public function getDecomptePrecedent()
    {
        /** @var \Laiso\ArmBundle\Entity\Decompte $decomptePrecedent */
        $decomptePrecedent = null;

        /** @var \Laiso\ArmBundle\Entity\Marche $marche */
        $marche = $this->getAvenant()->getMarche();

        for($k = count($marche->getAvenants()) - 1; $k >= 0 && $decomptePrecedent == null; $k--){
            /** @var \Laiso\ArmBundle\Entity\Avenant $avenant */
            $avenant = $marche->getAvenants()[$k];
            /** @var \Laiso\ArmBundle\Entity\Decompte $dcp */
            foreach ($avenant->getDecomptes() as $dcp) {
                $decomptePrecedent = $dcp->getNumero() == $this->getNumero() - 1 ? $dcp : $decomptePrecedent;
            }
        }

        return $decomptePrecedent;
    }

    /**
     * @return $this
     */
    public function selfUpdate()
    {
        $retenuePartielle = 0.0;
        $montantBrut = 0.0;
        $montantRestitution = 0.0;
        $montantRemboursement = 0.0;

        /** @var \Laiso\ArmBundle\Entity\Marche $marche */
        $marche = $this->getAvenant()->getMarche();

        /** @var \Laiso\ArmBundle\Entity\Attachement $attachement */
        foreach ($this->getAttachements() as $attachement) {
            if ($attachement->isTravaux()) {
                $montantBrut += $attachement->getMontantHTVA();
            }
            elseif ($attachement->isRetenue()) {
                $retenuePartielle += $attachement->getMontantHTVA();
            }
            elseif($attachement->isRemboursement()){
                $montantRemboursement += $attachement->getMontantHTVA();
            }
            elseif($attachement->isRestitution()){
                $montantRestitution += $attachement->getMontantHTVA();
            }
        }

        $this->setMontantRetenueHTVA($retenuePartielle);
        $this->setMontantBrutHTVA($montantBrut);
        $this->setMontantAvance($montantRemboursement);

        $decomptesPrecedents = $this->getDecomptesPrecedents();

        $acomptePrecedent = 0.0;
        $cumulRemboursementAvance = $montantRemboursement;
        $cumulRetenueGarantie = $retenuePartielle;

        $cumulRestitutionGarantie = $montantRestitution;

        /** @var \Laiso\ArmBundle\Entity\Decompte $decomptePrecedent */
        foreach ($decomptesPrecedents as $decomptePrecedent) {
            $acomptePrecedent += $decomptePrecedent->getMontantAcompteHTVA();


            $cumulRemboursementAvance += $decomptePrecedent->getMontantAvance();
            $cumulRetenueGarantie += $decomptePrecedent->getMontantRetenueHTVA();

            /** @var \Laiso\ArmBundle\Entity\Attachement $attc */
            foreach ($decomptePrecedent->getAttachements() as $attc) {
                if($attc->isRestitution())
                    $cumulRestitutionGarantie += $attc->getMontantHTVA();
            }
        }

        $resteApayer = $this->getMontantsBrutsPrecedentsHTVA() + $this->getMontantBrutHTVA() ;

        if($marche->hasAvance())
            $resteApayer += $marche->getMontantInitial() * $marche->getLotissement()->getCampagne()->getTauxAvance() / 100;

        $this->setMontantAcompteHTVA($resteApayer - $acomptePrecedent - $cumulRemboursementAvance - $cumulRetenueGarantie + $cumulRestitutionGarantie);

        return $this;
    }

    public function getDecomptesPrecedents()
    {
        $decomptes = array();
        /** @var \Laiso\ArmBundle\Entity\Avenant $avenant */
        foreach ($this->getAvenant()->getMarche()->getAvenants() as $avenant) {
            /** @var \Laiso\ArmBundle\Entity\Decompte $dp */
            foreach ($avenant->getDecomptes() as $dp) {
                if($dp->getNumero() < $this->getNumero())
                    array_push($decomptes, $dp);
            }
        }
        return $decomptes;
    }

    /**
     *
     * Tsy tafiditra ny avance de démarrage (numero 0) sy ny decompte en question (this)
     *
     * @return float|string
     */
    public function getMontantsBrutsPrecedentsHTVA(){
        /** @var \Laiso\ArmBundle\Entity\Marche $marche */
        $marche = $this->getAvenant()->getMarche();

        $montant = 0.0;

        /** @var \Laiso\ArmBundle\Entity\Avenant $avenant */
        foreach ($marche->getAvenants() as $avenant) {
            /** @var \Laiso\ArmBundle\Entity\Decompte $decompte */
            foreach ($avenant->getDecomptes() as $decompte) {
                if($this->getNumero() > $decompte->getNumero() && $decompte->getNumero() > 0)
                    $montant += $decompte->getMontantBrutHTVA();
            }
        }
        return $montant;
    }

    public function getAcompteDelivresHTVA()
    {
        /** @var \Laiso\ArmBundle\Entity\Marche $marche */
        $marche = $this->getAvenant()->getMarche();

        $montant = 0.0;

        /** @var \Laiso\ArmBundle\Entity\Avenant $avenant */
        foreach ($marche->getAvenants() as $avenant) {
            /** @var \Laiso\ArmBundle\Entity\Decompte $decompte */
            foreach ($avenant->getDecomptes() as $decompte) {
                if($this->getNumero() > $decompte->getNumero()) {
                    $montant += $decompte->getAcompte();
                }
            }
        }
        return $montant;
    }

    /**
     * @return float|string
     */
    public function getAcompte()
    {
        $retenue = 0.0; $brut = 0.0; $remboursementAvance = 0.0;  $restitution = 0.0;

        if($this->numero == 0)
            return $this->montantBrutHTVA;

        /** @var \Laiso\ArmBundle\Entity\Attachement $attachement */
        foreach ($this->getAttachements() as $attachement) {
            if($attachement->isTravaux())
                $brut += $attachement->getMontantHTVA();
            elseif($attachement->isRetenue())
                $retenue += $attachement->getRetenue();
            elseif($attachement->isRestitution())
                $restitution += $attachement->getMontantHTVA();
            elseif($attachement->isRemboursement()){
                $remboursementAvance += $attachement->getMontantHTVA();
            }
        }

        return $brut - $retenue + $restitution - $remboursementAvance;
    }

    /**
     * @return float|string
     */
    public function getRetenuesCumuleesHTVA()
    {
        $marche = $this->getAvenant()->getMarche();
        $montant = 0.0;

        /** @var \Laiso\ArmBundle\Entity\Avenant $avenant */
        foreach ($marche->getAvenants() as $avenant) {
            /** @var \Laiso\ArmBundle\Entity\Decompte $decompte */
            foreach ($avenant->getDecomptes() as $decompte) {
                if($decompte->getNumero() <= $this->getNumero())
                    $montant += $decompte->getMontantRetenueHTVA();
            }
        }

        return $montant;
    }

    /***********************************************
     *                   ASSOCIATIONS
     *
     *    Ne surtout pas modifier les annotations
     *      sauf en cas de modification du modèle
     *
     *                  (c) Laiso
     ***********************************************/

    /**
     * Decompte - Marché (Avenant ZERO)
     *
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\Avenant", inversedBy="decomptes")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     */
    private $avenant;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="Laiso\ArmBundle\Entity\Attachement", mappedBy="decompte")
     */
    private $attachements;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="Laiso\ArmBundle\Entity\LigneSuivi", mappedBy="decompte")
     */
    private $ligneSuivis;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return Decompte
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set dateDecompte
     *
     * @param \DateTime $dateDecompte
     *
     * @return Decompte
     */
    public function setDateDecompte($dateDecompte)
    {
        $this->dateDecompte = $dateDecompte;

        return $this;
    }

    /**
     * Get dateDecompte
     *
     * @return \DateTime
     */
    public function getDateDecompte()
    {
        return $this->dateDecompte;
    }

    /**
     * Set avenant
     *
     * @param \Laiso\ArmBundle\Entity\Avenant $avenant
     *
     * @return Decompte
     */
    public function setAvenant(\Laiso\ArmBundle\Entity\Avenant $avenant)
    {
        $this->avenant = $avenant;

        return $this;
    }

    /**
     * Get avenant
     *
     * @return \Laiso\ArmBundle\Entity\Avenant
     */
    public function getAvenant()
    {
        return $this->avenant;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->attachements = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add attachement
     *
     * @param \Laiso\ArmBundle\Entity\Attachement $attachement
     *
     * @return Decompte
     */
    public function addAttachement(\Laiso\ArmBundle\Entity\Attachement $attachement)
    {
        $this->attachements[] = $attachement;

        return $this;
    }

    /**
     * Remove attachement
     *
     * @param \Laiso\ArmBundle\Entity\Attachement $attachement
     */
    public function removeAttachement(\Laiso\ArmBundle\Entity\Attachement $attachement)
    {
        $this->attachements->removeElement($attachement);
    }

    /**
     * Get attachements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttachements()
    {
        return $this->attachements;
    }

    /**
     * @return boolean
     */
    public function isFerme()
    {
        return $this->ferme;
    }

    /**
     * @param boolean $ferme
     */
    public function setFerme($ferme)
    {
        $this->ferme = $ferme;
    }

    /**
     * @return string
     */
    public function getMontantBrutHTVA()
    {
        return $this->montantBrutHTVA;
    }

    /**
     * @param string $montantBrutHTVA
     */
    public function setMontantBrutHTVA($montantBrutHTVA)
    {
        $this->montantBrutHTVA = $montantBrutHTVA;
    }

    /**
     * @return string
     */
    public function getMontantRetenueHTVA()
    {
        return $this->montantRetenueHTVA;
    }

    /**
     * @param string $montantRetenueHTVA
     */
    public function setMontantRetenueHTVA($montantRetenueHTVA)
    {
        $this->montantRetenueHTVA = $montantRetenueHTVA;
    }

    /**
     * @return string
     */
    public function getMontantAcompteHTVA()
    {
        return $this->montantAcompteHTVA;
    }

    /**
     * @param string $montantAcompteHTVA
     */
    public function setMontantAcompteHTVA($montantAcompteHTVA)
    {
        $this->montantAcompteHTVA = $montantAcompteHTVA;
    }

    /**
     * Get ferme
     *
     * @return boolean
     */
    public function getFerme()
    {
        return $this->ferme;
    }

    /**
     * Set montantAvance
     *
     * @param float $montantAvance
     *
     * @return Decompte
     */
    public function setMontantAvance($montantAvance)
    {
        $this->montantAvance = $montantAvance;

        return $this;
    }

    /**
     * Get montantAvance
     *
     * @return float
     */
    public function getMontantAvance()
    {
        return $this->montantAvance;
    }

    /**
     * Add ligneSuivi
     *
     * @param \Laiso\ArmBundle\Entity\LigneSuivi $ligneSuivi
     *
     * @return Decompte
     */
    public function addLigneSuivi(\Laiso\ArmBundle\Entity\LigneSuivi $ligneSuivi)
    {
        $this->ligneSuivis[] = $ligneSuivi;

        return $this;
    }

    /**
     * Remove ligneSuivi
     *
     * @param \Laiso\ArmBundle\Entity\LigneSuivi $ligneSuivi
     */
    public function removeLigneSuivi(\Laiso\ArmBundle\Entity\LigneSuivi $ligneSuivi)
    {
        $this->ligneSuivis->removeElement($ligneSuivi);
    }

    /**
     * Get ligneSuivis
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLigneSuivis()
    {
        return $this->ligneSuivis;
    }
}
