<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Intervenant
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\IntervenantRepository")
 */
class Intervenant
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom", type="string", length=100)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="Fonction", type="string", length=50)
     */
    private $fonction;

    /***********************************************
     *                   ASSOCIATIONS
     *
     *    Ne surtout pas modifier les annotations
     *      sauf en cas de modification du modèle
     *
     *                  (c) Laiso
     ***********************************************/

    /**
     * @var
     * @ORM\OneToMany(targetEntity="Laiso\ArmBundle\Entity\LigneSuivi", mappedBy="intervenant")
     */
    private $ligneSuivis;

    public function __toString()
    {
        return $this->fonction . " - " . $this->nom;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Intervenant
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set fonction
     *
     * @param string $fonction
     *
     * @return Intervenant
     */
    public function setFonction($fonction)
    {
        $this->fonction = $fonction;

        return $this;
    }

    /**
     * Get fonction
     *
     * @return string
     */
    public function getFonction()
    {
        return $this->fonction;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ligneSuivis = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add ligneSuivi
     *
     * @param \Laiso\ArmBundle\Entity\Intervenant $ligneSuivi
     *
     * @return Intervenant
     */
    public function addLigneSuivi(\Laiso\ArmBundle\Entity\Intervenant $ligneSuivi)
    {
        $this->ligneSuivis[] = $ligneSuivi;

        return $this;
    }

    /**
     * Remove ligneSuivi
     *
     * @param \Laiso\ArmBundle\Entity\Intervenant $ligneSuivi
     */
    public function removeLigneSuivi(\Laiso\ArmBundle\Entity\Intervenant $ligneSuivi)
    {
        $this->ligneSuivis->removeElement($ligneSuivi);
    }

    /**
     * Get ligneSuivis
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLigneSuivis()
    {
        return $this->ligneSuivis;
    }
}
