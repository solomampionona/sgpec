<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ConsistanceAttachement
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\ConsistanceAttachementRepository")
 */
class ConsistanceAttachement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Libelle", type="string", length=100)
     */
    private $libelle;

    /***********************************************
     *                   ASSOCIATIONS
     *
     *    Ne surtout pas modifier les annotations
     *      sauf en cas de modification du modèle
     *
     *                  (c) Laiso
     ***********************************************/

    /**
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\Attachement", inversedBy="consistances")
     */
    private $attachement;

    /**
     * @ORM\OneToMany(targetEntity="Laiso\ArmBundle\Entity\LigneAttachement", mappedBy="consistance")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     */
    private $lignes;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\TypeConsistance")
     */
    private $type;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->lignes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add ligne
     *
     * @param \Laiso\ArmBundle\Entity\LigneAttachement $ligne
     *
     * @return ConsistanceAttachement
     */
    public function addLigne(\Laiso\ArmBundle\Entity\LigneAttachement $ligne)
    {
        $this->lignes[] = $ligne;

        return $this;
    }

    /**
     * Remove ligne
     *
     * @param \Laiso\ArmBundle\Entity\LigneAttachement $ligne
     */
    public function removeLigne(\Laiso\ArmBundle\Entity\LigneAttachement $ligne)
    {
        $this->lignes->removeElement($ligne);
    }

    /**
     * Get lignes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLignes()
    {
        return $this->lignes;
    }

     /**
     * Set attachement
     *
     * @param \Laiso\ArmBundle\Entity\Attachement $attachement
     *
     * @return ConsistanceAttachement
     */
    public function setAttachement(\Laiso\ArmBundle\Entity\Attachement $attachement = null)
    {
        $this->attachement = $attachement;

        return $this;
    }

    /**
     * Get attachement
     *
     * @return \Laiso\ArmBundle\Entity\Attachement
     */
    public function getAttachement()
    {
        return $this->attachement;
    }

    /**
     * Set type
     *
     * @param \Laiso\ArmBundle\Entity\TypeConsistance $type
     *
     * @return ConsistanceAttachement
     */
    public function setType(\Laiso\ArmBundle\Entity\TypeConsistance $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \Laiso\ArmBundle\Entity\TypeConsistance
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return ConsistanceAttachement
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
}
