<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Avenant
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\AvenantRepository")
 */
class Avenant
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Motif", type="text")
     */
    private $motif;

    /**
     * @var integer
     *
     * @ORM\Column(name="Numero", type="integer")
     *
    private $numero;*/

    /**
     * @var float
     *
     * @ORM\Column(name="Delai", type="float", nullable=true)
     */
    private $delai;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateSignatureEntreprise", type="date", nullable=true)
     */
    private $dateSignatureEntreprise;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateSignatureArm", type="date", nullable=true)
     */
    private $dateSignatureArm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateVisa", type="date", nullable=true)
     */
    private $dateVisa;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateOsNotification", type="date", nullable=true)
     */
    private $dateOsNotification;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateOsDemarrage", type="date", nullable=true)
     */
    private $dateOsDemarrage;

    /**
     * @var boolean
     *
     * Un avenant valide => DQE valide
     *
     * Si true: - impossible de modifier ou redéfinir les quantités dans les lignes du DQE
     *          - Si besoin de redéfinir les lignes du DQE => créer un nouvel avenant
     *
     * A ne pas confondre avec Lotissement::valide
     *
     * Remarque: ne peut être mis à true si l'avenant précédent est encore à false.
     *
     * cf Lotissement::valide
     *
     * @ORM\Column(name="Valide", type="boolean")
     */
    private $valide;

    /***********************************************
     *                   ASSOCIATIONS
     *
     *    Ne surtout pas modifier les annotations
     *      sauf en cas de modification du modèle
     *
     *                  (c) Laiso
     ***********************************************/


    /**
     * Un avenant est attaché à un et un seul marché.
     * Le marché initial est considéré comme l'avenant numéro ZERO
     *
     *
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\Marche", inversedBy="avenants")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     */
    private $marche;

    /**
     * Un avenant possède à un et un seul libellé.
     * Le marché initial est considéré comme l'avenant numéro ZERO
     *
     *
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\LibelleAvenant", inversedBy="avenants")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     */
    private $libelle;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="Laiso\ArmBundle\Entity\Decompte", mappedBy="avenant")
     */
    private $decomptes;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="Laiso\ArmBundle\Entity\LigneDQE", mappedBy="avenant")
     */
    private $lignes;

    public function __construct()
    {
        $this->decomptes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set delai
     *
     * @param float $delai
     *
     * @return Avenant
     */
    public function setDelai($delai)
    {
        $this->delai = $delai;

        return $this;
    }

    /**
     * Get delai
     *
     * @return float
     */
    public function getDelai()
    {
        return $this->delai;
    }

    /**
     * Set dateSignatureEntreprise
     *
     * @param \DateTime $dateSignatureEntreprise
     *
     * @return Avenant
     */
    public function setDateSignatureEntreprise($dateSignatureEntreprise)
    {
        $this->dateSignatureEntreprise = $dateSignatureEntreprise;

        return $this;
    }

    /**
     * Get dateSignatureEntreprise
     *
     * @return \DateTime
     */
    public function getDateSignatureEntreprise()
    {
        return $this->dateSignatureEntreprise;
    }

    /**
     * Set dateSignatureArm
     *
     * @param \DateTime $dateSignatureArm
     *
     * @return Avenant
     */
    public function setDateSignatureArm($dateSignatureArm)
    {
        $this->dateSignatureArm = $dateSignatureArm;

        return $this;
    }

    /**
     * Get dateSignatureArm
     *
     * @return \DateTime
     */
    public function getDateSignatureArm()
    {
        return $this->dateSignatureArm;
    }

    /**
     * Set dateVisa
     *
     * @param \DateTime $dateVisa
     *
     * @return Avenant
     */
    public function setDateVisa($dateVisa)
    {
        $this->dateVisa = $dateVisa;

        return $this;
    }

    /**
     * Get dateVisa
     *
     * @return \DateTime
     */
    public function getDateVisa()
    {
        return $this->dateVisa;
    }

    /**
     * Set dateOsNotification
     *
     * @param \DateTime $dateOsNotification
     *
     * @return Avenant
     */
    public function setDateOsNotification($dateOsNotification)
    {
        $this->dateOsNotification = $dateOsNotification;

        return $this;
    }

    /**
     * Get dateOsNotification
     *
     * @return \DateTime
     */
    public function getDateOsNotification()
    {
        return $this->dateOsNotification;
    }

    /**
     * Set dateOsDemarrage
     *
     * @param \DateTime $dateOsDemarrage
     *
     * @return Avenant
     */
    public function setDateOsDemarrage($dateOsDemarrage)
    {
        $this->dateOsDemarrage = $dateOsDemarrage;

        return $this;
    }

    /**
     * Get dateOsDemarrage
     *
     * @return \DateTime
     */
    public function getDateOsDemarrage()
    {
        return $this->dateOsDemarrage;
    }

    /**
     * Set marche
     *
     * @param \Laiso\ArmBundle\Entity\Marche $marche
     *
     * @return Avenant
     */
    public function setMarche(\Laiso\ArmBundle\Entity\Marche $marche)
    {
        $this->marche = $marche;

        return $this;
    }

    /**
     * Get marche
     *
     * @return \Laiso\ArmBundle\Entity\Marche
     */
    public function getMarche()
    {
        return $this->marche;
    }

    /**
     * Set libelle
     *
     * @param \Laiso\ArmBundle\Entity\LibelleAvenant $libelle
     *
     * @return Avenant
     */
    public function setLibelle(\Laiso\ArmBundle\Entity\LibelleAvenant $libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return \Laiso\ArmBundle\Entity\LibelleAvenant
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Add decompte
     *
     * @param \Laiso\ArmBundle\Entity\Decompte $decompte
     *
     * @return Avenant
     */
    public function addDecompte(\Laiso\ArmBundle\Entity\Decompte $decompte)
    {
        $this->decomptes[] = $decompte;

        return $this;
    }

    /**
     * Remove decompte
     *
     * @param \Laiso\ArmBundle\Entity\Decompte $decompte
     */
    public function removeDecompte(\Laiso\ArmBundle\Entity\Decompte $decompte)
    {
        $this->decomptes->removeElement($decompte);
    }

    /**
     * Get decomptes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDecomptes()
    {
        return $this->decomptes;
    }

    /**
     * Add ligne
     *
     * @param \Laiso\ArmBundle\Entity\LigneDQE $ligne
     *
     * @return Avenant
     */
    public function addLigne(\Laiso\ArmBundle\Entity\LigneDQE $ligne)
    {
        $this->lignes[] = $ligne;

        return $this;
    }

    /**
     * Remove ligne
     *
     * @param \Laiso\ArmBundle\Entity\LigneDQE $ligne
     */
    public function removeLigne(\Laiso\ArmBundle\Entity\LigneDQE $ligne)
    {
        $this->lignes->removeElement($ligne);
    }

    /**
     * Get lignes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLignes()
    {
        return $this->lignes;
    }

    /**
     * Set valide
     *
     * @param boolean $valide
     *
     * @return Avenant
     */
    public function setValide($valide)
    {
        $this->valide = $valide;

        return $this;
    }

    /**
     * Get valide
     *
     * @return boolean
     */
    public function getValide()
    {
        return $this->valide;
    }

    /**
     * Set motif
     *
     * @param string $motif
     *
     * @return Avenant
     */
    public function setMotif($motif)
    {
        $this->motif = $motif;

        return $this;
    }

    /**
     * Get motif
     *
     * @return string
     */
    public function getMotif()
    {
        return $this->motif;
    }
}
