<?php
/**
 * Created by PhpStorm.
 * User: rasolo
 * Date: 05/03/2016
 * Time: 06:10
 */

namespace Laiso\ArmBundle\Utils;


class CPA
{
    private $entity;
    private $resteAPayer;
    private $tva;
    private $cumulRetenue;
    private $montantRemboursementAvance;
    private $restitutionRetenue;
    private $acompte;
    private $numero;

    /**
     * CPA constructor.
     * @param $entity
     * @param $resteAPayer
     * @param $tva
     * @param $cumulRetenue
     * @param $montantRemboursementAvance
     * @param $restitutionRetenue
     * @param $acompte
     * @param $numero
     */
    public function __construct($entity, $resteAPayer, $tva, $cumulRetenue, $montantRemboursementAvance, $restitutionRetenue, $acompte, $numero)
    {
        $this->entity = $entity;
        $this->resteAPayer = $resteAPayer;
        $this->tva = $tva;
        $this->cumulRetenue = $cumulRetenue;
        $this->montantRemboursementAvance = $montantRemboursementAvance;
        $this->restitutionRetenue = $restitutionRetenue;
        $this->acompte = $acompte;
        $this->numero = $numero;
    }


    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param mixed $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return mixed
     */
    public function getResteAPayer()
    {
        return $this->resteAPayer;
    }

    /**
     * @param mixed $resteAPayer
     */
    public function setResteAPayer($resteAPayer)
    {
        $this->resteAPayer = $resteAPayer;
    }

    /**
     * @return mixed
     */
    public function getTva()
    {
        return $this->tva;
    }

    /**
     * @param mixed $tva
     */
    public function setTva($tva)
    {
        $this->tva = $tva;
    }

    /**
     * @return mixed
     */
    public function getCumulRetenue()
    {
        return $this->cumulRetenue;
    }

    /**
     * @param mixed $cumulRetenue
     */
    public function setCumulRetenue($cumulRetenue)
    {
        $this->cumulRetenue = $cumulRetenue;
    }

    /**
     * @return mixed
     */
    public function getMontantRemboursementAvance()
    {
        return $this->montantRemboursementAvance;
    }

    /**
     * @param mixed $montantRemboursementAvance
     */
    public function setMontantRemboursementAvance($montantRemboursementAvance)
    {
        $this->montantRemboursementAvance = $montantRemboursementAvance;
    }

    /**
     * @return mixed
     */
    public function getRestitutionRetenue()
    {
        return $this->restitutionRetenue;
    }

    /**
     * @param mixed $restitutionRetenue
     */
    public function setRestitutionRetenue($restitutionRetenue)
    {
        $this->restitutionRetenue = $restitutionRetenue;
    }

    /**
     * @return mixed
     */
    public function getAcompte()
    {
        return $this->acompte;
    }

    /**
     * @param mixed $acompte
     */
    public function setAcompte($acompte)
    {
        $this->acompte = $acompte;
    }

    /**
     * @return mixed
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param mixed $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }
}