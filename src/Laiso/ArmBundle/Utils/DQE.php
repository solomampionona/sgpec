<?php
/**
 * Created by PhpStorm.
 * User: laiso
 * Date: 26/12/2015
 * Time: 18:05
 */

namespace Laiso\ArmBundle\Utils;


class DQE
{
    private $avenant;

    private $consistances;

    public function __construct()
    {
        $this->consistances = array();
    }

    /**
     * @return mixed
     */
    public function getAvenant()
    {
        return $this->avenant;
    }

    /**
     * @param mixed $avenant
     */
    public function setAvenant($avenant)
    {
        $this->avenant = $avenant;
    }

    /**
     * @return mixed
     */
    public function getConsistances()
    {
        return $this->consistances;
    }

    /**
     * @param mixed $consistances
     */
    public function setConsistances($consistances)
    {
        $this->consistances = $consistances;
    }
}