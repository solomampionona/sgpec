<?php
/**
 * Created by PhpStorm.
 * User: rasolo
 * Date: 05/03/2016
 * Time: 07:05
 */

namespace Laiso\ArmBundle\Utils;


class DP
{
    private $attachements;
    private $wNonTermines;
    private $wTermines;
    private $retenues;
    private $avance;
    private $remboursements;
    private $restitutions;
    private $entity;
    private $marche;
    private $resteAPayer;

    /**
     * DP constructor.
     * @param $attachements
     * @param $wNonTermines
     * @param $wTermines
     * @param $retenues
     * @param $avance
     * @param $remboursements
     * @param $restitutions
     * @param $entity
     * @param $marche
     * @param $resteAPayer
     */
    public function __construct($attachements, $wNonTermines, $wTermines, $retenues, $avance, $remboursements, $restitutions, $entity, $marche, $resteAPayer)
    {
        $this->attachements = $attachements;
        $this->wNonTermines = $wNonTermines;
        $this->wTermines = $wTermines;
        $this->retenues = $retenues;
        $this->avance = $avance;
        $this->remboursements = $remboursements;
        $this->restitutions = $restitutions;
        $this->entity = $entity;
        $this->marche = $marche;
        $this->resteAPayer = $resteAPayer;
    }


    /**
     * @return mixed
     */
    public function getAttachements()
    {
        return $this->attachements;
    }

    /**
     * @param mixed $attachements
     */
    public function setAttachements($attachements)
    {
        $this->attachements = $attachements;
    }

    /**
     * @return mixed
     */
    public function getWNonTermines()
    {
        return $this->wNonTermines;
    }

    /**
     * @param mixed $wNonTermines
     */
    public function setWNonTermines($wNonTermines)
    {
        $this->wNonTermines = $wNonTermines;
    }

    /**
     * @return mixed
     */
    public function getWTermines()
    {
        return $this->wTermines;
    }

    /**
     * @param mixed $wTermines
     */
    public function setWTermines($wTermines)
    {
        $this->wTermines = $wTermines;
    }

    /**
     * @return mixed
     */
    public function getRetenues()
    {
        return $this->retenues;
    }

    /**
     * @param mixed $retenues
     */
    public function setRetenues($retenues)
    {
        $this->retenues = $retenues;
    }

    /**
     * @return mixed
     */
    public function getAvance()
    {
        return $this->avance;
    }

    /**
     * @param mixed $avance
     */
    public function setAvance($avance)
    {
        $this->avance = $avance;
    }

    /**
     * @return mixed
     */
    public function getRemboursements()
    {
        return $this->remboursements;
    }

    /**
     * @param mixed $remboursements
     */
    public function setRemboursements($remboursements)
    {
        $this->remboursements = $remboursements;
    }

    /**
     * @return mixed
     */
    public function getRestitutions()
    {
        return $this->restitutions;
    }

    /**
     * @param mixed $restitutions
     */
    public function setRestitutions($restitutions)
    {
        $this->restitutions = $restitutions;
    }

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param mixed $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return mixed
     */
    public function getMarche()
    {
        return $this->marche;
    }

    /**
     * @param mixed $marche
     */
    public function setMarche($marche)
    {
        $this->marche = $marche;
    }

    /**
     * @return mixed
     */
    public function getResteAPayer()
    {
        return $this->resteAPayer;
    }

    /**
     * @param mixed $resteAPayer
     */
    public function setResteAPayer($resteAPayer)
    {
        $this->resteAPayer = $resteAPayer;
    }


}