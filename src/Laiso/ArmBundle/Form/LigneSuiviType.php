<?php

namespace Laiso\ArmBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LigneSuiviType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lieuEntree')
            ->add('lieuSortie')
            ->add('dateEntree', 'date', array(
                'widget' => 'single_text',
                'input' => 'datetime',
                'format' => 'dd/MM/yyyy'
            ))
            ->add('dateSortie', 'date', array(
                'widget' => 'single_text',
                'input' => 'datetime',
                'format' => 'dd/MM/yyyy'
            ))
            ->add('observation')
            ->add('intervenant')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Laiso\ArmBundle\Entity\LigneSuivi'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'laiso_armbundle_lignesuivi';
    }
}
