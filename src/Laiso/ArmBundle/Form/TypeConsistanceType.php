<?php

namespace Laiso\ArmBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TypeConsistanceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', null, array(
                'attr' => array(
                    'data-validate-func' => "minlength",
                    'data-validate-arg' => 10,
                    'data-validate-hint'=>"Le nom de la consistance doit avoir moins 10 caractères de longueur"
                )
            ))
            ->add('abbrege', null, array(
                'attr' => array(
                    'data-validate-func' => "minlength",
                    'data-validate-arg' => 3,
                    'data-validate-hint'=>"L'abbréviation doit avoir moins 3 caractères de longueur"
                )
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Laiso\ArmBundle\Entity\TypeConsistance'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'laiso_armbundle_typeconsistance';
    }
}
