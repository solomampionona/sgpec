<?php

namespace Laiso\ArmBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BlocType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numero', null, array(
                'attr' => array(
                    'data-validate-func' => "number",
                    'data-validate-hint'=>"Numéro de bloc non valide"
            )))
            ->add('localisation', null, array(
                'attr' => array(
                    'data-validate-func' => "minlength",
                    'data-validate-arg' => 4,
                    'data-validate-hint'=>"La localisation doit avoir moins 4 caractères de longueur"
            )))
            ->add('siege', null, array(
                'attr' => array(
                    'data-validate-func' => "minlength",
                    'data-validate-arg' => 4,
                    'data-validate-hint'=>"La localisation du siège doit avoir moins 4 caractères de longueur"
            )))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Laiso\ArmBundle\Entity\Bloc'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'laiso_armbundle_bloc';
    }
}
