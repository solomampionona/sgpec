<?php

namespace Laiso\ArmBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class MarcheType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numero')
            ->add('entreprise', null, array(
                'required' => true,
            ))
            ->add('lotissement', 'entity', array(
                'class' => 'Laiso\ArmBundle\Entity\Lotissement',
                /*'query_builder' => function (EntityRepository $entityRepository){
                    return $entityRepository->createQueryBuilder('l')
                        ->where('l.valide' = 'false');
                }*/
            ))
            ->add('entrepriseAssujettie', 'checkbox', array(
                'required' => false
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Laiso\ArmBundle\Entity\Marche'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'laiso_armbundle_marche';
    }
}
