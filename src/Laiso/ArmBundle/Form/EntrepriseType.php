<?php

namespace Laiso\ArmBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EntrepriseType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('denomination', null, array(
                'attr' => array(
                    'data-validate-func' => "minlength",
                    'data-validate-arg' => 4,
                    'data-validate-hint'=>"La dénomination doit avoir moins 4 caractères de longueur"
                )
            ))
            ->add('sigle', null, array(
                'attr' => array(
                    'data-validate-func' => "minlength",
                    'data-validate-arg' => 4,
                    'data-validate-hint'=>"Le sigle doit avoir moins 4 caractères de longueur"
                )
            ))
            ->add('adresse', null, array(
                'attr' => array(
                    'data-validate-func' => "minlength",
                    'data-validate-arg' => 4,
                    'data-validate-hint'=>"L'adresse doit avoir moins 4 caractères de longueur"
                )
            ))
            ->add('statistique', null, array(
                'attr' => array(
                    'data-validate-func' => "number",
                    'data-validate-hint'=>"Numéro de statistique non valide"
                )
            ))
            ->add('nIF', null, array(
                'attr' => array(
                    'data-validate-func' => "number",
                    'data-validate-hint'=>"Numéro d'identification fiscal non valide"
                )
            ))
            ->add('telephone1', null, array(
                'attr' => array(
                    'data-validate-func' => "number",
                    'data-validate-hint'=>"Numéro de téléphone non valide"
                )
            ))
            ->add('telephone2', null, array(
                'attr' => array(
                    'data-validate-func' => "number",
                    'data-validate-hint'=>"Numéro de téléphone non valide"
                )
            ))
            ->add('email', null, array(
                'attr' => array(
                    'data-validate-func' => "email",
                    'data-validate-hint'=>"Adresse e-mail non valide"
                )
            ))
            ->add('responsable', null, array(
                'attr' => array(
                    'data-validate-func' => "minlength",
                    'data-validate-arg' => 4,
                    'data-validate-hint'=>"Le nom du responsable doit avoir moins 4 caractères de longueur"
                )
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Laiso\ArmBundle\Entity\Entreprise'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'laiso_armbundle_entreprise';
    }
}
