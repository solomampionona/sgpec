<?php
/**
 * Created by PhpStorm.
 * User: utilisateur1
 * Date: 22/12/2015
 * Time: 15:19
 */

namespace Laiso\ArmBundle\Security;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

use Laiso\ArmBundle\Entity\Lotissement;

class LotissementVoter extends Voter
{
    const CREATE = "create";
    const EDIT = "edit";

    /**
     * @var AccessDecisionManagerInterface
     */
    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    /**
     *
     * @param $attribute
     * @param $subject
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, array(self::CREATE, self::EDIT))) {
            return false;
        }

        if (!$subject instanceof Lotissement) {
            return false;
        }

        return true;
    }

    /**
     *
     * @param $attribute
     * @param $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        /** @var Lotissement */
        $lotissement = $subject; // $subject must be a Lotissement instance, thanks to the supports method

        if (!$user instanceof UserInterface) {
            return false;
        }

        switch ($attribute) {
            case self::CREATE:
                // if the user is an admin, allow them to create
                if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
                    return true;
                }

                break;
            case self::EDIT:

                if ($user->getBloc() === $lotissement->getBloc()) {
                    return true;
                }

                break;
        }

        return false;
    }
}