/**
 * Created by laiso on 04/12/2015.
 */
$(function () {
    var wizard = $('#wizard').wizard({
            stepperType: 'cycle',

            buttons:{
                cancel: false,

            },

            onCancel: function (page, wiz) {
                alert('canceled')
            },

            //The event fired when the button pressed is Prior
            onPrior: function (page, wiz) {
                alert('prior')
            },

            //The event fired when the button pressed is Next
            onNext: function (page, wiz) {
                alert('next')
                return true;
            },

            //The event fired when the page changed
            onPage: function (page, wiz) {
                //alert('page')
            },

            onFinish: function (page, wiz) {
                alert('finished')
            }
        }
    );
});