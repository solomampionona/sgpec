/**
 * Created by laiso on 19/11/2015.
 */

var loader = $("#loader").data('dialog');
var confirm = $('#dialog-confirm').data('dialog');

function showConfirm() {
    confirm.open();
}
function closeConfirm(){
    confirm.close();
}

function showDialog(data) {
    var close = "<button class='button default place-left' onclick=\"$('#dialog').data('dialog').close()\">Annuler</button>";
    $('#dialog-container').html(data).append(close);
    var dialog = $('#dialog').data('dialog');
    dialog.open();
}

$(function () {
    $('.scroll-y').mCustomScrollbar({
        theme: "dark-3", axis: "y",
    });

    $('.scroll-x').mCustomScrollbar({
        theme: "dark-3", axis: "x",
    });

    $('.closeFlash').click(function () {
        var flash = "#" + $(this).attr('flashId');
        $(flash).slideUp();
    });

    $('.inlineEditor').focusout(function () {
        var form = $(this).parent();
        var target = $(this).attr('data-target');

        $.ajax({
            dataType: 'json',
            url: form.attr('action'),
            data: form.serialize(),
            type: 'post',
            success: function(data){
                if(data.status == 1){
                    $('#' + target).html(data.montant);
                    $.Notify({type: 'success', caption: 'Enregistré', content: "Données enregistrées"});
                }/*else{
                    $.Notify({type: 'info', caption: 'Information', content: "Données non enregistrées"});
                }*/
            },
            error: function (xhr, ajaxOptions, thrownError){
                $.Notify({type: 'error', caption: 'Erreur', content: thrownError});
            }
        });
    });

    $('.ajax-button').click(function () {
        loader.open();
        var target = $(this).attr('href');
        $.ajax({
            url: target,
            method: 'get',
            success: function (data) {
                loader.close();
                showDialog(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                loader.close();
                showDialog("<h2 class='error'>Erreur : " + thrownError + "</h2><hr class='thin'><div style='max-height: 300px; overflow-y: scroll' >" + xhr.responseText +  "</div>");
            }
        });
        return false;
    });

    $('.ajax-link').click(function () {
        var id = $(this).attr('data-id');
        alert(id);
        return false;
    })

    $('a.confirm').click(function () {
        var lien = $(this).attr('href');
        var data = "<h4 class='laiso-bordered padding10 align-center margin50 no-margin-left no-margin-right no-margin-bottom'>Vous êtes sur le point de valider les modifications apportées. Confirmer les modifications</h4>" +
            "<br><hr class='thin'>" +
            "<a href='"+ lien +"' class='button primary place-right'>Valider</a>"
        showDialog(data);
        return false;
    })
});